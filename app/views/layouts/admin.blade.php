<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js"> <!--<![endif]-->
<head>
    @section('meta')
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <title>Interactive Ad Experience | @yield('title')</title>
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width">
    <meta name="csrf_token" content="{{ csrf_token() }}">
    <meta name="user_id" content="{{ isset($user) ? $user->id : '' }}">
    @show

    @section('styles')
    <!-- Library Styles -->
    {{ HTML::style('assets/css/lib/bootstrap-combined.no-icons.min.css') }}
    {{ HTML::style('assets/css/lib/font-awesome.min.css') }}
    {{ HTML::style('assets/code-mirror/codemirror.css') }}
    {{ HTML::style('assets/css/lib/jquery.selectBox.css') }}
    {{ HTML::style('assets/css/lib/bootstrap-switch.css') }}
    {{ HTML::style('assets/css/admin.css') }}

    <!-- IAX Styles -->
    {{ HTML::style('assets/css/main.css') }}
    @show
</head>
<body>

    @include('admin.menu')

    <div class="container">
        <div class="row">
            <div class="span2">
                <ul class="nav nav-list iax-admin-nav">
                    <li class="{{ Request::is( 'admin') ? 'active' : '' }}"><a href="/admin/"><i class="fa fa-lg fa-dashboard"></i><span>Dashboard</span></a></li>
                    <li class="nav-header">Manage</li>
                    <li class="{{ Request::is( 'admin/users*') ? 'active' : '' }}"><a href="/admin/users"><i class="fa fa-lg fa-user"></i><span>Users</span></a></li>
                    <li class="{{ Request::is( 'admin/adtypes*') ? 'active' : '' }}"><a href="/admin/adtypes"><i class="fa fa-lg fa-list"></i><span>Ad Types</span></a></li>
                    <li class="{{ Request::is( 'admin/adtags*') ? 'active' : '' }}"><a href="/admin/adtags"><i class="fa fa-lg fa-code"></i><span>Ad Tags</span></a></li>
                </ul>
            </div>
            <div class="span10">
                <p class="lead">@yield('title')</p>
                @include('ui.notifications')
                @yield('content')
            </div>
        </div>
    </div>


    @include('ui.footer')

    <!-- Global Scripts - Loaded on every page -->
    @section('scripts')
        {{ HTML::script('assets/js/lib/jquery-1.9.1.min.js') }}
        {{ HTML::script('assets/js/lib/lodash.underscore.js') }}
        {{ HTML::script('assets/js/lib/bootstrap.min.js') }}
        {{ HTML::script('assets/bootstrap-editable/js/bootstrap-editable.min.js') }}
        {{ HTML::script('assets/js/iax-core.js') }}
        {{ HTML::script('assets/js/iax-config.js') }}
        {{ HTML::script('assets/js/iax-utilities.js') }}
        {{ HTML::script('assets/js/iax-partials.js') }}
        {{ HTML::script('assets/js/iax-main.js') }}

    @show

    <!-- Loading Indicator -->
    <div class="iax-loading" style="display:none;">
        <div class="iax-loading-overlay"></div>
        <div class="iax-loading-content">
            <i class="fa fa-refresh fa-spin fa-4x"></i>
            <p>Loading</p>
        </div>
    </div>
    <!-- End Loading Indicator -->
</body>
</html>