<?php
    $viewer = Sentry::findGroupByName('Viewers');
    // Share user across views.
    $user = Sentry::getUser();
    if( $user ){
        $isViewer = $user->inGroup($viewer);
    }
?>

<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js"> <!--<![endif]-->
<head>
    @section('meta')
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <title>Interactive Ad Experience | @yield('title')</title>
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width">
    <meta name="csrf_token" content="{{ csrf_token() }}">
    <meta name="user_id" content="{{ isset($user) ? $user->id : '' }}">
    @show

    @section('styles')
    <!-- Library Styles -->
    {{ HTML::style('assets/css/lib/bootstrap-combined.no-icons.min.css') }}
    {{ HTML::style('assets/css/lib/font-awesome.min.css') }}
    {{ HTML::style('assets/code-mirror/codemirror.css') }}
    {{ HTML::style('assets/css/lib/jquery.selectBox.css') }}
    {{ HTML::style('assets/css/lib/bootstrap-switch.css') }}
    {{ HTML::style('assets/bootstrap-editable/css/bootstrap-editable.css') }}

    <!-- IAX Styles -->
    {{ HTML::style('assets/css/main.css') }}
    @show
</head>
<body>

    @include('ui.navbar.main')

    <div class="container iax-content">
        @include('ui.notifications')

        @yield('content')
    </div>

    @include('ui.footer')

    <!-- Global Scripts - Loaded on every page -->
    @section('scripts')
        <!-- JS Libs -->
        {{ HTML::script('assets/js/lib/jquery-1.9.1.min.js') }}
		{{ HTML::script('assets/js/lib/jquery-ui.js') }}
        {{ HTML::script('assets/js/lib/lodash.underscore.min.js') }}
        {{ HTML::script('assets/js/lib/bootstrap.min.js') }}
        {{ HTML::script('assets/bootstrap-editable/js/bootstrap-editable.min.js') }}
        {{ HTML::script('assets/js/lib/jquery.tagsinput.min.js') }}
        {{ HTML::script('assets/js/lib/spin.min.js') }}
        {{ HTML::script('assets/js/lib/notify.min.js') }}

        <!-- IAX Core JS -->
        {{ HTML::script('assets/js/iax-core.js') }}
        {{ HTML::script('assets/js/iax-config.js') }}
        {{ HTML::script('assets/js/iax-utilities.js') }}
        {{ HTML::script('assets/js/iax-partials.js') }}
        {{ HTML::script('assets/js/iax-main.js') }}
    @show

    <!-- Loading Indicator -->
    <div class="iax-loading" style="display:none;">
        <div class="iax-loading-overlay"></div>
        <div class="iax-loading-content">
            <span id="iax-spinner"></span>
        </div>
    </div>
    <!-- End Loading Indicator -->

    <!-- testing hotjar insights website tracking software -->
    <script>
        (function(f,b,g){
            var xo=g.prototype.open,xs=g.prototype.send,c;
            f.hj=f.hj||function(){(f.hj.q=f.hj.q||[]).push(arguments)};
            f._hjSettings={hjid:4430, hjsv:2};
            function ls(){f.hj.documentHtml=b.documentElement.outerHTML;c=b.createElement("script");c.async=1;c.src="//static.hotjar.com/c/hotjar-4430.js?sv=2";b.getElementsByTagName("head")[0].appendChild(c);}
            if(b.readyState==="interactive"||b.readyState==="complete"||b.readyState==="loaded"){ls();}else{if(b.addEventListener){b.addEventListener("DOMContentLoaded",ls,false);}}
            if(!f._hjPlayback && b.addEventListener){
                g.prototype.open=function(l,j,m,h,k){this._u=j;xo.call(this,l,j,m,h,k)};
                g.prototype.send=function(e){var j=this;function h(){if(j.readyState===4){f.hj("_xhr",j._u,j.status,j.response)}}this.addEventListener("readystatechange",h,false);xs.call(this,e)};
            }
        })(window,document,window.XMLHttpRequest);
    </script>
</body>
</html>
