<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js"> <!--<![endif]-->
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
        <title>Interactive Ad Experience | @yield('title')
        </title>
        <meta name="description" content="">
        <meta name="viewport" content="width=device-width">

        @section('styles')
        <!-- Library Styles -->
        {{ HTML::style('assets/css/lib/bootstrap-combined.no-icons.min.css') }}
        {{ HTML::style('assets/css/lib/font-awesome.min.css') }}
        {{ HTML::style('assets/code-mirror/codemirror.css') }}
        {{ HTML::style('assets/css/lib/jquery.selectBox.css') }}
        {{ HTML::style('assets/css/lib/bootstrap-switch.css') }}

        <!-- IAX Styles -->
        {{ HTML::style('assets/css/main.css') }}
        @show
    </head>

    <body>
        @include('ui.navbar.adops')

        <div class="container iax-content">
            @include('ui.notifications')

            @yield('content')
        </div>

        <!-- Global Scripts - Loaded on every page -->
        @section('scripts')
            {{ HTML::script('assets/js/lib/jquery-1.9.1.min.js') }}
            {{ HTML::script('assets/js/lib/lodash.underscore.min.js') }}
            {{ HTML::script('assets/js/lib/bootstrap.min.js') }}
            {{ HTML::script('assets/bootstrap-editable/js/bootstrap-editable.min.js') }}
            {{ HTML::script('assets/js/iax-core.js') }}
            {{ HTML::script('assets/js/iax-config.js') }}
            {{ HTML::script('assets/js/iax-utilities.js') }}
            {{ HTML::script('assets/js/iax-partials.js') }}
            {{ HTML::script('assets/js/iax-main.js') }}
        @show
    </body>
</html>