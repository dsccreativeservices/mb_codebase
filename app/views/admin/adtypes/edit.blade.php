@extends('layouts.admin')
<!-- User login form goes here -->

@section('title')
<i class="fa fa-list"></i> Ad Type Management | Edit Ad Type
@stop

@section('subnav')
@stop

@section('content')
<p class="lead"></p>
<div class="iax-new-user-form span6">
    <form class="form-horizontal clearfix" action=" {{ url('admin/adtypes/' . $adtype->id . '/edit') }} " method="post" >
        {{ Form::token() }}

        <div class="control-group">
            <label for="name" class="control-label">Name</label>
            <div class="controls"><input type="text" class="span4" placeholder="Name" name="name" tabindex="1" value="{{ $adtype->name }}"></div>
        </div>

        <div class="control-group">
            <label for="description" class="control-label">Description</label>
            <div class="controls"><textarea rows="5" type="text" class="span4" placeholder="Description" name="description" tabindex="2" >{{ $adtype->description }}</textarea></div>
        </div>
        
        <div class="control-group">
            <label for="slot_width" class="control-label">DFP Slot Width</label>
            <div class="controls"><input type="number" class="span4" placeholder="slot width" name="slot_width" tabindex="3" value="{{ $adtype->slot_width }}"></div>
        </div>
        
        <div class="control-group">
            <label for="slot_height" class="control-label">DFP Slot Height</label>
            <div class="controls"><input type="number" class="span4" placeholder="slot height" name="slot_height" tabindex="4" value="{{ $adtype->slot_height }}"></div>
        </div>

        <div class="controls">
            <button class="btn btn-small btn-primary pull-right" type="submit">Save</button>
        </div>
    </form>
</div>
@stop

@section('scripts')
    @parent
@stop