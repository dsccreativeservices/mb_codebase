@extends('layouts.admin')
<!-- User login form goes here -->

@section('title')
<i class="fa fa-list"></i> Ad Type Management
@stop

@section('subnav')
@stop

@section('content')
<a href="/admin/adtypes/new" class="btn btn-small btn-success pull-right"><i class="fa fa-plus"></i> New Ad Type</a>
<table class="table table-striped table-condensed" id="iax-permissions-table">
    <thead>
        <tr>
            <th>ID</th>
            <th>Name</th>
            <th>Description</th>
            <th>Slot Width</th>
            <th>Slot Height</th>
            <th></th>
        </tr>
    </thead>
    <tbody>
    @if( $adtypes )
        @foreach ($adtypes as $a)
            <tr>
                <td>{{ $a->id }}</td>
                <td>{{ $a->name }}</td>
                <td><small>{{ $a->description }}</small></td>
                @if( $a->slot_width )
                    <td>{{ $a->slot_width }}</td>
                @else
                    <td></td>
                @endif
                @if( $a->slot_height )
                    <td>{{ $a->slot_height }}</td>
                @else
                    <td></td>
                @endif
                <td>
                    <a href="/admin/adtypes/{{ $a->id }}/edit" class="iax-link-warning" title="Edit Ad Type"><i class="fa fa-edit fa-lg"></i></a>
                    <form action="/admin/adtypes/delete" method="POST" class="iax-table-action">
                      <input type="hidden" name="adtype_id" value="{{{$a->id}}}">
                      <input type="hidden" name="mode" value="delete">
                      {{ Form::token() }}
                      <button data-toggle="tooltip" type="submit" title="Delete Ad Type"><i class="fa fa-trash-o fa-lg"></i></button>
                    </form>
                </td>
            </tr>
        @endforeach
    @endif
    </tbody>
</table>

@stop

@section('scripts')
    @parent
@stop