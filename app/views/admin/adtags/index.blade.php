@extends('layouts.admin')
<!-- User login form goes here -->

@section('title')
<i class="fa fa-code"></i> Ad Tag Management
@stop

@section('subnav')
@stop

@section('content')
<a href="/admin/adtags/new" class="btn btn-small btn-success pull-right"><i class="fa fa-plus"></i> New Ad Tag</a>
<table class="table table-striped table-condensed" id="iax-permissions-table">
    <thead>
        <tr>
            <th>ID</th>
            <th>Site</th>
            <th>Name</th>
            <th>Tag Selector</th>
            <th>RegEx</th>
            <th></th>
        </tr>
    </thead>
    <tbody>
    @if( $adtags )
        @foreach ($adtags as $a)
            <tr>
                <td>{{ $a->id }}</td>
                <td>{{ $a->site->name }}</td>
                <td>{{ $a->name }}</td>
                @if( $a->tag_selector )
                    <td><code>{{ $a->tag_selector }}</code></td>
                @else
                    <td></td>
                @endif

                @if( $a->regex )
                    <td><code>{{ $a->regex }}</code></td>
                @else
                    <td></td>
                @endif
                <td>
                    <a href="/admin/adtags/{{ $a->id }}/edit" class="iax-link-warning" data-toggle="tooltip" title="Edit Ad Tag"><i class="fa fa-edit fa-lg"></i></a>
                    <form action="/admin/adtags/delete" method="POST" class="iax-table-action">
                      <input type="hidden" name="adtag_id" value="{{{$a->id}}}">
                      <input type="hidden" name="mode" value="delete">
                      {{ Form::token() }}
                      <button data-toggle="tooltip" type="submit" title="Delete Ad Tag"><i class="fa fa-trash-o fa-lg"></i></button>
                    </form>
                </td>
            </tr>
        @endforeach
    @endif
    </tbody>
</table>

@stop

@section('scripts')
    @parent
@stop