@extends('layouts.admin')
<!-- User login form goes here -->

@section('title')
<i class="fa fa-code"></i> Ad Tag Management | New Ad Tag
@stop

@section('subnav')
@stop

@section('content')
<div class="alert alert-block alert-info"><p>The IAX system supports two methods for setting ad tags. An ad tag refers to the page ad unit that is replaced with project creative during preview. For most ad units, you can use a simple jQuery-like selector. On sites where the ad unit cannot be targeted this way, you can use the regex option. Only put the JavaScript method call as exactly as it is in the page. <strong>Only one of these can be used.</strong></p></div>
<div class="iax-new-user-form span6">
    <form class="form-horizontal clearfix" action=" {{ url('admin/adtags/new') }} " method="post" >
        {{ Form::token() }}

        <div class="control-group">
            <label for="name" class="control-label">Name</label>
            <div class="controls"><input type="text" class="span4" placeholder="Name" name="name" tabindex="1" value="{{ Input::old('name') }}"></div>
        </div>

        <div class="control-group">
            <label for="tag_selector" class="control-label">Tag Selector</label>
            <div class="controls"><input type="text" class="span4" placeholder="Tag Selector" name="tag_selector" tabindex="2" value="{{ Input::old('tag_selector') }}"></div>
        </div>

        <div class="control-group">
            <label for="regex" class="control-label">RegEx Selector</label>
            <div class="controls"><input type="text" class="span4" placeholder="RegEx Selector" name="regex" tabindex="3" value="{{ Input::old('regex') }}" ></div>
        </div>

        <div class="control-group">
            <label for="site_id" class="control-label">Site</label>
            <div class="controls">
                {{ Form::select('site_id', array('' => 'Select Site&hellip;') + $sites, Input::old('site_id'), array('class' => 'span4 selectpicker', 'id' => 'site_id', 'data-size' => 6 )) }}
            </div>
        </div>

        <div class="controls">
            <button class="btn btn-small btn-primary pull-right" type="submit">Save</button>
        </div>
    </form>
</div>
@stop

@section('scripts')
    @parent
@stop