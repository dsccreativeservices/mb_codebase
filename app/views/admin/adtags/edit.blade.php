@extends('layouts.admin')
<!-- User login form goes here -->

@section('title')
<i class="fa fa-code"></i> Ad Tag Management | Edit Ad Tag
@stop

@section('subnav')
@stop

@section('content')
<p class="lead"></p>
<div class="iax-new-user-form span6">
    <form class="form-horizontal clearfix" action=" {{ url('admin/adtags/' . $adtag->id . '/edit') }} " method="post" >
        {{ Form::token() }}

        <div class="control-group">
            <label for="name" class="control-label">Name</label>
            <div class="controls"><input type="text" class="span4" placeholder="Name" name="name" tabindex="1" value="{{ $adtag->name }}"></div>
        </div>

        <div class="control-group">
            <label for="tag_selector" class="control-label">Tag Selector</label>
            <div class="controls"><input type="text" class="span4" placeholder="Tag Selector" name="tag_selector" tabindex="2" value="{{ $adtag->tag_selector }}"></div>
        </div>

        <!--
        <div class="control-group">
            <label for="regex" class="control-label">RegEx Selector</label>
            <div class="controls"><input type="text" class="span4" placeholder="RegEx Selector" name="regex" tabindex="3" value="{{ $adtag->regex }}" ></div>
        </div>-->

        <div class="control-group">
            <label for="site_id" class="control-label">Site</label>
            <div class="controls">
                {{ Form::select('site_id', array(0 => 'Select Site&hellip;') + $sites, $adtag->site_id, array('class' => 'span4 selectpicker', 'id' => 'site_id', 'data-size' => 6 )) }}
            </div>
        </div>

        <div class="control-group">
            <label for="adtype_id" class="control-label">Maps to Adtype</label>
            <div class="controls">
                {{ Form::select('adtype_id[]', $adtypes, null, array('class' => 'span4 selectpicker', 'id' => 'adtype_id', 'data-size' => 6, 'rel' => $adtag->adtype, 'multiple' => 'multiple', 'style' => 'height:300px')) }}
            </div>
        </div>

        <div class="controls">
            <button class="btn btn-small btn-primary pull-right" type="submit">Save</button>
        </div>
    </form>
</div>
@stop

@section('scripts')
    @parent
    <script>
        $(function(){
            var adtype_id = JSON.parse($('#adtype_id').attr('rel'));
            console.log(adtype_id);
            $('#adtype_id').val(adtype_id);
        });
    </script>
@stop
