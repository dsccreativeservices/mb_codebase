@extends('layouts.admin')

@section('content')
<div class="container">
    <div class="row">
      <div class="span12">
        <h3><span class="iax-admin-header">Admin | </span>Manage Groups</h3>
      </div>
    </div>

    <div class="row">
        <div class="span2">
            <ul class="nav nav-list">
                <li class="nav-header">Current Groups</li>
                @foreach ( $groups as $k => $v )
                  @if( Request::is('*/' . $v->id) )
                    <li class="active"><a href="/admin/groups/{{ $v->id }}">{{ $v->name }}</a></li>
                  @else
                    <li><a href="/admin/groups/{{ $v->id }}">{{ $v->name }}</a></li>
                  @endif
                @endforeach
            </ul>
        </div>
        <div class="span10">
            @include('ui.notifications')
            <h4>
              Group: {{ $current_group->name }}
            </h4>
            {{  Form::open(array(
                'action' => 'AdminController@post_group_permissions',
                'id' => 'iax-group-permissions'
                )) }}
            {{ Form::hidden('group_id', $current_group->id ) }}
            <table class="table table-striped" id="iax-permissions-table">
                <tbody>
                    @foreach ($perms as $k => $v)
                        <tr>
                            <td>{{ $k }}</td>
                            <td class="iax-right"><div class="make-switch switch-mini iax-switch" data-animated="false">{{ Iax\Helpers::convertToCheckbox($k, $v) }}</div></td>
                        </tr>
                    @endforeach
                </tbody>
            </table>
            <button class="btn btn-primary pull-right" type="submit">Save Changes</button>
            {{ Form::close() }}
        </div>
      </div>
    </div><!--/row-fluid-->
</div>
@stop

@section('scripts')
    @parent
    {{ HTML::script('assets/js/lib/bootstrap-switch.min.js') }}

    <script type="text/javascript">
        IAX.Main.boot('GroupPermissionsPage');
    </script>
@stop