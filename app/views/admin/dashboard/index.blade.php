@extends('layouts.admin')

@section('title')
<i class="fa fa-dashboard"></i> Dashboard
@stop

@section('subnav')
@stop

@section('content')
	<div class="iax-dash-wrap">
		<div class="iax-dash-info">
			<div class="inner">
				<span>{{ $users }}</span></p>
				Users
			</div>
		</div>
		<div class="iax-dash-info">
			<div class="inner">
				<span>{{ $advertisers }}</span></p>
				Advertisers
			</div>
		</div>
		<div class="iax-dash-info">
			<div class="inner">
				<span>{{ $campaigns }}</span></p>
				Campaigns
			</div>
		</div>
		<div class="iax-dash-info">
			<div class="inner">
				<span>{{ count($projects) }}</span></p>
				Projects
			</div>
		</div>
		<div class="iax-dash-info">
			<div class="inner">
				<span>{{ $creatives }}</span></p>
				Creatives
			</div>
		</div>
	</div>
	<div class="row">
		<div class="span5">
			<p class="lead"><i class="fa fa-list-ul"></i> Recent Events</p>
			<ul class="iax-event-list">
				@foreach( $useractions as $ua )
					<li><strong>{{ $ua->item_type }}</strong> {{ $ua->action }} by <strong>{{ $ua->user->username }}</strong></li>
				@endforeach
			</ul>
		</div>
		<div class="span5">
			<p class="lead"><i class="fa fa-star"></i> Latest Projects</p>
			<ul class="iax-event-list">
                @if( count($projects_recent) )
                    @foreach( $projects_recent as $p )
                        <li>{{ $p->name }}</li>
                    @endforeach
                @else
                    <li>
                        <span>No data.</span>
                    </li>
                @endif
			</ul>
		</div>
	</div>
@stop