@extends('layouts.admin')
<!-- User login form goes here -->

@section('title')
<i class="fa fa-users"></i> User Management | New User
@stop

@section('subnav')
@stop

@section('content')
<div class="iax-new-user-form span6">
    <form class="form-horizontal clearfix" action=" {{ url('admin/users/create') }} " method="post" >
        {{ Form::token(); }}

        <div class="control-group">
            <label for="email" class="control-label">Email</label>
            <div class="controls"><input type="text" class="span4" placeholder="Email" name="email" tabindex="1" ></div>
        </div>

        <div class="control-group">
            <label for="first_name" class="control-label">First Name</label>
            <div class="controls"><input type="text" class="span4" placeholder="First Name" name="first_name" tabindex="2" ></div>
        </div>

        <div class="control-group">
            <label for="last_name" class="control-label">Last Name</label>
            <div class="controls"><input type="text" class="span4" placeholder="Last Name" name="last_name" tabindex="3" ></div>
        </div>

        <div class="control-group">
            <label for="phone" class="control-label">Phone</label>
            <div class="controls"><input type="text" class="span4" placeholder="Phone Number" name="phone" tabindex="4" ></div>
        </div>

        <div class="control-group">
            <label for="password" class="control-label">Password</label>
            <div class="controls"><input type="password" class="span4" placeholder="Password" name="password" tabindex="5" value="password"></div>
        </div>

        <div class="control-group">
            <label for="password_confirm" class="control-label">Password Confirm</label>
            <div class="controls"><input type="password" class="span4" placeholder="Password Confirm" name="password_confirm" tabindex="6" value="password"></div>
        </div>

        <div class="control-group">
            <label for="group_id" class="control-label">Group</label>
            <div class="controls">{{ Form::select('group_id', $groups, null, array('class' => 'span4', 'id' => 'group_id', 'data-size' => 6)) }}</div>
        </div>

        <div class="controls">
            <button id="submitBtn" class="btn btn-small btn-primary pull-right" type="submit">Save</button>
        </div>
    </form>
</div>
@stop

@section('scripts')
    @parent
@stop
