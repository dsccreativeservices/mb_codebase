@extends('layouts.admin')
<!-- User login form goes here -->

@section('title')
<i class="fa fa-users"></i> User Management
@stop

@section('subnav')
@stop

@section('content')
<a href="/admin/users/new" class="btn btn-small btn-success pull-right"><i class="fa fa-plus"></i> New User</a>
<table class="table table-striped table-condensed" id="iax-permissions-table">
    <thead>
        <tr>
            <th>ID</th>
            <th>Email/Username</th>
            <th>Last Name</th>
            <th>First Name</th>
            <th>Status</th>
            <th></th>
        </tr>
    </thead>
    <tbody>
        @foreach ($users as $u)
            <tr>
                <td>{{ $u->id }}</td>
                <td>{{ $u->email }}</td>
                <td>{{ $u->last_name }}</td>
                <td>{{ $u->first_name }}</td>
                <td>
                    @if( ($u->banned) )
                        <span class="label label-important }}">Banned</span>
                    @elseif( $u->activated )
                        <span class="label label-success }}">Active</span>
                    @else
                        <span class="label label-warning }}">Inactive</span>
                    @endif
                </td>
                <td>
                    <a href="/admin/users/{{ $u->id }}/edit" class="iax-link-warning" title="Edit User"><i class="fa fa-edit fa-lg"></i></a>
                    @if( !$u->banned )
                    <form action="/admin/users/ban" method="POST" class="iax-table-action">
                      <input type="hidden" name="user_id" value="{{{$u->id}}}">
                      <input type="hidden" name="mode" value="ban">
                      {{ Form::token() }}
                      <button data-toggle="tooltip" type="submit" title="Ban User"><i class="fa fa-ban fa-lg"></i></button>
                    </form>
                    @else
                    <form action="/admin/users/unban" method="POST" class="iax-table-action">
                      <input type="hidden" name="user_id" value="{{{$u->id}}}">
                      <input type="hidden" name="mode" value="unban">
                      {{ Form::token() }}
                      <button data-toggle="tooltip" type="submit" title="Unban User"><i class="fa fa-check fa-lg"></i></button>
                    </form>
                    <form action="/admin/users/delete" method="POST" class="iax-table-action">
                      <input type="hidden" name="user_id" value="{{{$u->id}}}">
                      <input type="hidden" name="mode" value="delete">
                      {{ Form::token() }}
                      <button data-toggle="tooltip" type="submit" title="Delete User"><i class="fa fa-trash-o fa-lg"></i></button>
                    </form>
                    @endif
                </td>
            </tr>
        @endforeach
    </tbody>
</table>
@stop

@section('scripts')
    @parent
@stop