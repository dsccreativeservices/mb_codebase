@extends('layouts.admin')
<!-- User login form goes here -->

@section('title')
<i class="fa fa-list"></i> Ads Management
@stop

@section('subnav')
@stop

@section('content')
<a href="/admin/adtype/new" class="btn btn-success pull-right"><i class="fa fa-plus"></i> New Ad Type</a>
<table class="table table-striped table-condensed" id="iax-permissions-table">
    <thead>
    <tr>
        <th>Name</th>
        <th>Site</th>
        <th>Tag Name</th>
        <th></th>
    </tr>
    </thead>
    <tbody>
    @foreach ($adtypes as $a)
    <tr>
        <td>{{ $a->name}}</td>
        <td>{{ $a->site }}</td>
        <td> -- </td>
        <td>
            <a href="/admin/adtype/{{ $a->id }}/edit" class="iax-link-warning" title="Edit Ad"><i class="fa fa-edit fa-lg"></i></a> |
            <a href="/admin/adtype/{{ $a->id }}/delete" class="iax-link-danger" title="Delete Ad"><i class="fa fa-ban fa-lg"></i></a>
        </td>
    </tr>
    @endforeach
    </tbody>
</table>
@stop

@section('scripts')
@parent
@stop