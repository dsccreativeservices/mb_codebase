@extends('errors.master')

@section('content')
    <div class="page-header"><h2 class="header">We will be right back!</h2></div>
    <p class="lead">We are currently undergoing some routine maintenance. Please check back later.</p>
@stop