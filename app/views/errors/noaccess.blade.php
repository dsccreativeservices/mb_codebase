@extends('errors.master')

@section('content')
    <div class="page-header"><h2 class="header">Access Denied!</h2></div>
    <p class="lead">You do not have the required access level to view this page.</p>
@stop