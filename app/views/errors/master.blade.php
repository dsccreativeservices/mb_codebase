<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js"> <!--<![endif]-->
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
        <title>Interactive Ad Experience | @yield('title')</title>
        <meta name="description" content="">
        <meta name="viewport" content="width=device-width">

        <link href="//netdna.bootstrapcdn.com/twitter-bootstrap/2.3.2/css/bootstrap-combined.no-icons.min.css" rel="stylesheet">
        <link href="//netdna.bootstrapcdn.com/font-awesome/3.2.1/css/font-awesome.css" rel="stylesheet">

        {{ HTML::style('assets/css/main.css') }}

        <link href='http://fonts.googleapis.com/css?family=Ubuntu:300,400,500,700' rel='stylesheet' type='text/css'>

        <style>body {padding-top: 60px;padding-bottom: 40px;}</style>
    </head>

    <body>
        @include('ui.navbar.main')

        <div class="container">
            <div class="row">
                <div class="span10 offset1">
                    @section('content');
                    @show
                </div>
            </div>
        </div>

        <!-- jQuery -->
        <script src="//ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.js"></script>
        <script>window.jQuery || document.write('<script src="js/vendor/jquery-1.9.1.js"><\/script>')</script>

        {{ HTML::script('assets/js/lib/lodash.underscore.min.js') }}

        {{ HTML::script('assets/js/lib/bootstrap.min.js') }}

        <!-- Code Mirror -->
        {{ HTML::script('assets/code-mirror/codemirror.js') }}
        {{ HTML::script('assets/code-mirror/modes/css.js') }}
        {{ HTML::script('assets/code-mirror/modes/javascript.js') }}
        {{ HTML::script('assets/code-mirror/modes/xml.js') }}

        <!-- Other 3rd party libraries -->
        {{ HTML::script('assets/js/lib/dropzone.js') }}

        {{ HTML::script('assets/js/iax-main.js') }}
    </body>
</html>

