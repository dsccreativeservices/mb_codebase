@extends('errors.master')

@section('content')
    <div class="page-header">
        <h2 class="header">Uh Oh. 500 Server Error</h2>
    </div>
    <p class="lead">Looks like you found a bug. If you don't mind, please fill out the <a href="https://docs.google.com/forms/d/1FpqPnbvmCtXyp8yZVNLXi_vH5Cka3UI2mscn7lGrW8s/viewform" target="_blank">feedback form</a> describing the issue. </p>
    <p><a href="/" title="Homepage">Homepage</a>.</p>
@stop
