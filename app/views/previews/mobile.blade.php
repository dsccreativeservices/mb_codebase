<?php
    function getUrl() {
      $url  = @( $_SERVER["HTTPS"] != 'on' ) ? 'http://'.$_SERVER["SERVER_NAME"] :  'https://'.$_SERVER["SERVER_NAME"];
      $url .= ( $_SERVER["SERVER_PORT"] !== 80 ) ? ":".$_SERVER["SERVER_PORT"] : "";
      $url .= $_SERVER["REQUEST_URI"];
      return $url;
    }
    if(empty($_GET)) {
        //if string has no queries
        $url = getUrl() . "?google_nofetch";
        header("LOCATION: $url");
    } else {
        //if string already has query
        $url = getUrl();
        if (strpos($url,'google_nofetch') === false) {
            $newUrl = $url . "&google_nofetch";
            header("LOCATION: $newUrl");
        }
    }
?>

<html>
<head>
    <meta name="viewport" content="width=device-width; initial-scale=1.0; maximum-scale=1.0; user-scalable=0;">
    <meta name="site" content="{{ $site->host }}" />
</head>
<body>
     <!--
    ============================================================
    = IAX Preview Builder v1.0
    = Previewed at: {{ date('m/d/y g:i:s A T') . "\n" }}
    ============================================================

    Advertiser: {{ $advertiser->name . "\n"}}
    Campaign: {{ $campaign->name . "\n"}}
    Project: {{ $project->name . "\n"}}
    Creative: {{ $creative->name . "\n"}}
    Preview URL: {{ $preview->scrape_url . "\n"}}
    Ad Type: {{ $project->adtype->name . "\n"}}

    ============================================================
    -->
    <style>
        body > .container.iax-content{
            width:100%;
        }
    </style>
    <p class="mobileAlert">NOTE:<br/>The preview of this creative is best viewed directly on a mobile device.</p>

    <div class="mobilePreview">
    	<link rel="stylesheet" href="{{ asset('assets/css/mobilePreview/mobileFunctional.css') }}" type="text/css" media="screen" />
    	<div class="phoneWrapper">
            <img src="{{ asset('assets/img/mobilePreview/phone.png') }}" />
        </div>
        <div class="header">
            <img class="infoBar" src="{{ asset('assets/img/mobilePreview/safariHeader.png') }}" width="320" />
            <div class="urlBar">
            	<div class="backgroundColor"></div>
            	<img class="textOnly" src="{{ asset('assets/img/mobilePreview/safariHeader_textOnlyButton.png') }}" width="19" />
                <span class="url">{{ $preview_url }}</span>
                <img class="refresh" src="{{ asset('assets/img/mobilePreview/safariHeader_refreshButton.png') }}" width="19.5" />
            </div>
        </div>
        <div class="footer">
        	<div class="footerInner">
            	<img src="{{ asset('assets/img/mobilePreview/safariFooter.png') }}" width="320" />
            </div>
        </div>
        <div class="pageContent" style="overflow-x:hidden; overflow-y:scroll">
            <!--<iframe src="http://iax-staging.scrippsonline.com/{{ $previewSrc }}" width="320" height="480" frameborder="0"></iframe>-->
            {{ $preview_source }}

        </div>

    </div>

    <!--<script src="http://iax-staging.scrippsonline.com/assets/js/lib/jquery-1.9.1.min.js"></script>-->
    <script type="text/javascript">

        $(function(){

           //check if jquery is ready on the page yet
          function checkJquery(){
              if (typeof($) !== "undefined"){
                  console.log("jquery ready");
                  CS_injectCreative();
              }else{
                  //console.log("jquery NOT ready");
                  setTimeout(checkJquery, 100);
              }
          }checkJquery();

          function CS_injectCreative(){
             var iframeWidth = "<?php if(isset($iframeWidth)){ echo $iframeWidth; } ?>",
                iframeHeight = "<?php if(isset($iframeHeight)){ echo $iframeHeight; } ?>",
                filePath = "<?php if(isset($iframeSrc)){ echo $iframeSrc; } ?>",
                adtag = "<?php echo $adtag->tag_selector; ?>",
                site = "<?php if(isset($preview->scrape_url)){ echo $preview->scrape_url; } ?>",
                adtype = "<?php if(isset($project->adtype->name)){ echo $project->adtype->name; } ?>";

            //if travel channel mobile do crazy stuff
            if(adtype.indexOf("Mobile") > -1 && site.indexOf("travelchannel") > -1){
                console.log("travel channel mobile");
                $(".area[data-sni-area='content']").insertAfter(".pageContent #site");
                $(".footer.parbase").insertAfter(".area[data-sni-area='content']");
                $(".area[data-sni-area='content']").show();
                $(".footer.parbase").show();
            }

            function findAdtag(){
                if($(adtag).length > 0){
                    console.log("found adtag", adtag);
                    setTimeout(function(){
                        checkDFPRun();
                    },300);
                }else{
                    console.log("adtag not found, check again");
                    setTimeout(findAdtag,200);
                }
            }findAdtag();

            //check if DFP has initialized yet
            function checkDFPRun(){
               if($(adtag).css("display") === "none") {
                   console.log("dfp 325x50 is display none, means DFP done");
                   setTimeout(injectContent, 1200);
               }else{
                   setTimeout(checkDFPRun, 100);
               }
            }

            //once DFP has run inject content
            function injectContent(){
                console.log("inject content");
                var tag = $(adtag).eq(0);
                tag.show().html("<!-- Creative Inserted by IAX -->\n\n <div id='google_ads_iframe_adNum/food/home_1__container' class='IAX' style='border: 0pt none; margin: auto; text-align: center;'><iframe id='google_ads_iframe' width=" + iframeWidth + " height=" + iframeHeight + " scrolling=no marginwidth=0 marginheight=0 frameborder=0 style='border: 0px; vertical-align: bottom; display:block; margin:0 auto' src='http://" + window.location.host + '/' + filePath + "'></iframe></div>");

                if($(adtag).css('display') === 'none'){
                    $(adtag).css("display","block");
                }

                //if mobile interstitial prevent scroll
                if(adtype === "Mobile - Interstitial"){
                    console.log("mobile interstitial");
                    $(".mobilePreview .pageContent").css("overflow-y","hidden");
                    //if on mobile device viewing preview
                    if($(window).width() < 600){
                        $("#dfp_smartphone_banner").css({"background":"black", "position": "fixed", "width": "100%", "z-index": "10000"});
                    }
                    //if on FN
                    lowercaseSite = site.toLowerCase();
                    console.log(lowercaseSite);
                    if(lowercaseSite.indexOf("foodnetwork") > -1){
                        $(".adv-box-01").css({"max-height":"480px"});
                        $("#SearchGlobalBox").css({"top":"-550px"});
                        $(".header-menu#SearchGlobal[type=radio]:checked ~ .search-global").css({"top":"70px"});
                    }
                }

                //check content
                setTimeout(function(){
                    if($(adtag).html().length === 0){
                       console.log("no content");
                       injectContent();
                    }
                },400);
            }
         }
      });

	</script>
    <script src="{{ asset('assets/js/lib/mobilePreview/functions_functional.js') }}"></script>
</body>
</html>
