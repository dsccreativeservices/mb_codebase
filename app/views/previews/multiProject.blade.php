<!--
============================================================
= IAX Preview Builder v1.0
= Previewed at: {{ date('m/d/y g:i:s A T') . "\n" }}
============================================================

-->
<div id="creative1" data-selector="{{ $adtag1->tag_selector }}" data-name="{{ $adtag1->name }}" data-url="{{ $creative1_url }}" style="display:none"></div>
<div id="creative2" data-selector="{{ $adtag2->tag_selector }}" data-name="{{ $adtag2->name }}" data-url="{{ $creative2_url }}" style="display:none"></div>
<div id="creative3" style="display:none"></div>
<div id="creative4" style="display:none"></div>

{{ $preview_source }}


<script type="text/javascript">
    $(function(){
        //kill dfp ads
        var DFP_slots = $("div[id^='dfp_']");
        DFP_slots.each(function( index ) {
            el = $(this);
            var ID = el.attr("id");
            ID = ID + "_IAX";
            el.attr("id",ID);
        });
        
        var page = "{{ $scrape_url }}";
        console.log("page scrape is", page);
        var adtag1 = $("#creative1").data("selector");
        var adtag2 = $("#creative2").data("selector");

        console.log("adtag1 is ", adtag1, " adtag2 is ", adtag2);
        
        //write iframes
        var adtag1_name = $("#creative1").data("name");
        var adtag2_name = $("#creative2").data("name");
        
        var url1 = $("#creative1").data("url");
        var url2 = $("#creative2").data("url");
        console.log("url1 is ", url1, " url2 is ", url2);
        
        function fillFrame(frame, URL, name){
            //remove any spaces in URL
            URL = URL.replace(/\s/g, '');
            //write iframe
            console.log("frame is", frame, "URL is", URL, "name is", name);
            //if site using adapter file
            if(page.indexOf("food") !== -1){
                var divID = frame;
                if(frame === "#pushdown_adtag>.iax_inner"){
                     divID = "dfp_pushdown_brandscape";
                }else if(frame === "#bigbox"){
                     divID = "dfp_bigbox";
                }else if(frame === "#sponsorCtr2"){
                    frame = "#sponsorCtr1";
                    console.log("center module swap ID", frame);
                    divID = "dfp_rsi_result";
                }
                $(frame).html("<!-- Creative Inserted by IAX - mike --><div id='" + divID + "'><div id='google_ads_iframe_adNum/food/home_1__container__' style='border: 0pt none; margin: auto; text-align: center;'><iframe id='google_ads_iframe' width=1 height=1 scrolling=no marginwidth=0 marginheight=0 frameborder=0 style='border: 0px; vertical-align: bottom; display:block; margin:0 auto' src='http://iax.scrippsonline.com/" + URL + "'></iframe></div></div>");
            }else{
               $(frame).html("<!-- Creative Inserted by IAX - mike --><div id='google_ads_iframe_adNum/food/home_1__container__' style='border: 0pt none; margin: auto; text-align: center;'><iframe id='google_ads_iframe' width=1 height=1 scrolling=no marginwidth=0 marginheight=0 frameborder=0 style='border: 0px; vertical-align: bottom; display:block; margin:0 auto' src='http://iax.scrippsonline.com/" + URL + "'></iframe></div>"); 
            }
            
            
            var iframe = $(frame).find("iframe");
            console.log(name);
            switch (name) {
                case "300x250":
                    iframe.css({"width":"300px", "height":"250px"});
                    console.log("300x250 matches");
                    break
                case "300x150":
                    iframe.css({"width":"300px", "height":"150px"});
                    console.log("300x150 matches");
                    break
                case "Center Module":
                    iframe.css({"width":"1px", "height":"10px"});
                    console.log("Center Module matches");
                    break
                default: 
                    console.log("no matches");
            }
        }
        
        fillFrame(adtag1, url1, adtag1_name);
        fillFrame(adtag2, url2, adtag2_name);
   
    });
</script>