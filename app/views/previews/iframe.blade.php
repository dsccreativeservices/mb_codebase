<?php
    //redirect to non-secure for previews since site assets are not secure
    $url = false;
    function getUrl() {
      //$url  = @( $_SERVER["HTTPS"] != 'on' ) ? 'http://'.$_SERVER["SERVER_NAME"] :  'http://'.$_SERVER["SERVER_NAME"];
      $url = 'http://'.$_SERVER["SERVER_NAME"];
      //$url .= ( $_SERVER["SERVER_PORT"] !== 80 ) ? ":".$_SERVER["SERVER_PORT"] : "";
      $url .= $_SERVER["REQUEST_URI"];
      return $url;
    }
    if(empty($_GET)) {
        //if string has no queries
        $url = getUrl() . "?google_nofetch";
        //header("LOCATION: $url");
    } else {
        //if string already has query
        $url = getUrl();
        if (strpos($url,'google_nofetch') === false) {
            $newUrl = $url . "&google_nofetch";
            //header("LOCATION: $newUrl");
        }
    }

?>
<!-- JS redirect if necessary -->
<script type='text/javascript'>
    var url = "<?= $url; ?>";
    if(url !== false && url !== window.location.href){
        console.log("redirect to", url);
        document.location = url;
    }
</script>
<!DOCTYPE html>
<!--
============================================================
= IAX Preview Builder v1.0
= Previewed at: {{ date('m/d/y g:i:s A T') . "\n" }}
============================================================

Advertiser: {{ $advertiser->name . "\n"}}
Campaign: {{ $campaign->name . "\n"}}
Project: {{ $project->name . "\n"}}
Creative: {{ $creative->name . "\n"}}
Preview URL: {{ $preview->scrape_url . "\n"}}
Ad Type: {{ $project->adtype->name . "\n"}}

============================================================
-->
<?php
    //if FCOM remove the conversation box
    if($site->code == "FCOM"){
        echo "<style>.fd-af-ad-wrap{display:none !important;}</style>";
    }
?>

<!-- if logged in show edit project link -->
<?php if(Sentry::check()): ?>
    <style>
        .iax_subnav{
            position: absolute;
            top: 0;
            height:34px;
            z-index:10000;
            padding: 0 10px;
            background-color: #006dcc;
            box-shadow: 0 1px 10px rgba(0, 0, 0, 0.1);
        }
        .iax_subnav:after{
            content:"";
            display: block;
            position:absolute;
            top:0;
            right:0;
            transform: translateX(100%);
            width: 0;
            height: 0;
            border-style: solid;
            border-width: 34px 34px 0 0;
            border-color: #006dcc transparent transparent transparent;
        }
        .iax_subnav a, .iax_subnav a:link, .iax_subnav a:visited{
            position: relative;
            color:white;
            text-decoration: none;
            font-family: "Helvetica Neue",Helvetica,Arial,sans-serif;
            font-size: 14px;
            line-height:34px;
            padding-left:20px;
        }
            .iax_subnav a:hover, .iax_subnav a:link:hover, .iax_subnav a:visited:hover{
                color:white;
                text-decoration: underline;
            }
            .iax_subnav a:before{
                content:"";
                display:block;
                position:absolute;
                top: 5px;
                left: 10px;
                width:7px;
                height:7px;
                border-top: solid 2px white;
                border-left: solid 2px white;
                transform: rotate(-45deg);
            }
    </style>
    <div class="iax_subnav">
        <a href="http://iax.scrippsonline.com/project/{{ $project->id }}/edit" target="_blank">Edit Project</a>
    </div>
<?php endif; ?>

<meta name="site" content="{{ $site->host }}" />


{{ $preview_source }}



<script>

    //check if jquery is ready on the page yet
    function checkJquery(){
        if (typeof($) !== "undefined"){
            console.log("jquery ready");
            CS_injectCreative();
        }else{
            //console.log("jquery NOT ready");
            setTimeout(checkJquery, 100);
        }
    }checkJquery();

    function CS_injectCreative(){
        //get data for creative size and content
        var isLoggedIn = "<?php echo Sentry::check(); ?>"
            iframeWidth = "<?php if(isset($iframeWidth)){ echo $iframeWidth; } ?>",
            iframeHeight = "<?php if(isset($iframeHeight)){ echo $iframeHeight; } ?>",
            filePath = "<?php if(isset($iframeSrc)){ echo $iframeSrc; } ?>",
            adtag = "<?php echo $adtag->tag_selector; ?>",
            adtype = "<?php if(isset($project->adtype->name)){ echo $project->adtype->name; } ?>",
            site = "<?php if(isset($site->code)){ echo $site->code; } ?>",
            scrape = "<?php if(isset($preview->scrape_url)){ echo $preview->scrape_url; } ?>",
            isDFP = "<?php if(isset($isDFP)){ echo $isDFP; } ?>",
            loopCount = 0,
            loopDelay = 200,
            adInjected = false,
            scrollDelay = false,
            scrollDelayTimer = null,
            DFPDead = false;

        /* kill DFP so it doesn't remove content from adslots */
        var dfpCheckCount = 0;
        function killDFP(){
            if( dfpCheckCount < 10 ){
                if(typeof googletag.disablePublisherConsole === "function"){
                    googletag.disablePublisherConsole();
                    googletag.destroySlots();
                    DFPDead = true;
                    console.log("DFP should be killed");
                    findAdtag();
                }else{
                    setTimeout(killDFP, 200);
                    console.log("waiting on DFP");
                    dfpCheckCount++;
                }
            }
            else{
                console.log('dfp did not respond, moving on');
                DFPDead = true;
                findAdtag();
            }


        }
        killDFP();


        /*
            Find the adtag on the page
            If not found
               - check if native adtag of another type exists
               - check if another version of the adtag exists like a number at the end.
            If FCOM enable scroll based adtag search
            -- Called from killDFP();
        */
        function findAdtag(){
            console.log("looking for adtag", adtag);
            if($(adtag).length > 0){
                console.log("found adtag", adtag);
                //clear scroll timers
                clearTimeout(scrollDelayTimer);
                if(adtag === "#video"){
                    isDFP = false;
                }
                if(isDFP === "true"){
                    //add delay to allow DFP to run first
                    /*setTimeout(function(){
                        checkDFPRun();
                    },300);
                    console.log("using DFP");*/
                    checkDFP();
                }else{
                    nonDFPRun();
                }
            }else{
                //console.log("adtag not found, check again");
                if(loopCount < 100){
                    if(adtag === "#dfp_cartridge" || adtag === "#dfp_native_infeed" || adtag === "#dfp_native_recommendation"){
                        switch(adtag){
                            case "#dfp_cartridge":
                                adtag = "#dfp_native_infeed";
                                break;
                            case "#dfp_native_infeed":
                                adtag = "#dfp_native_recommendation";
                                break;
                            case "#dfp_native_recommendation":
                                adtag = "#dfp_cartridge";
                                break;
                        }
                    }
                    if(adtag === "#dfp_bigbox" && site === "FCOM"){
                        switch(adtag){
                            case "#dfp_bigbox":
                                adtag = "#dfp_bigbox_1";
                                break;
                            case "#dfp_bigbox_1":
                                adtag = "#dfp_bigbox";
                                break;
                        }
                    }
                    setTimeout(findAdtag, loopDelay);
                    loopCount++;
                }else{
                    if(site === "FCOM"){
                        //console.log("can't find adtag on food.com, means it might be down the page so check for it again on scroll");
                        $(window).on("scroll",findAdtagOnScroll);
                    }else{
                        alert("adtag does not exist on this page");
                    }
                }
            }
        }

        //FCOM find adtags on scroll
        function findAdtagOnScroll() {
            if(scrollDelay === false && adInjected === false){
                console.log("check for adtag");
                findAdtag();
                scrollDelay = true;
                scrollDelayTimer = setTimeout(function(){
                    scrollDelay = false;
                },500);
            }
        }

        /*
            Check if DFP has been killed yet
            Once killed call Inject Content
            -- Called from findAdtag();
        */
        function checkDFP(){
            if(DFPDead === true){
                injectContent();
            }else{
                setTimeout(checkDFP,200);
            }
        }


        //if not DFP ajax content from iframe into tag
        function nonDFPRun(){
            console.log("old add, use non-dfp method");
            var tag = $(adtag).eq(0);
            console.log("selector is", tag);
            var fullPath = "http://" + window.location.host + '/' + filePath;
            tag.load( fullPath, function() {
                console.log("load complete");
            });
        }



        ////////////////////////////////////////// if module then run builder ///////////////////////////////////
        function getQuery(name) {
            name = name.replace(/[\[]/, "\\[").replace(/[\]]/, "\\]");
            var regex = new RegExp("[\\?&]" + name + "=([^&#]*)"),
                results = regex.exec(location.search);
            return results === null ? "" : decodeURIComponent(results[1].replace(/\+/g, " "));
        }

        function initBuilder(){
            console.log("setup builder");
            //add variables for builder to access
            var advertiser = "<?php if(isset($advertiser->name)){ echo $advertiser->name; } ?>",
                projectName = "<?php if(isset($project->name)){ echo $project->name; } ?>",
                previewURL = "<?php if(isset($preview->scrape_url)){ echo $preview->scrape_url; } ?>",
                mbVars = {
                    project_id: "<?php if(isset($project->id)){ echo $project->id; } ?>",
                    user_id: "<?php if(isset($user->id)){ echo $user->id; } ?>",
                    _token: "<?php echo csrf_token(); ?>" //test token
                },
                types = {
                    "Native - 1 Across" : "editorial-promo",
                    "Native - 2 Across" : "two-across",
                    "Native - 3 Across" : "three-across",
                    "Native Ingredient" : "ingredient",
                    "RSI Module"        : "rsi"
                };

            function checkBuilder(){
                if(
                  (
                      typeof cs[0].nativeOptions !== "undefined"
                      && cs[0].nativeOptions
                      && typeof cs[0].nativeOptions.data !== "undefined"
                      && cs[0].nativeOptions.data
                  )
                  ||
                  (
                      typeof cs[0].rsiOptions !== "undefined"
                      && cs[0].rsiOptions
                      && typeof cs[0].rsiOptions.data !== "undefined"
                      && cs[0].rsiOptions.data
                  )
                ){
                    //console.log("data is ready");
                    $("body").addClass(site).addClass(types[adtype]);
                    window.cs[ 0 ]().builder({
                        type        : types[adtype],
                        site        : site.toLowerCase(),
                        mbVars      : mbVars,
                        advertiser  : advertiser,
                        projectName : projectName,
                        previewURL  : previewURL
                    });
                    //console.log("builder has run", window.cs[ 0 ]().builder);
                }else{
                    console.log("builder NOT ready");
                    setTimeout(checkBuilder,200);
                }
            }

            //first check if the CS library is available to us
            function checkCS(){
                if(window.cs){
                    //console.log("cs is now ready");
                    checkBuilder();
                }else{
                    console.log("cs not ready");
                    setTimeout(checkCS,500);
                }
            }
            checkCS();
        }



        ////////////////////////////////////  once DFP has run inject content  ////////////////////////////////////
        function injectContent(){
            console.log("inject content");
            var tag = $(adtag).eq(0);
            tag.show().html("<!-- Creative Inserted by IAX -->\n\n <div id='google_ads_iframe_adNum/food/home_1__container' class='IAX' style='border: 0pt none; margin: auto; text-align: center;'><iframe id='google_ads_iframe' width=" + iframeWidth + " height=" + iframeHeight + " scrolling=no marginwidth=0 marginheight=0 frameborder=0 style='border: 0px; vertical-align: bottom; display:block; margin:0 auto' src='http://" + window.location.host + '/' + filePath + "'></iframe></div>");

            adInjected = true;

            if($(adtag).css('display') === 'none'){
                $(adtag).css("display","block");
            }
            $(adtag).parents().each(function(){
                var el = $(this);
                if(el.css('display') === "none"){
                    el.css("display","block");
                }
            });
            //show blank BB slot
            if(adtag !== "#dfp_bigbox"){
                $("#dfp_bigbox").css({"width": "300px", "height": "250px", "margin":"0 auto", "background": "lightgray", "display":"block"});
            }

            //if PGI on food network modify to show PGI
            $(".crsl.photo-gallery.carousel-show").addClass("interstitial-show")

            //check content
            setTimeout(function(){
                if($(adtag).html().length === 0){
                    console.log("no content");
                    adInjected = false;
                    injectContent();
                }else{
                    //content check complete
                    console.log("ad injected successfully");
                    var shouldRunBuilder = getQuery('builder');
                    if(shouldRunBuilder === "true" && isLoggedIn === "1"){
                        initBuilder();
                    }
                }
                if($(adtag).css('display') === 'none'){
                    $(adtag).css("display","block");
                }
            },1200);

        }
    }

</script>
