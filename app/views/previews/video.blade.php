<html>
<head>
    <meta name="viewport" content="width=device-width; initial-scale=1.0; maximum-scale=1.0; user-scalable=0;">
    <style media="screen">
       .container{
          display: flex;
          justify-content: center;
          position:relative;
          top:30px;
       }
       video{
          position:relative;
          height:650px;
          width:366px;
          padding: 0 6vw;
       }

    </style>
</head>
<body>
     <!--
    ============================================================
    = IAX Preview Builder v1.0
    = Previewed at: {{ date('m/d/y g:i:s A T') . "\n" }}
    ============================================================

    Advertiser: {{ $advertiser->name . "\n"}}
    Campaign: {{ $campaign->name . "\n"}}
    Project: {{ $project->name . "\n"}}
    Ad Type: {{ $project->adtype->name . "\n"}}

    ============================================================
    -->

   <div class="container"></div>

   <script src="http://iax-staging.scrippsonline.com/assets/js/lib/jquery-1.9.1.min.js"></script>
   <script type="text/javascript">
      var content = unescape(<?php if(isset($content)){ echo $content; } ?>);
      $(function(){
         var wrap = $(".container"),
             findBr = /\r|\n/.exec(content); //does string have line breaks?
         if (findBr) {
            var array = content.split("\n");
         }else{
            var array = content.split(" "); //else split on space
         }
         console.log(array);

         for (var i = 0; i < array.length; i++) {
            var vURL = array[i];
            wrap.append('<video src="' + vURL + '" controls></video>');
         }

      });
   </script>
</body>
</html>
