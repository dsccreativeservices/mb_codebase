<html>
<head>
    <meta name="viewport" content="width=device-width; initial-scale=1.0; maximum-scale=1.0; user-scalable=0;">
</head>
<body>
     <!--
    ============================================================
    = IAX Preview Builder v1.0
    = Previewed at: {{ date('m/d/y g:i:s A T') . "\n" }}
    = PGI version
    ============================================================

    Advertiser: {{ $advertiser->name . "\n"}}
    Campaign: {{ $campaign->name . "\n"}}
    Project: {{ $project->name . "\n"}}
    Creative: {{ $creative->name . "\n"}}
    Preview URL: {{ $preview->scrape_url . "\n"}}
    Ad Type: {{ $project->adtype->name . "\n"}}

    ============================================================
    -->

    <?php
        function getUrl() {
          $url  = @( $_SERVER["HTTPS"] != 'on' ) ? 'http://'.$_SERVER["SERVER_NAME"] :  'https://'.$_SERVER["SERVER_NAME"];
          $url .= ( $_SERVER["SERVER_PORT"] !== 80 ) ? ":".$_SERVER["SERVER_PORT"] : "";
          $url .= $_SERVER["REQUEST_URI"];
          return $url;
        }
        if(empty($_GET)) {
            //if string has no queries
            $url = getUrl() . "?google_nofetch";
            header("LOCATION: $url");
        } else {
            //if string already has query
            $url = getUrl();
            if (strpos($url,'google_nofetch') === false) {
                $newUrl = $url . "&google_nofetch";
                header("LOCATION: $newUrl");
            }
        }
    ?>

    <style>
        /* HGTV code when interstitial is visible */
        #photo_interstitial_wrapper{
            display: block !important;
        }
        .pv-container-site .container-site.bs-docs-container.pv-override .photo-viewer .pv-stage .pv-content-wrapper .pv-content .pv-slideshow-wrapper{
            width:auto !important;
        }
        /*#photo_interstitial_wrapper{
            display: block !important;
            padding: 2rem 1rem 2rem;
            height: 100%;
            min-height: 569px;
            position: absolute;
            right: 160px;
            width: 100%;
        }
        .pv-content-wrapper #photo_interstitial_wrapper > * {
            display: block;
            position: absolute;
            top: 50%;
            left: 50%;
            -webkit-transform: translate(-50%,-50%);
            -ms-transform: translate(-50%,-50%);
            transform: translate(-50%,-50%);
            z-index: 1;
        }*/


        /* show arrows */
        .photoGalleryPage .inline-horizontal .rsArrow.rsArrowDisabled.rsArrowDisabled{
            display:block !important;
        }
    </style>


    <div class="innerContent" style="display:none">
        <div class="pv-content_IAX o-PhotoGalleryPromo__m-Content">
           <div class="o-PhotoGalleryPromo__m-SlideShowWrap pv-slideshow-wrapper" style="height: 612.667px;">
              <div id="photo_interstitial_wrapper">
                 <div id="dfp_photo_interstitial_1"></div>
              </div>
              <!-- slideshow would go here -->

           </div>
        </div>
    </div>

    {{ $preview_source }}


    <script type="text/javascript">

        //check if jquery is ready on the page yet
        function checkJquery(){
            if (typeof($) !== "undefined"){
                console.log("jquery ready");
                CS_injectCreative();
            }else{
                setTimeout(checkJquery, 100);
            }
        }checkJquery();

        function CS_injectCreative(){

            //get data for creative size and content
            var iframeWidth = "<?php if(isset($iframeWidth)){ echo $iframeWidth; } ?>",
                iframeHeight = "<?php if(isset($iframeHeight)){ echo $iframeHeight; } ?>",
                filePath = "<?php if(isset($iframeSrc)){ echo $iframeSrc; } ?>",
                adtag = "<?php echo $adtag->tag_selector; ?>",
                site = "<?php if(isset($preview->scrape_url)){ echo $preview->scrape_url; } ?>",
                adtype = "<?php if(isset($project->adtype->name)){ echo $project->adtype->name; } ?>";

            function formatHTML(){
                $(".pv-content").html( $(".innerContent .pv-content_IAX").html() );
                $(".pv-content-wrapper").addClass("interstitial-show");
                setTimeout(findAdtag, 500);
            }
            formatHTML();


            function findAdtag(){
                console.log("adtag is",adtag);
                if($(adtag).length > 0){
                    console.log("found adtag");
                    injectContent();
                }else{
                    console.log("adtag not found, check again");
                    setTimeout(findAdtag,200);
                }
            }

            function injectContent(){
                console.log("inject content");
                var tag = $(adtag).eq(0);
                tag.show().html("<!-- Creative Inserted by IAX -->\n\n <div id='google_ads_iframe_adNum/food/home_1__container' class='IAX' style='border: 0pt none; margin: auto; text-align: center;'><iframe id='google_ads_iframe' width=" + iframeWidth + " height=" + iframeHeight + " scrolling=no marginwidth=0 marginheight=0 frameborder=0 style='border: 0px; vertical-align: bottom; display:block; margin:0 auto' src='http://" + window.location.host + '/' + filePath + "'></iframe></div>");

                if($(adtag).css('display') === 'none'){
                    $(adtag).css("display","block");
                }
            }

        }
    </script>
</body>
</html>
