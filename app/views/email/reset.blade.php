<img src="{{ Config::get('app.url') }}/public/assets/img/logo_187x35_black.png" alt="Interactive Ad Experience" width="52" height="34">
<p>A password request has be submitted for the IAX account using this email. Click the link below or paste into a web browser to reset your password.</p>
<a href="{{ Config::get('app.url') }}/doreset/{{ $reset_code }}">{{ Config::get('app.url') }}/doreset/{{ $reset_code }}</a>

<br><br>
<p style="font-weight: bold;">*Note: If you did not request this action, please contact the administrator.</p>