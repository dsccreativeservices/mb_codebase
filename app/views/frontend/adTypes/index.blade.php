@extends('layouts.master')

@section('title')
All Projects
@stop

@section('content')
@if( Input::has('tag') )
<h4>Projects Tagged: <span class="label label-info">{{{ Input::get('tag') }}}</span></h4>
@else
<h4>{{ $adtype }} Projects</h4>
@endif

@if ( count($projects) > 0 )
<table class="table table-striped table-condensed iax-table">
  <thead>
    <tr>
      <th>Project ID</th>
      <th>Ad Type</th>
      <th>Advertiser</th>
      <th>Campaign</th>
      <th>Project</th>
      <th>Site</th>
      <th>Created</th>
      <th>Modified</th>
      <th></th>
    </tr>
  </thead>
  <tbody>
@foreach ( $projects as $p )
  <tr>
    <td>{{ $p->id }}</td>
    <td class="adType" data-adtype="{{ $p->adtype->id }}">{{ $p->adtype->name or 'NOT SET'}}</td>
    <td><a href="/advertiser/{{ $p->campaign->advertiser->id or '#doesntExist' }}">{{ $p->campaign->advertiser->name or 'NOT SET' }}</a></td>
    <td><a href="/campaign/{{ $p->campaign->id or '#doesntExist'}}" >{{ $p->campaign->name  or 'NOT SET' }}</a></td>
    <td><a href="/project/{{ $p->id }}/edit">{{ $p->name }}</a></td>
    <td><a class="site" data-site_id="{{ $p->site->id }}" href="/sites/{{ $p->site->id }}">{{ $p->site->name }}</a></td>
    <td>{{ $p->created_at->diffforhumans() }}</td>
    <td>{{ $p->updated_at->diffforhumans() }}</td>
    @if( isset($p->creatives->first()->preview->preview_id) )
      <td><a href="/preview/view/{{ $p->creatives->first()->preview->preview_id }}" target="_blank"><i class="fa fa-search"></i></a></td>
    @else
      <td></td>
    @endif
  </tr>
@endforeach
  </tbody>
</table>
<div class="iax-pagination">
  <?php echo $projects->links(); ?>
</div>

@else
<span class="alert alert-info">You have not worked on any projects yet.</span>
@endif
@stop

@section('scripts')
@parent

<script type="text/javascript">
   $(function(){
       /*$('a.site').on("click",function(e){
            console.log("site anchor clicked");
            e.preventDefault();
            var el = $(this);
            var adType = $(".adType").data("adtype");
            var siteId = el.data("site_id");
           
            var filterData = {
                adType_id: adType,
                site_id: siteId,
                _token: IAX.Main.meta.csrfToken
            };

            var request = $.ajax({
                url: IAX.Config.apiEndpoint + "/adtypes/site",
                type: "POST",
                data: filterData
            });

            //if successful, return message
            request.done(function(data) {
                if( data.status ){
                    IAX.Utils.notify(data.message, 'success');
                    console.log(data.message);
                }
            });
            //if fail, return error
            request.fail(function(jqXHR, textStatus) {
                IAX.Utils.notify('Something went wrong!', 'error');
            });
       });*/
   });
</script>
@stop