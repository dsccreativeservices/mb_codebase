@extends('layouts.master')

@section('content')
<div class="row">
	<div class="span10 offset1 iax-search-results">
		<h3>Search Results</h3>
		@include('ui.notifications')
		<p class="iax-primary">Results for: <span class="iax-search-highlight">"{{ $term }}"</span></p>
		<!-- Advertisers -->
		@if( isset($advertisers) )
			<h4>Advertiers</h4>
			{{ $advertisers }}
		@endif

		<!-- Campaigns -->
		@if( isset($campaigns) )
		<h4>Campaigns</h4>
		{{ $campaigns }}
		@endif

		<!-- Projects -->
		@if( isset($projects) )
		<h4>Projects</h4>
		{{ $projects }}
		@endif

		<!-- Placements -->
		@if( isset($placements) )
		<h4>Placements</h4>
		{{ $creatives }}
		@endif
        
        <!-- tags -->
		@if( isset($tags) )
		<h4>Tags</h4>
		{{ $tags }}
		@endif
        
        <!-- sites -->
		@if( isset($sites) )
		<h4>Sites</h4>
		{{ $sites }}
		@endif
        
        <!-- adTypes -->
		@if( isset($adTypes) )
		<h4>Ad Types</h4>
		{{ $adTypes }}
		@endif
	</div>
</div>
<div class="row">
	<div class="span10 offset1">
		<hr>
		<!-- <div class="alert alert-info">
			<h5>Search Flags</h5>
			<p>
				You can use search flags to perform a more targeted search. To use a flag, just put it at the beginning of your search followed by a colon.
			</p>
		</div> -->
	</div>
</div>
@stop