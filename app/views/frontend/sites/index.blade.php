@extends('layouts.master')

@section('title')
All Projects
@stop

@section('content')
@if( Input::has('tag') )
<h4>Projects Tagged: <span class="label label-info">{{{ Input::get('tag') }}}</span></h4>
@else
<h4>Projects on {{ $site }}</h4>
@endif

@if ( count($projects) > 0 )
<table class="table table-striped table-condensed iax-table">
  <thead>
    <tr>
      <th>Project ID</th>
      <th class="adType">Ad Type</th>
      <th>Advertiser</th>
      <th>Campaign</th>
      <th>Project</th>
      <th>Site</th>
      <th>Created</th>
      <th>Modified</th>
      <th></th>
    </tr>
  </thead>
  <tbody>
@foreach ( $projects as $p )
  <tr>
    <td>{{ $p->id }}</td>
    <td><a href="/adTypes/{{ $p->adtype->id or '#doesntExist' }}">{{ $p->adtype->name or 'NOT SET'}}</a></td>
    <td><a href="/advertiser/{{ $p->campaign->advertiser->id or '#doesntExist' }}">{{ $p->campaign->advertiser->name or 'NOT SET' }}</a></td>
    <td><a href="/campaign/{{ $p->campaign->id or '#doesntExist' }}">{{ $p->campaign->name or 'NOT SET' }}</a></td>
    <td><a href="/project/{{ $p->id }}/edit">{{ $p->name }}</a></td>
    <td>{{ $p->site->name }}</td><!-- removed link to site here since already at site view level -->
    <td>{{ $p->created_at->diffforhumans() }}</td>
    <td>{{ $p->updated_at->diffforhumans() }}</td>
    @if( isset($p->creatives->first()->preview->preview_id) )
      <td><a href="/preview/view/{{ $p->creatives->first()->preview->preview_id }}" target="_blank"><i class="fa fa-search"></i></a></td>
    @else
      <td></td>
    @endif
  </tr>
@endforeach
  </tbody>
</table>
<div class="iax-pagination">
  <?php echo $projects->links(); ?>
</div>

@else
<span class="alert alert-info">You have not worked on any projects yet.</span>
@endif
@stop
