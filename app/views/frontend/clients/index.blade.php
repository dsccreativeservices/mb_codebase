@extends('layouts.master')

@section('title')
Client Area
@stop

@section('content')
  <h1>Client Area</h1>
  <p class="lead">You are in the public client area.</p>
@stop

@section('scripts')
  @parent
  <script type="text/javascript">
      IAX.Main.boot('HomePage');
  </script>
@stop