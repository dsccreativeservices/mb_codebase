@extends('layouts.master')
<!-- User login form goes here -->

@section('content')
@include('ui.notifications')
<h4>New Creative</h4>
<hr>
<div class="iax-form iax-new-creative">
        {{  Form::open(array(
            'action' => 'CreativeController@post_new',
            'class' => 'iax-login-box form-horizontal'
            )) }}
        {{ Form::token() }}
        <fieldset>
            <div class="control-group">
                {{ Form::label('advertiser', 'Advertiser', array('class' => 'control-label')) }}
                <div class="controls">
                    {{ Form::select('advertiser_id', $advertisers, null, array('class' => 'span6')) }}
                </div>
            </div>
            <div class="control-group">
                {{ Form::label('campaign', 'Campaign', array('class' => 'control-label')) }}
                <div class="controls">
                    {{ Form::select('campaign_id', $campaigns, null, array('class' => 'span6')) }}
                </div>
            </div>
            <div class="control-group">
                {{ Form::label('project', 'Project', array('class' => 'control-label')) }}
                <div class="controls">
                    {{ Form::select('project_id', $projects, null, array('class' => 'span6')) }}
                </div>
            </div>
            <div class="control-group">
                {{ Form::label('creative_name', 'Creative Name', array('class' => 'control-label')) }}
                <div class="controls">
                    {{ Form::text('creative_name', null, array('class' => 'span4')) }}
                </div>
            </div>
            <div class="control-group">
                {{ Form::label('preview_url',
                    'Preview URL',
                     array('class' => 'control-label')
                     )
                 }}
                <div class="controls">
                    {{ Form::text('preview_url',
                       null,
                       array('class' => 'span6',
                        'placeholder' => 'http://www.example.com')
                       )
                    }}
                </div>
            </div>
            <div class="control-group">
                {{ Form::label('creative', 'Creative', array('class' => 'control-label')) }}
                <div class="controls">
                    {{ Form::textarea('creative', null, array('class' => 'span8')) }}
                </div>
            </div>
            <div class="form-actions">
                {{ Form::submit('Submit', array('class' => 'btn btn-small btn-primary')) }}
            </div>
            {{ Form::close() }}
        </fieldset>
        </form>
    </div>
</div>
</div>
@stop

@section('scripts')
{{ HTML::script('assets/js/prism.js') }}
<script type="text/javascript">
window.prettyPrint && prettyPrint()
</script>
@stop