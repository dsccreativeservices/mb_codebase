@extends('layouts.master')

@section('title')
Creatives
@stop

@section('content')

@include('ui.notifications')

@if( isset($page_header) )
<h4> {{ $page_header }} </h4>
@endif

@if ( count($creatives) > 0 )
<table border="0" class="table table-striped iax-table">
  <thead>
    <tr>
      <th>ID</th>
      <th>Advertiser</th>
      <th>Campaign</th>
      <th>Project</th>
      <th>Name</th>
      <th>Created</th>
      <th>Modified</th>
      <th>Actions</th>
    </tr>
  </thead>
  <tbody>

    @foreach ($creatives as $a)
    <tr>
      <td>{{ $a->cre_id }}</td>
      <td>{{ $a->adv_name }}</td>
      <td>{{ $a->cam_name }}</td>
      <td>{{ $a->pro_name }}</td>
      <td>{{ $a->cre_name }}</td>
      <td>{{ date('m-d-y',strtotime($a->created_at)) }}</td>
      <td>{{ date('m-d-y',strtotime($a->updated_at)) }}</td>
      <td class="iax-table-actions">
        <a href="/preview/view/{{ $a->preview_id }}" class="" type="button" id="view" title="View Preview"><i class="icon-eye-open fa-large fa-black"></i></a>
        <a href="/creatives/{{ $a->cre_id }}" class="" type="button" id="view" title="View Details"><i class="fa fa-search fa-large fa-black"></i></a>
        <!-- <a href="" class="" type="button" id="edit"><i class="fa fa-pencil fa-black"></i></a> -->
        <a href="/creatives/delete/{{ $a->cre_id }}" class="" type="button" id="remove"><i class="fa fa-trash fa-large"></i></a>
      </td>
    </tr>
    @endforeach

        </tbody>
      </table>

@else
<h4>There are currently no creatives for this project.</h4>
@endif
@stop

@section('scripts')
{{ HTML::script('assets/js/prism.js') }}
<script type="text/javascript">
window.prettyPrint && prettyPrint()
</script>
@stop