@extends('layouts.master')

@section('title')
Creative Detail : {{ $info->pro_name }}
@stop

@section('content')
{{ HTML::style('assets/css/prism.css') }}

<ul class="breadcrumb">
  <li><a href="#">{{ $info->adv_name }}</a> <span class="divider">/</span></li>
  <li><a href="#">{{ $info->cam_name }}</a> <span class="divider">/</span></li>
  <li><a href="#">{{ $info->pro_name }}</a> <span class="divider">/</span></li>
  <li class="active">{{ $info->cre_name }}</li>
</ul>

<h5>Advertiser: {{ $info->adv_name }}</h5>
<h5>Campaign: {{ $info->cam_name }}</h5>
<h5>Project: {{ $info->pro_name }}</h5>
<h5>Creative: {{ $info->cre_name }}</h5>
<p> {{ $info->desc }} </p>

<pre id="" class="iax-code-view"><code class="language-markup iax-code-view">{{{ $info->creative }}}</code></pre>
@stop

@section('scripts')
	{{ HTML::script('assets/js/prism.js') }}
	<script type="text/javascript">
	window.prettyPrint && prettyPrint()
	</script>
@stop