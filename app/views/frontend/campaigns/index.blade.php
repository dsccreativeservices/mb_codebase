@extends('layouts.master')

@section('title')
Campaign Detail : {{ $campaign->name }}
@stop

@section('content')

<h4>{{ $campaign->name }}</h4>
<h5>Advertiser: {{ $campaign->adv_name }}</h5>
<p> {{ $campaign->desc }} </p>

<table border="0" class="table table-striped table-hover iax-table">
  <thead>
    <tr>
      <th>Project Name</th>
      <th>Url</th>
      <th>Created</th>
      <th>Updated</th>
      <th>Created By</th>
      <th>Updated By</th>
  </tr>
</thead>
<tbody>

    @foreach ($projects as $p)
    <tr>
      <td>{{ $p->name }}</td>
      <td><a href="{{ $p->url }}" target="_blank" title="{{ $p->url }}">{{ $p->url }}</a></td>
      <td>{{ date('m-d-y h:m a',strtotime($p->created_at)) }}</td>
      <td>{{ date('m-d-y h:m a',strtotime($p->updated_at)) }}</td>
      <td><em>N/A</em></td>
      <td><em>N/A</em></td>
</tr>
@endforeach

</tbody>
</table>

@stop

@section('scripts')
@parent
<script type="text/javascript">
    IAX.Main.boot('CampaignDetail');
</script>
@stop