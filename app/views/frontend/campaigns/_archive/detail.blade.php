@extends('layouts.master')

@section('title')
Campaign Detail : {{ $campaign->name }}
@stop

@section('styles')
   @parent
   <style media="screen">
      body{
         overflow-x:hidden;
      }
   </style>
@stop

@section('content')


@if( $isClient )

@else
<ul class="iax-breadcrumbs breadcrumb">
    <li><a href="/advertisers">Advertisers</a> <span class="divider">/</span></li>
    <li><a href="/advertiser/{{ $campaign->advertiser->id }}">{{ $campaign->advertiser->name }}</a> <span class="divider">/</span></li>
    <li>{{ $campaign->name }}</li>
</ul>
@endif

<div class="iax-stack-hd">
	<h5><a href="/advertiser/{{ $campaign->advertiser->id }}">{{ $campaign->advertiser->name }}</a></h5>
    <h4 id="iax-campaign-name" style="display:inline;">{{ $campaign->name }} Campaign</h4>
  	@if( !$isClient && !$isViewer  )
    	<div class="campaignBtns" style="margin-top:20px;">
            <a class="addProjectToCampaign btn btn-primary" href="/project/new?a={{$campaign->advertiser->id}}&c={{ $campaign->id }}">Add Project</a>
            <span class="multiPreview btn btn-primary" style="margin-left:10px">Create Multi-Project Preview</span>
        </div>
    	<h5 class="clientLink">Client Link: <a id="campaign" href="{{ URL::to('client/campaign', $campaign->id) }}" >{{ URL::to('client/campaign', $campaign->id) }}</a></h5>
    @endif
</div>

@if( !$isClient && !$isViewer  )
    <!-- create multi-project preview -->
    <div class="multiPreviewContainer">
        <h3>Create Multi-Project Preview Page</h3>
        <p class="note">
            Create a new preview of 2 or more projects from this campaign.<br/>
            <b>The projects that you choose below must match the Preview Page URL you provide.</b><br/>
            <small>For example, if you provide http://www.hgtv.com as your Preview Page scrape, then the projects you select from the dropdowns must also, in their settings, be on HGTV.</small>
        </p>
        <form>
            <div class="halfCol">
                <label for="previewName">Preview Page Name</label>
                <input type="text" name="previewName" id="name" placeholder="Name your preview so you can find it later">
            </div>
            <div class="halfCol">
                <label for="previewPage">Preview Page URL</label>
                <input type="text" name="previewPage" id="URL" placeholder="URL of page to display the projects on - ex: http://www.hgtv.com">
            </div>
            <h4>Project Placement</h4>
            <p>Select an Adtag and a Project from this campaign.<br/>
                <span style="font-size:12px">(Only Adtags and Projects that currently exist in this Campaign are available)</span></p>
            <div class="item" id="item1">

                <div class="fullCol">
                    <label for="project">Project</label>
                    <select id="project">
                        <option>Select an Project</option>
                        @foreach ($campaign->projects as $p)
                            <option data-projectid="{{ $p->id }}">{{ $p->name }}</option>
                        @endforeach
                    </select>
                </div>
                <span class="removeItem">+</span>
            </div>
            <span class="addRow btn btn-primary">Add Another</span>
            <span class="createPreview btn btn-primary" data-campaign_id="{{ $campaign->id }}">Create Multi-Project Preview</span>
        </form>

    </div>
    @if(isset($previews) && count($previews) > 0)
        <div class="row previews">
            <h4>Existing Multi-Project Previews <span>View All</span></h4>
            <?php $count = 1; ?>
            @foreach ($previews as $pre)
                <div class="row">
                    <span class="previewNum">{{ $count }}</span>
                    <p><b>Name: </b>{{ $pre->name }}</p>
                    <p style="max-width: 460px; height: 20px; overflow: hidden;"><b>Scrape URL: </b>{{ $pre->scrape_url }}</p>
                    <p><b>Preview URL: </b><a href="/client/campaign/preview/{{ $pre->preview_id }}" target="_blank">Click Here</a></p>
                    <p class="timestamp">
                        <span><b>Updated at:</b> {{ $pre->updated_at }}</span>
                    </p>
                    <a href="#" class="iax-preview-delete" rel="{{ $pre->preview_id }}">Delete Preview</a>
                </div>
                <?php $count++; ?>
            @endforeach
        </div>
    @endif

    <!-- END multi-project preview -->
@endif

@if( !$isViewer)
<!-- modal box -->
<div class="campaignOptions">
    <div>
        <p>Multi-project options:</p>
        <ul>
            <li class="editProjects">Edit Projects (New Tabs) <span></span></li>
            <li class="flatComps">Get Flat Comp Links <span></span></li>
            <li class="previewProjects">Get Preview Links <span></span></li>
            <li class="trafficProjects">Set to Traffic and Get Links <span></span></li>
        </ul>
    </div>
</div>
@endif

@if( count($campaign->projects) < 1 )
    <p class="text-info alert alert-info">There are currently no projects associated with <strong> {{ $campaign->name }}. </strong></p>
@else
    <?php $count = 0; ?>
    @foreach ( $campaign->projects as $p )
        <?php
            $count++;
            $uCreated = Sentry::getUserProvider()->findById($p->created_by);
            if($p->updated_by != 0){
                $uUpdated = Sentry::getUserProvider()->findById($p->updated_by);
            }else{
                $uUpdated = Sentry::getUserProvider()->findById($p->created_by);
            }
        ?>
        @if( $count % 2 === 0 )
            <div class="iax-stack-item" rel="{{ $p->id }}" data-site="{{ $p->site->name }}">
        @else
            <div class="iax-stack-item alt" rel="{{ $p->id }}" data-site="{{ $p->site->name }}">
        @endif

            @if( $isClient  )
                <h5>{{ $p->name }}</h5>
                @if( isset($p->creatives->first()->preview) )
                    <!--<i class=""></i> <a href="{{ URL::to('preview/iframe', $p->creatives->first()->preview->preview_id) }}" target="_blank">Functional Preview</a>-->
                    @if( $p->AdType->name == "Mobile")
                        <a id="preview-url" href="{{ URL::to('preview/iframe', $p->creatives->first()->preview->preview_id) }}" target="_blank">Functional Preview</a>
                    @elseif( $p->AdType->name == "Tablet")
                        <a id="preview-url" href="/preview/iframe/{{$p->creatives->first()->preview->preview_id}}?tablet=true" target="_blank">Functional Preview</a>
                    @else
                        <a href="{{ URL::to('preview/iframe', $p->creatives->first()->preview->preview_id) }}" target="_blank" title="View Preview">Functional Preview</a>
                    @endif
                @elseif( count($p->images) )
                    <?php $query ?>
                    @if( $p->AdType->name == "Mobile")
                        @if( false !== stripos($p->AdTag->name, 'App'))
                            <?php $query = "?mobile=true&inApp=true";  ?>
                        @else
                            <?php $query = "?mobile=true&inApp=false"; ?>
                        @endif
                    @else
                        <?php $query = ""; ?>
                    @endif
                    <a href="/client/project/{{ $p->id }}/images{{$query}}" target="_blank">View Comps</a><br>
                @else
                    <span class="label label-default">No previews available.</span>
                @endif
                </p>
            @else
                <!-- select box -->
                @if( !$isViewer)
                    <input type="checkbox" name="{{ $p->name }}" rel="{{ $p->id }}">
                @endif
                <h5>
                    @if( isset($p->creatives->first()->preview->preview_id) )
                        @if( $p->AdType->name == "Mobile")
                            <a id="preview-url" href="{{ URL::to('preview/iframe', $p->creatives->first()->preview->preview_id) }}" target="_blank">{{ $p->name }}</a>
                        @elseif( $p->AdType->name == "Tablet")
                            <a id="preview-url" href="/preview/iframe/{{$p->creatives->first()->preview->preview_id}}?tablet=true" target="_blank">{{ $p->name }}</a>
                        @else
                            <a href="{{ URL::to('preview/iframe', $p->creatives->first()->preview->preview_id) }}" target="_blank" title="View Preview">{{ $p->name }}</a>
                        @endif
                    @else
                    {{ $p->name }}
                    @endif
                    <!--
                    remove favorite star from campaign level
                    @if( Favorite::isFavorite( $user->id, $p->id ) )
                        <a class="iax-favorite" rel="{{ $p->id }}" data-toggle="tooltip" data-position="right" title="Remove from favorites"><i class="fa fa-star"></i></a>
                        <a class="iax-favorite" rel="{{ $p->id }}" data-position="right" title="Remove from favorites"><i class="fa fa-star"></i></a>
                    @else
                        <a class="iax-favorite" rel="{{ $p->id }}" data-toggle="tooltip" data-position="right" title="Add to favorites"><i class="fa fa-star-o"></i></a>
                        <a class="iax-favorite" rel="{{ $p->id }}" data-position="right" title="Add to favorites"><i class="fa fa-star-o"></i></a>
                    @endif
                    -->
                </h5>
                <div class="timestamp">
                <p class="createModInfo">
                    <small>Created: {{ $p->created_at->toDayDateTimeString() }} by {{ $uCreated->first_name}} {{ $uCreated->last_name }}</small>
                    <small>Modified: {{ $p->updated_at->toDayDateTimeString() }} by {{ $uUpdated->first_name}} {{ $uUpdated->last_name }}</small>
                </p>
                </div>
                <div class="status">
                    <!-- status indicator -->
                    <!-- status text -->
                    @if( $p->status === 'complete' )
                        <p class="statusText">Traffic</p>
                    @elseif( isset($p->creatives->first()->preview->preview_id) )
                        <p class="statusText">Functional</p>
                    @elseif( count($p->images) )
                        <p class="statusText">Flat Comp</p>
                    @else
                        <p class="statusText">Empty</p>
                    @endif
                    <!-- blue blocks -->
                    @if( count($p->images) )
                        <?php $query ?>
                        @if( $p->AdType->name == "Mobile")
                            @if( false !== stripos($p->AdTag->name, 'App'))
                                <?php $query = "?mobile=true&inApp=true";  ?>
                            @else
                                <?php $query = "?mobile=true&inApp=false"; ?>
                            @endif
                        @else
                            <?php $query = ""; ?>
                        @endif
                        <span onclick="window.open('/client/project/{{ $p->id }}/images{{$query}}','_blank')" style="background:#007fbf; border:0; cursor:pointer"></span>
                    @else
                        <span></span>
                    @endif
                    @if( isset($p->creatives->first()->preview->preview_id) )
                        @if( $p->AdType->name == "Mobile")
                            <span onclick="window.open('{{ URL::to('preview/iframe', $p->creatives->first()->preview->preview_id) }}','_blank')" style="background:#007fbf; border:0; cursor:pointer"></span>
                        @elseif( $p->AdType->name == "Tablet")
                            <span onclick="window.open('/preview/iframe/{{$p->creatives->first()->preview->preview_id}}?tablet=true','_blank')" style="background:#007fbf; border:0; cursor:pointer"></span>
                        @else
                            <span onclick="window.open('{{ URL::to('preview/iframe', $p->creatives->first()->preview->preview_id) }}','_blank')" style="background:#007fbf; border:0; cursor:pointer"></span>
                        @endif
                    @else
                        <span></span>
                    @endif
                    @if( $p->status === 'complete' )
                        <span onclick="window.open('{{ URL::to('adops/traffic/project', $p->id) }}','_blank')" style="background:#007fbf; border:0; cursor:pointer"></span>
                    @else
                        <span></span>
                    @endif
                    <!-- end blue blocks -->
                </div>
                @if( !$isViewer)
                <div class="controls" style="display:none">
                    <div class="editExpand">
                        <ul>
                            <li>
                                <!-- edit flat comps -->
                                <a href="/project/{{ $p->id }}/edit#flatComp" class="iax-edit" rel="{{ $p->id }}" title="Edit Flat Comps">
                                    Flat Comps
                                    <svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
                                    	 viewBox="0 0 22 22" style="enable-background:new 0 0 22 22;" xml:space="preserve">
                                    <path id="gray" fill="#58595B" d="M18.8,1.1H3.2c-1.2,0-2.1,0.9-2.1,2.1v15.5c0,1.2,0.9,2.1,2.1,2.1h15.5c1.2,0,2.1-0.9,2.1-2.1
                                    	V3.2C20.9,2.1,19.9,1.1,18.8,1.1z M19.6,18.6c0,0.5-0.4,1-1,1H5.8l9-9l4.8,4.6V18.6z M19.6,13.5l-4.8-4.8L4,19.6H3.4
                                    	c-0.5,0-1-0.4-1-1V3.4c0-0.5,0.4-1,1-1h15.2c0.5,0,1,0.4,1,1V13.5z M7.2,5C6,5,5,6,5,7.2s1,2.2,2.2,2.2s2.2-1,2.2-2.2S8.4,5,7.2,5z
                                    	 M7.2,8.1c-0.5,0-1-0.4-1-1s0.4-1,1-1s1,0.4,1,1S7.7,8.1,7.2,8.1z"/>
                                    </svg>
                                </a>
                            </li>
                            <li>
                                <!-- edit functional -->
                                <a href="/project/{{ $p->id }}/edit#functional" class="iax-edit" rel="{{ $p->id }}" title="Edit Functional">
                                    Functional
                                    <svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
                                    	 viewBox="0 0 22 22" style="enable-background:new 0 0 22 22;" xml:space="preserve">
                                     <path id="gray" fill="#58595B" d="M5.6,20.9c-0.3,0-0.5,0-0.8,0c-0.3-0.1-0.6-0.1-0.9-0.2c-0.7-0.2-0.9-1-0.4-1.5
                                     	c0.5-0.5,0.9-0.9,1.4-1.4c0,0,0.1-0.1,0.1-0.1c-0.3-0.3-0.5-0.5-0.8-0.8c0,0-0.1,0.1-0.1,0.2c-0.5,0.5-0.9,1-1.4,1.4
                                     	c-0.5,0.5-1.2,0.3-1.4-0.3c0-0.1-0.1-0.2-0.1-0.3c-0.1-0.2-0.1-0.5-0.1-0.7c0-0.3,0-0.5,0-0.8c0-0.1,0-0.2,0.1-0.3
                                     	c0.5-2.4,2.9-3.9,5.2-3.2c0.1,0,0.2,0,0.3-0.1c0.5-0.5,1-1,1.6-1.6c0.2-0.2,0.2-0.4,0-0.6c-0.7-0.7-1.3-1.3-2-2c0,0-0.1-0.1-0.1-0.1
                                     	C5.9,8.6,5.6,8.7,5.3,8.7c-0.1,0-0.1,0.1-0.1,0.2c0,0.3-0.1,0.5-0.3,0.8C4.7,9.8,4.5,10,4.4,10.2c-0.4,0.4-1,0.4-1.4,0
                                     	C2.5,9.7,2.1,9.3,1.6,8.8C1.4,8.6,1.2,8.4,1.1,8.1c0-0.1,0-0.2,0-0.3c0.1-0.3,0.2-0.5,0.4-0.6C1.7,7,1.8,6.9,1.9,6.8
                                     	c0.2-0.3,0.5-0.4,0.9-0.3c0.1,0,0.2,0,0.2-0.2c0-0.3,0.1-0.6,0.3-0.8C4.1,4.6,5,3.7,5.8,2.9C6.7,2,7.8,1.5,9,1.3
                                     	c0.8-0.1,1.6,0,2.4,0.3c0.3,0.1,0.4,0.3,0.5,0.6c0,0.3-0.1,0.5-0.3,0.6c-0.1,0-0.2,0.1-0.2,0.1c-1,0.3-1.7,0.9-2.4,1.7
                                     	C8.8,4.8,8.8,4.9,8.8,5c0.1,0.4,0.1,0.7-0.2,1c0.7,0.7,1.5,1.5,2.2,2.2c0.1,0.1,0.4,0.1,0.5,0c0.5-0.5,1-1,1.4-1.4
                                     	c0.1-0.1,0.1-0.2,0.1-0.3c-0.2-0.7-0.2-1.3-0.1-2c0.5-2.4,3-3.8,5.2-3.1c0.8,0.3,0.9,1,0.4,1.5C18,3.2,17.6,3.6,17.2,4
                                     	c-0.1,0.1-0.3,0.2-0.4,0.3c0.3,0.3,0.5,0.5,0.8,0.8c0.5-0.5,1-1,1.5-1.5c0.5-0.5,1.3-0.4,1.5,0.4c0.3,0.8,0.3,1.6,0,2.5
                                     	c-0.7,2.1-2.9,3.3-5.1,2.7c-0.1,0-0.2,0-0.3,0.1c-0.2,0.2-0.5,0.5-0.7,0.7c-0.2,0.2-0.5,0.5-0.8,0.7c0.1,0.1,0.2,0.1,0.2,0.2
                                     	c2.1,2,4.3,4.1,6.4,6.1c0.6,0.5,0.6,1.3,0,1.8c-0.5,0.5-1,1-1.5,1.5c-0.6,0.6-1.3,0.6-1.9,0c-2-2.1-4-4.2-6.1-6.4
                                     	c-0.1-0.1-0.1-0.1-0.1-0.2c-0.6,0.6-1.1,1.1-1.7,1.7c-0.1,0.1-0.1,0.2-0.1,0.3c0.6,2.2-0.6,4.5-2.8,5.1C6.1,20.8,5.9,20.8,5.6,20.9z
                                     	 M2.5,7.9c0.4,0.4,0.8,0.8,1.2,1.2C3.8,9,3.9,8.9,3.9,8.7c0-0.2,0-0.4,0.2-0.5c0.2-0.3,0.4-0.5,0.7-0.7c0.2-0.1,0.4-0.1,0.6-0.2
                                     	c0.1,0,0.2,0,0.2-0.1c0.2-0.2,0.4-0.4,0.6-0.6C7.3,7.9,8.5,9,9.6,10.2c0,0,0.1,0.2,0,0.2c0,0.1,0,0.2,0.1,0.3
                                     	c2.7,2.8,5.4,5.7,8.1,8.5c0,0,0.1,0.1,0.1,0.2c0.6-0.6,1.1-1.1,1.6-1.6c0,0,0,0-0.1-0.1c-2.9-2.7-5.7-5.5-8.6-8.2
                                     	c-0.1-0.1-0.2-0.1-0.3-0.1c-0.1,0-0.2,0-0.3-0.1c-1-1-2-2-3-3C7.2,6.3,7,6.1,6.9,5.9c0.2-0.2,0.4-0.4,0.6-0.6c0,0,0.1-0.1,0.1-0.2
                                     	C7.4,4.8,7.6,4.4,7.8,4.1c0.4-0.5,0.8-1,1.3-1.4c0.1,0,0.1-0.1,0.2-0.1c0,0-0.1,0-0.1,0c0,0-0.1,0-0.1,0C8.2,2.7,7.4,3,6.7,3.7
                                     	C6,4.5,5.2,5.2,4.4,6C4.3,6.1,4.2,6.2,4.3,6.4c0.1,0.5-0.9,1.4-1.4,1.3C2.7,7.7,2.6,7.8,2.5,7.9z M2.4,17.1C2.5,17,2.5,17,2.5,16.9
                                     	c0.4-0.4,0.7-0.7,1.1-1.1c0.4-0.4,1-0.4,1.5,0c0.3,0.3,0.7,0.7,1,1c0.5,0.5,0.5,1.1,0,1.5c-0.4,0.4-0.7,0.7-1.1,1.1
                                     	c0,0-0.1,0.1-0.2,0.2c0.1,0,0.2,0,0.3,0c1.2,0,2.4-0.7,2.8-1.9c0.3-0.8,0.2-1.6-0.2-2.4c-0.1-0.1-0.1-0.2,0.1-0.3
                                     	c0.7-0.7,1.4-1.4,2.1-2.1c0,0,0.1-0.1,0.1-0.2c-0.3-0.3-0.5-0.5-0.7-0.8c-0.8,0.8-1.6,1.6-2.4,2.4c-1.2-0.7-2.4-0.7-3.5,0.2
                                     	C2.6,15.2,2.3,16.1,2.4,17.1z M17.1,2.5C17.1,2.4,17.1,2.4,17.1,2.5C17,2.4,17,2.4,16.9,2.4c-1.2-0.1-2.4,0.7-2.8,1.8
                                     	c-0.3,0.8-0.3,1.5,0.1,2.3c0.2,0.4,0.2,0.4-0.1,0.7c-0.6,0.6-1.2,1.2-1.7,1.7c0,0-0.1,0.1-0.1,0.2c0.3,0.2,0.5,0.5,0.8,0.7
                                     	c0.1-0.1,0.1-0.1,0.2-0.2c0.6-0.6,1.1-1.1,1.7-1.7c0.3-0.3,0.3-0.3,0.8-0.1c1.5,0.7,3.3,0,3.9-1.6c0.2-0.4,0.2-0.8,0.2-1.3
                                     	C19.5,5,19.5,5,19.5,5.1c-0.4,0.4-0.7,0.7-1.1,1.1c-0.4,0.4-1,0.4-1.5,0c-0.3-0.3-0.7-0.7-1-1c-0.5-0.5-0.5-1,0-1.5
                                     	c0.4-0.4,0.7-0.7,1.1-1.1C17,2.5,17.1,2.5,17.1,2.5z"/>
                                    </svg>
                                </a>
                            </li>
                        </ul>
                    </div>
                	<a href="/project/{{ $p->id }}/edit" class="iax-edit" rel="{{ $p->id }}" title="Edit">
                        <svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
	 viewBox="0 0 22 22" enable-background="new 0 0 22 22" xml:space="preserve">
                            <path id="gray" fill="#58595B" d="M19.1,2.8c-0.7-0.7-1.6-1.2-2.4-1.2c-0.4,0-0.8,0.1-1.1,0.4l-1.3,1.3L3.8,13.8c-0.1,0.1-0.2,0.2-0.2,0.3
                            l-1.3,4.5c-0.1,0.3,0,0.5,0.2,0.7c0.1,0.1,0.3,0.2,0.5,0.2c0.1,0,0.1,0,0.2,0l4.5-1.3c0.1,0,0.2-0.1,0.3-0.2L18.6,7.5c0,0,0,0,0,0
                            l1.3-1.3C20.6,5.5,20.3,4,19.1,2.8z M16.6,3.1C16.6,3.1,16.7,3.1,16.6,3.1c0.3,0,0.9,0.2,1.4,0.7c0.6,0.6,0.8,1.2,0.7,1.4L18.1,6
                            l-2.1-2.1L16.6,3.1z M4.7,15.8l1.4,1.4l-2,0.6L4.7,15.8z M7.5,16.5l-2.1-2.1l9.5-9.5L17,7L7.5,16.5z"/>
                        </svg>
                    </a>
                    <a href="#" class="iax-delete" rel="{{ $p->id }}" title="Delete">
                        <svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
	 viewBox="0 0 22 22" enable-background="new 0 0 22 22" xml:space="preserve">
                            <path id="gray" fill="#58595B" d="M18.9,5.2h-3.4V4.3c0-1.4-1.1-2.6-2.6-2.6H9.5C8.1,1.8,7,2.9,7,4.3v0.9H3.5C3.1,5.2,2.7,5.5,2.7,6
                            c0,0.5,0.4,0.9,0.9,0.9h0.9v10.2c0,1.4,1.1,2.6,2.6,2.6h8.5c1.4,0,2.6-1.1,2.6-2.6V6.9h0.9c0.5,0,0.9-0.4,0.9-0.9
                            C19.8,5.5,19.4,5.2,18.9,5.2z M8.7,4.3c0-0.5,0.4-0.9,0.9-0.9h3.4c0.5,0,0.9,0.4,0.9,0.9v0.9H8.7V4.3z M16.3,17.1
                            c0,0.5-0.4,0.9-0.9,0.9H7c-0.5,0-0.9-0.4-0.9-0.9V6.9h10.2V17.1z"/>
                        </svg>

                    </a>
                    <a href="#" class="iax-copy" rel="{{ $p->id }}" title="Duplicate this project">
                        <svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
	 viewBox="0 0 22 22" enable-background="new 0 0 22 22" xml:space="preserve">
                            <path id="gray" fill="#58595B" d="M14.9,1.6h-1.2H8.6c-1,0-1.8,0.8-1.8,1.8v1.2H5.3c-1,0-1.8,0.8-1.8,1.8v11.3c0,1,0.8,1.8,1.8,1.8h8.8
                            c1,0,1.8-0.8,1.8-1.8v-1.2h1.5c1,0,1.8-0.8,1.8-1.8V5.9C19.3,5.9,14.9,1.6,14.9,1.6z M14.7,3.7l1.3,1.3l1.2,1.2h-2.4V3.7z
                             M14.3,17.7c0,0.1-0.1,0.2-0.2,0.2H5.3c-0.1,0-0.2-0.1-0.2-0.2V6.4c0-0.1,0.1-0.2,0.2-0.2h1.5v8.5c0,1,0.8,1.8,1.8,1.8h5.6V17.7z
                             M17.6,14.7c0,0.1-0.1,0.2-0.2,0.2H8.6c-0.1,0-0.2-0.1-0.2-0.2V3.4c0-0.1,0.1-0.2,0.2-0.2H13v4.5h4.6V14.7z"/>
                        </svg>
                    </a>
                    <a href="#" class="iax-traffic" rel="{{ $p->id }}" title="Set this project to traffic">
                        <svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
	 viewBox="0 0 22 22" enable-background="new 0 0 22 22" xml:space="preserve">
                            <path id="gray" fill="#58595B" d="M11,2.6c-4.6,0-8.4,3.8-8.4,8.4s3.8,8.4,8.4,8.4s8.4-3.8,8.4-8.4S15.6,2.6,11,2.6z M11,17.9
                        	c-3.8,0-6.9-3.1-6.9-6.9S7.2,4.1,11,4.1s6.9,3.1,6.9,6.9S14.8,17.9,11,17.9z M13,8.9l-2.7,2.7l-1.1-1.1c-0.3-0.3-0.8-0.3-1.1,0
                        	c-0.3,0.3-0.3,0.8,0,1.1l1.6,1.6c0.3,0.3,0.8,0.3,1.1,0l3.2-3.2c0.3-0.3,0.3-0.8,0-1.1C13.8,8.6,13.3,8.6,13,8.9z"/>
                        </svg>
                    </a>
                </div>
                @endif

            @endif

            @if( count($p->creatives) > 0 )
            <div class="iax-acc-button">
              <span class="accordion-toggle collapsed" data-toggle="collapse" data-target="#project-{{ $p->id }}" rel="{{ $p->id }}"><i class="fa fa fa-double-angle-down fa-2x"></i></span>
            </div>
            <div class="iax-acc-box collapse" id="project-{{ $p->id }}" >
                <div class="iax-box-inner">
                    <h5>Placements for {{ $p->name }}</h5>
                    <div class="iax-items">
                        <p>Loading <i class="fa fa-refresh fa-spin"></i></p>
                    </div>
                    <a href="#" class="btn btn-small btn-primary">New Placement</a>
                </div>
            </div>
            @endif
        </div>
    @endforeach
    @if( !$isClient  )
    	<div class="newProject_container">
    		<iframe id="newProject_inWin" src="#" width="500px" height="100%" frameborder="0" scrolling="no"></iframe>
        </div>
    @endif
@endif
@stop

@section('scripts')
@parent

<script type="text/javascript">
    IAX.Main.boot('CampaignDetail');

    if( document.location.href.indexOf('client') < 0){
        $('#iax-campaign-name').editable({
            type: 'text',
            pk: '{{ $campaign->id }}',
            url: '/api/v1/campaign/update',
            ajaxOptions: {
                type: "POST"
            },
            params: {
                _token: IAX.Main.meta.csrfToken
            },
            placement: 'bottom',
            title: 'Edit Campaign',
            success: function(response, newValue) {
                console.log("Campaign Edited");
            },
            error: function(response, newValue) {
                console.log("Error");
            }
        });
		// add editable class to project divs on non-client view
		if($(".iax-stack-item .controls")){
			$('.iax-stack-item').addClass('editable');
			$(".iax-stack-item .controls").show();
		}
		//add project to campaign in slide - mike added
		//var newProjectIframe = document.getElementById("newProject_inWin");
		var newProjectIframe = $('#newProject_inWin'); // or some other selector to get the iframe
		var newProjectContainer =$(".newProject_container");

		$(".addProjectToCampaign").on("click",function(e){
			e.preventDefault();
			var href = $(this).attr('href');
			newProjectIframe.attr("src", href);

			setTimeout(function(){
				$('#header', newProjectIframe.contents()).hide();
				$('#footer', newProjectIframe.contents()).hide();
				$("#iax-project-form .control-group:first-of-type", newProjectIframe.contents()).hide();
				$("#iax-project-form .control-group:nth-of-type(2)", newProjectIframe.contents()).hide();
				$('body', newProjectIframe.contents()).addClass("darkLayout");
				$('.container.iax-content', newProjectIframe.contents()).prepend("<span class='closeBtn'>Close</span>");
				newProjectContainer.addClass("active");
				$('.container .closeBtn', newProjectIframe.contents()).on("click",function(){
					newProjectContainer.removeClass("active");
				});
			},800);
		});

		//phone specific stuff
		if($( window ).width() < 400){
			//change text for client link
			$(".clientLink").find("a").text("Click Here");

			//prevent preview link launch on click so edit/delete will show up
            $(".iax-stack-item h5 a").on("click",function(e){
				e.preventDefault();
			});

		}

        //multi-project preview
        var width = $( window ).width(),
            margin = (width - 1170)/2,
            count = 1,
            previewContainer = $(".multiPreviewContainer"),
            existingPreviews = $(".row.previews");
        previewContainer.css({"width":width, "margin-left":-(margin), "padding-left": margin, "padding-right": margin});
        existingPreviews.css({"width":width, "margin-left":-(margin), "padding-left": margin, "padding-right": margin});

        $(".multiPreview").on("click", function(){
            previewContainer.toggleClass("active");
            var txt = previewContainer.hasClass("active") ? 'Hide Multi-Project Preview' : 'Create Multi-Project Preview';
            $(this).text(txt);
        });

        $(".row.previews h4").on("click", function(){
            $(this).parent().toggleClass("expand");
            if($(this).parent().hasClass("expand")){
                $(this).find("span").text("Close");
            }else{
                $(this).find("span").text("View All");
            }
        });

        //create new row
        /*previewContainer.find("select").on("change",function() {
            el = $(this);
            el_id = el.attr("id");
            if( el.find("option:selected")){
                if(el_id === "adTag"){
                    adTag = true;
                    console.log("adtag selected");
                }
                if(el_id === "project"){
                    project = true;
                    console.log("project selected");
                }
            }
            if(adTag && project){
                createRow();
            }
        });*/
        function createRow(){
            console.log("create new row");
            count++;
            var lastItem = previewContainer.find(".item:last-of-type");
            console.log(lastItem);
            lastItem.clone().insertAfter(lastItem).attr("id", "item" + count);
            adTag = false;
            project = false;
            removeItems();
        }
        previewContainer.find(".addRow").on("click",function(){
           createRow();
        });

        //remove row
        function removeItems(){
            previewContainer.find(".removeItem").on("click", function(){
                console.log("remove clicked");
                el = $(this).parent();
                el.remove();
            });
        }

        //form submit
        previewContainer.find(".createPreview").on("click",function(){
            console.log("create multi-project preview");
            var el = $(this);
            var campaignID = el.data("campaign_id");
            console.log("campaign id is", campaignID);
            //get name and URL
            var previewName = previewContainer.find("#name").val();
            console.log("preview Name is", previewName);
            if(previewName === ""){
                previewContainer.find("#name").focus();
                IAX.Utils.notify('Name for this preview is required', 'error');
                return false;
            }
            var previewURL = previewContainer.find("#URL").val();
            console.log("preview url is", previewURL);
            if(previewURL === ""){
                previewContainer.find("#URL").focus();
                IAX.Utils.notify('URL for this preview is required', 'error');
                return false;
            }
            //loop through projects and create array
            var items = previewContainer.find(".item");
            var itemArray = [];
            var count = 1;
            if(items.length > 1){
                items.each(function( index ) {
                    console.log("running through items");
                    var item = $(this);
                    //get adtag - removed adtags, get from project instead
                    //var adTagVal = item.find("#adTag option:selected").data("id");
                    //console.log("adTag is", adTagVal);
                    //get project id
                    var projectVal = item.find("#project option:selected").data("projectid");
                    console.log("project is", projectVal);
                    //push to variables
                    itemArray.push(projectVal);
                    //item1 = {"adTag": adTagVal, "project": projectVal};
                    count++;
                });
                /*if(!window["item3"]){
                    window["item3"] = "";
                }
                if(!window["item4"]){
                    window["item4"] = "";
                }
                console.log(item1, item2, item3, item4);*/
                console.log(itemArray);

                //post to server
                var previewData = {
                    campaign_id: campaignID,
                    name: previewName,
                    URL: previewURL,
                    /*item1: item1,
                    item2: item2,
                    item3: item3,
                    item4: item4,*/
                    itemArray: itemArray,
                    _token: IAX.Main.meta.csrfToken
                };

                var request = $.ajax({
                    url: IAX.Config.apiEndpoint + "/campaign/multiPreview",
                    type: "POST",
                    data: previewData
                });

                //if successful, return message
                request.done(function(data) {
                    if( data.status ){
                        IAX.Utils.notify('Multi-Project Preview successfully created!', 'success');
                        console.log("success, update previews", data.preview);
                        previewContainer.removeClass("active");
                        $(".multiPreview").text("Create Multi-Project Preview");
                        existingPreviews.show().addClass("expand"); //expand existing previews
                        existingPreviews.append('<div class="row"><span class="previewNum">New!</span><p><b>Name: </b>' + data.name + '</p><p><b>Scrape URL: </b>' + data.url + '</p><p><b>Preview URL: </b><a href="/client/campaign/preview/' + data.id + '" target="_blank">Click Here</a></p><p class="timestamp"><span><b>Updated at:</b> Just now</span></p><a href="#" class="iax-preview-delete" rel="' + data.id + '">Delete Preview</a></div>');
                    }
                });
                //if fail, return error
                request.fail(function(jqXHR, textStatus) {
                    IAX.Utils.notify('Something went wrong!', 'error');
                });
            }else{
                 IAX.Utils.notify('Only one project added. Please add minimum of 2', 'error');
            }
        });

        //delete preview
        $(".iax-preview-delete").on("click",function(){
            var el = $(this);
            var preview_id = el.attr('rel');
            console.log("preview id is", preview_id);
            //post to server
            var previewData = {
                preview_id: preview_id,
                _token: IAX.Main.meta.csrfToken
            };

            var request = $.ajax({
                url: IAX.Config.apiEndpoint + "/campaign/deletePreview",
                type: "POST",
                data: previewData
            });

            //if successful, return message
            request.done(function(data) {
                if( data.status ){
                    IAX.Utils.notify('Multi-Project Preview successfully deleted!', 'success');
                    el.parent().remove();
                }
            });
            //if fail, return error
            request.fail(function(jqXHR, textStatus) {
                IAX.Utils.notify('Something went wrong!', 'error');
            });
        });

        //show time info
        $(".iax-time").on("click",function(){
            $(this).parent().siblings(".timestamp").toggleClass("active");
        });
    }


</script>
@stop
