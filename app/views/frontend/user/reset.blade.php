@extends('layouts.master')
<!-- User login form goes here -->

@section('title')
Reset Password
@stop

@section('content')
<div class="container">
    @include('ui.notifications')
    <div class="row">
        <div class="span12">
            <div class="iax-login-form">
                <form class="iax-login-box clearfix" action="{{ url('reset') }}" method="post">
                    {{ Form::token(); }}
                    <h4>Password Reset Request</h4>
                    <p>Please enter the username you wish to reset .</p>
                    <div class="input-prepend">
                        <span class="add-on"><i class="fa fa-user"></i></span>
                        <input class="input-large" type="text" placeholder="Username" name="username">
                    </div>
                    <p class="pull-left iax-form-submit-subtext"><a href="/login" title="Return to Login">I remember now<br>Back to Login</a></p>
                    <p class="pull-right"><button class="btn btn-primary btn-small" title="Submit Reset Request" type="submit">Submit</button></p>
                </form>
            </div> <!-- ./iax-login-form -->
        </div>
    </div><!--/row-->
</div>
@stop