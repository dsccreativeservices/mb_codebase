@extends('layouts.master')
<!-- User login form goes here -->

@section('title')
User Profile
@stop

@section('content')
<div class="container">
    <div class="row">
        <div class="span3"></div>
        <div class="span6">
        <h3>Information</h3>
            <div class="iax-user-profile">
                <table class="table table-bordered table-condensed">
                    <tr>
                        <td>Username</td>
                        <td>{{ $user->username }}</td>
                    </tr
                    <tr>
                        <td>Email</td>
                        <td>{{ $user->email }}</td>
                    </tr>
                    <tr>
                        <td>First Name</td>
                        <td>{{ $user->first_name }}</td>
                    </tr>
                    <tr>
                        <td>Last Name</td>
                        <td>{{ $user->last_name }}</td>
                    </tr>
                    <tr>
                        <td>Last Login</td>
                        <td>{{ $user->last_login->diffForHumans() }}</td>
                    </tr>
                    <tr>
                        <td>User Group</td>
                        <td>{{ $group->name }}</td>
                    </tr>
                </table>
            </div> <!-- ./iax-login-form -->
        </div>
        <div class="span3"></div>
    </div><!--/row-->
    <div class="row">
    <div class="span3"></div>
        <div class="span6">
            <form class="iax-reset-box clearfix" action="{{ url('passchange') }}" method="post">
            <h3>Change Password</h3>
                {{ Form::token() }}
                <div class="input-prepend">
                    <span class="add-on"><i class="fa fa-lock"></i></span>
                    <input class="input-large" type="password" placeholder="New Password" name="password">
                </div>
                <div class="input-prepend">
                    <span class="add-on"><i class="fa fa-lock"></i></span>
                    <input class="input-large" type="password" placeholder="New Password Confirm" name="password_confirmation">
                </div>
                <p class="pull-right"><button class="btn btn-primary" type="submit">Submit</button></p>
            </form>
        </div>
        <div class="span3"></div>
    </div>
</div>
@stop