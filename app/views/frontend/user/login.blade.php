@extends('layouts.master')

@section('title')
User Login
@stop

@section('content')
<div class="container">
    <div class="row">
        <div class="span12">
            <div class="iax-login-form">
                <form class="iax-login-box clearfix" action="{{ url('login') }}" method="post">
                    {{ Form::token(); }}
                    <h4>Login to IAX</h4>
                    <div class="input-prepend">
                        <span class="add-on"><i class="fa fa-user"></i></span>
                        <input class="input-large" type="text" placeholder="Username" name="username">
                    </div>
                    <div class="input-prepend">
                        <span class="add-on"><i class="fa fa-lock"></i></span>
                        <input class="input-large" type="password" placeholder="Password" name="password">
                    </div>
                    <div class="checkbox">
                        <label name="remember"><small><input class="" type="checkbox" name="remember">Remember me?</small></label>
                    </div>
                    <p class="pull-left iax-form-submit-subtext"><a href="/reset">Forgot password?</a></p>
                    <p class="pull-right"><button class="btn btn-small btn-primary" type="submit">Login</button></p>
                </form>
            </div> <!-- ./iax-login-form -->
        </div>
    </div><!--/row-->
</div>
@stop

@section('scripts')
  @parent
  <!-- Add any page specific JS here -->
  <script>
  $('input[name=email]').focus();
  </script>
@stop