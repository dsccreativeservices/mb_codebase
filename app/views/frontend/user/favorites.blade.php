@extends('layouts.master')

@section('title')
Favorite Projects
@stop

@section('content')
<h3>Favorite Projects</h3>
@if ( count($projects) > 0 )
<table class="table table-striped table-condensed iax-table">
  <thead>
    <tr>
      <th>Project ID</th>
      <th>Ad Type</th>
      <th>Advertiser</th>
      <th>Campaign</th>
      <th>Project</th>
      <th>Site</th>
      <th>Created</th>
      <th>Modified</th>
      <th>
        <!-- <form action="/favorites" method="POST" class="iax-table-action">
          <input type="hidden" name="mode" value="remove_all">
          {{ Form::token() }}
          <button type="submit" title="Remove All"><i class="fa fa-times"></i></button>
        </form> -->
      </th>
    </tr>
  </thead>
  <tbody>
@foreach ( $projects as $p )
  <tr>
    <td>{{ $p->id }}</td>
    <td>{{ $p->adtype->name }}</td>
    <td><a href="/advertiser/{{ $p->campaign->advertiser->id }}">{{ $p->campaign->advertiser->name }}</a></td>
    <td><a href="/campaign/{{ $p->campaign->id }}" title="{{ $p->campaign->name }}">{{ $p->campaign->name }}</a></td>
    <td><a href="/project/{{ $p->id }}/edit">{{ $p->name }}</a></td>
    <td>{{ $p->site->name }}</td>
    <td>{{ $p->created_at }}</td>
    <td>{{ $p->updated_at }}</td>
    <td>
      <form action="/favorites" method="POST" class="iax-table-action">
        <input type="hidden" name="project_id" value="{{$p->id}}">
        <input type="hidden" name="mode" value="remove_single">
        {{ Form::token() }}
        <button data-toggle="tooltip" type="submit" title="Remove"><i class="fa fa-times"></i></button>
      </form>
    </td>
  </tr>
@endforeach
  </tbody>
</table>
@else
<p class="lead">You have not marked any favorite projects yet.</p>
@endif
@stop