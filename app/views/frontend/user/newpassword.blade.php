@extends('layouts.master')
<!-- User login form goes here -->

@section('title')
Reset Password
@stop

@section('content')
<div class="container">
    @include('ui.notifications')
    <div class="row">
        <div class="span12">
            <div class="iax-login-form">
                <form class="iax-reset-box clearfix" action="{{ url('doreset') }}" method="post">
                    {{ Form::token() }}
                    <h4>Password Reset</h4>
                    {{ Form::hidden('reset_code', $reset_code) }}
                    <div class="input-prepend">
                        <span class="add-on"><i class="fa fa-lock"></i></span>
                        <input class="input-large" type="password" placeholder="New Password" name="password">
                    </div>
                    <div class="input-prepend">
                        <span class="add-on"><i class="fa fa-lock"></i></span>
                        <input class="input-large" type="password" placeholder="New Password Confirm" name="password_confirmation">
                    </div>
                    <p class="pull-right"><button class="btn btn-primary" type="submit">Submit</button></p>
                </form>
            </div> <!-- ./iax-login-form -->
        </div>
    </div><!--/row-->
</div>
@stop