@extends('layouts.master')

@section('title')
Advertiser
@stop

@section('content')

<div class="row iax-advertiser-detail">
    <div class="span12">
        <ul class="iax-breadcrumbs breadcrumb">
            <li><a href="/advertisers">Advertisers</a> <span class="divider">/</span></li>
            <li><strong>{{ $advertiser->name }}</strong></li>
        </ul>
        <div class="iax-stack-hd">
            @if( $isClient  )
                <h4>{{ $advertiser->name }}</h4>
            @else
                <h4 id="iax-advertiser-name" style="display:inline;">{{ $advertiser->name }}</h4>
                <!--<h4>{{ $advertiser->name }} <small>Advertiser ID: {{ $advertiser->id }}</small></h4>-->
            @endif

        </div>
        @if( count($campaigns) < 1 )
        <p class="text-info alert alert-info">There are currently no campaigns associated with <strong> {{ $advertiser->name }}. </strong></p>
        @else
        <?php $count = 0; ?>
        @foreach ( $campaigns as $c )
            <?php
                $count++;
                $u = Sentry::getUserProvider()->findById($c->created_by);
            ?>
            @if( $count % 2 === 0 )
                <div class="iax-stack-item">
            @else
                <div class="iax-stack-item alt">
            @endif

            @if( $isClient  )
                <h5><a href="/client/campaign/{{ $c->id }}">{{ $c->name }}</a></h5>
            @else
            <h5><a href="/campaign/{{ $c->id }}">{{ $c->name }}</a></h5>
            <p>
                <small>Created: <strong>{{ $c->created_at->toDayDateTimeString() }}</strong> <!-- by <a href="/user/{{ $u->id }}">{{ $u->first_name}} {{ $u->last_name }}</a>--></small>
            </p>
            <p class="count">
            	{{ count($c->projects) }} Projects
            </p>
            <div class="controls" style="display:none">
           		<a href="/campaign/{{ $c->id }}" class="iax-edit">Edit</a>
                <a href="#" class="iax-delete" rel="{{ $c->id }}">Delete</a>
            </div>
            @endif
            </div>
        @endforeach
        @endif
    </div>
</div>
<div class="iax-pagination">
  <?php echo $campaigns->links(); ?>
</div>
@stop
@section('scripts')
    @parent
    <script type="text/javascript">
		 IAX.Main.boot('AdvertiserDetailPage');
		if( document.location.href.indexOf('client') < 0){
			<!-- add editable class to project divs on non-client view -->
			if($(".iax-stack-item .controls")){
				$('.iax-stack-item').addClass('editable');
				$(".iax-stack-item .controls").show();
			}

         $('#iax-advertiser-name').editable({
            type: 'text',
            pk: '{{ $advertiser->id }}',
            url: '/api/v1/advertiser/update',
            ajaxOptions: {
               type: "POST"
            },
            params: {
               _token: IAX.Main.meta.csrfToken
            },
            placement: 'bottom',
            title: 'Edit Advertiser Name',
            success: function(response, newValue) {
               console.log("Advertiser Edited");
            },
            error: function(response, newValue) {
               console.log("Error");
            }
         });
		};
	</script>
@stop
