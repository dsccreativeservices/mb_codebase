@extends('layouts.master')

@section('title')
Advertiser
@stop

@section('content')
    @if( isset($page_header) )
      <h3> {{ $page_header }} </h3>
    @endif


@if ( count($advertisers) < 1 )
    <p class="text-info">There are currently no advertisers in the system.</p>
@else
    <h4>All Advertisers <small>({{ count($advertisers) }})</small></h4>

    <?php $letters = str_split('ABCDEFGHIJKLMNOPQRSTUVWXYZ'); ?>
    <!-- Build Navigation -->
    <div class="iax-letter-nav pagination-centered pagination"><ul>
        @foreach( $letters as $l )
            @if( count($advertisers_alpha[$l]) > 0 )
                <li><a href="#pg-{{ $l }}">{{ $l }}</a></li>
            @else
                <li class="disabled"><a href="#">{{ $l }}</a></li>
            @endif
        @endforeach
    </ul></div>


    <!-- Build Listing -->
    <div class="iax-advertisers">
    @foreach( $letters as $l )
        @if( count($advertisers_alpha[$l]) > 0 )
            <a id="pg-{{ $l }}"></a>
            <h4>{{ $l }}</h4>
                @for ($i = 0; $i < count($advertisers_alpha[$l]); $i+=3)
                <div class="row">
                    <div class="span4">
                       <a href="/advertiser/{{ $advertisers_alpha[$l][$i]->id }}">{{ $advertisers_alpha[$l][$i]->name }}</a> <small class="muted">({{ count($advertisers_alpha[$l][$i]->campaigns) }})</small>
                    </div>
                    @if( $i < count($advertisers_alpha[$l]) - 1 )
                        <div class="span4">
                            <a href="/advertiser/{{ $advertisers_alpha[$l][$i+1]->id }}">{{ $advertisers_alpha[$l][$i+1]->name }}</a> <small class="muted">({{ count($advertisers_alpha[$l][$i+1]->campaigns) }})</small>
                        </div>
                    @endif
                    @if( $i < count($advertisers_alpha[$l]) - 2 )
                        <div class="span4">
                            <a href="/advertiser/{{ $advertisers_alpha[$l][$i+2]->id }}">{{ $advertisers_alpha[$l][$i+2]->name }}</a> <small class="muted">({{ count($advertisers_alpha[$l][$i+2]->campaigns) }})</small>
                        </div>
                    @endif
                </div>
                @endfor
        @endif
    @endforeach
    </div>

@endif
<span class="iax-to-top iax-fade"><i class="icon-double-angle-up"></i></span>
@stop

@section('scripts')
    @parent
    <script type="text/javascript">
        IAX.Main.boot('AdvertiserPage');
    </script>
@stop
