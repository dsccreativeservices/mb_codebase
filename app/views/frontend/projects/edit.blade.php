@extends('layouts.master')

@section('meta')
@parent
    <meta name="assetURL" content="/uploads/{{ $project->campaign->advertiser->id }}/{{ $project->campaign->id }}/{{ $project->id }}/">
    <meta name="project_id" content="{{ $project->id }}">
    <meta name="creative_id" content="{{ $creative->id }}">
    <meta name="creative_locked" content="{{ $creative->locked }}">
@stop

@section('content')

{{ HTML::style('assets/lightbox/css/lightbox.css') }}

<div class="row">
    <div class="span12">
    <ul class="iax-breadcrumbs breadcrumb">
      <li><a href="/advertiser/{{ $project->campaign->advertiser->id }}">{{ $project->campaign->advertiser->name }}</a> <span class="divider">/</span></li>
      <li><a href="/campaign/{{ $project->campaign->id }}">{{ $project->campaign->name }}</a> <span class="divider">/</span></li>
      <li>{{ $project->name }}</li>
    </ul>
        <h4>
        <!-- Edit Project -->
        <a href="/advertiser/{{ $project->campaign->advertiser->id }}">{{ $project->campaign->advertiser->name }}</a>
        	<span class="favorite">
                @if( $favorite )
                    <a class="iax-favorite" rel="{{ $project->id }}" title="Remove from favorites"><i class="fa fa-star"></i></a>
                @else
                    <a class="iax-favorite" rel="{{ $project->id }}" title="Add to favorites"><i class="fa fa-star-o"></i></a>
                @endif
            </span>
        </h4>
        <h5 style="margin-top:-5px"><a id="campaign" href="{{ URL::to('campaign', $project->campaign->id) }}" >{{ $project->campaign->name }} Campaign</a> | {{ $project->name }}</h5>
        <p class="createModInfo projectLevel userHover">
        	<?php
				$uCreated = Sentry::getUserProvider()->findById($project->created_by);
                if($project->updated_by != 0){
                    $uUpdated = Sentry::getUserProvider()->findById($project->updated_by);
                }else{
                    $uUpdated = Sentry::getUserProvider()->findById($project->created_by);
                }

			?>
            <small>Created: {{ $project->created_at->toDayDateTimeString() }}<span class="user">by {{ $uCreated->first_name}} {{ $uCreated->last_name }}</span></small>
            <small>Modified: {{ $project->updated_at->toDayDateTimeString() }}<span class="user">by {{ $uUpdated->first_name}} {{ $uUpdated->last_name }}</span></small>
        </p>


        <div class="iax-tags">
            <p class="iax-tags-wrap">
                <input class="iax-tag" style="display: none;" value="{{ implode($project->tags->lists('name'), ',') }}" />
            </p>
        </div> <!-- end .iax-tags -->

        @if(!$isViewer)
        <ul class="nav nav-tabs" id="iax-form-tab">
          <li class="active"><a href="#iax-client-info-tab" data-toggle="tab">General Info</a></li>
          <li class=""><a href="#iax-comp-upload-tab" data-toggle="tab">Flat Comps</a></li>
          <li class=""><a href="#iax-creative-tab" data-toggle="tab">Creative</a></li>
          <!--<li class=""><a href="#iax-comp-overview-tab" data-toggle="tab">Overview</a></li> removed since Overview tab included in general -->
          <li class="status" style="top:0px">
          	<!-- status indicator -->
            <!-- status text -->
            @if( $project->status === 'complete' )
                <p class="statusText">Traffic</p>
            @elseif( $creative->creative )
                <p class="statusText">Functional</p>
            @elseif( count($project->images) )
                <p class="statusText">Flat Comp</p>
            @else
                <p class="statusText">Empty</p>
            @endif
            <!-- blue blocks -->
            @if( count($project->images) )
                <?php $query ?>
                @if( false !== stripos($project->AdType->name, "Mobile"))
                    @if( false !== stripos($project->AdTag->name, 'App'))
                        <?php $query = "?mobile=true&inApp=true";  ?>
                    @else
                        <?php $query = "?mobile=true&inApp=false"; ?>
                    @endif
                @else
                    <?php $query = ""; ?>
                @endif
                <span onclick="window.open('/client/project/{{ $project->id }}/images{{$query}}','_blank')" style="background:#007fbf; border:0; cursor:pointer"></span>
            @else
                <span></span>
            @endif
            @if( $creative->creative )
                @if( $project->AdType->name == "Mobile")
                    <span onclick="window.open('{{ URL::to('preview/iframe', $project->creatives->first()->preview->preview_id) }}','_blank')" style="background:#007fbf; border:0; cursor:pointer"></span>
                @elseif( $project->AdType->name == "Tablet")
                    <span onclick="window.open('/preview/iframe/{{$project->creatives->first()->preview->preview_id}}?tablet=true','_blank')" style="background:#007fbf; border:0; cursor:pointer"></span>
                @else
                    <span onclick="window.open('{{ URL::to('preview/iframe', $project->creatives->first()->preview->preview_id) }}','_blank')" style="background:#007fbf; border:0; cursor:pointer"></span>
                @endif
            @else
                <span></span>
            @endif
            @if( $project->status === 'complete' )
                <span onclick="window.open('{{ URL::to('adops/traffic/project', $project->id) }}','_blank')" style="background:#007fbf; border:0; cursor:pointer"></span>
            @else
                <span></span>
            @endif
            <!-- end blue blocks -->
          </li>
        </ul>
        @endif
        <!-- Tab Panes -->
        <div id="iax-tab-group" class="tab-content">

            <!-- General Info Tab -->
            <div class="tab-pane active" id="iax-client-info-tab">
                <h4>Project Information</h4>

                <div class="row halfCol">
                    <div class="span5">
                        <div class="iax-form">
                             {{  Form::open(array(
                                 'class' => 'form-horizontal',
                                 'id' => 'iax-project-form'
                                 )) }}
                             {{ Form::hidden('user_id', Sentry::getUser()->id ) }}
                             {{ Form::hidden('project_id', $project->id ) }}
                             <fieldset>

                                <!-- Advertiser Select -->
                                 <div class="control-group">
                                     {{ Form::label('advertiser_id', 'Advertiser', array('class' => 'control-label')) }}
                                     <div class="controls">
                                         @if($isViewer)
                                            {{ Form::text('advertiser', $project->campaign->advertiser->name, array('disabled')) }}
                                         @else

                                            {{ Form::select('advertiser_id', array(), null, array('class' => 'span4 selectpicker', 'id' => 'advertiser_id', 'data-size' => 6, 'rel' => $project->campaign->advertiser->id)) }}
                                         @endif
                                     </div>
                                 </div>

                                 <!-- Campaign Select -->
                                 <div class="control-group">
                                     {{ Form::label('campaign_id', 'Campaign', array('class' => 'control-label')) }}
                                     <div class="controls">
                                         @if($isViewer)
                                             {{ Form::text('campaign', $project->campaign->name, array('disabled')) }}
                                         @else
                                            {{ Form::select('campaign_id', array(), null, array('class' => 'span4 selectpicker', 'disabled' => 'disabled', 'id' => 'campaign_id', 'data-size' => 6, 'rel' => $project->campaign->id)) }}
                                         @endif
                                     </div>
                                 </div>

                                 <!-- Project Name Text Input -->
                                 <div class="control-group">
                                     {{ Form::label('project_name', 'Project Name', array('class' => 'control-label')) }}
                                     <div class="controls">
                                         @if($isViewer)
                                            {{ Form::text('project_name', $project->name, array('disabled')) }}
                                         @else
                                            {{ Form::text('project_name', $project->name, array('class' => 'span4', 'placeholder' => 'eg: Pushdown')) }}
                                         @endif
                                     </div>
                                 </div>

                                <!-- Ad Type Select -->
                                 <div class="control-group">
                                     {{ Form::label('adtype_id', 'Ad Type', array('class' => 'control-label')) }}
                                     <div class="controls">
                                         @if($isViewer)
                                            <?php $adtype = AdType::find($project->adtype_id); ?>
                                            {{ Form::text('adtype', $adtype->name, array('disabled')) }}
                                         @else
                                            {{ Form::select('adtype_id', array(0 => 'Select Ad Type&hellip;') + $adtypes, $project->adtype_id, array('class' => 'span4 selectpicker', 'id' => 'adtype_id', 'data-size' => 6)) }}
                                         @endif
                                     </div>
                                 </div>

                                 <!-- Site Select -->
                                 <div class="control-group">
                                     {{ Form::label('site_id', 'Site', array('class' => 'control-label')) }}
                                     <div class="controls">
                                         @if($isViewer)
                                            <?php $site = Site::find($project->site_id); ?>
                                            {{ Form::text('site', $site->name, array('disabled')) }}
                                         @else
                                            {{ Form::select('site_id', $sites, $project->site_id, array('class' => 'span4 selectpicker', 'id' => 'site_id', 'data-size' => 6, 'rel' => $project->site_id)) }}
                                         @endif
                                     </div>
                                 </div>

                                <!-- Show adtag select -->
                                <div id="iax-adtag-select" class="control-group">
                                    {{ Form::label('adtag_id', 'Ad Tag', array('class' => 'control-label')) }}
                                    <div class="controls">
                                        @if($isViewer)
                                            <?php $adTag = AdTag::find($project->adtag_id); ?>
                                            {{ Form::text('site', $adTag->name, array('disabled')) }}
                                        @else
                                            {{ Form::select('adtag_id', array(0 => 'Select Ad Tag&hellip;') + $adtags, $project->adtag_id, array('class' => 'span4 selectpicker', 'id' => 'adtag_id', 'data-size' => 6, 'rel' => $project->adtag_id)) }}
                                        @endif
                                    </div>
                                </div>

                                <!-- Project Status Button Bar -->
                                @if(!$isViewer)
                                 <div class="control-group">
                                     {{ Form::label('project_name', 'Status', array('class' => 'control-label', 'rel' => $project->status, 'id' => 'status')) }}
                                     <div class="controls iax-status-bar">
                                         <div class="btn-group" data-toggle="buttons-radio" >
                                           <button type="button" data="internal" class="btn btn-mini btn-default active" rel="btn-primary" >Internal Only</button>
                                           <button type="button" data="external" class="btn btn-mini btn-default" rel="btn-primary">External Preview</button>
                                           <button type="button" data="complete" class="btn btn-mini btn-default" rel="btn-primary">Ready for Traffic</button>
                                         </div>
                                     </div>
                                 </div>
                                 @endif

                             </fieldset>
                        </div> <!-- end .iax-form -->
                        <div class="control-group">
                            @if(!$isViewer)
                                <a href="#" class="btn btn-primary pull-right" id="iax-project-tab1-update"><i class="fa fa-save"></i> Save</a>
                            @endif
                            {{ Form::close() }}
                        </div>
                    </div>

                </div>


                <!-- added overview tab info here -->
                <!--<div class="tab-pane form-horizontal" id="iax-comp-overview-tab">-->
                <div class="projectLinks halfCol">
                    <!--<h4>Project Overview</h4>
                    <h4><a id="advertiser" href="{{ URL::to('advertiser', $project->campaign->advertiser->id) }}" >{{ $project->campaign->advertiser->name }}</a></h4>
                    <h5>Campaign: <a id="campaign" href="{{ URL::to('campaign', $project->campaign->id) }}" >{{ $project->campaign->name }}</a></h5>-->
                    <div class="well">
                        <!-- if nothing exists -->
                        @if( $project->status != 'complete' && empty($creative->creative) && count($project->images) == 0)
                            <span class="emptyMessage">Links to Flat Comps, Functional Preview, and Traffic Code will appear here as content is added to this project.</span>
                        @endif

                        <!-- if flat comps -->

                        <?php $query; $previewType ?>
                        @if( false !== stripos($project->AdType->name, 'Mobile'))
                            @if( false !== stripos($project->AdTag->name, 'App'))
                                <?php $query = "?mobile=true&inApp=true"; $previewType = "[Mobile App]"; ?>
                            @else
                                <?php $query = "?mobile=true&inApp=false"; $previewType = "[Mobile Web]"; ?>
                            @endif
                        @else
                            <?php $query = ""; $previewType = "[Desktop]"; ?>
                        @endif
                        @if( count($project->images) )
                            <span style="padding:0 0 18px 0; border-bottom:solid 1px #B5B5B5; margin-bottom:20px;">
                                <b>Flat Comps:</b> <a class="flatCompURL" href="/client/project/{{ $project->id }}/images{{$query}}">View Flat Comps {{$previewType}}</a>
                            </span>
                        @else
                            <span class="compsPlaceholder" style="padding:0 0 18px 0; border-bottom:solid 1px #B5B5B5; margin-bottom:20px; display:none">
                                <b>Flat Comps:</b> <a class="flatCompURL" href="/client/project/{{ $project->id }}/images{{$query}}">View Flat Comps {{$previewType}}</a>
                            </span>
                        @endif

                         <!-- if mobile display only mobile preview link, else display both standalone and preview URL -->
                        @if( $project->AdType->name == "Mobile")
                            @if( isset($creative->preview->preview_id))
                                @if( false !== stripos($project->AdTag->name, 'App'))
                                    <?php $query = "?mobile=true&inApp=true"; $previewType = "[Mobile App]"; ?>
                                @else
                                    <?php $query = "?mobile=true&inApp=false"; $previewType = "[Mobile Web]"; ?>
                                @endif
                                    <b>Mobile Preview URL:</b> <a id="preview-url" href="{{ URL::to('preview/iframe', $creative->preview->preview_id) }}" target="_blank">Click Here</a>

                            @endif
                        @elseif( $project->AdType->name == "Tablet")
                            @if( isset($creative->preview->preview_id))
                                <b>Tablet Preview URL:</b> <a id="preview-url" href="/preview/iframe/{{$creative->preview->preview_id}}?tablet=true" target="_blank">Click Here</a>
                            @endif
                        @else
                            @if( $creative->preview )
                                <span>
                                    <b>Preview URL:</b>
                                    <a id="preview-url" href="{{ URL::to('preview/iframe', $creative->preview->preview_id) }}" target="_blank">Click Here</a>
                                </span>
                            @else
                                <span class="previewPlaceholder" style="display:none">
                                    <b>Preview URL:</b>
                                    <a id="preview-url" href="#" target="_blank">Click Here</a>
                                </span>
                            @endif
                            <!-- end added -->

                        @endif

                        @if( $project->status === 'complete' && !$isViewer )
                            <span style="margin-top:20px; border-top:solid 1px #B5B5B5; padding-top:18px">
                            <b>Traffic Link:</b>
                                <a id="iax-traffic-link" href="{{ URL::to('adops/traffic/project', $project->id) }}" target="_blank">{{ URL::to('adops/traffic/project', $project->id) }}</a>
                            </span>
                        @else
                            <span class="trafficPlaceholder" style="margin-top:20px; border-top:solid 1px #B5B5B5; padding-top:18px; display:none">
                                <b>Traffic Link:</b>
                                <a id="iax-traffic-link" href="{{ URL::to('adops/traffic/project', $project->id) }}" target="_blank">{{ URL::to('adops/traffic/project', $project->id) }}</a>
                            </span>
                        @endif
                    </div>
                </div>
            </div>

            @if(!$isViewer)
            <!-- Comp Image Upload Tab -->
            <div class="tab-pane form-horizontal" id="iax-comp-upload-tab">
                <h4>Manage Images</h4>
                <!--<div class="options">
                	<input type="checkbox" id="useMobile"> Mobile Preview
                </div>-->
                <div id="iax-images-loading">
                </div>
                <div class="iax-image-holder"></div>

                <!-- drag and drop image uploaded, mike added -->
                <style>
                    .control-group.roundNum{
                        margin-top: 40px;
                        margin-bottom: 15px;
                    }
                    .control-group.roundNum label{
                        width: 260px;
                        margin-right: 12px;
                        padding-top: 4px;
                    }
                    .control-group.roundNum input{
                        width: 50px;
                    }
                </style>
                <div class="control-group roundNum">
                    <label for="roundNum" class="control-label">Add Flat Comp to Round Number:</label>
                    <div class="controls">
                         <input class="span4" name="roundNum" type="number" value="1" id="round">
                    </div>
                </div>

                <div class="dropArea">
                    <p>Drag and Drop multiple files here to upload new images. (up to 10 files at once)</p>
                    <p><small>Note: Large files can take a long time to upload, so use the normal upload button if you want them to process quicker.</small></p>
                </div>

                <!-- SWF image uploader replaced by drag and drop -->
                <!--<style>
                    #file_upload{
                        float: right;
                        padding-right: 5px;
                        margin-top:10px;
                    }
                </style>
                <div id="iax-upload-image">
                    <div class="control-group">
                      <form action="">
                        <input type="file" name="file_upload" id="file_upload" />
                      </form>
                    </div>
                </div>-->
            </div>

            <!-- Creative Entry Tab -->
            <div class="tab-pane" id="iax-creative-tab">
                {{  Form::open(array(
                    'class' => 'form-stacked',
                    'id' => 'iax-project-form-creative'
                    )) }}
                {{ Form::hidden('user_id', Sentry::getUser()->id ) }}
                {{ Form::hidden('project_id', $project->id ) }}

                <div class="control-group previewURL">
                    {{ Form::label('project_url', 'Project URL', array('class' => 'control-label')) }}
                    <div class="controls">
                        @if($project->AdType->name == 'Snapchat')
                             {{ Form::text('project_url', 'Not Applicable', array('class' => 'span6', 'placeholder' => '', 'disabled')) }}
                        @elseif( !empty($creative->creative) )
                           {{ Form::text('project_url', $creative->url, array('class' => 'span6', 'placeholder' => 'Enter preview page url&hellip;')) }}
                        @else
                           <!-- PGI starter links -->
                           @if( $project->AdType->name == "Photo Gallery Interstitial" && $project->Site->code == "HGTV")
                                {{ Form::text('project_url', 'http://www.hgtv.com/design/rooms/kitchens/40-white-kitchens-that-are-anything-but-vanilla-pictures', array('class' => 'span6', 'placeholder' => '')) }}
                           @elseif( $project->AdType->name == "Photo Gallery Interstitial" && $project->Site->code == "FN")
                                {{ Form::text('project_url', 'http://www.foodnetwork.com/grilling/grilling-central-burgers-and-hot-dogs/best-burgers-and-hot-dogs0.html', array('class' => 'span6', 'placeholder' => '')) }}
                           @else
                                {{ Form::text('project_url', 'http://' . $project->site->host, array('class' => 'span6', 'placeholder' => 'Enter preview page url&hellip;')) }}
                           @endif
                        @endif
                    </div>
                </div>

                <!-- module builder launch -->
                @if(strpos($project->AdType->name,'Native') !== false || strpos($project->AdType->name,'RSI') !== false)
                    <style>
                        .control-group.previewURL{
                            display:inline-block;
                        }
                            .control-group.previewURL input{
                                width:785px;
                            }
                        #iax-project-launchBuilder, .launchContainer #iax-project-tab3-updateURL{
                            margin-top: -10px;
                        }
                        p.manual{
                            margin-top:35px
                        }
                    </style>
                    <div class="launchContainer" style="display: inline-block;">
                        <a href="#" class="btn btn-primary" id="iax-project-tab3-updateURL"><i class="fa fa-save"></i> Update Preview URL</a>
                        @if( $creative->preview )
                            <!-- creative exists -->
                            <a href="/preview/iframe/{{$creative->preview->preview_id}}?builder=true" class="btn btn-primary" id="iax-project-launchBuilder" target="_blank"><i class="fa fa-external-link"></i> Launch Module Builder</a>
                        @else
                            <a href="/preview/iframe/{{$previewID}}?builder=true" class="btn btn-primary" id="iax-project-launchBuilder" target="_blank"><i class="fa fa-external-link"></i> Launch Module Builder</a>
                        @endif

                    </div>
                    <p class="manual">Or create custom code below [ Click inside code editor to activate ]</p>
                    <style>
                        .control-group.manualCode{
                            opacity:0.3;
                            transition: opacity 0.5s;
                        }
                    </style>
                @endif
                @if(strpos($project->AdType->name,'Snapchat') !== false)
                   <p class="manual">Paste Video URLs (MP4 only, on scrippsonline) in the code box below</p>
                @endif
                   <div class="control-group manualCode">
                       <div class="row">
                           <div class="span2">
                           <label for="creative" class="control-label">Creative <i class="fa fa-exclamation" style="display: none;"></i></label>
                           </div>
                           <div class="span8 pull-right iax-editor-controls">
                               <span class="iax-editor-msg" style="display: none;"></span>
                               @if( $creative->locked )
                                 <a href="" class="btn btn-danger iax-editor-lock btn-small pull-right" data-toggle="tooltip" title="Lock Creative"><i class="fa fa-lock"></i></a>
                               @else
                                 <a href="" class="btn btn-default iax-editor-lock btn-small pull-right" data-toggle="tooltip" title="Unlock Creative"><i class="fa fa-unlock"></i></a>
                               @endif
                               <a href="" class="btn btn-default iax-editor-undo btn-small pull-right disabled" data-toggle="tooltip" title="Undo last Edit"><i class="fa fa-undo"></i></a>
                               <span class="cachebuster btn btn-small btn-default" style="width:auto">Insert DFP Cachebuster</span>
                               <span class="clickMacro btn btn-small btn-default" style="width:auto">Insert DFP Click Macro</span>
                           </div>
                       </div>
                       <div class="controls iax-code-wrap">
                           @if( $creative )
                               {{ Form::textarea('creative', $creative->creative, array('class' => 'span8', 'id' => 'iax-code-editor')) }}
                           @else
                               {{ Form::textarea('creative', '', array('class' => 'span8', 'id' => 'iax-code-editor')) }}
                           @endif
                       </div>
                       <div class="control-group" style="margin-top:20px">
                           <div class="controls">
                               <a href="#" class="btn btn-primary pull-right" id="iax-project-tab3-save"><i class="fa fa-save"></i> Save</a>
                           </div>
                       </div>
                   </div>

                {{ Form::close() }}


                <div class="codeValidationArea">
                    <div class="cssValidation"></div>
                    <div class="jsValidation"></div>
                </div>


            </div> <!-- end .iax-form -->
            @endif
            <!-- Overview Tab --- moved into General info tab -->


        </div> <!-- end #iax-tab-group -->
    </div>
</div>
@stop

@section('scripts')
    @parent
    <!-- Code Mirror -->
    {{ HTML::script('assets/code-mirror/codemirror.js') }}
    {{ HTML::script('assets/code-mirror/modes/css.js') }}
    {{ HTML::script('assets/code-mirror/modes/javascript.js') }}
    {{ HTML::script('assets/code-mirror/modes/xml.js') }}

    <!-- Other 3rd party libraries -->
    {{ HTML::script('assets/js/lib/dropzone.js') }}
    {{ HTML::script('assets/uploadify/jquery.uploadify.js') }}
    {{ HTML::script('assets/lightbox/js/lightbox-2.6.min.js') }}

    <!-- module builder -->
    <!--{{ HTML::script('assets/js/moduleBuilder/moduleBuilder_mod.js') }} -->

    <!-- Init Edit Project Page -->
    <script type="text/javascript">

        IAX.Main.boot('EditProjectPage');

        $('.iax-tag').tagsInput({
            height: 20,
            defaultText : 'Tag+',
            'minChars' : 2,
            'maxChars' : 20,
            onClickTag: function(t){
              // Load all records for that tag
              window.location = '/projects?tag=' + $(t).text().trim();
            },
            onAddTag: function(tag){
                $.ajax({
                    url: IAX.Config.apiEndpoint + '/tags/add',
                    type: 'POST',
                    global: false, // prevents global loading indicator
                    dataType: 'json',
                    data: {
                        'project_id' : IAX.Main.meta.projectId,
                        'tag_name' : tag,
                        '_token' : IAX.Main.meta.csrfToken
                    },
                    success: function(data, textStatus, xhr) {
                        IAX.Utils.notify('Tag added successfully!', 'success');
                        return true;
                    },
                    error: function(xhr, textStatus, errorThrown) {
                        return false;
                    }
                });
            },
            onRemoveTag: function(tag){
              $.ajax({
                  url: IAX.Config.apiEndpoint + '/tags/remove',
                  type: 'POST',
                  dataType: 'json',
                  global: false, // prevents global loading indicator
                  data: {
                      'project_id' : IAX.Main.meta.projectId,
                      'tag_name' : tag,
                      '_token' : IAX.Main.meta.csrfToken
                  },
                  success: function(data, textStatus, xhr) {
                      return true;
                  },
                  error: function(xhr, textStatus, errorThrown) {
                      return false;
                  }
              });
            }

        });

        $(".manualCode").on("click",function(){
            $(this).css("opacity",1);
        });

    </script>
@stop
