@extends('layouts.master')

@section('title')
All Projects
@stop

@section('content')
@include('ui.filter')
<?php
    parse_str($_SERVER['QUERY_STRING']);
?>

@if( Input::has('tag') )
<h4>Projects Tagged: <span class="label label-info">{{{ Input::get('tag') }}}</span></h4>
@else
<h4>All Projects</h4>
@endif

@if ( count($projects) > 0 )
<table class="table table-striped table-condensed iax-table">
  <thead>
    <tr>
      <!--<th>Project ID</th>-->
      <th>Ad Type</th>
      <th>Advertiser</th>
      <th>Campaign</th>
      <th>Project</th>
      <th>Site</th>
      <th>Created</th>
      <th>Modified</th>
      <th></th>
    </tr>
  </thead>
  <tbody>
@foreach ( $projects as $p )
  <tr>
    <!--<td>{{ $p->id }}</td>-->
    <td><a href="/filter?site=0&adType={{$p->adtype->id}}&advertiser=0">{{ $p->adtype->name or 'NOT SET'}}</a></td>
    <td><a href="/advertiser/{{ $p->campaign->advertiser->id or '#doesntExist' }}">{{ $p->campaign->advertiser->name or 'NOT SET' }}</a></td>
    <td><a href="/campaign/{{ $p->campaign->id or '#doesntExist' }}">{{ $p->campaign->name or 'NOT SET' }}</a></td>
    <td><a href="/project/{{ $p->id }}/edit">{{ $p->name }}</a></td>
    <td><a href="/filter?site={{$p->site->id}}&adType=0&advertiser=0">{{ $p->site->name }}</a></td>
    <td>{{ $p->created_at->diffforhumans() }}</td>
    <td>{{ $p->updated_at->diffforhumans() }}</td>
    @if( isset($p->creatives->first()->preview->preview_id) )
      <td><a href="/preview/iframe/{{ $p->creatives->first()->preview->preview_id }}" target="_blank"><i class="fa fa-search"></i></a></td>
    @else
      <td></td>
    @endif
  </tr>
@endforeach
  </tbody>
</table>
<div class="iax-pagination">
  <?php echo $projects->links(); ?>
</div>

@else
<span class="alert alert-info">There are no projects matching the filters you have selected. </span>
@endif
@stop

@section('scripts')
  @parent
  <script type="text/javascript">
      //change pagination links
      var pagination = $(".pagination"),
          pagLinks = pagination.find("a"),
          site = "<?php if(isset($site)){ echo $site;} ?>",
          adType = "<?php if(isset($adType)){ echo $adType;} ?>",
          advertiser = "<?php if(isset($advertiser)){ echo $advertiser;} ?>";
      pagLinks.each(function(){
          var el = $(this);
          var href = el.attr("href");
          var newHref = href + "&site=" + site + "&adType=" + adType + "&advertiser=" + advertiser;
          el.attr("href",newHref);
      });
  </script>
@stop
