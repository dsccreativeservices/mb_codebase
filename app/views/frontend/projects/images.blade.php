@extends('layouts.master')

@section('meta')
@parent
    <meta name="viewport" content="width=device-width; initial-scale=1.0; maximum-scale=1.0; user-scalable=0;">
@stop

@section('title')
Images for Project
@stop

@section('content')

<?php
	parse_str($_SERVER['QUERY_STRING']);
    if(!isset($inApp)){ $inApp = "false"; };
?>

@if( !$isClient )
<a class="" href="/project/{{ $images->first()->project_id }}/edit">Back to project</a>
@endif

<style>
	.container, .container .row .span12{
		width:100%;
	}
    .iax-content{
        display:none;
    }
    body{
        padding-bottom: 100px !important;
    }

    @media (min-width: 670px){
        .imageThumbnails{
            position: fixed;
            top: 0;
            left: 0;
            width: 8vw;
            height: calc(100vh - 124px);
            padding: 62px 10px;
            background: #f1f1f1;
            overflow-y: scroll;
        }
            .imageThumb{
                border: solid 1px #DDD;
                border-radius: 5px;
                padding: 10px;
                background: white;
                cursor: pointer;
                transition: transform 0.3s;
            }
                .imageThumb:hover{
                    transform: scale(1.05);
                }
                .imageThumb.active{
                    border: solid 3px #006AA2;
                }
                .imageThumb + .imageThumb{
                    margin-top: 10px;
                }
                .imageThumb p{
                    margin-top: 5px;
                    margin-bottom: 0;
                    font-size: 11px;
                    font-weight: bold;
                    line-height: 12px;
                    text-align: center;
                }

                .imageThumbCrop{
                    max-height: 4.55vw;
                    overflow: hidden;
                    text-align: center;
                }
                    .imageThumbCrop img{
                        max-height:100%;
                    }
        .imageWrapper{
            padding-left: calc(10vw + 15px);
            padding-right: 22px;
        }
    }
    @media (max-width: 1024px){
        .imageThumb p{
            display:none;
        }
    }

</style>

@if(!empty($mobile))
	<link rel="stylesheet" href="{{ asset('assets/css/mobilePreview/flexslider.css') }}" type="text/css" media="screen" />
	<link rel="stylesheet" href="{{ asset('assets/css/mobilePreview/mobileStatic.css') }}" type="text/css" media="screen" />
    <div class="phoneWrapper">
        <img src="{{ asset('assets/img/mobilePreview/phone.png') }}" />
    </div>
    <div class="header">
        <img class="infoBar" src="{{ asset('assets/img/mobilePreview/safariHeader.png') }}" width="320" />
        <div class="urlBar">
            <div class="backgroundColor"></div>
            <img class="textOnly" src="{{ asset('assets/img/mobilePreview/safariHeader_textOnlyButton.png') }}" width="19" />
            <span class="url">iax.scrippsonline.com</span>
            <img class="refresh" src="{{ asset('assets/img/mobilePreview/safariHeader_refreshButton.png') }}" width="19.5" />
        </div>
    </div>
    <div class="footer">
        <div class="footerInner">
            <img src="{{ asset('assets/img/mobilePreview/safariFooter.png') }}" width="320" />
        </div>
    </div>
    <div class="flexslider carousel">
      <ul class="slides">
        @if( count($images) )
      		@foreach($images as $img)
            	<li><img src="/{{ $img->path }}"></li>
            @endforeach
   		@else
      		<span class="alert alert-info">No images to display</span>
    	@endif
      </ul>
    </div>
    <div class="alertBox">
        <div class="innerAlert">
            <h3>Swipe</h3>
            <p>Swipe left or right to view more comp images</p>
            <img src="{{ asset('assets/img/mobilePreview/prev_arrow_mobile.png') }}" width="13" />
            <img src="{{ asset('assets/img/mobilePreview/next_arrow_mobile.png') }}" width="13" />
            <button class="close">Close</button>
            <button class="closeSetCookie">Don't Show This Again</button>
        </div>
    </div>

@else
    @if( count($images) )
      <?php if(isset($round)){ ?>
           <h3 style="text-align:center; margin-bottom: 50px; padding-bottom: 5px; border-bottom: solid 1px #e2e2e2;">Round {{ $roundNum }} Images</h3>
      <?php }; ?>

      <div class="flex">

          <!-- thumbnails -->
          <div class="imageThumbnails">
              @foreach($images as $key => $value)
                @if($key === 0)
                <div class="imageThumb active" data-id="{{ $value->id }}">
                @else
                <div class="imageThumb" data-id="{{ $value->id }}">
                @endif
                    <div class="imageThumbCrop">
                        <img src="/{{ $value->path }}">
                    </div>
                    @if( !empty($value->title) )
                        <p>{{ $value->title }}</p>
                    @endif
                </div>
              @endforeach
          </div>

          <!-- images -->
          <div class="imageWrapper">
              @foreach($images as $img)
                  <div class="iax-image @if(Sentry::check())dragArea@endif" data-id="{{ $img->id }}">
                    <h3>{{ $img->title }}</h3>
                    @if( $img->description)
                        <p class="comments" style="margin-top:0px">
                            {{ $img->description}}  <br>
                            <a href="{{ url($img->path) }}" target="_blank">{{ url($img->path) }}</a>
                        </p>
                    @endif
                    <img class="img-polaroid" src="/{{ $img->path }}">

                    <!-- notes -->
                    <!--<div class="notesWrap">
                        !-- current notes --
                        @if( $img->notes)
                            <ul class="notes">
                                {{ $img->notes }}
                            </ul>
                        @endif
                        !-- add new note --
                        <a href="#" class="btn btn-primary" id="iax-image-addNote">Add a comment to this image</a>
                    </div>
                    -->
                    <!-- END notes -->
                  </div>

              @endforeach
          </div>
    </div>

    @else
      <span class="alert alert-info">You have not worked on any projects yet.</span>
    @endif

@endif

@section('scripts')
    @parent
    @if(!empty($mobile))
    <script src="{{ asset('assets/js/lib/mobilePreview/jquery.flexslider.js') }}"></script>
	<script src="{{ asset('assets/js/lib/mobilePreview/jquery.easing.js') }}"></script>
    <script src="{{ asset('assets/js/lib/mobilePreview/jquery.mousewheel.js') }}"></script>
    <script src="{{ asset('assets/js/lib/mobilePreview/jquery.cookie.js') }}"></script>
    <script src="{{ asset('assets/js/lib/mobilePreview/functions.js') }}"></script>
    @endif
    <script type="text/javascript">
		$(function(){
            if(<?php echo $inApp; ?>){
                if(<?php echo $inApp; ?> === true){
                    console.log("in app view");
                    $(".header, .footer, .carousel").addClass("inApp");
                }else{
                    console.log("in browser view");
                }
            }

            //if no user meta user_id will be empty, thus disable

            if($(".iax-image").hasClass("dragArea")){
                var holder = $(".iax-image");

                holder.each(function( index ) {
                    var el = $(this);
                    console.log(el);
                    el.on("dragover",function(e){
                        if(!el.hasClass("hover")){
                            el.addClass("hover");
                        }
                        e.preventDefault();
                    });
                    el.on("dragleave",function(e){
                        el.removeClass("hover");
                        e.preventDefault();
                    });
                    el.on("dragend",function(e){
                        el.removeClass("hover");
                        e.preventDefault();
                    });
                    el.on("drop",function(e){
                        el.removeClass("hover");
                        e.preventDefault();
                        var image = "";
                        var name = "";
                        var imageID = el.data("id");

                        //var file = e.dataTransfer.files[0],
                        var file = e.originalEvent.dataTransfer.files[0];
                        name = file.name;
                        reader = new FileReader();

                        reader.onload = function (event) {
                            image = event.target.result;
                            sendUpdate(image, name);
                        };
                        reader.readAsDataURL(file);

                        function sendUpdate(newImage, name){
                            var updateData = {
                                id: imageID,
                                newImg: newImage,
                                name: name,
                                _token: IAX.Main.meta.csrfToken
                            };

                            var request = $.ajax({
                                url: IAX.Config.apiEndpoint + "/projects/image/updateImg",
                                type: "POST",
                                data: updateData
                            });

                            //if successful, return message
                            request.done(function(data) {
                                if( data.status ){
                                    IAX.Utils.notify('Image updated successfully!', 'success');
                                    console.log("success, set image", data.image);
                                    var newSrc = "/" + data.image;
                                    el.find("img").attr("src", newSrc);
                                }
                            });
                            //if fail, return error
                            request.fail(function(jqXHR, textStatus) {
                                //error message
                            });
                        }
                        return false;
                    });
                });
            }


            //check visiblity of item
            function checkVisibility(item, margin, amount){
                if(typeof margin === 'undefined'){
                    margin = 0;
                }
                if(typeof amount === 'undefined'){
                    amount = "top";
                }
                var itemTop = item.offset().top,
                    itemBottom = itemTop + item.outerHeight(),
                    scrollDist = $(window).scrollTop(),
                    winHeight = $(window).height();

                //console.debug(itemTop, scrollDist, itemBottom);
                if(amount === "top"){
                    if(scrollDist > (itemTop + margin)){
                        return true;
                    }else{
                        return false;
                    }
                }else if(amount === "bottom"){
                    if((scrollDist + winHeight) > (itemBottom + margin)){
                        return true;
                    }else{
                        return false;
                    }
                }else if(amount === "any"){
                    //any part of the element is on screen
                    if(scrollDist > itemTop && scrollDist < itemBottom){
                        return true;
                    }else{
                        return false;
                    }
                }else if(amount === "full"){
                    //full element is visible on screen
                    if((scrollDist + winHeight) > itemTop && scrollDist < itemBottom){
                        return true;
                    }else{
                        return false;
                    }
                }
            }


            //thumbnails
            $(".imageThumb").on("click",function(){
                var thumb = $(this),
                    id = thumb.data("id"),
                    mainImg = $(".iax-image[data-id='" + id + "']"),
                    top = mainImg.offset().top - 62, //62 is height of nav
                    currentTop = $(window).scrollTop(),
                    time = Math.abs(( currentTop - top ) / 3);

                console.log(time);

                $("html, body")
                    .stop()
                    .animate({
                        scrollTop: top
                    }, time, 'swing', function() {
                        thumb.addClass("active").siblings().removeClass("active");
                    });
            });

            function scroll(){
                $(".iax-image").each(function(){
                    var img = $(this),
                        id = $(this).data("id");

                    if( checkVisibility(img, 0, "any") === true ){
                        var thumb = $(".imageThumbnails").find("[data-id='" + id + "']").addClass("active").siblings().removeClass("active");
                    }
                });
            }

            var scrollThrottle = false;
            $(window).on("scroll",function(){
                if(scrollThrottle === false){
                    scrollThrottle = true;
                    scroll();
                    setTimeout(function(){
                        scrollThrottle = false;
                    },200);
                }
            });



		});
	</script>
@stop
