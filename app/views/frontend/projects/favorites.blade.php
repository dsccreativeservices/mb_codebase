@extends('layouts.master')

@section('title')
Favorite Projects
@stop

@section('content')
@include('ui.filter')

<h4>Favorite Projects</h4>

@if ( count($projects) > 0 )
<table class="table table-striped table-condensed iax-table">
    <thead>
        <tr>
            <!--<th>Project ID</th>-->
            <th>Ad Type</th>
            <th>Advertiser</th>
            <th>Campaign</th>
            <th>Project</th>
            <th>Site</th>
            <th>Created</th>
            <th>Modified</th>
            <th></th>
            <th></th>
        </tr>
    </thead>
    <tbody>
    @foreach ( $projects as $p )
        <tr>
            <!--<td>{{ $p->id }}</td>-->
            <td><a href="/filter?site=0&adType={{$p->adtype->id}}&advertiser=0">{{ $p->adtype->name or 'NOT SET'}}</a></td>
            <td><a href="/advertiser/{{ $p->campaign->advertiser->id or '#doesntExist' }}">{{ $p->campaign->advertiser->name or 'NOT SET' }}</a></td>
            <td><a href="/campaign/{{ $p->campaign->id or '#doesntExist' }}">{{ $p->campaign->name or 'NOT SET' }}</a></td>
            <td><a href="/project/{{ $p->id }}/edit">{{ $p->name }}</a></td>
            <td><a href="/filter?site={{$p->site->id}}&adType=0&advertiser=0">{{ $p->site->name }}</a></td>
            <td>{{ $p->created_at->diffforhumans() }}</td>
            <td>{{ $p->updated_at->diffforhumans() }}</td>
            <!-- preview project -->
            @if( isset($p->creatives->first()->preview->preview_id) )
                <td><a href="/preview/iframe/{{ $p->creatives->first()->preview->preview_id }}" target="_blank"><i class="fa fa-search"></i></a></td>
            @else
                <td></td>
            @endif
            <!-- remove project -->
            <td>
              <form action="/favorites" method="POST" class="iax-table-action">
                <input type="hidden" name="project_id" value="{{$p->id}}">
                <input type="hidden" name="mode" value="remove_single">
                {{ Form::token() }}
                <button data-toggle="tooltip" type="submit" title="Remove"><i class="fa fa-times"></i></button>
              </form>
            </td>
        </tr>
    @endforeach
    </tbody>
</table>
<div class="iax-pagination">
  <?php echo $projects->links(); ?>
</div>
@else
<p class="lead">You have not marked any favorite projects yet.</p>
@endif
@stop
