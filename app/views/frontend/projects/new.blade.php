@extends('layouts.master')

@section('content')
<?php 
	parse_str($_SERVER['QUERY_STRING']);
?>

<div class="row">
    <div class="span10 offset1">
        <h3>Create New Project</h3>
        <p class="">Use the form below to create a new project. Once created, you will be able to enter ad creative, manage image uploads, and see previews.</p>
        <!-- Tab Panes -->
        <div id="iax-tab-group" class="tab-content">

            <!-- General Info Tab -->
            <div class="tab-pane active" id="iax-client-info-tab">
                <div class="row">
             <div class="span5">
             <div class="iax-form">
                     {{  Form::open(array(
                         'action' => 'ProjectController@post_new',
                         'class' => 'form-horizontal',
                         'id' => 'iax-project-form'
                         )) }}
                     {{ Form::hidden('user_id', Sentry::getUser()->id ) }}
                     <fieldset>

                        <!-- Advertiser Select -->
                         <div class="control-group">
                             {{ Form::label('advertiser_id', 'Advertiser', array('class' => 'control-label')) }}
                             <div class="controls">
                             	@if( isset($a))
                                	{{ Form::text('advertiser_id', null, array('class' => 'span4', 'placeholder' => "advertiser automatically added")) }}
                                @else
                                	{{ Form::select('advertiser_id', $advertisers, null, array('class' => 'span4 selectpicker', 'id' => 'advertiser_id', 'data-size' => 6)) }}
                                @endif
                             </div>
                         </div>

                         <!-- Campaign Select -->
                         <div class="control-group">
                             {{ Form::label('campaign_id', 'Campaign', array('class' => 'control-label')) }}
                             <div class="controls">
                             	@if( isset($c))
                                    {{ Form::text('campaign_id', null, array('class' => 'span4', 'placeholder' => "campaign automatically added")) }}
                                @else
                                	{{ Form::select('campaign_id', array(), null, array('class' => 'span4 selectpicker', 'disabled' => 'disabled', 'id' => 'campaign_id', 'data-size' => 6)) }}
                                @endif
                             </div>
                         </div>

                         <!-- Project name -->
                         <div class="control-group">
                             {{ Form::label('project_name', 'Project Name', array('class' => 'control-label')) }}
                             <div class="controls">
                                 {{ Form::text('project_name', null, array('class' => 'span4', 'placeholder' => 'eg: Pushdown')) }}
                             </div>
                         </div>

                         <!-- Ad Type select -->
                         <div class="control-group">
                             {{ Form::label('adtype_id', 'Ad Type', array('class' => 'control-label')) }}
                             <div class="controls">
                                 {{ Form::select('adtype', $adtypes, null, array('class' => 'span4 selectpicker', 'id' => 'adtype_id', 'data-size' => 6)) }}
                             </div>
                         </div>

                         <!-- site select -->
                         <div class="control-group">
                             {{ Form::label('site_id', 'Site', array('class' => 'control-label')) }}
                             <div class="controls">
                                 {{ Form::select('site_id', $sites, null, array('class' => 'span4 selectpicker', 'id' => 'site_id', 'data-size' => 6)) }}
                             </div>
                         </div>

                         <!-- adtag select -->
                         <div id="iax-adtag-select" class="control-group">
                             {{ Form::label('adtag_id', 'Ad Tag', array('class' => 'control-label')) }}
                             <div class="controls">
                                 {{ Form::select('adtag_id', array(0 => 'Select Ad Tag&hellip;') + $adtags, 0, array('class' => 'span4 selectpicker', 'disabled' => 'disabled', 'id' => 'adtag_id', 'data-size' => 6 )) }}
                             </div>
                         </div>

                          <div class="control-group">
                              {{ Form::label('project_name', 'Status', array('class' => 'control-label', 'rel' => 'internal', 'id' => 'status')) }}
                              <div class="controls iax-status-bar">
                                  <div class="btn-group" data-toggle="buttons-radio" >
                                    <button type="button" data="internal" class="btn btn-mini btn-default active" rel="btn-primary" >Internal Only</button>
                                    <button type="button" data="external" class="btn btn-mini btn-default" rel="btn-primary">External Preview</button>
                                    <button type="button" data="complete" class="btn btn-mini btn-default" rel="btn-primary">Ready for Traffic</button>
                                  </div>
                              </div>
                          </div>

                         <div class="control-group">
                             <a href="#" class="btn btn-primary pull-right" id="iax-project-tab1-save">Create Project</a>
                         </div>
                     </fieldset>

                     {{ Form::close() }}
                    </div>
                </div>
            </div>
        </div>

        </div> <!-- end #iax-tab-group -->

    </div>
</div>
<!-- Set CSRF token. Any dynamic forms will use this -->
<form style="" id="iax-csrf-token">{{ Form::token() }}</form>
@stop

@section('scripts')
    @parent
    <!-- Code Mirror -->
    {{ HTML::script('assets/code-mirror/codemirror.js') }}
    {{ HTML::script('assets/code-mirror/modes/css.js') }}
    {{ HTML::script('assets/code-mirror/modes/javascript.js') }}
    {{ HTML::script('assets/code-mirror/modes/xml.js') }}

    <!-- Other 3rd party libraries -->
    {{ HTML::script('assets/js/lib/dropzone.js') }}

    <!-- IAX core files -->
    <script type="text/javascript">
        IAX.Main.boot('NewProjectPage');
    </script>
    
    <script type="text/javascript">
       $(function(){
		  // set advertiser and campaign if available
		  var advertiser = "<?php if(isset($a)){ echo $a;} ?>";
		  var campaign = "<?php if(isset($c)){ echo $c;} ?>";
		  if(advertiser && campaign){
			  setTimeout(function(){
				  $("#advertiser_id").val(advertiser);
				  setTimeout(function(){
					  $("#campaign_id").val(campaign);
				  },300);
			  },300);
			  $('#iax-project-tab1-save').on("click",function(e) {
				  console.log("form submitted");
				  parent.window.$('.newProject_container').removeClass("active");
				  setTimeout(function(){
					  parent.window.location.reload();
				  },500);
			  });
		  }
	   });
    </script>

@stop