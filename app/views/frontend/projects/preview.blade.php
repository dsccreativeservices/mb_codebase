@extends('layouts.master')

@section('title')
Advertiser
@stop

@section('content')
<div class="container">
  <div class="row">
    @if( isset($page_header) )
    <h3> {{ $page_header }} </h3>
    @endif
    {{ HTML::style('assets/css/prism.css') }}

    <h4>Preview Test URL: <a href="{{ $preview_url }}" target="_blank">{{ $preview_url }}</a></h4>

    <h5>Raw Source</h5>
    <pre><code class="language-markup iax-code-view">{{{ $raw }}}</code></pre>
    <h5>Tokenized Source</h5>
    <pre><code class="language-markup iax-code-view">{{{ $tokenized }}}</code></pre>

    {{ HTML::script('assets/js/prism.js') }}
  </div>
</div>
@stop
