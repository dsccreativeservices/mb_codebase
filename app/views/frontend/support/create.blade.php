@extends('layouts.master')
<!-- User login form goes here -->

@section('title')
Create s Support Ticket
@stop

@section('content')
<div class="container">
    <div class="row">
        <div class="span8 offset2">
            <h3>Create a Support Ticket</h3>
            <p class="text-lead">Fill out the form below to submit a support ticket. A link with the support ticket will be emailed to you.</p>
            {{  Form::open(array(
                'action' => 'SupportController@post_create',
                'class' => 'iax-login-box form-horizontal'
                )) }}

            <div class="control-group">
            {{ Form::label('ticketName', 'Issue Title:', array('for' => 'ticketName', 'class' => 'control-label')) }}
            <div class="controls">{{ Form::text('ticketName') }}</div>
            </div>

            <div class="control-group">
                {{ Form::label('ticketType', 'Issue Type: ', array('for' => 'ticketType', 'class' => 'control-label')) }}
                <div class="controls">
                    {{ Form::select('ticketType', array(
                        '' => 'Select&hellip;',
                        'bug' => 'Application Bug',
                        'feature' => 'Feature Request',
                        'comment' => 'Comment',
                        'other' => 'Other'
                    )) }}
                </div>
            </div>

            <div class="control-group">
                {{ Form::label('ticketSeverity', 'Issue Severity: ', array('for' => 'ticketSeverity', 'class' => 'control-label')) }}
                <div class="controls">
                    {{ Form::select('ticketSeverity', array(
                        '' => 'Select&hellip;',
                        'low' => 'Low',
                        'medium' => 'Medium',
                        'high' => 'High'
                    )) }}
                </div>
            </div>

            <div class="control-group">
            {{ Form::label('ticketBody', 'Body: ', array('for' => 'ticketBody', 'class' => 'control-label')) }}
            <div class="controls">
                {{ Form::textarea('ticketBody', null, array('class' => 'span6') ) }}
            </div>
            </div>

            <div class="control-group">
                <div class="controls">
                    {{ Form::submit('Submit Ticket', array('class' => 'btn btn-primary'))}}
                </div>
            </div>


            {{ Form::close() }}
        </div>
    </div><!--/row-->
</div>
@stop