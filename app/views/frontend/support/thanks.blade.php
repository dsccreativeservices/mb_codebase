@extends('layouts.master')
<!-- User login form goes here -->

@section('title')
Create s Support Ticket
@stop

@section('content')
<div class="container">
    <div class="row">
        <div class="span8 offset2">
            <h3>Thanks for your feedback!</h3>
            <p class="text-lead">Your ticket has been submitted.</p>
            <a href="/" class="btn btn-small btn-primary"><i class="fa fa-arrow-left"></i> Return Home</a>
        </div>
    </div><!--/row-->
</div>
@stop