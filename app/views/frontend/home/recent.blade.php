@extends('layouts.master')

@section('title')
Main
@stop

@section('content')
<h4>Your Recent Projects</h4>
  @if( count($projects) > 0 )
  <?php $count = 0; ?>
  <table class="table table-striped table-condensed iax-table">
    <thead>
      <tr>
        <th>Project ID</th>
        <th>Ad Type</th>
        <th>Advertiser</th>
        <th>Campaign</th>
        <th>Title</th>
        <th>Site</th>
        <th>Created</th>
        <th>Modified</th>
      </tr>
    </thead>
    <tbody>
  @foreach ( $projects as $p )
    <tr>
      <td>{{ $p->id }}</td>
      <td>{{ $p->adtype->name }}</td>
      <td><a href="">Advertiser</a></td>
      <td><a href="">{{ $p->campaign->name }}</a></td>
      <td><a href="">{{ $p->name }}</a></td>
      <td>{{ $p->site->name }}</td>
      <td>{{ $p->created_at }}</td>
      <td>{{ $p->updated_at }}</td>
    </tr>
  @endforeach
    </tbody>
  </table>
  @else
  <span class="alert alert-info">You have not worked on any projects yet.</span>
  @endif
  </div>
@stop

@section('scripts')
  @parent
  <script type="text/javascript">
      IAX.Main.boot('HomePage');
  </script>
@stop