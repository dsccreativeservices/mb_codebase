@extends('layouts.master')

@section('title')
Main
@stop

@section('content')
@include('ui.filter')

@if( count($activeProjects) > 0 )

    <style>
        .recentActive{
            position:relative;
            margin: 40px 0;
            height: 0px;
            overflow: hidden;
        }
            .recentActive:after{
                content:"";
                position:absolute;
                left:0;
                bottom:-40px;
                width:100%;
                height:1px;
                background: #ccc;
            }
            .recentActive.visible{
                margin: 90px 0;
                height:auto;
                overflow:visible;
            }
            .recentActive a.setToTraffic i{
                color: #aaa;
                transition: color 0.3s;
            }
                .recentActive a.setToTraffic:hover i{
                    color:#005580;
                }
    </style>

    <div class="recentActive">
      <h4><?php echo $headline ?> <span style="font-weight: 400; font-size: 12px;">Recent projects less than 10 weeks old - not set to "Traffic"</span></h4>
      <?php $count = 0; ?>
      <table class="table table-striped table-condensed iax-table">
        <thead>
          <tr>
            <!--<th>Project ID</th>-->
            <th>Ad Type</th>
            <th>Advertiser</th>
            <th>Campaign</th>
            <th>Project</th>
            <th>Site</th>
            <th>Created</th>
            <th>Modified</th>
            <th></th>
            <th></th>
          </tr>
        </thead>
        <tbody>
      @foreach ( $activeProjects as $p )
        <tr>
          <!--<td>{{ $p->id }}</td>-->
          <td><a href="/filter?site=0&adType={{$p->adtype->id}}&advertiser=0">{{ $p->adtype->name or 'NOT SET'}}</a></td>
          <td><a href="/advertiser/{{ $p->campaign->advertiser->id or '#doesntExist' }}">{{ $p->campaign->advertiser->name or 'NOT SET' }}</a></td>
          <td><a href="/campaign/{{ $p->campaign->id or '#doesntExist' }}">{{ $p->campaign->name or 'NOT SET' }}</a></td>
          <td><a href="/project/{{ $p->id }}/edit">{{ $p->name }}</a></td>
          <td><a href="/filter?site={{$p->site->id}}&adType=0&advertiser=0">{{ $p->site->name }}</a></td>
          <td>{{ $p->created_at->diffforhumans() }}</td>
          <td>{{ $p->updated_at->diffforhumans() }}</td>
          @if( isset($p->creatives->first()->preview->preview_id) )
            <td><a href="/preview/iframe/{{ $p->creatives->first()->preview->preview_id }}" target="_blank"><i class="fa fa-search"></i></a></td>
          @else
            <td></td>
          @endif
          <td><a href="#" class="setToTraffic" data-id="{{ $p->id }}"><i class="fa fa-check"></i></a></td>
        </tr>
      @endforeach
        </tbody>
      </table>
    </div>
@else
    <style>
        .iax-content{
            margin-top:80px;
        }
    </style>
@endif

<h4>Your Projects</h4>
  @if( count($projects) > 0 )
  <?php $count = 0; ?>
  <table class="table table-striped table-condensed iax-table">
    <thead>
      <tr>
        <!--<th>Project ID</th>-->
        <th>Ad Type</th>
        <th>Advertiser</th>
        <th>Campaign</th>
        <th>Project</th>
        <th>Site</th>
        <th>Created</th>
        <th>Modified</th>
        <th></th>
      </tr>
    </thead>
    <tbody>
  @foreach ( $projects as $p )
    <tr>
      <!--<td>{{ $p->id }}</td>-->
      <td><a href="/filter?site=0&adType={{$p->adtype->id}}&advertiser=0">{{ $p->adtype->name or 'NOT SET'}}</a></td>
      <td><a href="/advertiser/{{ $p->campaign->advertiser->id or '#doesntExist' }}">{{ $p->campaign->advertiser->name or 'NOT SET' }}</a></td>
      <td><a href="/campaign/{{ $p->campaign->id or '#doesntExist' }}">{{ $p->campaign->name or 'NOT SET' }}</a></td>
      <td><a href="/project/{{ $p->id }}/edit">{{ $p->name }}</a></td>
      <td><a href="/filter?site={{$p->site->id}}&adType=0&advertiser=0">{{ $p->site->name }}</a></td>
      <td>{{ $p->created_at->diffforhumans() }}</td>
      <td>{{ $p->updated_at->diffforhumans() }}</td>
      @if( isset($p->creatives->first()->preview->preview_id) )
        <td><a href="/preview/iframe/{{ $p->creatives->first()->preview->preview_id }}" target="_blank"><i class="fa fa-search"></i></a></td>
      @else
        <td></td>
      @endif
    </tr>
  @endforeach
    </tbody>
  </table>
  <div class="iax-pagination">
    <?php echo $projects->links(); ?>
  </div>
  @else
  <span class="alert alert-info">You have not worked on any projects yet.</span>
  @endif
  </div>
@stop

@section('scripts')
  @parent
  <script type="text/javascript">
      IAX.Main.boot('HomePage');
      //if on paginated page remove active projects
      function getParameterByName(name) {
        name = name.replace(/[\[]/, "\\[").replace(/[\]]/, "\\]");
        var regex = new RegExp("[\\?&]" + name + "=([^&#]*)"),
            results = regex.exec(location.search);
        return results === null ? "" : decodeURIComponent(results[1].replace(/\+/g, " "));
      }
      var page = getParameterByName('page');
      if(page === "" || page === "1"){
          $(".recentActive").addClass("visible");
      }

      //set recent item to traffic
      $(".setToTraffic").on("click",function(){
          var item = $(this),
              projectId = item.data("id"),
              data = {
                  project_id: projectId,
                  _token: IAX.Main.meta.csrfToken
              };
          $.ajax({
              url: IAX.Config.apiEndpoint + '/project/setTraffic',
              type: 'POST',
              dataType: 'json',
              data: data,
              success: function(data, textStatus, xhr) {
                  console.log("project set to traffic");
                  item.parents("tr").css({
                      "transform": "translateX(-100%)",
                      "opacity": 0,
                      "transition" : "opacity 0.5s, transition 0.5s"
                  });
                  setTimeout(function(){
                      item.parents("tr").hide();
                  },600);
                  return true;
              },
              error: function(xhr, textStatus, errorThrown) {
                  //called when there is an error
                  return false;
              }
          });
      });

  </script>
@stop
