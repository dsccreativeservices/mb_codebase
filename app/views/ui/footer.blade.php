<div id="footer" class="container iax-footer">
    <div class="row">
        <div class="span12">
            <p>
            Interactive Ad Experience | &copy; 2013 - {{ date('Y') }} <a href="http://www.scrippsnetworksinteractive.com/">Scripps Networks Interactive</a><br>
            @if( Sentry::check() )
                <!--<a href="{{ URL::to('/support/create') }}" class="btn btn-mini btn-primary">Submit Feedback</a>-->
            @endif
            @if( $app->environment() !== 'production' )
                <span class="iax-env">
                  <strong><span class="label label-info">{{ $app->environment() }}</span></strong>
                </span>
            @endif
            </p>
        </div>
    </div>
</div> <!-- /container -->
