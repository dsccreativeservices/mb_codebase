<div id="header" class="navbar navbar-inverse navbar-fixed-top">
    <div class="navbar-inner">
        <div class="container">
            @if( !isset($isClient) )
                <a class="brand" href="/client">
                    <img src="{{ asset('assets/img/logo_with_title_white.png') }}" alt="IAX Logo" width="218" height="35" />
                </a>
                @if( isset($project) )
                <ul class="nav">
                    <li><a href="/client/campaign/{{ $project->campaign->id }}">Return to Campaign</a></li>
                </ul>
                @endif
            @else
                <a class="brand" href="/">
                    <img class="desktopLogo" src="{{ asset('assets/img/logo_with_title_white.png') }}" alt="IAX Logo" width="218" height="35" />
                    <img class="mobileLogo" src="{{ asset('assets/img/logo_noTitle.png') }}" alt="IAX Logo" width="54" height="37" />
                </a>
                <div class="desktopMenu">
                    @include('ui.navbar.menu')
                    @include('ui.navbar.account')
                    @include('ui.navbar.search')
                </div>
                <div class="mobileMenu">
                	@if( !$isClient )
                    	<span class="icon"><img src="{{ asset('assets/img/mobileMenuIcon.svg') }}" width="30"/></span>
                    @endif
                    <span class="account">@include('ui.navbar.account')</span>
                	<div class="mobileDropdown">
                        @include('ui.navbar.menu')
                        @include('ui.navbar.search')
                    </div>
                </div>
            @endif
        </div>
    </div>
</div>

@section('scripts')
    @parent
    <script type="text/javascript">
		$(function(){
			$(".mobileMenu .icon").on("click",function(){
				$(".mobileDropdown").toggleClass("active");
			});
		});
	</script>
@stop