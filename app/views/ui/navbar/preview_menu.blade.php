<div class="iax-navbar-right">
<!-- Preview Nav Items Here -->
<ul class="nav">
    <li><a href="{{ URL::to('/preview/iframe', $preview_id) }}"><i class="fa fa-cross"></i> View Standalone Preview</a></li>
    <li><a href="/project/{{ $project_id }}/edit"><i class="fa fa-cross"></i> Edit Project</a></li>
</ul>
@if( Sentry::check() )
    @if( Request::is('preview*') )
    @endif
@else
{{-- Not logged in --}}
@endif
</div>
