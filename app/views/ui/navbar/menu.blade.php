<div class="iax-navbar-right">
@if( Sentry::check() )
    @if( Request::has('preview') )
        <ul class="nav">
            <li><a href="/">Return to Home</a></li>
        </ul>
    @else
        <ul class="nav">
            @if(!$isViewer)
                <li {{ Request::is( 'project/new') ? ' class="active"' : '' }} ><a href="/project/new">New Project</a></li>
            @endif
            <li {{ Request::is( 'advertisers') ? ' class="active"' : '' }} ><a href="/advertisers">All Advertisers</a></li>
            <li {{ Request::is( 'projects') ? ' class="active"' : '' }} ><a href="/projects">All Projects</a></li>
        </ul>
    @endif
@endif
</div>
