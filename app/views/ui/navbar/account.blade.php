@if( $isClient )
    <!--<p class="navbar-text pull-right">Client View</p>-->
@else
    @if ( Sentry::check() )
        {{-- Logged in --}}
        <p class="navbar-text pull-right">
        	<!--<span class="label label-info">BETA</span>-->
        </p>
        <ul class="nav pull-right iax-navbar-account">
            <li class="dropdown">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown"> <small>{{ $user->first_name . ' ' . $user->last_name}}</small> </a>
                <ul class="dropdown-menu" role="menu">
                    <li><a href="/"><i class="fa fa-list"></i> My Projects</a></li>
                    <li><a href="/favorites"><i class="fa fa-star"></i> My Favorites</a></li>
                    <li><a href="/profile"><i class="fa fa-user"></i> My Account</a></li>
                    @if( $isAdmin )
                        <li class="divider"></li>
                        <li><a href="/admin"><i class="fa fa-cog"></i> Admin Area</a></li>
                    @endif
                    <li class="divider"></li>
                    <li><a href="{{ URL::to('logout') }}"><i class="fa fa-power-off"></i> Sign Out</a></li>
                </ul>
            </li>
        </ul>
        @else
        <!-- Not logged in -->
        <p class="navbar-text pull-right"></p>
    @endif
@endif