@if( Sentry::check() )
    @if( !Request::is('preview*') )
        <form class="navbar-search pull-right" action="/search" method="get" >
            <input type="text" class="iax-search search-query span2" name="q" placeholder="Search">
        </form>
    @endif
@endif


@section('scripts')
    @parent
    <!-- IAX core files -->
    <script type="text/javascript">
        IAX.Main.boot();
    </script>

@stop