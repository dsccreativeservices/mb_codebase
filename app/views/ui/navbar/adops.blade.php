<div class="navbar navbar-inverse navbar-fixed-top">
    <div class="navbar-inner">
        <div class="container">
            <a class="brand" href="/">
                <img src="{{ asset('assets/img/logo_with_title_white.png') }}" alt="IAX Logo" width="218" height="35" />
            </a>
        </div>
    </div>
</div>