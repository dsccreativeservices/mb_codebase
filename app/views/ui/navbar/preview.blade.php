<div class="navbar navbar-inverse navbar-fixed-top">
    <div class="navbar-inner">
        <div class="container">
            <a class="brand" href="/">
                <img class="desktopLogo" src="{{ asset('assets/img/logo_with_title_white.png') }}" alt="IAX Logo" width="218" height="35" />
                <img class="mobileLogo" src="{{ asset('assets/img/logo_noTitle.png') }}" alt="IAX Logo" width="54" height="37" />
            </a>

			<div class="desktopMenu">
                @include('ui.navbar.preview_menu')
                @include('ui.navbar.account')
                @include('ui.navbar.search')
            </div>
            <div class="mobileMenu">
                @if( !$isClient )
                    <span class="icon"><img src="{{ asset('assets/img/mobileMenuIcon.svg') }}" width="30"/></span>
                @endif
                <span class="account">@include('ui.navbar.account')</span>
                <div class="mobileDropdown">
                    @include('ui.navbar.preview_menu')
                    @include('ui.navbar.search')
                </div>
            </div>


        </div>
    </div>
</div>

@section('scripts')
    @parent
    <script type="text/javascript">
		$(function(){
			$(".mobileMenu .icon").on("click",function(){
				$(".mobileDropdown").toggleClass("active");
			});
		});
	</script>
@stop