<!-- Form validation errors -->
@if ( $errors->count() )
<div class="alert alert-error">
    <button type="button" class="close" data-dismiss="alert">&times;</button>
    <span class="label label-important">Error</span> The following errors occured:
    <ul>
        @foreach ($errors->all() as $e)
        <li> {{ $e }} </li>
        @endforeach
    </ul>
</div>
@endif

<!-- Success messages -->
@if ( $message = Session::get('flash-success') )
<div class="alert alert-success">
    <button type="button" class="close" data-dismiss="alert">&times;</button>
    <span class="label label-success">Success</span> {{ $message }}
</div>
@endif

<!-- Info messages -->
@if ($message = Session::get('flash-info'))
<div class="alert alert-info">
    <button type="button" class="close" data-dismiss="alert">&times;</button>
    <span class="label label-info">Info</span> {{ $message }}
</div>
@endif

<!-- Warning messages -->
@if ( $message = Session::get('flash-warn') )
<div class="alert alert-warning">
    <button type="button" class="close" data-dismiss="alert">&times;</button>
    <span class="label label-warning">Warning</span> {{ $message }}
</div>
@endif

<!-- General errors -->
@if ( $message = Session::get('flash-error') )
<div class="alert alert-error">
    <button type="button" class="close" data-dismiss="alert">&times;</button>
    <span class="label label-important">Error</span> {{ $message }}
</div>
@endif