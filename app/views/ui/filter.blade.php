<div class="filter">
    <ul>
        <?php
            //set filter if already filtered
            parse_str($_SERVER['QUERY_STRING']);
            $advertisers = array('0' => 'No Filter&hellip;') + Advertiser::orderBy('name', 'asc')->get()->lists('name', 'id');
            $sites = array('0' => 'No Filter&hellip;') + Site::orderBy('name', 'asc')->get()->lists('name', 'id');
            $adtypes = array('0' => 'No Filter&hellip;') + AdType::orderBy('name', 'asc')->get()->lists('name', 'id');;
        ?>
        {{  Form::open(array(
            'action' => 'ProjectController@filter',
            'class' => 'form-horizontal',
            'id' => 'iax-filter'
         )) }}
        <li>
            <span>Site:</span>
            {{ Form::select('site_id', $sites, null, array('class' => 'span4 selectpicker', 'id' => 'site_id', 'data-size' => 6)) }}
        </li>
        <li>
            <span>Ad Type:</span>
            {{ Form::select('adtype', $adtypes, null, array('class' => 'span4 selectpicker', 'id' => 'adtype_id', 'data-size' => 6)) }}
        </li>
        <li style="display:none">
            <span>Advertiser:</span>
            {{ Form::select('advertiser_id', $advertisers, null, array('class' => 'span4 selectpicker', 'id' => 'advertiser_id', 'data-size' => 6)) }}
        </li>
        <a href="#" class="btn btn-primary" id="iax-filter-save">Filter</a>
        <a href="/" class="btn btn-primary" id="iax-filter-clear">Reset</a>
        {{ Form::close() }}
    </ul>
    <div class="errors"></div>
</div>

@section('scripts')
    @parent
    <script>
        var site = "<?php if(isset($site)){ echo $site;} ?>",
            adType = "<?php if(isset($adType)){ echo $adType;} ?>",
            advertiser = "<?php if(isset($advertiser)){ echo $advertiser;} ?>";
        //update select dropdowns with current parameters
        if(site > 0){
            $("#site_id").val(site).addClass("active");
        }
        if(adType > 0){
            $("#adtype_id").val(adType).addClass("active");
        }
        //if(advertiser > 0){
            $("#advertiser_id").val(advertiser).attr("disabled","disabled");
        //}

        //click and go
        $("#iax-filter-save").on("click", function(e){
            e.preventDefault();
            console.log("filter clicked");
            var site = 0,
                adType = 0,
                advertiser = 0,
                error = $(".filter .errors");

            if($("#site_id option:selected").val() !== "0") {
                site = $("#site_id").val();
            }
            if($("#adtype_id option:selected").val() !== "0") {
                adType = $("#adtype_id").val();
            }
            if($("#advertiser_id option:selected").val() !== "0") {
                if(site === 0 && adType === 0){
                    error.text("If filtering by advertiser you must also choose site or adtype").addClass("active");
                    setTimeout(function(){
                        error.removeClass("active");
                    },4000);
                    $("#site_id").focus();
                    return false;
                }
                advertiser = $("#advertiser_id").val();
            }

            window.location.assign("/filter?site=" + site + "&adType=" + adType + "&advertiser=" + advertiser);
        });
    </script>
@stop
