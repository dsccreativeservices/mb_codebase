<ul class="nav nav-list">
    <li class="nav-header">Quick Links</li>
    <li {{ Request::is( 'advertisers') ? ' class="active"' : '' }} ><a href="/advertisers">Advertisers</a></li>
    <!-- <li {{ Request::is( 'campaigns') ? ' class="active"' : '' }} ><a href="/campaigns">Campaigns</a></li> -->
    <li class="disabled" ><a href="/campaigns">Campaigns</a></li>
    <!-- <li {{ Request::is( 'projects') ? ' class="active"' : '' }} ><a href="/projects">Projects</a></li> -->
    <li class="disabled" ><a href="/projects">Projects</a></li>
    <li {{ Request::is( 'creatives') ? ' class="active"' : '' }} ><a href="/creatives">Creatives</a></li>
</ul>