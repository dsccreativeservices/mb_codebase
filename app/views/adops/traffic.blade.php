@extends('layouts.adops')

@section('title')
Completed Projects
@stop

@section('content')
    <h4>Completed Project</h4>
    <div class="well">
        <table class="table table-condensed iax-info-table">
            <tr>
                <td>Advertiser</td>
                <td>{{ $project->campaign->advertiser->name }}</td>
            </tr>
            <tr>
                <td>Campaign</td>
                <td>{{ $project->campaign->name }}</td>
            </tr>
            <tr>
                <td>Project</td>
                <td>{{ $project->name }}</td>
            </tr>
            <tr>
                <td>Site</td>
                <td>{{ $project->site->name }}</td>
            </tr>
            <tr>
                <td>Ad Type</td>
                <td>{{ $project->adtype->name }}</td>
            </tr>
            <tr>
                <td>Preview Link</td>
                <td><a href="{{ URL::to('preview/iframe', $project->creatives[0]->preview->preview_id) }}" target="_blank">{{ URL::to('preview/iframe', $project->creatives[0]->preview->preview_id) }}</a></td>
            </tr>
        </table>
    </div>

    <p class="clearfix">
        <a href="/project/{{ $project->id }}/edit">
            <button style="margin-left: 7px;" class="btn btn-primary btn-small pull-right" title="Edit project"><i class="fa fa-edit"></i> Edit this project</button>
        </a>
        <button class="btn btn-primary btn-small pull-right" id="iax-select-button" title="Copy creative to clipboard"><i class="fa fa-copy"></i> Highlight Creative</button>
        <a style="margin-right: 7px;;" href="/adops/traffic/project/{{ $project->id }}/raw" class="btn btn-primary btn-small pull-right" id="iax-download-button" title="Download creative."><i class="fa fa-download"></i> Download Creative</a>
    </p>
    <div class="well">
        <textarea name="" id="iax-code-show" cols="30" rows="10" class="span12" >{{ $creative }}</textarea>
    </div>

@stop

@section('scripts')
    @parent
    {{ HTML::script('assets/code-mirror/codemirror.js') }}
    {{ HTML::script('assets/code-mirror/modes/css.js') }}
    {{ HTML::script('assets/code-mirror/modes/javascript.js') }}
    {{ HTML::script('assets/code-mirror/modes/xml.js') }}
    {{ HTML::script('assets/zeroclipboard/ZeroClipboard.min.js') }}

    <script>
    var cm = document.getElementById('iax-code-show');
    if (cm) {
        var iax_code_cm = CodeMirror.fromTextArea(cm, {
            tabSize: 2,
            lineWrapping: true,
            tabindex: 2,
            lineNumbers: true,
            mode: 'xml',
            readOnly: true
        });
        $('#iax-select-button').on('click', function(e){
            iax_code_cm.execCommand('selectAll');
        });

    }

    $('.CodeMirror').on('click', function(e){
        iax_code_cm.execCommand('selectAll');
    });



    </script>
@stop
