@extends('layouts.master')

@section('title')
Completed Projects
@stop

@section('content')
<div class="container">
  <div class="row">
    <h3>Recently Completed Projects</h3>

    @if ( count($projects) > 0 )
    <table class="table table-striped table-condensed iax-table">
      <thead>
        <tr>
          <th>Project ID</th>
          <th>Ad Type</th>
          <th>Advertiser</th>
          <th>Campaign</th>
          <th>Project</th>
          <th>Site</th>
          <th>Created</th>
          <th>Modified</th>
        </tr>
      </thead>
      <tbody>
    @foreach ( $projects as $p )
      <tr>
        <td>{{ $p->id }}</td>
        <td>{{ $p->adtype->name }}</td>
        <td><a href="/advertiser/{{ $p->campaign->advertiser->id }}">{{ $p->campaign->advertiser->name }}</a></td>
        <td><a href="/campaign/{{ $p->campaign->id }}" title="{{ $p->campaign->name }}">{{ $p->campaign->name }}</a></td>
        <td><a href="/project/{{ $p->id }}/edit">{{ $p->name }}</a></td>
        <td><a href="/adops/traffic/project/{{ $p->id }}">Traffic Link</a></td>
        <td>{{ $p->site->name }}</td>
        <td>{{ $p->updated_at->diffforhumans() }}</td>
      </tr>
    @endforeach
      </tbody>
    </table>
    <div class="iax-pagination">
      <?php echo $projects->links(); ?>
    </div>

    @else
        <span class="alert alert-info">There are not completed project available.</span>
    @endif


  </div>
</div>
@stop

@section('scripts')
    @parent

@stop