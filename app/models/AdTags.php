<?php

class AdTag extends Eloquent {
    protected $table = 'adtags';
    protected $guarded = array('id');

    public function site()
    {
        return $this->belongsTo('Site', 'site_id');
    }

    public function projects()
    {
        return $this->hasMany('Project', 'adtag_id');
    }

}
