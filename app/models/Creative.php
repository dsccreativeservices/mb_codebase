<?php

use Venturecraft\Revisionable\Revisionable;

class Creative extends Revisionable {
    protected $table = 'creatives';
    protected $guarded = array('id');

    public function project()
    {
        return $this->belongsTo('Project', 'project_id');
    }

    public function preview()
    {
        return $this->hasOne('Preview');
    }

    /**
     * Get all the current creatives.
     * @param  Integer $project_id Unique id for the project
     * @return Object
     */
    public static function getCreatives($project_id)
    {
        $items = DB::table('creatives')
            ->where('creatives.project_id', '=', $project_id)
            ->orderby('updated_at' , 'desc')
            ->get();

        return $items;
    }

    /**
     * Get creative details by ID.
     * @param  Integer $cid Unique id for the creative
     * @return Object
     */
    public static function getDetail($cid)
    {
        $details = DB::table('creatives')
            ->join('projects', 'creatives.project_id', '=', 'projects.id')
            ->join('advertisers', 'creatives.advertiser_id', '=', 'advertisers.id')
            ->join('campaigns', 'creatives.campaign_id', '=', 'campaigns.id')
            ->select('projects.id', 'advertisers.name AS adv_name', 'campaigns.name AS cam_name', 'projects.name AS pro_name', 'creatives.name AS cre_name', 'projects.desc', 'creatives.created_at', 'creatives.creative')
            ->where('creatives.id', '=', $cid)
            ->first();

        return $details;
    }

}