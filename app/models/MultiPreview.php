<?php

if (Schema::hasTable('multipreviews')){
    //do nothing
}else{
    Schema::create('multipreviews', function($table){
        $table->increments('id');
        $table->integer('preview_id');
        $table->integer('campaign_id'); //all previews associated with campaign
        $table->string('name'); //name of preview
        $table->string('scrape_url'); //page used for scrape
        $table->string('preview_url'); //location of preview on server
    });   
}

class MultiPreview extends Eloquent {
    // Link up the Previews table. (Eloquent, you so awesome.)
    protected $table = 'multipreviews';
    protected $guarded = array('id');

    protected $fillable = array('campaign_id', 'tokenized_source', 'scrape_url', 'preview_url', 'preview_id', 'name');

    public function campaign(){
        return $this->belongsTo('Campaign');
    }


    /**
     * Get the latest scrape
     * @param  String $hash    Hash of the preview
     * @return String          [description]
     */
    public function getPreview($hash = '')
    {
        if( length($hash) > 0 )
        {
            // get using hash
            $source = Preview::where('preview_id', '=', $hash)->first();
        }
        else
        {
            // Get using url
            $source = Preview::where('site_id', '=', $this->getSiteInfo($url)->id)->first();
        }

        return $source;
    }

}