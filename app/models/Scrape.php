<?php

class Scrape extends Eloquent {
    protected $guarded = array('id');

    public static $rules = array();

    /**
     * Given a html source, will replace actual ad tags with tokens for generating previews. It will also try to remove any core page tracking.
     * @param  String $source Some html source.
     * @return String         Tokenized verions of source.
     */
    public function tokenizeAdTags($source, $url)
    {
        $host = parse_url($url)['host'];

        $adTags = AdType::getTags($host); // array of ad tags for this site

        $tokenizedSource = $source;

        // Loop through each tag and tockenize in the source.
        foreach ($adTags as $key => $value) {
            // Escape any RegEx special chars.
            $cleanTag = str_replace(array('(', ')'), array('\(', '\)'), $value->tag);
            $tokenizedSource = preg_replace('/<script type="text\/javascript">(\s)*' . $cleanTag . '[\w]*(\s)*<\/script>/', $value->token, $tokenizedSource);
        }

        return $tokenizedSource;
    }

    /**
     * Make an changes to the source needed for safe previewing. Things like tracking, meta data changes etc. The source should be effectively same to the system after this process.
     * @param  String $source [description]
     * @return String         Cleaned source.
     */
    public function cleanSource($source)
    {
        // Update any meta tags for preview.
        $source = preg_replace(
            // Regular expression to match against
            array(
                '/mdManager\.addParameter\("Sponsorship",[\s]*"[A-Za-z ]*"\);/',
                '/mdManager\.addParameter\(\'UserId\',[\s]*[A-Za-z ]*\);/',
                '/mdManager\.addParameter\(\'UserIdEmail\',[\s]*[A-Za-z ]*\);/',
                '/mdManager\.addParameter\(\'UserIdCreated\',[\s]*[A-Za-z ]*\);/',
                '/mdManager\.addParameter\(\'UserIdVersion\',[\s]*[A-Za-z ]*\);/',
                ),
            // Content to replace.
            array(
                'mdManager.addParameter("Sponsorship", "IAX-PREVIEW");',
                'mdManager.addParameter("UserId", "IAX-PREVIEW");',
                'mdManager.addParameter("UserIdEmail", "IAX-PREVIEW-USER-EMAIL");',
                'mdManager.addParameter("UserIdCreated", "IAX-PREVIEW-USER-ID");',
                'mdManager.addParameter("UserIdVersion", "IAX-PREVIEW-USER-ID-VERSION");',
                ),
            $source);

        return $source;
    }

    /**
     * Get preview url source.
     * @param  String $url The page url.
     * @return [type]      A string of the source.
     */
    public function getScrape($url)
    {
        $curl = new Curl;

        $scrape = DB::table('scrapes')->where('url', '=', $url)->first();
        if( $scrape )
        {
            $scrape = DB::table('scrapes')->where('url', '=', $url)->first();
            Log::info("Scrape exists. Pulling existing scrape.");
        }
        else
        {
            Log::info("No scrape found. Requestin new version.");
            // $source = $curl->get($url, $vars = array()); # The Curl object will append the array of $vars to the $url as a query string
            // $source = file_get_contents('app/dummySource.txt');
            $source = file_get_contents('tagSource.txt');

            // Store scrape data
            DB::table('scrapes')->insert(array(
                'url' => $url,
                'raw' => $source,
                'tokenized' => $this->tokenizeAdTags($source, $url)
                ));
            $scrape = DB::table('scrapes')->where('url', '=', $url)->first();

        }
        Log::info(e($scrape->tokenized));
        return $scrape;
    }

}