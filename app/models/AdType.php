<?php

class AdType extends Eloquent {
    protected $table = 'adtypes';
    protected $guarded = array('id');

    public function adtag(){
        return $this->belongsTo('AdTag');
    }

    public function projects(){
        return $this->hasMany('Project', 'adtype_id');
    }

    /**
     * Get array of adtypes with id and name for creating dropdown form controls.
     * @return [type] [description]
     */
    public static function getDropdown()
    {
        $names =  DB::table('adtypes')->select('id', 'name')->get();
        $items = array();
        foreach ($names as $key => $value) {
            $items[$value->id] = $value->name;
        }
        return $items;
    }

    /**
     * Return an array of ad types for this site based on the $host value
     * @param  String $host Host of preview page.
     * @return Array       Array of tag objects
     */
    public static function getTags($host)
    {

        $tags = DB::table('sites')
            ->join('adtypes', 'sites.id', '=', 'adtypes.site')
            ->select('adtypes.name', 'adtypes.token', 'adtypes.tag', 'sites.name', 'sites.host')
            ->where('sites.host', '=', $host)
            ->get();

        return $tags;
    }

}
