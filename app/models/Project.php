<?php

use Venturecraft\Revisionable\Revisionable;

class Project extends Revisionable {
    // Link up the Campaign table. (Eloquent, you so awesome.)
    protected $table = 'projects';
    protected $guarded = array('id');

    public function campaigns()
    {
        return $this->belongsTo('Campaign');
    }

    public function creatives()
    {
        return $this->hasMany('Creative', 'project_id');
    }

    public function useractions()
    {
        return $this->hasMany('UserAction', 'item_id');
    }

    public function campaign()
    {
        return $this->belongsTo('Campaign');
    }

    public function site()
    {
        return $this->belongsTo('Site', 'site_id');
    }

    public function adtype()
    {
        return $this->belongsTo('AdType', 'adtype_id');
    }

	/* mike added */
	 public function adtag()
    {
        return $this->belongsTo('AdTag', 'adtag_id');
    }


    public function favorites()
    {
        return $this->hasMany('Favorite', 'project_id');
    }

    public function tags()
    {
        return $this->belongsToMany('Tag');
    }

    public function images()
    {
        return $this->hasmany('Image');
    }

    /**
     * Get the recent project list for a given user or all if no user provided.
     * @param  integer $user_id
     * @return Object
     */
    public static function getRecent($user_id = 0)
    {
        if( $user_id == 0 )
        {
            // All projects
            $items = DB::table('projects')
                ->join('users', 'projects.created_by', '=', 'users.id')
                ->join('campaigns', 'projects.campaign_id', '=', 'campaigns.id')
                ->join('advertisers', 'campaigns.advertiser_id', '=', 'advertisers.id')
                ->join('adtypes', 'projects.adtype_id', '=', 'adtypes.id')
                ->join('sites', 'projects.site_id', '=', 'sites.id')
                ->orderby('projects.updated_at' , 'desc')
                ->select(
                    'advertisers.name AS advertiser',
                    'advertisers.id AS advertiser_id',
                    'adtypes.name AS adtype',
                    'adtypes.id AS adtype_id',
                    'campaigns.name AS campaign',
                    'campaigns.id AS campaign_id',
                    'projects.name AS project',
                    'projects.id AS project_id',
                    'projects.status AS status',
                    'sites.code AS site',
                    'sites.id AS site_id',
                    'projects.created_at AS created_at',
                    'projects.updated_at AS updated_at');
        }
        else
        {
            // By user
            $items = DB::table('projects')
                ->join('users', 'projects.created_by', '=', 'users.id')
                ->join('campaigns', 'projects.campaign_id', '=', 'campaigns.id')
                ->join('advertisers', 'campaigns.advertiser_id', '=', 'advertisers.id')
                ->join('adtypes', 'projects.adtype_id', '=', 'adtypes.id')
                ->join('sites', 'projects.site_id', '=', 'sites.id')
                ->where('users.id', '=', $user_id)
                ->orderby('projects.updated_at' , 'desc')
                ->select(
                    'advertisers.name AS advertiser',
                    'advertisers.id AS advertiser_id',
                    'adtypes.name AS adtype',
                    'adtypes.id AS adtype_id',
                    'campaigns.name AS campaign',
                    'campaigns.id AS campaign_id',
                    'projects.name AS project',
                    'projects.id AS project_id',
                    'projects.status AS status',
                    'sites.code AS site',
                    'sites.id AS site_id',
                    'projects.created_at AS created_at',
                    'projects.updated_at AS updated_at');
        }

        return $items;
    }

    /**
     * Get all the current projects by campaign_id
     * @param  Integer $campaign_id Unique id for the project
     * @return Object
     */
    public static function getProjects($campaign_id)
    {
        $items = DB::table('projects')
            ->where('projects.campaign_id', '=', $campaign_id)
            ->orderby('updated_at' , 'desc')
            ->get();

        return $items;
    }


    /**
     * Get the details for a given project id
     * @param  Integer $project_id
     * @return Object
     */
    public static function getDetail($project_id)
    {
        $details = DB::table('projects')
            ->join('campaigns', 'projects.campaign_id', '=', 'campaigns.id')
            ->join('advertisers', 'campaigns.advertiser_id', '=', 'advertisers.id')
            ->select('projects.id', 'advertisers.name AS adv_name', 'campaigns.name AS cam_name', 'projects.name AS pro_name', 'projects.desc', 'projects.created_at', 'projects.url')
            ->where('projects.id', '=', $project_id)
            ->first();

        return $details;
    }

}