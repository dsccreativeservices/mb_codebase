<?php

//add campaign_id
if (Schema::hasColumn('previews', 'campaign_id')){
    //do nothing
}else{
    Schema::table('previews', function($table){
        $table->integer('campaign_id'); //all previews associated with campaign
    }); 
}
//add name
if (Schema::hasColumn('previews', 'name')){
    //do nothing
}else{
    Schema::table('previews', function($table){
        $table->string('name'); //name of preview
    });
}

class Preview extends Eloquent {
    // Link up the Previews table. (Eloquent, you so awesome.)
    
    protected $table = 'previews';
    protected $guarded = array('id');

    protected $fillable = array('site_id', 'creative_id', 'preview_source', 'tokenized_source', 'scrape_url', 'preview_id', 'creative_items', 'name', 'campaign_id');

    public function creative(){
        return $this->belongsTo('Creative');
    }


    /**
     * Get the latest scrape
     * @param  String $hash    Hash of the preview
     * @return String          [description]
     */
    public function getPreview($hash = '')
    {
        if( length($hash) > 0 )
        {
            // get using hash
            $source = Preview::where('preview_id', '=', $hash)->first();
        }
        else
        {
            // Get using url
            $source = Preview::where('site_id', '=', $this->getSiteInfo($url)->id)->first();
        }

        return $source;
    }

}