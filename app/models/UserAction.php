<?php

class UserAction extends Eloquent {
    protected $table = 'user_actions';
    protected $guarded = array('id');


    public function user()
    {
        return $this->belongsTo('User', 'user_id');
    }

    /**
     * Get the most recent action by item id
     * @param String $type The type of element to check. (advertiser|campaign|project|creative)
     * @param Integer $id The id of the item to check.
     * @return Object or null
     */
    public static function getLastUpdate($type, $id)
    {
        $action = DB::table('user_actions')
            ->where('action', '=', 'update')
            ->where('item_type', '=', $type)
            ->where('item_id', '=', $id)
            ->orderBy('created_at', 'desc')
            -first();

        return $action;
    }

}