<?php

class Favorite extends Eloquent {
    protected $table = 'favorites';
    protected $guarded = array('id');

    public function project(){
    	return $this->hasOne('project', 'id');
    }

    public function user(){
    	return $this->hasOne('user', 'user_id');
    }

    public static function isFavorite($user_id, $project_id){
    	return Favorite::whereRaw('user_id = ' . $user_id . ' AND project_id = ' . $project_id)->exists();
    }
}