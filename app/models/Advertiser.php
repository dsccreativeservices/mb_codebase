<?php

use Venturecraft\Revisionable\Revisionable;

class Advertiser extends Revisionable {
    protected $table = 'advertisers';
    protected $guarded = array('id');

    public function campaigns()
    {
        return $this->hasMany('Campaign', 'advertiser_id');
    }

    public function useractions()
    {
        return $this->hasMany('UserAction', 'item_id');
    }


}