<?php

use Venturecraft\Revisionable\Revisionable;

class Campaign extends Revisionable {
    protected $table = 'campaigns';
    protected $guarded = array('id');

    /*==============================================
    =            Eloquent Relationships            =
    ==============================================*/
    public function advertiser()
    {
        return $this->belongsTo('Advertiser', 'advertiser_id');
    }

    public function projects()
    {
        return $this->hasMany('Project');
    }

    public function useractions()
    {
        return $this->hasMany('UserAction', 'item_id');
    }
    /*-----  End of Eloquent Relationships  ------*/

    /**
     * Get the details for a given project id
     * @param  Integer $project_id
     * @return Object
     */
    public static function getDetail($campaign_id)
    {
        $details = DB::table('campaigns')
            ->join('advertisers', 'campaigns.advertiser_id', '=', 'advertisers.id')
            ->select('campaigns.id', 'advertisers.name AS adv_name', 'campaigns.name AS name', 'campaigns.desc AS desc', 'campaigns.updated_at', 'campaigns.created_at')
            ->where('campaigns.id', '=', $campaign_id)
            ->first();

        return $details;
    }

}