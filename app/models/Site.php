<?php

class Site extends Eloquent {
    // Link up the Campaign table. (Eloquent, you so awesome.)
    protected $table = 'sites';
    protected $guarded = array('id');

    public function adtags()
    {
        return $this->hasMany('AdTag');
    }

    /**
     * Provide UID for site base don URL
     * @param  String $url
     * @return Integer
     */
    public static function getSiteInfo($url)
    {
        // get the base site
        $parts = parse_url($url);
        $host = $parts['host'];

        return DB::table('sites')->where('host', '=', $host)->get();
    }

}