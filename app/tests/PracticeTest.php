<?php

/**
* PracticeTest
*/
class PracticeTest extends TestCase {

    public function setUp()
    {
        parent::setUp();

        Artisan::call('migrate');
    }

    public function testName()
    {
        AdTag::create(array('site_id' => 1, 'name' => 'Leaderboard', 'tag_selector' => '#leaderboard','regex' => ''));

        $adtag = AdTag::where('site_id', 1)->first();

        $this->assertEquals( $adtag->name, 'Leaderboard', 'Adtag name must match leaderboard.');
    }
}