<?php
return array(
	/*
	|--------------------------------------------------------------------------
	| Application Settings
	|--------------------------------------------------------------------------
	*/

	// Pagination size
	'page_size' => 25,

	// The minimum string length a search can use. Integers are treated as IDs and not affected by this.
	'search' => array(
		'min' => 2,
		// Character between flags and search term
		'flag_separator' => ':',
		// Flags and corresponding model. More can added.
		'flags' => array(
			'a' => 'Advertiser',
			'c' => 'Campaign',
			'p' => 'Project',
			'cr' => 'Creative',
			't' => 'Tag',
			'u' => 'User',
		)
	),

	// Image upload API configuration.
	'upload' => array(
		'upload_dir' => 'uploads', // This is relative to the public directory
		'max_filesize' => 5000, // kb
	),
	// File cache location
	'file_download' => 'downloads' // This is relative to the public directory
);