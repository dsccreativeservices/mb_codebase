<?php

return array(

	/*
	|--------------------------------------------------------------------------
	| oAuth Config
	|--------------------------------------------------------------------------
	*/

	/**
	 * Storage
	 */
	'storage' => 'Session',

	/**
	 * Consumers
	 */
	'consumers' => array(

		/**
		 * Github - for posting issues
		 */
        'Github' => array(
            'client_id'     => 'c454efd1378591b3a2c7',
            'client_secret' => '93c2c8310786c56a8ea28a50524efabd83423c22',
            'scope'         => array('repo'),
        ),

	)

);