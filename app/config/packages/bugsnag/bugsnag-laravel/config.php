<?php return array(
    /**
     * Set your Bugsnag API Key.
     * You can find your API Key on your Bugsnag dashboard.
     */
    'api_key' => '1a3a090dd2ffb31cd6d6035c8e87471a',

    /**
     * Set which release stages should send notifications to Bugsnag
     */
    'notify_release_stages' => array('production', 'staging')
);
