<?php
return array(
    'key'    => $_ENV['AWS_KEY'], // AWS Access Key
    'secret' => $_ENV['AWS_SECRET'], // AWS Secret Access Key
    'region' => 'us-east-1',
    'config_file' => null
);
