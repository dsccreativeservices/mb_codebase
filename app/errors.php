<?php

/*
|--------------------------------------------------------------------------
| Application Error Handler
|--------------------------------------------------------------------------
|
| Here you may handle any errors that occur in your application, including
| logging them or displaying custom views for specific errors. You may
| even register several error handlers to handle different types of
| exceptions. If nothing is returned, the default error view is
| shown, which includes a detailed stack trace during debug.
|
*/


// Only handle exceptions in staging and production so we can debug without tailing log files.
if( App::environment() === 'production' ){

    /**
     * Log routing exceptions.
     */

    App::error(function($exception)
    {
        Bugsnag::notifyException($exception);
        return Response::view('errors.500', array(), 500);
    });

    App::error(function(\Symfony\Component\HttpKernel\Exception\NotFoundHttpException $exception)
    {
        Bugsnag::notifyException($exception);
        return Response::view('errors.404', array(), 404);
    });


}