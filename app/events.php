<?php

/*
|--------------------------------------------------------------------------
| Application Event Bindings
|--------------------------------------------------------------------------
|
| Use events to log various actions taken by users. Basically, every change to the DB is
| logged with info on the user, item, etc.
|
*/

Advertiser::created(function($advertiser){
    UserAction::create(array(
        'user_id' => Sentry::getUser()->id,
         'action' => 'created',
         'item_type' => 'advertiser',
         'item_id' => $advertiser->id,
    ));
});

Advertiser::updated(function($advertiser){
    UserAction::create(array(
        'user_id' => Sentry::getUser()->id,
         'action' => 'updated',
         'item_type' => 'advertiser',
         'item_id' => $advertiser->id
    ));
});

Campaign::created(function($campaign){
    UserAction::create(array(
        'user_id' => Sentry::getUser()->id,
         'action' => 'created',
         'item_type' => 'campaign',
         'item_id' => $campaign->id,
    ));
});

Campaign::updated(function($campaign){
    UserAction::create(array(
        'user_id' => Sentry::getUser()->id,
         'action' => 'updated',
         'item_type' => 'campaign',
         'item_id' => $campaign->id,
    ));
});

Project::created(function($project){
    UserAction::create(array(
        'user_id' => Sentry::getUser()->id,
         'action' => 'updated',
         'item_type' => 'project',
         'item_id' => $project->id
    ));
});

Project::updated(function($project){
    UserAction::create(array(
        'user_id' => Sentry::getUser()->id,
         'action' => 'updated',
         'item_type' => 'project',
         'item_id' => $project->id
    ));
});

Creative::created(function($creative){
    UserAction::create(array(
        'user_id' => Sentry::getUser()->id,
         'action' => 'updated',
         'item_type' => 'creative',
         'item_id' => $creative->id
    ));
});

Creative::updated(function($creative){
    UserAction::create(array(
        'user_id' => Sentry::getUser()->id,
         'action' => 'updated',
         'item_type' => 'creative',
         'item_id' => $creative->id
    ));
});


