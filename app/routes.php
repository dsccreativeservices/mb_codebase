<?php

/**
* Declare route patterns to use.
**/
Route::pattern('id', '[0+-9]+'); // integer ids
Route::pattern('preview_id', '[0+-9A-Za-z]+'); // alphanumeric preview id
Route::pattern('reset_id', '[0+-9A-Za-z]+'); // alphanumeric password reset id

/**
* Main Page
**/
Route::get('/', 'HomeController@index');


/**
 * Client routes - No login required
 */
Route::group(array('prefix' => 'client'), function(){
    Route::get('/', 'ClientController@getIndex');
    Route::get('/advertiser/{id}', 'AdvertiserController@getDetail');
    Route::get('/campaign/{id}', 'CampaignController@getDetail');
    Route::get('/project/{id}', 'ProjectController@getDetail');
    Route::get('/project/{id}/images', 'ImageController@showProjectImages');
    Route::get('/campaign/preview/{preview_id}', 'CampaignController@multiPreview');//mike added
    Route::get('/preview/mobile/{preview_id}', 'PreviewController@previewIframe'); //mike added
});

/**
 * User routes
 */
// Login/Logout
Route::get('/login', 'LoginController@get_login');
Route::post('/login', 'LoginController@post_login');
Route::get('/logout', 'LoginController@post_logout');
// Password reset routes
Route::get('/reset', 'LoginController@get_reset');
Route::post('/reset', 'LoginController@post_reset');
Route::get('/doreset/{reset_id}', 'LoginController@get_newpassword');
Route::post('/doreset', 'LoginController@post_newpassword');
// Current User pages
Route::get('/profile', 'UserController@get_profile');
Route::post('/passchange', 'UserController@change_password');
Route::get('/favorites', 'FavoriteController@index');
Route::post('/favorites', 'FavoriteController@remove');

/**
* Project Layer Routes
**/
// Get All of an entity. (eg: All advertisers)
Route::get('/advertisers', 'AdvertiserController@index');
Route::get('/campaigns', 'CampaignController@index');
Route::get('/projects', 'ProjectController@index');
Route::get('/creatives', 'CreativeController@index');
Route::get('/filter', 'ProjectController@filter'); //mike added
// Detail routes
Route::get('/advertiser/{id}', 'AdvertiserController@getDetail');
Route::get('/campaign/{id}', 'CampaignController@getDetail');
Route::get('/campaign/preview/{preview_id}', 'CampaignController@multiPreview');//mike added
Route::get('/project/{id}', 'ProjectController@get_edit');
Route::get('/tag/{id}', 'TagController@showProjects'); //mike added
Route::get('/sites/{id}', 'SitesController@showSites'); //mike added
Route::get('/adTypes/{id}', 'adTypesController@showType'); //mike added - route for all adtypes
// Form load routes
Route::get('/project/new', 'ProjectController@get_new');
Route::get('/project/{id}/edit', 'ProjectController@get_edit');
Route::get('/project/{id}/images', 'ImageController@showProjectImages');

// Form Posts
Route::post('/projects/new', 'ProjectController@post_new');
// Delete
Route::get('/creative/delete/{id}', 'CreativeController@getDelete');

/**
* Preview Routes
**/
Route::get('/preview/view/{preview_id}', 'PreviewController@viewPreview');
Route::get('/preview/iframe/{preview_id}', 'PreviewController@previewIframe');
Route::get('/preview/mobile/{preview_id}', 'PreviewController@previewIframe'); //mike added

/**
* Search Routes
**/
Route::get('search', 'SearchController@basic_search');

/**
* API Routes
**/
Route::group(array('prefix' => 'api/v1', 'before' => 'api_auth'), function()
{
    Route::get('creatives', 'api_CreativeController@getAll');
    Route::get('advertisers', 'api_AdvertiserController@getAll');
    Route::get('projects/{id}/creatives', 'api_CreativeController@getByProject');
    Route::get('campaigns/{id}', 'api_CampaignController@getSelectCampaigns');
    Route::get('adtypes/{id}', 'api_AdtypeController@getAdTypesBySite');
    Route::get('adtypes/site', 'api_AdtypeController@getProjectsWithAdtypeAndSite');  //mike added
    Route::get('adtags/{id}', 'api_AdTagController@getAdTagsBySite');

    Route::post('project/{id}', 'api_ProjectController@getDetails');

    // Image API Routes --------------
    Route::post('projects/image/all', 'api_ImageController@getImages');
    Route::post('projects/image/single', 'api_ImageController@getImage');
    Route::post('projects/image/upload', 'api_ImageController@uploadImage');
    Route::post('projects/image/update', 'api_ImageController@updateImage');
    Route::post('projects/image/updateImg', 'api_ImageController@updateImageOnly');  //mike added
    Route::post('projects/image/multiImages', 'api_ImageController@multiImages');  //mike added
    Route::post('projects/image/update_sort', 'api_ImageController@updateImageSort');
    Route::post('projects/image/remove', 'api_ImageController@removeImage');
    // ENBD Image API Routes ---------

    Route::post('project/new', 'api_ProjectController@new_project');
    Route::post('project/update', 'api_ProjectController@update_project');
    Route::post('project/setTraffic', 'api_ProjectController@setTraffic'); //mike added
	Route::post('project/delete', 'api_ProjectController@delete_project'); //mike added
    Route::post('project/copy', 'api_ProjectController@copy_project'); //mike added
    Route::post('project/saveMod', 'api_ProjectController@save_mod'); //mike added

    Route::post('creative/update', 'api_ProjectController@update_creative');
    Route::get('creative/{id}/toggle_lock', 'api_CreativeController@toggle_lock');
    Route::get('creative/{id}/lock', 'api_CreativeController@lock');
    Route::get('creative/{id}/unlock', 'api_CreativeController@unlock');

    Route::get('advertiser/new', 'api_AdvertiserController@new_advertiser');
    Route::post('advertiser/update', 'api_AdvertiserController@update_advertiser'); //mike added
    Route::get('campaign/new', 'api_CampaignController@new_campaign');
    Route::post('campaign/update', 'api_CampaignController@update_campaign');
	Route::post('campaign/delete', 'api_CampaignController@delete_campaign'); //mike added
    Route::post('campaign/multiPreview', 'api_CampaignController@multiPreview'); //mike added
    Route::post('campaign/deletePreview', 'api_CampaignController@deletePreview'); //mike added

    // Favorites - add/remove favorite
    Route::get('favorite/toggle', 'api_FavoriteController@toggleFavorite');

    // Tags
    Route::get('tags/autocomplete', 'api_TagController@getAutocomplete');
    Route::post('tags/remove', 'api_TagController@removeTagFromProject');
    Route::post('tags/add', 'api_TagController@addTagToProject');
});

Route::group(array('prefix' => 'api/external', 'before' => 'external'), function()
{
    Route::get('favorite/all', 'api_FavoriteController@getAll'); //mike added
});


/**
* Ad Ops group traffic Routes
**/
Route::group(array('prefix' => 'adops', 'before' => 'adops_auth'), function(){
    Route::get('/traffic/project/{id}', 'AdopsController@getProject');
    Route::get('/traffic/project/{id}/raw', 'AdopsController@getProjectRaw');
    Route::get('/traffic/projects/completed', 'AdopsController@getCompletedProjects');
});

/**
 * Admin Area Routes
 */
Route::group(array('prefix' => 'admin', 'before' => 'admin_auth'), function()
{
    Route::get('/', 'controllers\admin\AdminController@getDashboard');

    /* User Admin Routes */
    Route::get('/users', 'controllers\admin\UserController@index');
	Route::get('/users/new', 'controllers\admin\UserController@newUser');
    Route::get('/users/{id}/edit', 'controllers\admin\UserController@edit');
    Route::post('/users/create', 'controllers\admin\UserController@create');
    Route::post('/users/{id}/edit', 'controllers\admin\UserController@update');
    Route::post('/users/ban', 'controllers\admin\UserController@ban');
    Route::post('/users/unban', 'controllers\admin\UserController@unban');

    // Ad Types Routes
    Route::get('/adtypes', 'controllers\admin\AdtypeController@index');

    Route::get('/adtypes/new', 'controllers\admin\AdtypeController@create');
    Route::post('/adtypes/new', 'controllers\admin\AdtypeController@store');

    Route::get('/adtypes/{id}/edit', 'controllers\admin\AdtypeController@edit');
    Route::post('/adtypes/{id}/edit', 'controllers\admin\AdtypeController@update');
    Route::post('/adtypes/delete', 'controllers\admin\AdtypeController@delete');

    // Ad Tags Routes
    Route::get('/adtags', 'controllers\admin\AdtagController@index');

    Route::get('/adtags/new', 'controllers\admin\AdtagController@create');
    Route::post('/adtags/new', 'controllers\admin\AdtagController@store');

    Route::get('/adtags/{id}/edit', 'controllers\admin\AdtagController@edit');
    Route::post('/adtags/{id}/edit', 'controllers\admin\AdtagController@update');
    Route::post('/adtags/delete', 'controllers\admin\AdtagController@delete');
});

/*
* Support Routes
*/
Route::group(array('prefix' => 'support'), function()
{
    Route::get('/create', 'SupportController@get_create');
    Route::post('/create', 'SupportController@post_create');
    Route::get('/thanks', 'SupportController@thanks');
});
