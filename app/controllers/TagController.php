<?php

use Iax\Repositories\ProjectRepositoryInterface;

class TagController extends BaseController {

    /**
     * @var \Iax\Repositories\ProjectRepositoryInterface
    */ 
    protected $project;

    public function __construct(ProjectRepositoryInterface $project)
    {
        parent::__construct();

        //Check CSRF token on POST
        $this->beforeFilter('csrf', array('on' => 'post'));

        // Inject instanec of ProjectRepositoryInterface
        $this->project = $project;
    }
	
	/* show projects by tag id */
	public function showProjects($tag_id){				
		$tag = Tag::find($tag_id);
		
		$data['projects'] = $this->project->getByTags( $tag->name );
		
		return View::make('frontend.projects.index')->with($data);
	}

}
