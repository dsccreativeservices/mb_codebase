<?php

class CreativeController extends BaseController {

    public function __construct()
    {
        parent::__construct();

        //Check CSRF token on POST
        $this->beforeFilter('csrf', array('on' => 'post'));
    }

    /**
     * Get all creatives
     *
     * @return Response
     */
    public function index()
    {
        $data['creatives'] = Creative::getCreatives();
        $data['creatives']->load('project');
        $data['page_header'] = "Creatives";
        return View::make('frontend.creatives.index')->with($data);
    }

    /**
     * Delete creative and preview associated with it.
     * @param  Integer $id
     * @return Reponse
     */
    public function getDelete($id)
    {
        Session::flash('flash-warn', "Deletion is currently disabled. Your desire to delete Creative ID:$id has been been noted however.");
        return Redirect::to('/creatives');
    }

    /**
     * Show new creative form
     * @return Response
     */
    public function get_new()
    {
        $data['advertisers'] = Advertiser::getDropdown();
        $data['campaigns'] = Campaign::getDropdown();
        $data['projects'] = Project::getDropdown();


        return View::make('frontend.creatives.new')->with($data);
    }

    /**
     * Create a new creative item.
     * @return Response
     */
    public function post_new()
    {
        $input = array(
            'advertiser_id' => Input::get('advertiser_id'),
            'campaign_id' => Input::get('campaign_id'),
            'project_id' => Input::get('project_id'),
            'creative_name' => Input::get('creative_name'),
            'creative' => Input::get('creative'),
            'url' => Input::get('preview_url')
            );

        $rules = array(
            'advertiser_id' => 'required',
            'campaign_id' => 'required',
            'project_id' => 'required',
            'creative_name' => 'required',
            'creative' => 'required',
            'url' => 'required|url'
            );

        // Create validator object.
        $v = Validator::make($input, $rules);

        if( $v->fails() )
        {
            // Form validation failed
            return Redirect::to('creative/new')->withErrors($v)->withInput();
        }
        else
        {
            $creative = new Creative();

            $creative->advertiser_id = $input['advertiser_id'];
            $creative->campaign_id = $input['campaign_id'];
            $creative->project_id = $input['project_id'];
            $creative->name = $input['creative_name'];
            $creative->creative = e(($input['creative']));

            // Get project details
            $project = Project::find($creative->project_id);
            // Get Campaign Info
            $campaign = Campaign::find($project->campaign_id);

            try {
                $creative->save();
                // Create a preview item for this creative if one not already setup
                Session::flash('flash-success', "New creative was added successfully.");

                // Preview ID for item
                $preview_id = md5(Sentry::getUser()->email . $input['url'] . $creative->id);

                if( Preview::where('preview_id', '=', $preview_id)->count() == 0 ){
                    try {
                        $preview = new Preview();
                        $preview->site_id = 1; // TODO: Add site id to project table and load that.
                        $preview->creative_id = $creative->id; // id of new creative
                        $preview->scrape_url = $input['url'];
                        $preview->preview_id = $preview_id;

                        $preview->save();
                    } catch (Exception $e) {
                        Session::flash('flash-error', "There was an error setting up the preview.");
                        return Redirect::to('creative/new');
                    }
                }
                else {

                }
                return Redirect::to('creatives');
            } catch (Exception $e) {
                Session::flash('flash-error', "There was an error processing new creative.");
                // return Redirect::to('creative/new');
            }
        }
    }

    /**
     * Get details by id/
     * @return Response
     */
    public function getDetails($cid)
    {
        $data['info'] = Creative::getDetail($cid);
        return View::make('frontend.creatives.detail')->with($data);
    }

}