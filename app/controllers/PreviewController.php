<?php

use Iax\Repositories\ProjectRepositoryInterface;

/*<?php
$html = file_get_contents( 'http://www.food.com/' );
echo $html;
?>*/


class PreviewController extends BaseController {

    public function __construct(ProjectRepositoryInterface $project)
    {
        parent::__construct();

        $this->project = $project;

        //Check CSRF token on POST
        $this->beforeFilter('csrf', array('on' => 'post'));
}

    /**
     * Preview page based on preview unnique hash value.
     * @param  String $preview_hash
     * @return Response
     */
    public function viewPreview($preview_id)
    {
        $preview = Preview::where('preview_id', $preview_id)->first();
        $preview->load('creative');

        $data['preview_id'] = $preview_id;
        $data['project_id'] = $preview->creative->project_id;

        return View::make('previews.wrapper')->with($data);
    }

    /**
     * Render the iframe source for previews based on the preview ID value.
     * @param  String $preview_id
     * @return Response
     */
    public function previewIframe($preview_id)
    {
        $data['preview_source'] = NULL; //var to hold the page source

        $data['preview'] = $preview = Preview::where('preview_id', '=', $preview_id)->first();
        $data['creative'] = $creative = Creative::find($preview->creative_id);
        $data['project'] = $project = Project::where('id', $creative->project_id)->with('adtype')->first();
        $data['campaign'] = $campaign = Campaign::find($project->campaign_id);
        $data['adtype'] = $adtype = AdType::find($project->adtype_id);
        $data['advertiser'] = $advertiser = Advertiser::find($campaign->advertiser_id);
        $data['adtag'] = $adtag = AdTag::find($project->adtag_id);
        $data['site'] = $site = Site::find($project->site_id);
        //$data['user'] = Sentry::getUser()->id;

        //find device header and match when grabbing site
        function check_user_agent ( $type = NULL ) {
            $user_agent = strtolower ( $_SERVER['HTTP_USER_AGENT'] );
            if ( $type == 'bot' ) {
                    // matches popular bots
                    if ( preg_match ( "/googlebot|adsbot|yahooseeker|yahoobot|msnbot|watchmouse|pingdom\.com|feedfetcher-google/", $user_agent ) ) {
                            return true;
                            // watchmouse|pingdom\.com are "uptime services"
                    }
            } else if ( $type == 'browser' ) {
                    // matches core browser types
                    if ( preg_match ( "/mozilla\/|opera\//", $user_agent ) ) {
                            return true;
                    }
            } else if ( $type == 'mobile' ) {
                    // matches popular mobile devices that have small screens and/or touch inputs
                    // mobile devices have regional trends; some of these will have varying popularity in Europe, Asia, and America
                    // detailed demographics are unknown, and South America, the Pacific Islands, and Africa trends might not be represented, here
                    if( preg_match ( "/ipad/", $user_agent ) ){
                        return false;
                    } else if ( preg_match ( "/phone|iphone|itouch|ipod|symbian|android|htc_|htc-|palmos|blackberry|opera mini|iemobile|windows ce|nokia|fennec|hiptop|kindle|mot |mot-|webos\/|samsung|sonyericsson|^sie-|nintendo/", $user_agent ) ) {
                            // these are the most common
                            return true;
                    } else if ( preg_match ( "/mobile|pda;|avantgo|eudoraweb|minimo|netfront|brew|teleca|lg;|lge |wap;| wap /", $user_agent ) ) {
                            // these are less common, and might not be worth checking
                            return true;
                    }
            }
            return false;
        }
        $ismobile = check_user_agent('mobile');
        if($ismobile || strpos($data['adtype'],'mobile')) {
            //yes mobile
            $opts = array('http' =>
                array(
                    'header'  => 'User-agent: Mozilla/5.0 (iPhone; CPU iPhone OS 8_0 like Mac OS X) AppleWebKit/600.1.3 (KHTML, like Gecko) Version/8.0 Mobile/12A4345d Safari/600.1.4',
                )
            );
            $context  = stream_context_create($opts);
            if(!empty($preview->scrape_url)){
               $html = file_get_contents($preview->scrape_url, false, $context);
            }else{
               return "Preview URL is empty - please update";
            }
        } else {
            //no desktop
            if(!empty($preview->scrape_url)){
               $html = file_get_contents($preview->scrape_url);
            }else{
               return "Preview URL is empty - please update";
            }
        }

        //replace cachebuster with random number
        if(strpos($creative->creative,'%%CACHEBUSTER%%')){
            $ranNum = rand(100000, 999999);
            $creative->creative = preg_replace('/%%CACHEBUSTER%%/', $ranNum, $creative->creative);
        }
        if(strpos($creative->creative,'%%cachebuster%%')){
            $ranNum = rand(100000, 999999);
            $creative->creative = preg_replace('/%%cachebuster%%/', $ranNum, $creative->creative);
        }


        //if regular creative, not snapchat, grab page scrape etc.
        if(!empty($preview->scrape_url) && $adtype->name != "Snapchat"){
           //generate iframe
           $sHTML = "<html><head><style>body{margin:0; padding:0}</style></head><body id='CS_creative'>";
           $sHTML .= $creative->creative;
           $sHTML .= "<script src='/assets/js/lib/gremlins.min.js'></script>";
           $sHTML .= "</body></html>";

           $filename = "iframeSrc.html";

           // Build path: <uploaddir>/advertiserId/CampaignId/ProjectId/iframeSrc
           $destinationPath = Config::get('iax.upload.upload_dir') . DIRECTORY_SEPARATOR;
           $destinationPath .= $project->campaign->advertiser->id . DIRECTORY_SEPARATOR;
           $destinationPath .= $project->campaign->id . DIRECTORY_SEPARATOR;
           $destinationPath .= $project->id . DIRECTORY_SEPARATOR;

           $filePath = $destinationPath;
           $filePath .= $filename;

           //check if folder structure exists, if not make it
           if (!file_exists($destinationPath)) {
               mkdir($destinationPath, 0777, true);
           }

           $uploaded = file_put_contents ( $filePath , $sHTML );
           if( $uploaded ){
               $iframeWidth = $adtype->slot_width;
               $iframeHeight = $adtype->slot_height;

               //pass iframe data to preview page, then use JS to insert like DFP would
               $data['iframeWidth'] = $iframeWidth;
               $data['iframeHeight'] = $iframeHeight;
               $data['iframeSrc'] = $filePath;

           }

           //determine if using DFP or not
           $previewTime = $project->updated_at;
           $cutoffTime = Carbon::createFromDate(2014, 10, 01, 'America/New_York');
           if($previewTime > $cutoffTime /*&& !strpos($preview->scrape_url,'ulive') */){
               $data['isDFP'] = "true";
           }else{
               //non dfp method
               $data['isDFP'] = "false"; //$creative->creative;
           }
           $data['preview_source'] = $html;


           // Add base tag for pages using relative paths
           $url = parse_url($preview->scrape_url);
           $baseTag = '<base href="' . $url['scheme'] . '://' . $url['host'] . '/"  />';
           // Append after title tag
           if (strpos($preview->scrape_url,'cookingchannel') || strpos($preview->scrape_url,'travelchannel') || strpos($preview->scrape_url,'diynetwork')){
               $data['preview_source'] = preg_replace('/<head>/', '<head>' .  $baseTag, $data['preview_source']);
               //cooking doesn't seem to work unless base tag comes first
           }else{
               $data['preview_source'] = preg_replace('/<\/title>/', '</title>' .  $baseTag, $data['preview_source']);
           }


           //include font for DIY
           if(strpos($preview->scrape_url,'diynetwork')){
               $data['preview_source'] = preg_replace('/<\/title>/', '</title>' .  '<link href="http://fonts.googleapis.com/css?family=Open+Sans+Condensed:700" rel="stylesheet" type="text/css"><style>header#diy-header.main .nav>ul.section-nav>li>a, header#diy-header.main .nav>#global-footer .footer-list.section-nav>li>a, #global-footer .nav>.section-nav>li>a{ font-family:"Open Sans Condensed", sans-serif;} </style>', $data['preview_source']);
           }

           //fix ulive video
           if(strpos($preview->scrape_url,'ulive')){
               $data['preview_source'] = preg_replace('/<\/title>/', '</title>' .  '<style>video{opacity:1 !important; }</style>', $data['preview_source']);
           }

           //change page title
           $title = 'Interactive Ad Experience | Preview';
           $data['preview_source'] = preg_replace('/<title>(.*)<\/title>/', '<title>' .  $title . '</title>', $data['preview_source']);

           //if mobile
           if(strpos($data['adtype'],'mobile')){
               //put page scrape into iframe

               //set cookie and stylesheets for mobile overrides
               if(strpos($preview->scrape_url,'hgtv')){
                   setcookie("layout", "mobile", time()+3600);
                   $data['preview_source'] = preg_replace('/<\/head>/', '<link media="all" type="text/css" rel="stylesheet" href="http://iax.scrippsonline.com/assets/css/siteOverrides/hgtv.css"></head>', $data['preview_source']);
               }
               else if(strpos($preview->scrape_url,'travelchannel')){
                   $data['preview_source'] = preg_replace('/<\/head>/', '<link media="all" type="text/css" rel="stylesheet" href="http://iax.scrippsonline.com/assets/css/siteOverrides/travel.css"></head>', $data['preview_source']);
               }
               else if(strpos($preview->scrape_url,'food.com')){
                   $data['preview_source'] = preg_replace('/<\/head>/', '<link media="all" type="text/css" rel="stylesheet" href="http://iax.scrippsonline.com/assets/css/siteOverrides/foodCom.css"></head>', $data['preview_source']);
               }

               $previewHTML = $data['preview_source'];

               $previewName = "previewPage.html";

               $previewPath = $destinationPath;
               $previewPath .= $previewName;

               $previewUploaded = file_put_contents ( $previewPath , $previewHTML );
               if( $previewUploaded ){
                    $data['previewSrc'] = $previewPath;
               }
               $data['preview_url'] = $preview->scrape_url;
               return View::make('previews.mobile', $data);
           }
           //if PGI
           else if(strpos($data['adtype'],'Photo Gallery Interstitial') && $site->name == "HGTV"){
              if(!strpos($preview->scrape_url, 'photos.hgtv.com')){
                   //$tmpURL = "http://iax.scrippsonline.com/templates/hgtvGallery.html";
                   //$data['preview_source'] = file_get_contents($tmpURL);
               }
               return View::make('previews.pgi', $data);
           }

           return View::make('previews.iframe', $data);
       }

       //if snapchat use blank video blade, no preview URL
       else if ($adtype->name == "Snapchat") {
          $data['content'] = json_encode($creative->creative);
          return View::make('previews.video', $data);
       }

       else {
          return Response::json(array('status' => 'fail', 'message' => 'Unable to create preview'));
       }
    }


	/* Mobile preview */
	public function previewMobile($preview_id){
		$data['preview'] = $preview = Preview::where('preview_id', '=', $preview_id)->first();
        $data['creative'] = $creative = Creative::find($preview->creative_id);
        $data['project'] = $project = Project::where('id', $creative->project_id)->with('adtype')->first();
        $data['campaign'] = $campaign = Campaign::find($project->campaign_id);
        $data['adtype'] = $adtype = AdType::find($project->adtype_id);
        $data['advertiser'] = $advertiser = Advertiser::find($campaign->advertiser_id);
        $data['adtag'] = $adtag = AdTag::find($project->adtag_id);

        // get mobile version of preview site
        $opts = array('http' =>
            array(
                'header'  => 'User-agent: Mozilla/5.0 (iPhone; U; CPU like Mac OS X; en) AppleWebKit/420.1 (KHTML, like Gecko) Version/3.0 Mobile/3B48b Safari/419.3',
                //'header'  => 'Mozilla/5.0 (iPhone; CPU iPhone OS 7_0 like Mac OS X; en-us) AppleWebKit/537.51.1 (KHTML, like Gecko) Version/7.0 Mobile/11A465 Safari/9537.53'
            )
        );
        $context  = stream_context_create($opts);
        $html = file_get_contents($preview->scrape_url, false, $context);
		$data['preview_source'] = $html;

        //create iframe for creative content
        $sHTML = "<html><head><style>body{margin:0; padding:0}</style></head><body>";
        $sHTML .= $creative->creative;
        $sHTML .= "</body></html>";

        $filename = "iframeSrc.html";

        // Build path: <uploaddir>/advertiserId/CampaignId/ProjectId/iframeSrc
        $destinationPath = Config::get('iax.upload.upload_dir') . DIRECTORY_SEPARATOR;
        $destinationPath .= $project->campaign->advertiser->id . DIRECTORY_SEPARATOR;
        $destinationPath .= $project->campaign->id . DIRECTORY_SEPARATOR;
        $destinationPath .= $project->id . DIRECTORY_SEPARATOR;

        $filePath = $destinationPath;
        $filePath .= $filename;

        //check if folder structure exists, if not make it
        if (!file_exists($destinationPath)) {
            mkdir($destinationPath, 0777, true);
        }

        $uploaded = file_put_contents ( $filePath , $sHTML );
        if( $uploaded ){
            // Image files saved. Now create DB record
            $iframeWidth = 320;
            $iframeHeight = 50;
            $data['iframe'] = $filePath;
        }

        $url = parse_url($preview->scrape_url);
        $baseTag = '<base href="' . $url['scheme'] . '://' . $url['host'] . '/" />';
		$data['preview_source'] = preg_replace('/<\/title>/', '</title>' .  $baseTag, $data['preview_source']);
        $data['preview_url'] = $preview->scrape_url;


		return View::make('previews.mobile', $data);
	}

}
