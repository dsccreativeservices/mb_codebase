<?php

use Iax\Repositories\ProjectRepositoryInterface;
use Iax\Repositories\ImageRepositoryInterface;

class ImageController extends BaseController {

    protected $image;
    protected $project;

    public function __construct(ImageRepositoryInterface $image, ProjectRepositoryInterface $project)
    {
        parent::__construct();

        $this->image = $image;
        $this->project = $project;

        //Check CSRF token on POST
        // $this->beforeFilter('csrf', array('on' => 'get'));
    }

    /**
     * Returns images for a given project_id ordered by pos
     */
    public function showProjectImages($pid)
    {
        /* this is a public area so need to determine if logged in or not */

        /*
        $data['images'] = $this->image->getAll($pid);
        $data['project'] = Project::find($pid);

        return View::make('frontend.projects.images', $data);
        */

        $data['images'] = $this->image->getAll($pid);

        $requestedRound = Input::get("round");
        if(isset($requestedRound)){
            //return images for the queried round
            $r = $requestedRound - 1;
            $data['images'] = Image::where('project_id', '=', $pid)->where('round', $r)->orderBy('pos', 'ASC')->get();
            $data['roundNum'] = $requestedRound;
        }else{
            //return images for current (highest) round
            $currRound = 0;
            foreach ($data['images'] as $img) {
                if($img->round > $currRound){
                    $currRound = $img->round;
                    $data['roundNum'] = $currRound + 1;
                }
            }
            $data['images'] = Image::where('project_id', '=', $pid)->where('round', $currRound)->orderBy('pos', 'ASC')->get();
        }

        return View::make('frontend.projects.images', $data);
    }
}
