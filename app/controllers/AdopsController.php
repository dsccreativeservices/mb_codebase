<?php

use Iax\Repositories\ProjectRepositoryInterface;

class AdopsController extends GuestBaseController {

    /**
     * @var \Iax\Repositories\ProjectRepositoryInterface
     */
    protected $project;

    public function __construct(ProjectRepositoryInterface $project)
    {
        parent::__construct();

        //Check CSRF token on POST
        $this->beforeFilter('csrf', array('on' => 'post'));

        View::share('isAdmin', FALSE);

        // Inject instanec of ProjectRepositoryInterface
        $this->project = $project;
    }

    public function getCompletedProjects(){
        $data['projects'] = $this->project->getCompletedProjects();
        $data['isAdmin'] = FALSE;
        return View::make('adops.completed', $data);
    }

    public function getProject($pid){
        $data['project'] = $project = $this->project->getByProjectId($pid);

        //modify S3 URLs into CloudFront URLs
        $creative = $project->creatives->first()->creative;
        $data['creative'] = $this->project->modifyCreativeForTraffic($creative);

        return View::make('adops.traffic', $data);
    }

    public function getProjectRaw($pid){
        $project = $this->project->getByProjectId($pid);

        $creative = $project->creatives->first()->creative;
        $filename = Config::get('iax.file_download') . '/' . $project->campaign->advertiser->name . '_' .  $project->campaign->name . '_' . $project->name . '.html';

        $filename = strtolower(str_replace(' ', '_', $filename));

         File::put($filename, $creative);

        // Create a plaintext response
        return Response::download($filename);
    }

}
