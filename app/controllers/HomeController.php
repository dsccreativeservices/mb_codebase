<?php

use Iax\Repositories\ProjectRepositoryInterface;

class HomeController extends BaseController {

	protected $project;

	public function __construct(ProjectRepositoryInterface $project)
	{
		parent::__construct();

		$this->project = $project;
	}

	public function index()
	{
		if( !Sentry::check() )
		{
			// Not logged in, or session expired.
			return Redirect::to('/login');
		} else {
			$u = Sentry::getUser();

			//if viewer redirect to projects
			$viewer = Sentry::findGroupByName('Viewers');
			if( $u ){
				if($u->inGroup($viewer)){
					return Redirect::to('/projects');
				}
	    	}

            //all project by user
			$data['projects'] = $this->project->getRecentByUser($u->id);

			//active campaigns
			    $date = new DateTime;
			    $date->modify('-10 weeks');
			    $cutoffDate = $date->format('Y-m-d H:i:s');


			    $HomeManagers = Sentry::findGroupByName('HomeManagers');
				$FoodManagers = Sentry::findGroupByName('FoodManagers');
			    if( $u ){
			        //for managers
			        if($u->inGroup($HomeManagers)){
						//HGTV = 2    DIY = 3    Travel = 6    GAC = 11
						$siteFilter = array(2,3,6,11);

						$data['activeProjects'] = Project::with('campaign.advertiser', 'site', 'adtype')
							->whereHas('site', function($q) use ($siteFilter){
								$q->whereIn('id', $siteFilter);
							})
			                ->where('status', '!=', 'complete')
			                ->where('updated_at', '>', $cutoffDate)
			                ->where('created_at', '>', $cutoffDate)
			                ->orderby('updated_at', 'desc')->get();

						$data['headline'] = "Active Home or Travel Projects";
			        }
					elseif($u->inGroup($FoodManagers)){
						//FN = 1    FCOM = 9    Cooking = 12
						$siteFilter = array(1,9,12);

						$data['activeProjects'] = Project::with('campaign.advertiser', 'site', 'adtype')
							->whereHas('site', function($q) use ($siteFilter){
								$q->whereIn('id', $siteFilter);
							})
			                ->where('status', '!=', 'complete')
			                ->where('updated_at', '>', $cutoffDate)
			                ->where('created_at', '>', $cutoffDate)
			                ->orderby('updated_at', 'desc')->get();

						$data['headline'] = "Active Food Projects";
			        }
			        //for designers
			        else{
			            $data['activeProjects'] = Project::with('campaign.advertiser', 'site', 'adtype')
			                ->where('created_by', '=', $u->id)
			                ->where('status', '!=', 'complete')
			                ->where('updated_at', '>', $cutoffDate)
			                ->where('created_at', '>', $cutoffDate)
			                ->where('updated_by', '=', $u->id)
			                ->where('adtype_id', '!=', 42)
			                ->orderby('updated_at', 'desc')->get();

						$data['headline'] = "Your Recent Active Projects";
			        }
			    }
				/*
				$data['activeProjects'] = Project::with('campaign.advertiser', 'site', 'adtype')
			        ->where('created_by', '=', $u->id)
			        ->where('status', '!=', 'complete')
			        ->where('updated_at', '>', $cutoffDate)
			        ->where('created_at', '>', $cutoffDate)
			        ->where('updated_by', '=', $u->id)
			        ->where('adtype_id', '!=', 42)
			        ->orderby('updated_at', 'desc')->get();
				*/

			//END active campaigns

			return View::make("frontend.home.index")->with($data);
		}
	}

}
