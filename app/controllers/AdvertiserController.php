<?php

use Iax\Repositories\AdvertiserRepositoryInterface;

class AdvertiserController extends BaseController {

    protected $advertiser;

    public function __construct(AdvertiserRepositoryInterface $advertiser)
    {
        parent::__construct();

        //Check CSRF token on POST
        $this->beforeFilter('csrf', array('on' => 'post'));

        $this->advertiser = $advertiser;
    }

    /**
     * Show the advertiser listing page.
     *
     * @return Response
     */
    public function index()
    {
        $data['advertisers'] = $this->advertiser->getAll();
        $data['advertisers_alpha'] = $this->advertiser->getAllWithAlpha();

        return View::make('frontend.advertisers.index')->with($data);
    }

    /**
     * Get details by id
     * @return Response
     */
    public function getDetail($advertiser_id)
    {
        $data['advertiser'] = Advertiser::find($advertiser_id);
        //$data['campaigns'] = Campaign::where('advertiser_id', '=', $advertiser_id)->orderBy('name')->get();
		$data['campaigns'] = Campaign::where('advertiser_id', '=', $advertiser_id)->orderby('updated_at', 'desc')->paginate(20);

        // $data['campaigns']->load('useractions.user'); // eager load the user and useractions associated with each campaign

        return View::make('frontend.advertisers.detail')->with($data);
    }

}
