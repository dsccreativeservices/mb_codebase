<?php

class SupportController extends BaseController {

    public function __construct()
    {
        parent::__construct();

        //Check CSRF token on POST
       $this->beforeFilter('csrf', array('on' => 'post'));
    }

    public function get_create()
    {
        return View::make('frontend.support.create');
    }


    public function thanks()
    {
        return View::make('frontend.support.thanks');
    }

    public function post_create()
    {
        $input = Input::all();

        $rules = array(
            'ticketName' => 'required',
            'ticketType' => 'required',
            'ticketSeverity' => 'required',
            'ticketBody' => 'required'
        );

        $v = Validator::make($input, $rules);

        if( !$v->fails() ){
            $user = Sentry::getUser();

            // Send ticket to uservoice
            // Attempt sending reset email
            try {
                $data['body'] = $input['ticketBody'];
                $data['severity'] = $input['ticketSeverity'];
                $data['type'] = $input['ticketType'];
                $data['email'] = $user->email;
                $data['name'] = $user->first_name . " " . $user->last_name;

                Mail::queue('email.support', $data, function($message) use ($input, $user)
                {
                    $message->to('tickets@iax.uservoice.com')
                        ->subject($input['ticketName']);
                });

                return Redirect::to('support/thanks');

            } catch (Swift_TransportException $e) {
                Log::error($e->getMessage());
            }


        } else {
            return Redirect::to('support/create')->withErrors($v)->withInput();
        }


    }

}
