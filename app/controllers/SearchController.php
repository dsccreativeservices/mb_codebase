<?php

use Iax\Repositories\TagRepositoryInterface;

class SearchController extends BaseController {

	protected $tag;

	public function __construct(TagRepositoryInterface $tag)
	{
		parent::__construct();

		$this->tag = $tag;
	}

	public function basic_search()
	{
		$results = [];

		$term = $results['term'] = strToLower(Input::get('q'));
		// Clockwork::info($term);

		if( strpos($term, Config::get('iax.search.flag_separator')) !== FALSE ){
			// Clockwork::info('advanced search');
			// pass to advanced
			$this->advanced_search($term);
		}

		/* * Basic Search processing */
		if( is_numeric($term) ){
			// ID Search
			$results['advertisers'] = Advertiser::where('id', '=', $term)->orderBy('name', 'asc')->get();
			$results['campaigns'] = Campaign::where('id', '=', $term)->orderBy('name', 'asc')->get();
			$results['projects'] = Project::where('id', '=', $term)->orderBy('name', 'asc')->get();
			$results['creatives'] = Creative::where('id', '=', $term)->orderBy('name', 'asc')->get();
		} else {
			// String Search

			if( strlen($term) < Config::get('iax.search.min') ){
				Session::flash('flash-error', 'Search terms must be greater than 3 characters unless doing an ID search');
				return View::make('frontend.search.results', $results);
			}

			$results['advertisers'] = Advertiser::where('name', 'LIKE', "%$term%")->orderBy('name', 'asc')->get();
			$results['campaigns'] = Campaign::where('name', 'LIKE', "%$term%")->orderBy('name', 'asc')->get();
			$results['projects'] = Project::where('name', 'LIKE', "%$term%")->orderBy('name', 'asc')->get();
			$results['creatives'] = Creative::where('name', 'LIKE', "%$term%")->orderBy('name', 'asc')->get();
			$results['tags'] = Tag::where('name', 'LIKE', "%$term%")->orderBy('name', 'asc')->get();
			$results['sites'] = Site::where('name', 'LIKE', "%$term%")->orderBy('name', 'asc')->get();
            $results['adTypes'] = AdType::where('name', 'LIKE', "%$term%")->orderBy('name', 'asc')->get();

			
		}
        
        // Generate List Markup using results.
        if(isset($results['advertisers'])){
            $results['advertisers'] = Iax\Helpers::createMultiColumnList($results['advertisers']->toArray(), 3, 'advertiser');
        }
        if(isset($results['campaigns'])){
            $results['campaigns'] = Iax\Helpers::createMultiColumnList($results['campaigns']->toArray(), 3, 'campaign');
        }
        if(isset($results['projects'])){
            $results['projects'] = Iax\Helpers::createMultiColumnList($results['projects']->toArray(), 3, 'project');
        }
        if(isset($results['tags'])){
            $results['tags'] = Iax\Helpers::createMultiColumnList($results['tags']->toArray(), 3, 'tag');
        }
        if(isset($results['sites'])){
            $results['sites'] = Iax\Helpers::createMultiColumnList($results['sites']->toArray(), 3, 'sites');
        }
        if(isset($results['adTypes'])){
            $results['adTypes'] = Iax\Helpers::createMultiColumnList($results['adTypes']->toArray(), 3, 'adTypes');
        }

		return View::make('frontend.search.results', $results);
	}

	protected function advanced_search($term)
	{
		$results = [];

		// Process search flags array
		$flags = Config::get('iax.search.flags');
		$flag = explode(Config::Get('iax.search.flag_separator'), $term)[0];
		$term = explode(Config::Get('iax.search.flag_separator'), $term)[1];

		switch ($flag) {
			case 'tag':
				// Clockwork::info('Tag search');
				// tag based search
				$results['projects'] = $this->tag->getProjectsByTag($term);

				return $results;

				break;
			default:
				# code...
				break;
		}



		return View::make('frontend.search.results', $results);
	}
}
