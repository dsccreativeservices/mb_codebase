<?php

use Iax\Repositories\ProjectRepositoryInterface;
use Iax\Repositories\ImageRepositoryInterface;

class api_ImageController extends BaseController {

    protected $image;
    protected $project;

    public function __construct(ImageRepositoryInterface $image, ProjectRepositoryInterface $project)
    {
        parent::__construct();

        $this->image = $image;
        $this->project = $project;

        //Check CSRF token on POST
        // $this->beforeFilter('csrf', array('on' => 'get'));
    }

    /**
     * Returns images for a given project_id ordered by pos
     */
    public function getImages()
    {
        $images = $this->image->getAll(Input::get('project_id'));
        return Response::json($images);
    }

    /**
     * Returns images for a given project_id ordered by pos
     */
    public function getImage()
    {
        $image = $this->image->getSingle(Input::get('image_id'));

        return Response::json($image);
    }

    /**
     * Uploads a new image and stores it.
     * @return Array New image info.
     */
    public function uploadImage()
    {
        $input = Input::all();
        $file = Input::file('Filedata');

        // Load the current project data
        $project = $this->project->getByProjectId($input['project_id']);

        // Validate image size and file valid image type (png, jpg, gif, bmp)
        $rules = array(
            'file' => 'image|max:' . Config::get('iax.upload.max_filesize'),
        );

        $validation = Validator::make($input, $rules);

        if ($validation->fails())
        {
            return Response::json($validation->errors->first(), 400);
        }

        // Pass the project object to image add()
        // /iax/app/Iax/Repositories/ImageRepositoryInterface.php
        $newImage = $this->image->add($project, $file);

        if( !$newImage ){
            return Response::json(array('status' => false, 'error' => 'Upload Failed: Duplicate filename.'), 400);
        }

        return Response::json($newImage);
    }

    /*
        Uploads multiple images use the drag and drop data
        returns array of new images and info
    */
    public function multiImages(){
        $data = Input::all();
        /*  project: id,
            names: names,
            data: data,*/

        $project = Project::find($data['project']);
        $nameArray = $data['names'];
        $dataArray = $data['data'];

        //find directory for images
        $destinationPath = Config::get('iax.upload.upload_dir') . DIRECTORY_SEPARATOR;
        $destinationPath .= $project->campaign->advertiser->id . DIRECTORY_SEPARATOR;
        $destinationPath .= $project->campaign->id . DIRECTORY_SEPARATOR;
        $destinationPath .= $project->id . DIRECTORY_SEPARATOR;

        //check if folder structure exists, if not make it
        if (!file_exists($destinationPath)) {
            mkdir($destinationPath, 0777, true);
        }

        //find position
        $highestPosImage = DB::table('images')->where('project_id', '=', $project->id)->orderBy('pos', 'DESC')->first();
        if( $highestPosImage ){
            $newPos = $highestPosImage->pos + 1;
        } else {
            $newPos = 0; // First image
        }

        //loop through images from arrays and create each, then save
        $numProjects = count($data['names']);
        $imgPaths = array();
        $imgID = array();
        for ($i = 0; $i < $numProjects; $i++) {
            $imgName = $nameArray[$i];
            $imgData = $dataArray[$i];
            $imgPath = $destinationPath . $imgName;

            if(!strpos($imgName, '.jpg')){
                return Response::json(array('status' => false, 'message' => "Image is not a jpg. Only jpg files are allowed"));
            }
            elseif(file_exists($imgPath)){
                return Response::json(array('status' => false, 'message' => "Image already exists. Replace by dragging new image onto thumbnail"));
            }
            else{
                list($type, $imgData) = explode(';', $imgData);
                list(, $imgData)      = explode(',', $imgData);
                $uploaded = file_put_contents($imgPath, base64_decode($imgData));
                if( $uploaded ){

                    //create thumbnail
                    $imgSrc = $imgPath;
                    //getting the image dimensions
                    list($width, $height) = getimagesize($imgSrc);
                    //saving the image into memory (for manipulation with GD Library)
                    $myImage = imagecreatefromjpeg($imgSrc);
                    // calculating the part of the image to use for thumbnail
                    if ($width > $height) {
                      $y = 0;
                      $x = ($width - $height) / 2;
                      $smallestSide = $height;
                    } else {
                      $x = 0;
                      $y = ($height - $width) / 2;
                      $smallestSide = $width;
                    }
                    // copying the part into thumbnail
                    $thumbSize = 120;
                    $thumb = imagecreatetruecolor($thumbSize, $thumbSize);
                    imagecopyresampled($thumb, $myImage, 0, 0, $x, $y, $thumbSize, $thumbSize, $smallestSide, $smallestSide);

                    //create the file path
                    $thumbFilename = $imgName;
                    $thumbExtention = pathinfo($thumbFilename, PATHINFO_EXTENSION); //find image type (extension)
                    $withoutExt = preg_replace('/\\.[^.\\s]{3,4}$/', '', $thumbFilename); //remove file extension
                    $thumbPath = $destinationPath . $withoutExt . '_thumb.' . $thumbExtention; //compile file path

                    //$thumbUploaded = file_put_contents($thumbPath, $thumb); //save to server
                    $thumbUploaded = imagejpeg($thumb, $thumbPath);

                    if($thumbUploaded){
                        $newImage = new Image();
                        $newImage->project_id = $project->id;
                        $newImage->round = $data['round'];
                        $newImage->title = "";
                        $newImage->description = "";
                        $newImage->path = $imgPath; //change image path
                        $newImage->thumbPath = $thumbPath;
                        $newImage->pos = $newPos + $i;

                        $newImage->save();

                        //create array to return

                        $imgPaths[$i] = $newImage->path;
                        $imgID[$i] = $newImage->id;

                    }
                }
            }
        }


        try {
            //$image->save();

            return Response::json(array(
                'status' => true,
                'message' => "Image uploaded",
                'paths' => $imgPaths,
                'id' => $imgID
            ));
        } catch (Exception $e) {
            return Response::json(array(
                'status' => false,
                'error' => "Image upload failed"
            ));
        }
    }

    /**
     * Update details an image.
     * @return Response JSON response with status of change.
     */
   public function updateImage()
    {
        $input['image_id'] = Input::get('id');
        $input['title'] = Input::get('title');
        $input['desc'] = Input::get('desc');

        $image = Image::find($input['image_id']);

        $image->title = $input['title'];
        $image->description = $input['desc'];

        try {
            $image->save();

            return Response::json(array(
                'status' => "success",
                'message' => "Image info updated successfully"
            ));
        } catch (Exception $e) {
            return Response::json(array(
                'status' => "error",
                'message' => "Image info update failed"
            ));
        }
    }

    /**
     * Replaces the current image with a new updated one, no text updates
     * @return Response JSON response with status of change.
     */
    public function updateImageOnly(){
        $data = Input::all();

        $image = Image::find($data['id']);
        $filePath = $image->path; //path of existing image
        $filename = $data['name']; //name for new image
        $destinationPath = dirname($filePath) . "/"; //directory of existing image

        $imagePath = $destinationPath;
        $imagePath .= $filename; //file path for new image


        //update file
        $imageData = $data['newImg'];
        list($type, $imageData) = explode(';', $imageData);
        list(, $imageData)      = explode(',', $imageData);
        //$data = base64_decode($imageData);
        $uploaded = file_put_contents($imagePath, base64_decode($imageData));
        if( $uploaded ){

            //create thumbnail
            $imgSrc = $imagePath;
            //getting the image dimensions
            list($width, $height) = getimagesize($imgSrc);
            //saving the image into memory (for manipulation with GD Library)
            $myImage = imagecreatefromjpeg($imgSrc);
            // calculating the part of the image to use for thumbnail
            if ($width > $height) {
              $y = 0;
              $x = ($width - $height) / 2;
              $smallestSide = $height;
            } else {
              $x = 0;
              $y = ($height - $width) / 2;
              $smallestSide = $width;
            }
            // copying the part into thumbnail
            $thumbSize = 120;
            $thumb = imagecreatetruecolor($thumbSize, $thumbSize);
            imagecopyresampled($thumb, $myImage, 0, 0, $x, $y, $thumbSize, $thumbSize, $smallestSide, $smallestSide);

            //create the file path
            $thumbFilename = $filename;
            $thumbExtention = pathinfo($thumbFilename, PATHINFO_EXTENSION); //find image type (extension)
            $withoutExt = preg_replace('/\\.[^.\\s]{3,4}$/', '', $thumbFilename); //remove file extension
            $thumbPath = $destinationPath . $withoutExt . '_thumb.' . $thumbExtention; //compile file path

            //$thumbUploaded = file_put_contents($thumbPath, $thumb); //save to server
            $thumbUploaded = imagejpeg($thumb, $thumbPath);

            if($thumbUploaded){
                $image->path = $imagePath; //change image path
                $image->thumbPath = $thumbPath;

                $image->save();
                return Response::json(array('status' => true, 'success' => "Update succeeded", 'thumb' => $thumbPath, 'image' => $imagePath));
            }
        }

    }

    /**
     * Updates the position values for a set of images.
     * @return Response JSON response with status of change.
     */
    public function updateImageSort()
    {
        if( Request::ajax() ){
            // get the passed in data
            $sort_input = Input::get("list");

            //loop through and upate images
            $status = FALSE;
            foreach( $sort_input as $image ){
                $status = $this->image->updatePositions($image["id"], $image["index"]);
            }
            if( $status ){
                return Response::json(array(
                    'status' => true,
                    'message' => "Image order updated successfully."
                ));
            } else {
                return Response::json(array(
                    'status' => false,
                    'message' => "Not complete yet."
                ));
            }
        }
    }

    /**
     * Delete image from application by calling remove method on image repository
     * @return
     */
    public function removeImage()
    {
        return Response::json($this->image->remove(Input::get('image_id')));
    }
}
