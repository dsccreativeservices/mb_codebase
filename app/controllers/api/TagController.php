<?php

class api_TagController extends BaseController {

    public function __construct()
    {
        parent::__construct();
    }

    public function getAutocomplete()
    {
        $t = Input::get('t');

        $tags = Tag::where('name', 'LIKE', $t . '%')->select('name')->get();

        return $tags;
    }

    public function removeTagFromProject()
    {
        $tag_name = Input::get('tag_name');
        $project_id = Input::get('project_id');

        $tag = Tag::where('name', '=', $tag_name)->get()->first();

        try {
            $p = Project::find($project_id)->tags()->detach($tag->id);
            return array(
                'status' => 'success',
                'message' => 'Tag removed successfully.'
                );
        } catch (Exception $e) {
            return array(
                'status' => 'fail',
                'message' => 'Tag removal failed.'
                );
        }
    }

    public function addTagToProject()
    {
        $tag_name = Input::get('tag_name');
        $project_id = Input::get('project_id');

        $tag = Tag::where('name', '=', $tag_name)->get()->first();

        // If tag is new, create it first
        if( is_null($tag) ){
            $tag = Tag::create(array(
                'name' => $tag_name
            ));
        }

        try {
            $p = Project::find($project_id)->tags()->attach($tag->id);
            return array(
                'status' => 'success',
                'message' => 'Tag added successfully.'
                );
        } catch (Exception $e) {
            return array(
                'status' => 'fail',
                'message' => 'Tag adding failed.'
                );
        }
    }
}