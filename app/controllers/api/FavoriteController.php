<?php

use Iax\Repositories\ProjectRepositoryInterface;
use Iax\Repositories\ImageRepositoryInterface;

//setup structure for returning favorites to IAX
class IAX_project{}

class api_FavoriteController extends BaseController {

    protected $user;
    protected $image;
    protected $project;

    public function __construct(ImageRepositoryInterface $image, ProjectRepositoryInterface $project)
    {
        parent::__construct();

        //Check CSRF token on GET
        //$this->beforeFilter('csrf', array('on' => 'get'));

        $this->user = Sentry::getUser();

        $this->image = $image;
        $this->project = $project;
    }

    /**
     * Create a new favorite record for current user and provided project id
     */
    public function toggleFavorite(){
        $project_id = Input::get('project_id');

        // Check if favorite exists (this code is alittle ugly)
        if( is_null(Favorite::whereRaw('user_id = ' . $this->user->id . ' AND project_id = ' . $project_id)->first()) ){
            // Create Favorite
            Favorite::create(array(
                'user_id' => $this->user->id,
                'project_id' => $project_id
            ));

            $data['status'] = 'success';
            $data['is_favorite'] = TRUE;
        } else {
            // Delete Favorite
            $f = Favorite::whereRaw('user_id = ' . $this->user->id . ' AND project_id = ' . $project_id)->first();
            $f->delete();
            $data['status'] = 'success';
            $data['is_favorite'] = FALSE;
        }

        return Response::json($data);
    }

    /* return projects for IAX showcase site */
    public function getAll(){
        $data = Input::all();
        $adType = AdType::where('name', $data['type'])->first();
        $type = $adType->id;


        //get all projects that are favorites
        $projects = Project::whereHas('favorites', function($q){
            $q->where('user_id', '>', '0');
        })
        ->whereHas('adtype', function($q) use ($type){
            $q->where('id', "=", $type);
        })
        ->orderby('updated_at', 'desc')->get();


        $data['projects'] = array();

        foreach ($projects as $p) {
            //$p->thumb = $this->image->getAll($p->id);
            $site = Site::find($p->site_id);
            $adType = AdType::find($p->adtype_id);
            $newP = new IAX_project;
            $newP->name = $p->name;
            $newP->site = $site->name;
            $newP->adType = $adType->name;
            //find flat comp
            if(Image::where('project_id', '=', $p->id)->exists()){
                $source = Image::where('project_id', '=', $p->id)->first();
                $newP->image = $source->path;
            }
            //find preview URL
            //first find Creative
            if( $p->has('creatives') ){
                $creative = $p->creatives->first();
                if( Preview::where('creative_id', '=', $creative->id)->exists() ){
                    $newP->url = 'http://iax.scrippsonline.com/preview/iframe/' . $creative->preview->preview_id;
                }
            }
            array_push($data['projects'],$newP);
        }

        //$data['user'] = encrypt("mellis");
        //$data['pass'] = encrypt("pass");

        //$data['images'] = $this->image->getAll($pid);

        return $data['projects'];
    }


}
