<?php

use Iax\Repositories\CampaignRepositoryInterface;

class api_CampaignController extends BaseController {

    public function __construct()
    {
        parent::__construct();

        //Check CSRF token on POST
        $this->beforeFilter('csrf', array('on' => 'post'));
    }

    /**
     * Get all Campaigns
     *
     * @return Response
     */
    public function getAll()
    {
        $campaigns = Campaign::api_all();
        return Response::json($campaigns);
    }

    /**
     * Get campaigns with name and id for use in form select elements.
     * @param  integer $adv_id If 0 load all camapaigns
     * @return Response        Returns JSON response.
     */
    public function getSelectCampaigns($adv_id)
    {
        if( $adv_id === 0){
            $campaigns = Campaign::select('id', 'name')->orderBy('name', 'asc')->lists('name', 'id');
        } else {
            $campaigns = Campaign::select('id', 'name')->where('advertiser_id', '=', $adv_id)->orderBy('name', 'asc')->lists('name', 'id');
        }

        // Add in create option.
        $campaigns = array('0' => 'Select&hellip;') + $campaigns + array('new' => '-- Create New --');

        $intKey = 1; // Each item is put in a property with the intKey as the key to handle Chromes crap with object sorting.
        foreach ( $campaigns as $key => $value ){
            $data[$intKey] = array('key' => $key, 'value' => $value);
            $intKey++;
        }

        return Response::json($data);
    }

    /**
     * Save a new Campaign. Returns new Campaign id.
     * @return Mixed New Campaign object on success. False on fail.
     */
    public static function new_campaign()
    {
        $data = Input::all();

        $a = new Campaign();
        $a->name = $data['campaign_name'];
        $a->advertiser_id = $data['advertiser_id'];
        $a->created_by = Sentry::getUser()->id;

        try {
             $a->save();
             return $a;
        } catch (Exception $e) {
            return false;
        }
    }

    public function update_campaign()
    {
        $input = Input::all();
        if( isset($input['pk']) && isset($input['value']) ){
            $campaign = Campaign::find($input['pk']);
            $campaign->name = $input['value'];
            try {
                $campaign->save();
                return Response::json(array(
                    "status" => "success",
                    "msg" => "Campaign name updated successfully."
                ));
            } catch (Exception $e) {
                return Response::json(array(
                    "status" => "failed",
                    "msg" => "Error saving campaign update."
                ));
            }

        } else {
            return Response::json(array(
                "status" => "failed",
                "msg" => "Incorrect data provided."
            ));
        }
    }

	
	public function delete_campaign()
    {
        
		$data = Input::all();

        if( Campaign::where('id', '=', $data['campaign_id'])->exists() ){
            $campaign = Campaign::where('id', '=', $data['campaign_id'])->first();
            
            // delete campaign
            $campaign->delete();
			
			//find projects associated with campaign and also delete
			$associatedProjects = $campaign->load(array('projects.useractions' => function($q){
				$q->where('item_type', '=', 'project');
			}));
			$associatedProjects->delete();

            return Response::json(array('status' => 'success', 'message' => 'Campaign deleted.'));
        } else {
            return Response::json(array('status' => 'fail', 'message' => 'Campaign does not exist.'));
        }

    }
    
    public function multiPreview(){
        $data = Input::all();

        $campaign = Campaign::where('id', '=', $data['campaign_id'])->first();
        $name = $data['name'];
  
        $preview_id = rand(100000, 999999); //using preview_id as campaign_id TO DO: make this not random

        try {
            $preview = new Preview();
            $preview->site_id = 1; // try as name of preview
            //$preview->creative_id = $data['name']; // id of new creative
            $preview->scrape_url = $data['URL'];
            $preview->preview_id = $preview_id;
            $preview->creative_items = serialize($data['itemArray']); //pass in the array of items to use in the preview
            $preview->name = $data['name'];
            $preview->campaign_id = $data['campaign_id'];

            $preview->save();
            return Response::json(array('status' => 'success', 'message' => 'Multi-Preview Created.', 'name' => $preview->name, 'url' => $preview->scrape_url, 'id' => $preview->preview_id ));
        } catch (Exception $e) {
            return Response::json(array('status' => 'error', 'message' => 'Something went wrong.'));
        }
  
    }
    public function deletePreview(){
        $data = Input::all();

        if( Preview::where('preview_id', '=', $data['preview_id'])->exists() ){
            $preview = Preview::where('preview_id', '=', $data['preview_id'])->first();
            
            // delete project
            $preview->delete();

            return Response::json(array('status' => 'success', 'message' => 'Preview deleted.'));
        } else {
            return Response::json(array('status' => 'fail', 'message' => 'Preview does not exist.'));
        }   
    }

}