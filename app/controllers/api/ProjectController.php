<?php

use Iax\Repositories\ProjectRepositoryInterface;
use Iax\Repositories\ImageRepositoryInterface;

class api_ProjectController extends BaseController {

    protected $image;
    protected $project;

    public function __construct(ImageRepositoryInterface $image, ProjectRepositoryInterface $project)
    {
        parent::__construct();

        $this->image = $image;
        $this->project = $project;

        //Check CSRF token on POST
        $this->beforeFilter('csrf', array('on' => 'get'));
    }

    /**
     * Create a new project item
     * @return Mixed       Id of new project on success or null.
     */
    public function new_project()
    {
        $data = Input::get();

        // Create new project
        $project = new Project();

        $project->campaign_id = $data['campaign_id'];
        $project->name = $data['project_name'];
        $project->adtype_id = $data['adtype'];
        $project->status = ( $data['status'] === 'undefined' ) ? 'internal' : $data['status'];
        $project->created_by = $data['user_id'];
        $project->updated_by = $data['user_id'];
        $project->site_id = $data['site_id'];
        $project->adtag_id = $data['adtag_id'];

        // Try to save
        try {
            $project->save(); //save to db

            // Create an empty Creative object for the new project.
            $creative = new Creative();
            $creative->project_id = $project->id;
            $creative->name = $project->name;
            $creative->created_by = $project->created_by;
            $creative->locked = 0;

            $creative->save(); //save empty creative

            $data['id'] = $project->id;
            $data['name'] = $project->name;

            return Response::json($data);
        } catch (Exception $e) {
            return Response::json(array('status' => 'fail', 'message' => 'Unable to store new project.'));
        }
    }

    public function update_project()
    {
        $data = Input::all();

        if( Project::where('id', '=', $data['project_id'])->exists() ){
            $project = Project::where('id', '=', $data['project_id'])->first();

            // Set project fields
            $project->campaign_id = $data['campaign_id'];
            $project->name = $data['project_name'];
            $project->adtype_id = $data['adtype_id'];
            $project->adtag_id = $data['adtag_id'];
            $project->status = ( $data['status'] === 'undefined' ) ? 'internal' : $data['status'];
            $project->site_id = $data['site_id'];
            $project->updated_by = $data['user_id'];

            // Check status to set locking state and update lock state for related creative
            $creative = Creative::where('project_id', $data['project_id'])->first();
            if( $project->status === "complete" ){
                if( $creative ){
                    // Ready for traffic. Lock creative
                    $creative->locked = TRUE;
                    $creative->save();
                }
            } else {
                if( $creative ){
                    $creative->locked = FALSE;
                    $creative->save();
                }
            }
            // Save project
            $project->save();

            return Response::json($project);
        } else {
            return Response::json(array('status' => 'fail', 'message' => 'Project does not exist.'));
        }
    }

    public function update_creative()
    {
        $data = Input::all();


        if( Creative::where('project_id', '=', $data['project_id'])->exists() ){
            // Update existing creative
            $creative = Creative::where('project_id', '=', $data['project_id'])->first();
        } else {
            // create new creative
            $creative = new Creative();
        }

        //find project
        $project = Project::where('id', '=', $data['project_id'])->first();

        // Set creative fields
        $creative->project_id = $data['project_id'];
        $creative->name = "Project Creative";

        if(!empty($data['creative'])){
            $creative->creative = $data['creative'];
        }

        //URL validation, except for snapchat and native ads which should skip this
        //RSI is 5
        //natives are 49, 50, 51, 53, 54
        //snapchat = 52
        if(!($project->adtype_id == 52)){
          if(!empty($data['project_url'])){
            //validate URL
            $curl = curl_init($data['project_url']);
            curl_setopt($curl, CURLOPT_NOBODY, true);
            $result = curl_exec($curl);
            if($result !== false) {
               $statusCode = curl_getinfo($curl, CURLINFO_HTTP_CODE);
               if($statusCode == 404){
                  return Response::json(array('status' => 'fail', 'message' => 'Preview URL is not a valid URL'));
               }else{
                  $creative->url = $data['project_url'];
               }
            }else{
               return Response::json(array('status' => 'fail', 'message' => 'Preview URL is not a valid URL'));
            }
          }else{
             //project URL empty - only provide error for ads not native since by default they get a preview URL value
             if(!($project->adtype_id >= 49 && $project->adtype_id <= 54) && $project->adtype_id != 5){
                return Response::json(array('status' => 'fail', 'message' => 'Preview URL is empty'));
             }
          }
       }
        // END URL validation
        $creative->created_by = $data['user_id'];

        $creative->locked = FALSE;

        // Save creative to DB
        $creative->save();



        // updated project with modified user data
        $project = Project::where('id', '=', $data['project_id'])->first();
        $project->updated_by = $data['user_id'];
        $project->save();

        $p = $this->project->generateCreativePreview($creative->id);

        if( $p ){
            $creative->previewUrl = $p['preview'];
            $creative->sourceUrl = $p['preview'];
        }

        return Response::json($creative);
    }

    public function getDetails($pid)
    {
        return Response::json($this->project->getByProjectId($pid));
    }

	public function delete_project(){

		$data = Input::all();

        if( Project::where('id', '=', $data['project_id'])->exists() ){
            $project = Project::where('id', '=', $data['project_id'])->first();

            // delete project
            $project->delete();

            return Response::json(array('status' => 'success', 'message' => 'Project deleted.'));
        } else {
            return Response::json(array('status' => 'fail', 'message' => 'Project does not exist.'));
        }
	}

    public function copy_project(){

		$data = Input::all();
        // Create new project
        $newProject = new Project();

        if( Project::where('id', '=', $data['project_id'])->exists() ){
            $currProject = Project::where('id', '=', $data['project_id'])->first();

            $newProject->campaign_id = $currProject->campaign_id;
            $newProject->name = $currProject->name .= "_copy";
            $newProject->adtype_id = $currProject->adtype_id;
            $newProject->status = ( $data['status'] === 'undefined' ) ? 'internal' : $data['status'];
            $newProject->created_by = $data['user_id'];
            $newProject->site_id = $currProject->site_id;
            $newProject->adtag_id = $currProject->adtag_id;

            // Try to save
            try {
                $newProject->save(); //save to db

                //if adtype is native copy creative, else create new empty
                //44/45 for staging       49/51 for production
                if($newProject->adtype_id == 49 || $newProject->adtype_id == 51){
                    //get current creative
                    if( Creative::where('project_id', '=', $data['project_id'])->exists() ){
                        $currCreative = Creative::where('project_id', '=', $data['project_id'])->first();
                        //setup new creative
                        $creative = new Creative();
                        $creative->project_id = $newProject->id;
                        $creative->name = $newProject->name;

                        $creative->url = $currCreative->url;
                        $creative->created_by = $newProject->created_by;
                        $creative->locked = 0;

                        //modify creative with new JS file URL
                        $newCreative = $currCreative->creative;
                        if(strpos($newCreative,'cs-replacer')){
                            //split the string into pieces
                            $pieces = explode('id="cs-replacer" src="', $newCreative);
                            $beforeDataURL = $pieces[0] . 'id="cs-replacer" src="';
                            $scriptString = explode('"', $pieces[1]);
                            $dataURL = $scriptString[0];
                            $afterDataURL = '"' . $scriptString[1];

                            //get file at URL and save to server
                            $JS = file_get_contents($dataURL);

                            $destinationPath = Config::get('iax.upload.upload_dir') . DIRECTORY_SEPARATOR;
                            $destinationPath .= $newProject->campaign->advertiser->id . DIRECTORY_SEPARATOR;
                            $destinationPath .= $newProject->campaign->id . DIRECTORY_SEPARATOR;
                            $destinationPath .= $newProject->id . DIRECTORY_SEPARATOR;
                            $filePath = $destinationPath;
                            $fileName = $newProject->name . "-copiedJS.js";
                            $fileName = str_replace(' ', '', $fileName);
                            $filePath .= $fileName;


                            if (!file_exists($destinationPath)) {
                                mkdir($destinationPath, 0777, true);
                            }
                            $uploaded = file_put_contents ( $filePath , $JS );

                            //modify data URL in creative
                            if( $uploaded ){
                                $newCreative = $beforeDataURL . $fileName . $afterDataURL;
                                $creative->creative = $newCreative;
                            }
                        }
                        try {
                            $creative->save(); //save creative

                            //setup preview
                            $preview = new Preview();
                            $preview->site_id = $newProject->site_id ; // TODO: Add site id to project table and load that.
                            $preview->creative_id = $creative->id; // id of new creative
                            $preview->scrape_url = $creative->url;
                            $preview->preview_id = $preview_id = md5(Sentry::getUser()->email . $creative->url . $creative->id);
                            try {
                                $preview->save();
                            } catch (Exception $e) {
                                return Response::json(array('status' => 'fail', 'message' => 'Unable to create new preview'));
                            }
                        } catch (Exception $e) {
                            return Response::json(array('status' => 'fail', 'message' => 'Unable to create new creative (copy content)'));
                        }
                    }
                }else{
                    // Create an empty Creative object for the new project.
                    $creative = new Creative();
                    $creative->project_id = $newProject->id;
                    $creative->name = $newProject->name;
                    $creative->created_by = $newProject->created_by;
                    $creative->locked = 0;
                    try {
                        $creative->save(); //save creative
                    } catch (Exception $e) {
                        return Response::json(array('status' => 'fail', 'message' => 'Unable to create new creative'));
                    }
                }

                $data['id'] = $newProject->id;
                $data['name'] = $newProject->name;
                $data['site'] = $newProject->site_id;

                return Response::json($data);
            } catch (Exception $e) {
                return Response::json(array('status' => 'fail', 'message' => 'Unable to copy project.' . $e));
            }
        } else {
            return Response::json(array('status' => 'fail', 'message' => 'Project does not exist.'));
        }
	}

    public function setTraffic(){
        $data = Input::all();

        if( Project::where('id', '=', $data['project_id'])->exists() ){
            $project = Project::where('id', '=', $data['project_id'])->first();
            $project->status = "complete";

            // Save project
            try {
                $project->save();
                return Response::json(array('status' => 'success', 'message' => 'Project successfully set to traffic.'));
            } catch (Exception $e) {
                return Response::json(array('status' => 'fail', 'message' => 'Unable to set project to traffic.'));
            }
        }
    }


    public function save_mod(){
        $data = Input::all();

        if( Project::where('id', '=', $data['project'])->exists() ){
            $project = Project::where('id', '=', $data['project'])->first();

            //save to creative slot in DB
            if( Creative::where('project_id', '=', $data['project'])->exists() ){
                // Update existing creative
                $creative = Creative::where('project_id', '=', $data['project'])->first();
            } else {
                // create new creative
                $creative = new Creative();
            }

            //write content to JS file and save to server
            $script = $data["JSString"];
            $filename = "data.js";

            $destinationPath = Config::get('iax.upload.upload_dir') . DIRECTORY_SEPARATOR;
            $destinationPath .= $project->campaign->advertiser->id . DIRECTORY_SEPARATOR;
            $destinationPath .= $project->campaign->id . DIRECTORY_SEPARATOR;
            $destinationPath .= $project->id . DIRECTORY_SEPARATOR;
            /*$destinationPath = "adftp.scrippsnetworks.com/opt/internet/adimages/ad_images/iax" . DIRECTORY_SEPARATOR;
            $destinationPath .= date("Y") . DIRECTORY_SEPARATOR;
            $destinationPath .= "temp" . DIRECTORY_SEPARATOR;
            $destinationPath .= $project->campaign->advertiser->name . DIRECTORY_SEPARATOR;
            $destinationPath .= $project->campaign->id . DIRECTORY_SEPARATOR;
            $destinationPath .= $project->id . DIRECTORY_SEPARATOR;*/

            $filePath = $destinationPath;
            $filePath .= $filename;

            /*FTP method to adimages

            $adimages = "adftp.scrippsnetworks.com";
            $username = "iax-mellis";
            $pass = "dvc34fnr";
            $conn_id = ftp_connect($adimages) or die("Could not connect to $adimages");
            $login_result = ftp_login($conn_id, $username, $pass);
            //ftp_chdir($conn_id, "/opt/internet/adimages/ad_images/iax/");

            //check if folder structure exists, if not make it
            if (!file_exists($destinationPath)) {
                //mkdir($destinationPath, 0777, true);
                function ftp_mksubdirs($ftpcon,$ftpbasedir,$ftpath){
                   @ftp_chdir($ftpcon, $ftpbasedir); // /var/www/uploads
                   $parts = explode('/',$ftpath); // 2013/06/11/username
                   foreach($parts as $part){
                      if(!@ftp_chdir($ftpcon, $part)){
                         ftp_mkdir($ftpcon, $part);
                         ftp_chdir($ftpcon, $part);
                         //ftp_chmod($ftpcon, 0777, $part);
                      }
                   }
                }
                $path_of_storage = 'opt/internet/adimages/ad_images/iax/2015/temp/';
                $newftpdir = 'advertiser/campaign/project';
                ftp_mksubdirs($conn_id,$path_of_storage,$newftpdir);
            }
            $filePath = 'opt/internet/adimages/ad_images/iax/2015/temp/' . 'advertiser/campaign/project' . $filename;
            $uploaded = ftp_put($conn_id, $filePath, $script, FTP_ASCII);

            // close the connection
            ftp_close($conn_id);
            */
            if (!file_exists($destinationPath)) {
                mkdir($destinationPath, 0777, true);
            }
            $uploaded = file_put_contents ( $filePath , $script );

            if( $uploaded ){
                //get html template for this module and compile code to save in creative slot for preview
                $site = Site::where('id', '=', $project->site_id)->first();

                //template location such as "templates/FN/3Across/template.html"
                $tURL = "http://" . $_SERVER['SERVER_NAME'];
                $tURL .= "/templates" . DIRECTORY_SEPARATOR;
                $tURL .= $site->code . DIRECTORY_SEPARATOR;
                $tURL .= $data['moduleType'] . DIRECTORY_SEPARATOR;
                $tURL .= "template.html";
                $template = file_get_contents($tURL);
                $filePath = "http://" . $_SERVER['SERVER_NAME'] . DIRECTORY_SEPARATOR . $filePath; //remove when using adimages for data files instead of iax.
                $template .= '<script src="' . $filePath . '"></script>';// add data URL into HTML template

                // Set creative fields and save
                $creative->project_id = $data['project'];
                $creative->name = "Project Creative";
                $creative->creative = $template; //this is the html template for preview purposes
                $creative->temp_data = $data['JSObject']; //this is the JS object of all the data for repopulating the form fields.
                $creative->url = $data['previewURL'];
                $creative->created_by = $data['user_id'];
                $creative->locked = FALSE;
                $creative->save(); // Save creative to DB


                //send to builder
                //project id
                //user





                return Response::json(array('status' => 'success', 'message' => 'Saved JS file.'));
            }else{
                return Response::json(array('status' => 'fail', 'message' => 'JS file failed to save.'));
            }
        }else{
            return Response::json(array('status' => 'fail', 'message' => 'Project does not exist.'));
        }

    }


}
