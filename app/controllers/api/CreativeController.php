<?php

class api_CreativeController extends BaseController {

    public function __construct()
    {
        parent::__construct();

        //Check CSRF token on POST
        $this->beforeFilter('csrf', array('on' => 'post'));
    }

    /**
     * Get all creatives
     *
     * @return Response
     */
    public function getAll()
    {
        $creatives = Creative::all();

        return Response::json($creatives);
    }

    public function getByProject($id)
    {
        $creatives = Creative::where('creatives.project_id', '=', $id)
            ->join('users', 'created_by', '=', 'users.id')
            ->select('creatives.id', 'creatives.name', 'creatives.created_at')
            ->get();
        return Response::json($creatives);
    }

    /**
     * Toggles the locked status for the given creative.
     * @param  Integer $cid Id for creative
     * @return Boolean New locked status
     */
    public function toggle_lock($cid)
    {
        $response = array();
        try {
            $creative = Creative::find($cid);
            $creative->locked = ($creative->locked) ? FALSE : TRUE;
            $creative->save();

            $response['status'] = 'ok';
            $response['creative'] = $creative->locked;

        } catch (Exception $e) {
            $response['status'] = 'failed';
            $response['msg'] = 'An error occured while updating creative.';
        }

        return Response::json($response);
    }

    /**
     * Toggles the locked status for the given creative.
     * @param  Integer $cid Id for creative
     * @return Boolean New locked status
     */
    public function lock($cid)
    {
        $response = array();
        try {
            $creative = Creative::find($cid);
            if( $creative->locked ){
                $response['status'] = 'ok';
                $response['creative'] = $creative->locked;
            } else {
                // Updated record
                $creative->locked = TRUE;
                $creative->save();
                $response['status'] = 'ok';
                $response['creative'] = $creative->locked;
            }
        } catch (Exception $e) {
            $response['status'] = 'failed';
            $response['msg'] = 'An error occured while updating creative.';
        }

        return Response::json($response);
    }

     /**
      * Toggles the locked status for the given creative.
      * @param  Integer $cid Id for creative
      * @return Boolean New locked status
      */
     public function unlock($cid)
     {
         $response = array();
         try {
             $creative = Creative::find($cid);
             if( $creative->locked ){
                 // Updated record
                 $creative->locked = FALSE;
                 $creative->save();
                 $response['status'] = 'ok';
                 $response['creative'] = $creative->locked;
             } else {
                $response['status'] = 'ok';
                $response['creative'] = $creative->locked;
             }
         } catch (Exception $e) {
             $response['status'] = 'failed';
             $response['msg'] = 'An error occured while updating creative.';
         }

         return Response::json($response);
     }

}