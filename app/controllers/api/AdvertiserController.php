<?php

class api_AdvertiserController extends BaseController {

    public function __construct()
    {
        parent::__construct();

        //Check CSRF token on POST
        $this->beforeFilter('csrf', array('on' => 'post'));
    }

    /**
     * Get all Advertisers
     *
     * @return Response
     */
    public function getAll()
    {
        $advertisers = Advertiser::orderBy('name', 'asc')->lists('name', 'id');

        // Add in create option.
        $advertisers = array(0 => 'Select&hellip;') + $advertisers;
        $advertisers = $advertisers + array('new' => '-- Create New --');

        $intKey = 1; // Each item is put in a property with the intKey as the key to handle Chromes crap with object sorting.
        foreach ( $advertisers as $key => $value ){
            $data[$intKey] = array('key' => $key, 'value' => $value);
            $intKey++;
        }

        return Response::json($data);
    }


    /**
     * Save a new Advertiser. Returns new advertiser id.
     * @return Mixed New Advertiser object on success. False on fail.
     */
    public static function new_advertiser()
    {
        $u = Sentry::getUser();

        $data = Input::all();

        $a = new Advertiser();
        $a->name = $data['advertiser_name'];
        $a->created_by =$u->id;

        try {
             $a->save();
             return $a;
        } catch (Exception $e) {
            return false;
        }

    }

    /* update advertiser name */
    public function update_advertiser()
    {
        $input = Input::all();
        if( isset($input['pk']) && isset($input['value']) ){
            $advertiser = Advertiser::find($input['pk']);
            $advertiser->name = $input['value'];
            try {
                $advertiser->save();
                return Response::json(array(
                    "status" => "success",
                    "msg" => "Advertiser name updated successfully."
                ));
            } catch (Exception $e) {
                return Response::json(array(
                    "status" => "failed",
                    "msg" => "Error saving advertiser update."
                ));
            }
        } else {
            return Response::json(array(
                "status" => "failed",
                "msg" => "Incorrect data provided."
            ));
        }
    }
}
