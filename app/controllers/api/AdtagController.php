<?php

class api_AdTagController extends BaseController {

    public function __construct()
    {
        parent::__construct();

        //Check CSRF token on POST
        $this->beforeFilter('csrf', array('on' => 'post'));
    }

    /**
     * Get all AdTags for a site.
     *
     * @return Response
     */
    public function getAdTagsBySite($site_id)
    {
        if( $site_id ){
            $matchingAdtags = AdTag::where('site_id', $site_id)->orderBy('name', 'asc')->get();//->lists('name', 'id', 'adtype');
            //$adtags = array(0 => 'Select Ad Tag&hellip;') + $matchingAdtags;

            return $matchingAdtags;


            /*$intKey = 1; // Each item is put in a property with the intKey as the key to handle Chromes crap with object sorting.
            foreach ( $adtags as $key => $value ){
                $data[$intKey] = array('key' => $key, 'value' => $value);
                $intKey++;
            }
            */
        }

        return Response::json($data);
    }
}
