<?php

class api_AdtypeController extends BaseController {

    public function __construct()
    {
        parent::__construct();

        //Check CSRF token on POST
        $this->beforeFilter('csrf', array('on' => 'post'));
    }

    /**
     * Get all AdTags for a site.
     *
     * @return Response
     */
    public function getAdTypesBySite($site_id)
    {
        if( $site_id ){
            $adtypes = AdType::where('site', $site_id)->orderBy('name', 'asc')->lists('name', 'id');
            $adtypes = array(0 => 'Select type&hellip;') + $adtypes;

            $intKey = 1; // Each item is put in a property with the intKey as the key to handle Chromes crap with object sorting.
            foreach ( $adtypes as $key => $value ){
                $data[$intKey] = array('key' => $key, 'value' => $value);
                $intKey++;
            }

        }

        return Response::json($data);
    }
    
    /*
        Given a specific ad type get all on a specific site
    */
    public function getProjectsWithAdtypeAndSite(){
        
        
        return Response::json(array(
            'status' => true,
            'message' => "Filter Successful",
        ));
    }
}