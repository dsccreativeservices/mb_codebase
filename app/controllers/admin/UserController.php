<?php namespace controllers\admin;

use Input, Validator, View, Sentry, DB, Session, Redirect, Exception;

class UserController extends AdminController {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		// $data['users'] = Sentry::getUserProvider()->createModel()->orderBy('last_name', 'asc')->get();
		$data['users'] = Sentry::getUserProvider()->createModel()
			->leftJoin('throttle', function($join){
					$join->on('throttle.user_id', '=', 'users.id')
						->where('throttle.banned', '=', 1);
				})
			->select(array(
				'users.id',
				'users.email',
				'users.first_name',
				'users.last_name',
				'users.activated',
				'throttle.banned'
			))
			->orderBy('users.last_name', 'asc')
			->get();

		// \Iax\Helpers::debug($data['users']);

		return View::make('admin.user.index', $data);
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function newUser()
	{
        $data['user'] = Sentry::getUser();
        $data['groups'] = DB::table('groups')->lists('name', 'id');

        // Create new user
        return View::make('admin.user.new', $data);

	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function create()
	{
		$input['email'] = Input::get('email');
		$input['first_name'] = Input::get('first_name');
		$input['last_name'] = Input::get('last_name');
		$input['password'] = Input::get('password');
		$input['password_confirm'] = Input::get('password_confirm');
		$input['group'] = Input::get('group_id');

		$rules = array(
		    'email' => 'required',
		    'first_name' => 'required',
		    'last_name' => 'required',
		    'password' => 'required',
		    'password_confirm' => 'required|same:password'
		);

		$v = Validator::make($input, $rules);

		if( $v->fails() ){
		    return Redirect::back()->withErrors($v)->withInput();
		} else {
			try{
				//create username from email
				$username = explode("@", $input['email']);

				$user = Sentry::createUser(array(
					'username'  => $username[0],
			    	'email' => $input['email'],
			    	'first_name' => $input['first_name'],
			    	'last_name' => $input['last_name'],
			    	'password' => $input['password'],
			    	'activated' => true,
			    ));

			    // Find the group and add to user
			    $group = Sentry::findGroupById($input['group']);
			    $user->addGroup($group);

			    Session::flash('msg', $user['email'] . " account setup successfully!");
			    return Redirect::to('/admin/users');

			} catch (Exception $e) {
				Session::flash('flash-error', "There was an error while setting up this account.", $e);

                $data['groups'] = DB::table('groups')->lists('name', 'id');
			    return View::make('admin.user.new', $data);
			}
		}
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		//
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		$data['user'] = Sentry::findUserById($id);
		$data['user_group'] = $data['user']->getGroups()->first();

		$data['groups'] = DB::table('groups')->lists('name', 'id');
		return View::make('admin.user.edit', $data);
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		try
		{
			$user = Sentry::findUserById($id);

		    $user->email = Input::get('email');
		    $user->first_name = Input::get('first_name');
		    $user->last_name = Input::get('last_name');
		    $user->password = Input::get('password');

		    $user->save();

		    // Find the group and add to user
		    $group = Sentry::findGroupById(Input::get('group_id'));
		    $user->addGroup($group);

		    Session::flash('flash-info', $user['email'] . " account updated successfully!");
		    return Redirect::to('/admin/users');
		} catch (Exception $e) {
		    Session::flash('flash-error', "There was an error while updating up this account.");
		    return Redirect::to('/admin/users');
		}
	}

	public function ban()
	{
		$id = Input::get('user_id');

		try
		{
		    // Find the user using the user id
		    $throttle = Sentry::findThrottlerByUserId($id);

		    // Ban the user
		    $throttle->ban();

		    return Redirect::to('admin/users')->with('flash-info', "User #$id was banned successfully. This user will no longer be able to access the system.");
		}
		catch (Cartalyst\Sentry\Users\UserNotFoundException $e)
		{
		    return Redirect::to('admin/users')->with('flash-error', "User #$id does not exist.");
		}
	}

	public function unban()
	{
	    $id = Input::get('user_id');

	    try
	    {
	        // Find the user using the user id
	        $throttle = Sentry::findThrottlerByUserId($id);

	        // Ban the user
	        $throttle->unBan();

	        return Redirect::to('admin/users')->with('flash-info', "User #$id was un-banned successfully. This user will no longer be able to access the system.");
	    }
	    catch (Cartalyst\Sentry\Users\UserNotFoundException $e)
	    {
	        return Redirect::to('admin/users')->with('flash-error', "User #$id does not exist.");
	    }
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		//
	}

}
