<?php namespace controllers\admin;

use Input, Validator, View, Session, Redirect, Exception, AdTag, AdType, Site;

class AdtagController extends AdminController {

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        //$data['adtags'] = AdTag::all();
        //$data['adtags']->load('site');
        $data['adtags'] = AdTag::orderBy('site_id')->get();


        return View::make('admin.adtags.index', $data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        $data['sites'] = Site::all()->lists('name', 'id');

        return View::make('admin.adtags.new', $data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function store()
    {
        $input['name'] = Input::get('name');
        $input['tag_selector'] = Input::get('tag_selector');
        //$input['slot_width'] = Input::get('slot_width');
        //$input['slot_height'] = Input::get('slot_height');
        //$input['regex'] = Input::get('regex');
        $input['site_id'] = Input::get('site_id');

        $rules = array(
            'name' => 'required',
            'tag_selector' => '',
            'regex' => '',
            'site_id' => 'required'
            );

        $v = Validator::make($input, $rules);

        if( $v->fails() ){
            return Redirect::back()->withErrors($v)->withInput();
        } else {
            try
            {
                $adtag = AdTag::create(array(
                    'name' => $input['name'],
                    'tag_selector' => $input['tag_selector'],
                    //'slot_width' => $input['slot_width'],
                    //'slot_height' => $input['slot_height'],
                    //'regex' => $input['regex'],
                    'site_id' => $input['site_id'],
                    ));

                Session::flash('flash-success', "New ad tag created successfully!");
                return Redirect::to('/admin/adtags');
            } catch (Exception $e) {
                Session::flash('flash-error', "There was an error creating ad tag.");
                return View::make('admin.adtags.new');
            }
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function edit($id)
    {
        $data['adtag'] = AdTag::find($id);
        $data['adtypes'] = AdType::orderBy('name', 'asc')->get()->lists('name', 'id');
        $data['sites'] = Site::all()->lists('name', 'id');

        return View::make('admin.adtags.edit', $data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function update($id)
    {
        $input['name'] = Input::get('name');
        $input['tag_selector'] = Input::get('tag_selector');
        //$input['regex'] = Input::get('regex');
        $input['site_id'] = Input::get('site_id');
        $input['adtype_id'] = Input::get('adtype_id');

        $rules = array(
            'name' => 'required',
            'tag_selector' => '',
            'regex' => '',
            'site_id' => 'required',
            'adtype_id' => 'required'
            );

        $v = Validator::make($input, $rules);

        if( $v->fails() ){
            return Redirect::back()->withErrors($v)->withInput();
        } else {
            try
            {
                $adtag = AdTag::find($id);
                $adtag->name = $input['name'];
                $adtag->tag_selector = $input['tag_selector'];
                //$adtag->regex = $input['regex'];
                //$adtag->slot_width = $input['slot_width'];
                //$adtag->slot_height = $input['slot_height'];
                $adtag->site_id = $input['site_id'];
                $adtag->adtype = json_encode($input['adtype_id']);
                $adtag->save();

                Session::flash('flash-success', "Ad tag updated successfully!");
                return Redirect::to('/admin/adtags');
            } catch (Exception $e) {
                Session::flash('flash-error', "There was an error updating ad tag.");
                return View::make('admin.adtags.new');
            }
        }

    }

    /**
     * Delete an ad type by its id. adtype_id is set in POST variable.
     * @return Response
     */
    public function delete()
    {
        $id = Input::get('adtag_id');
        $adtag= AdTag::find($id);
        if( count($adtag->projects) ){
            return Redirect::to('admin/adtags')->with('flash-warn', "Ad Tag#$id is currently used by one or more projects. You must update any projects using this adtag before deleting.");
        } else {
            $adtag->delete();
            return Redirect::to('admin/adtags')->with('flash-success', "Ad Tag #$id was removed successfully.");
        }
    }

}
