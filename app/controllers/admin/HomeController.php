<?php


class admin_HomeController extends BaseController {
    /**
     * Instantiate a new UserController
     */
    public function __construct()
    {
        //Check CSRF token on POST
        $this->beforeFilter('csrf', array('on' => 'post'));

        // Admin uses its own filter so no need to call parents constructor. Might make a admin base controller at some point.
        $this->beforeFilter('admin_auth');
    }

    /**
     * Return admin page.
     *
     * @return Response
     */
    public function index()
    {
        //
    }


    /*=====================================
    =            User Creation            =
    =====================================*/
    /**
     * Show the create user form
     * @return Response
     */
    public function show_create_user(){
        //
        return View::make('admin.create_user');
    }

    /**
     * Create new user.
     * @return [type]
     */
    public function post_create_user(){
        // TODO: Form validation

        $data = array();

        $userData['email'] = Input::get('email');
        $userData['first_name'] = Input::get('first_name');
        $userData['last_name'] = Input::get('last_name');
        $userData['password'] = Input::get('password');
        // $userData['password_confirm'] = Input::get('password_confirm');

        // New Sentry user
        try
        {
            $user = Sentry::getUserProvider()->create($userData);
            Session::flash('msg', $user['email'] . " account setup successfully!");
            return View::make('admin.create_user');
        } catch (Exception $e) {
            // uhoh
            Session::flash('flash-error', "There was an error while setting up this account.");
            return View::make('admin.create_user');
        }
    }

    /**
     * Get all the current users.
     * @return Response
     */
    public function get_users(){

    }

    /**
     * Shows the current users permissions
     * @return Response
     */
    public function get_group_permissions($gid){

        $data = array();

        try {
            $data['current_group'] = Sentry::findGroupById($gid);
            $data['groups'] = Sentry::findAllGroups();

            $data['current_permissions'] = $data['current_group']->getPermissions();
            $data['all_permissions'] = Permission::select("name")->get();

            // Becasue disabled permission are not stored in the DB for groups, the current list is
            // compared with the full list and missing items are added as "0" items for creating the switchboard.
            $data['perms'] = array();
            for ($i=0; $i < count($data['all_permissions']); $i++) {
                // If a permission is missing, it is considered off for the form creation.
                if( array_key_exists($data['all_permissions'][$i]->name, $data['current_permissions'] ) ){
                    $data['perms'] = array_add($data['perms'], $data['all_permissions'][$i]->name, "1");
                } else {
                    $data['perms'] = array_add($data['perms'], $data['all_permissions'][$i]->name, "0");
                }
            } // end list creation

            return View::make('admin.permissions', $data);

        } catch (Cartalyst\Sentry\Groups\GroupNotFoundException $e) {
            Session::flash("error", "There is no group associated with that ID.");
            return View::make('admin.permissions', $data);
        }

    }


    public function post_group_permissions(){
    	// Grab all the permission form values.
        $input_permissions = Input::except(array('_token', 'group_id'));
        $gid = Input::get('group_id');

        // Convert keys back to periods in prep for storage. Browser submits with _ (ie: user_create to user.create)
        $prepped_permissions = array();
        foreach ($input_permissions as $key => $value) {
            $prepped_permissions[str_replace("_", ".", $key)] = $value;
        }

        $group = Sentry::findGroupById($gid);

        // Update permissions and set flash message.
        try {
            $group->permissions = $prepped_permissions;
            $group->save();
            Session::flash("success", "Permissions updated successfully.");
            return Redirect::to('/admin/group/' . $gid);
        } catch (Exception $e) {
            Session::flash("error", "Unabled to update group permissions. <br> Error Message: " . $e);
            return Redirect::to('/admin/group/' . $gid);
        }
    }
}