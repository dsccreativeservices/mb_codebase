<?php namespace controllers\admin;

use Iax\Repositories\ProjectRepositoryInterface;
use Iax\Repositories\UserActionRepositoryInterface;
use Iax\Repositories\UserRepositoryInterface;

class AdminController extends \BaseController {

    protected $user;
    protected $useraction;
    protected $project;

    /**
     * Instantiate a new AdminContoller
     */
    public function __construct(UserRepositoryInterface $user, UserActionRepositoryInterface $useraction, ProjectRepositoryInterface $project)
    {
        parent::__construct();

        //Check CSRF token on POST
        $this->beforeFilter('csrf', array('on' => 'post'));

        $this->user = $user;
        $this->useraction = $useraction;
        $this->project = $project;

        // Give all views access to current user
        \View::share('user', \Sentry::getUser());
    }

    /**
     * Return admin page.
     *
     * @return Response
     */
    public function index()
    {

    }

    /*========================================
    =            Dashboard Controller        =
    ========================================*/
    public function getDashboard(){
        $data['advertisers'] = \Advertiser::count();
        $data['campaigns'] = \Campaign::count();
        $data['projects'] = \Project::count();
        $data['creatives'] = \Creative::count();
        $data['previews'] = \Preview::count();
        $data['users']= \User::count();

        $data['projects_recent'] = \Project::with('campaign.advertiser')->orderBy('created_at')->limit(10)->get();


        $data['useractions'] = $this->useraction->recent(10);

        return \View::make('admin.dashboard.index', $data);
    }

    /*=======================================
    =            Group Management           =
    ========================================*/

    /**
     * Shows the current users permissions
     * @param $gid
     * @return Response
     */
    public function get_group_permissions($gid){
        $data = array();

        try {
            $data['current_group'] = Sentry::findGroupById($gid);
            $data['groups'] = Sentry::findAllGroups();

            $data['current_permissions'] = $data['current_group']->getPermissions();
            $data['all_permissions'] = Permission::select("name")->get();

            // Becasue disabled permission are not stored in the DB for groups, the current list is
            // compared with the full list and missing items are added as "0" items for creating the switchboard.
            $data['perms'] = array();
            for ($i=0; $i < count($data['all_permissions']); $i++) {
                // If a permission is missing, it is considered off for the form creation.
                if( array_key_exists($data['all_permissions'][$i]->name, $data['current_permissions'] ) ){
                    $data['perms'] = array_add($data['perms'], $data['all_permissions'][$i]->name, "1");
                } else {
                    $data['perms'] = array_add($data['perms'], $data['all_permissions'][$i]->name, "0");
                }
            } // end list creation

            return View::make('admin.groups', $data);

        } catch (Cartalyst\Sentry\Groups\GroupNotFoundException $e) {
            Session::flash("error", "There is no group associated with that ID.");
            return View::make('admin.groups', $data);
        }

    }



    public function post_group_permissions(){
    	// Grab all the permission form values.
        $input_permissions = Input::except(array('_token', 'group_id'));
        $gid = Input::get('group_id');

        // Convert keys back to periods in prep for storage. Browser submits with _ (ie: user_create to user.create)
        $prepped_permissions = array();
        foreach ($input_permissions as $key => $value) {
            $prepped_permissions[str_replace("_", ".", $key)] = $value;
        }

        $group = Sentry::findGroupById($gid);

        // Update permissions and set flash message.
        try {
            $group->permissions = $prepped_permissions;
            $group->save();
            Session::flash("flash-success", "Permissions updated successfully.");
            return Redirect::to('/admin/groups/' . $gid);
        } catch (Exception $e) {
            Session::flash("flash-error", "Unabled to update group permissions. <br> Error Message: " . $e);
            return Redirect::to('/admin/groups/' . $gid);
        }
    }

    /*========================================
    =            Ads Management              =
    ========================================*/

    /**
     * Get ads listing
     */
    public function get_adtypes(){
        $data['adtypes'] = AdType::all();

        return View::make('admin.ads.index', $data);
    }

}
