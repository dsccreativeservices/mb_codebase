<?php namespace controllers\admin;

use Input, Validator, View, Sentry, DB, Session, Redirect, Exception, AdType;

class AdtypeController extends AdminController {

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        $data['adtypes'] = AdType::orderBy('name')->get();


        return View::make('admin.adtypes.index', $data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        return View::make('admin.adtypes.new');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function store()
    {
        $input['name'] = Input::get('name');
        $input['description'] = Input::get('description');
        $input['slot_width'] = Input::get('slot_width');
        $input['slot_height'] = Input::get('slot_height');

        $rules = array(
            'name' => 'required'
            );

        $v = Validator::make($input, $rules);

        if( $v->fails() ){
            return Redirect::back()->withErrors($v)->withInput();
        } else {
            try
            {
                $adtype = AdType::create(array(
                    'name' => $input['name'],
                    'description' => $input['description'],
                    'slot_width' => $input['slot_width'],
                    'slot_height' => $input['slot_height'],
                    ));

                Session::flash('msg', "New ad type created successfully!");
                return Redirect::to('/admin/adtypes');
            } catch (Exception $e) {
                Session::flash('flash-error', "There was an error creating ad type.");
                return View::make('admin.adtypes.new');
            }
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function edit($id)
    {
        $data['adtype'] = AdType::find($id);

        return View::make('admin.adtypes.edit', $data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function update($id)
    {
        $input['name'] = Input::get('name');
        $input['description'] = Input::get('description');
        $input['slot_width'] = Input::get('slot_width');
        $input['slot_height'] = Input::get('slot_height');

        $rules = array(
            'name' => 'required'
            );

        $v = Validator::make($input, $rules);

        if( $v->fails() ){
            return Redirect::back()->withErrors($v)->withInput();
        } else {
            try
            {
                $adtype = AdType::find($id);
                $adtype['name'] = $input['name'];
                $adtype['description'] = $input['description'];
                $adtype->slot_width = $input['slot_width'];
                $adtype->slot_height = $input['slot_height'];
                $adtype->save();

                Session::flash('flash-success', "Ad type was updated successfully!");
                return Redirect::to('/admin/adtypes');
            } catch (Exception $e) {
                Session::flash('flash-error', "There was an error updating ad type.");
                return View::make('admin.adtypes.edit');
            }
        }
    }

    /**
     * Delete an ad type by its id. adtype_id is set in POST variable.
     * @return Response
     */
    public function delete()
    {
        $id = Input::get('adtype_id');
        $adtype = AdType::find($id);
        if( count($adtype->projects) ){
            return Redirect::to('admin/adtypes')->with('flash-warn', "Ad Type #$id is currently used by one or more projects. You must update any projects using this adtype before deleting.");
        } else {
            $adtype->delete();
            return Redirect::to('admin/adtypes')->with('flash-success', "Ad Type #$id was removed successfully.");
        }
    }

}
