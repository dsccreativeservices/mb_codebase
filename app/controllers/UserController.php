<?php

class UserController extends BaseController {

    public function __construct()
    {
        parent::__construct();

        //Check CSRF token on POST
        $this->beforeFilter('csrf', array('on' => 'post'));
    }

    /**
     * Show user profile for currently logged in user.
     * @return Response
     */
    public function get_profile()
    {
        if(Sentry::check() ){
            $data['user'] = Sentry::getUser();
            $data['group'] = $data['user']->getGroups()->first();

            return View::make('frontend.user.profile', $data);
        }
    }

    public function change_password()
    {
        $input['password'] = Input::get('password');
        $input['password_confirmation'] = Input::get('password_confirmation');

        $user = Sentry::getUser();

        $rules = array(
            'password' => 'required|min:3|confirmed',
            'password_confirmation' => 'required'
            );

        // Create validator object.
        $v = Validator::make($input, $rules);

        if( $v->fails() ) {
            return Redirect::to('profile')->withErrors($v);
        } else {
            // Form validated, lets change the PW
            $user = Sentry::getUser();

            $user->password = $input['password'];
            try {
                $user->save();
                return Redirect::to('/profile')->with('flash-success', 'Password updated successfully.');
            } catch (Exception $e) {
                return Redirect::to('/profile')->with('flash-error', 'An error occured while changing password.');
            }
        }
    }
}
