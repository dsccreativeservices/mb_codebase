<?php

class ClientController extends BaseController {

    protected $user;

    public function __construct()
    {
        parent::__construct();

        //Check CSRF token on POST
        $this->beforeFilter('csrf', array('on' => 'post'));
    }

    public function getIndex()
    {
        $data = array();
        return View::make('frontend.clients.index')->with($data);
    }

}