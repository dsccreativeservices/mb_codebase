<?php

class BaseController extends Controller {

protected $isClient;
protected $isAdmin;
protected $isViewer;

	/**
	 * Instantiate a new BaseController
	 */
	public function __construct()
	{
		$this->isAdmin = FALSE;
		$this->isViewer = FALSE;

	    //Check CSRF token on POST
	    $this->beforeFilter('csrf', array('on' => 'post'));

	    // Make sure user is logged in for all controller if not in client area
	    if (Request::is('client*') )
	    {
	        $this->isClient = TRUE;
	    } else {
	    	$this->beforeFilter('auth');

	    	// Setup admin flag
	    	$admin = Sentry::findGroupByName('Admins');
			$viewer = Sentry::findGroupByName('Viewers');

	    	// Share user across views.
	    	$user = Sentry::getUser();

	    	if( $user ){
	    		View::share('user', $user);
	    		$this->isAdmin = $user->inGroup($admin);
				$this->isViewer = $user->inGroup($viewer);
	    	}

	    	$this->isClient = FALSE;
	    }

	    // Share isClient Flag across views.
	    View::share('isClient', $this->isClient);
	    View::share('isAdmin', $this->isAdmin);
		View::share('isViewer', $this->isViewer);
	}


	/**
	 * Setup the layout used by the controller.
	 *
	 * @return void
	 */
	protected function setupLayout()
	{
		if ( ! is_null($this->layout))
		{
			$this->layout = View::make($this->layout);
		}
	}

}
