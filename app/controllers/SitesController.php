<?php

use Iax\Repositories\ProjectRepositoryInterface;

class SitesController extends BaseController {

    /**
     * @var \Iax\Repositories\ProjectRepositoryInterface
     */
    protected $project;

    public function __construct(ProjectRepositoryInterface $project)
    {
        parent::__construct();

        //Check CSRF token on POST
        $this->beforeFilter('csrf', array('on' => 'post'));

        // Inject instanec of ProjectRepositoryInterface
        $this->project = $project;
    }

    /**
     * Get all projects. Sorted by last updated by default.
     * @return Response
     */
    public function showSites($site_id){			
		$site = Site::find($site_id);
		
		$data = [];
		$data['projects'] = $this->project->getBySites( $site->name );
		$data['site'] = $site->name;
	   
        //return View::make('frontend.sites.index')->with($data);
		return View::make('frontend.sites.index', $data);
    }
}
