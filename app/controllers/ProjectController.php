<?php

use Iax\Repositories\ProjectRepositoryInterface;
use Iax\nativeModules\templates;

class ProjectController extends BaseController {

    /**
     * @var \Iax\Repositories\ProjectRepositoryInterface
     */
    protected $project;
    protected $templates;

    public function __construct(ProjectRepositoryInterface $project)
    {
        parent::__construct();

        //Check CSRF token on POST
        $this->beforeFilter('csrf', array('on' => 'post'));

        // Inject instanec of ProjectRepositoryInterface
        $this->project = $project;
    }

    /**
     * Get all projects. Sorted by last updated by default.
     * @return Response
     */
    public function index()
    {
        if( Input::has('tag') ){
            // Get by tag
            $data['projects'] = $this->project->getByTags( Input::get('tag') );
        } else {
            $data['projects'] = $this->project->getPaginatedProjects();
        }

        return View::make('frontend.projects.index')->with($data);
    }

    /**
     * Shoe new project form. If ids for dropdown options passed, set those to selected.
     * @param  integer $advertiser_id Optional advertiser id
     * @param  integer $campaign_id   Optional campaign id.
     * @return Response
     */
    public function get_new($advertiser_id = 0, $campaign_id = 0)
    {
        $data['advertiser_selected'] = $advertiser_id;
        $data['campaign_selected'] = $campaign_id;

        $data['advertisers'] = Advertiser::orderBy('name', 'asc')->get()->lists('name', 'id');
        $data['campaigns'] = Campaign::all()->lists('name', 'id');
        $data['projects'] = Project::all()->lists('name', 'id');
        //$data['adtypes'] = AdType::all()->lists('name', 'id');
        $data['adtypes'] = AdType::orderBy('name', 'asc')->get()->lists('name', 'id');
        $data['adtags'] = AdTag::all()->lists('name', 'id');
        $data['sites'] = Site::orderBy('name', 'asc')->get()->lists('name', 'id');

        // Add in additional select items for dropdown menus
        $data['advertisers'] = array('0' => 'Select&hellip;') + $data['advertisers'] + array('new' => 'Create New');
        $data['campaigns'] = array('0' => 'Select&hellip;') + $data['campaigns'] + array('new' => 'Create New');
        $data['sites'] = array('0' => 'Select&hellip;') + $data['sites'];
        $data['adtypes'] = array('0' => 'Select&hellip;') + $data['adtypes'];

        return View::make('frontend.projects.new')->with($data);
    }

    /**
     * Create a new creative item.
     * @return Response
     */
    public function post_new()
    {
        $input = array(
            'advertiser_id' => Input::get('advertiser_id'),
            'campaign_id' => Input::get('campaign_id'),
            'project_id' => Input::get('project_id'),
            'adtype_id' => Input::get('adtype_id'),
            'creative_name' => Input::get('creative_name'),
            'creative' => Input::get('creative'),
            'url' => Input::get('preview_url')
            );

        $rules = array(
            'advertiser_id' => 'required',
            'campaign_id' => 'required',
            'project_id' => 'required',
            'creative_name' => 'required',
            'creative' => 'required',
            'url' => 'required|url'
            );

        // Create validator object.
        $v = Validator::make($input, $rules);

        if( $v->fails() )
        {
            // Form validation failed
            return Redirect::to('creative/new')->withErrors($v)->withInput();
        }
        else
        {
            $creative = new Creative();

            $creative->advertiser_id = $input['advertiser_id'];
            $creative->campaign_id = $input['campaign_id'];
            $creative->project_id = $input['project_id'];
            $creative->name = $input['creative_name'];
            $creative->creative = e(($input['creative']));

            // Get project details
            $project = Project::find($creative->project_id);
            // Get Campaign Info
            $campaign = Campaign::find($project->campaign_id);

            try {
                $creative->save();
                // Create a preview item for this creative if one not already setup
                Session::flash('flash-success', "New creative was added successfully.");

                // Preview ID for item
                $preview_id = md5(Sentry::getUser()->email . $input['url'] . $creative->id);

                if( Preview::where('preview_id', '=', $preview_id)->count() == 0 ){
                    try {
                        $preview = new Preview();
                        $preview->site_id = 1; // TODO: Add site id to project table and load that.
                        $preview->creative_id = $creative->id; // id of new creative
                        $preview->scrape_url = $input['url'];
                        $preview->preview_id = $preview_id;

                        $preview->save();
                    } catch (Exception $e) {
                        Session::flash('flash-error', "There was an error setting up the preview.");
                        return Redirect::to('creative/new');
                    }
                }
                else {

                }
                return Redirect::to('creatives');
            } catch (Exception $e) {
                Session::flash('flash-error', "There was an error processing new creative.");
                // return Redirect::to('creative/new');
            }
        }
    }

    /**
     * Show the edit page for a project_id
     * @param  Integer $project_id
     * @return Response
     */
    public function get_edit($project_id)
    {
        $project = $this->project->getByProjectId($project_id);
        $project->load('images');

        // Get favorite status
        $data['favorite'] = Favorite::whereRaw('user_id = ' . Sentry::getUser()->id . ' AND project_id = ' . $project_id)->exists();

        if( $project->has('creatives') ){
            $data['creative'] = $creative = $project->creatives->first();
            $adType = AdType::find($project->adtype_id);

    //////////////////////////////////// MODULE BUILDER STARTUP CODE ////////////////////////////////
            if( $adType->name == 'Native - 1 Across'  ||
                $adType->name == 'Native - 2 Across' ||
                $adType->name == 'Native - 3 Across' ||
                $adType->name == 'RSI Module' ||
                $adType->name == 'Native Ingredient'
            ){

                if(empty($creative->creative)){

                    //setup site vars
                    $site = Site::find($project->site_id);
                    $siteInitials = strtolower($site->code);

                    //find HTML for template
                    //uses Iax/nativeModules/templates.php
                    $templates = new templates;
                    $template = $templates->getTmpl($adType->name, $siteInitials);
                    $creative->creative = $template; //set template to creative slot

                    //setup preview URLs if empty
                    if(empty($creative->url)){
                        $creative->url = $templates->getPreviewURL($siteInitials, $adType->name);
                    }

                    try{
                       $creative->save();
                       // setup new preview
                       $preview_id = md5(Sentry::getUser()->email . $creative->url . $creative->id);
                       if( Preview::where('preview_id', '=', $preview_id)->count() == 0 ){
                           try {
                               $preview = new Preview();
                               $preview->site_id = $site->id;
                               $preview->creative_id = $creative->id; // id of new creative
                               $preview->scrape_url = $creative->url;
                               $preview->preview_id = $preview_id;

                               $preview->save();
                               $data['previewID'] = $preview->preview_id;
                           } catch (Exception $e) {
                               Session::flash('flash-error', "There was an error setting up the preview.");
                               return Redirect::to('creative/new');
                           }
                       }
                    }catch (Exception $e){
                        Session::flash('flash-error', "There was an error saving the creative");
                    }
                }
            }

        }

         //if creative is empty and adtype is Mobile Pushdown and site is FN
        /*if($adType->name == 'Mobile - Pushdown' && $project->site->id == 1){
            if(empty($creative->creative)){
                $FNMobileOverride = '<!-- this code is required to make mobile pushdowns behave correctly on Food Network --><script type="text/javascript">$ = parent.window.jQuery; parent.window.$("body").append("<link rel=\'stylesheet\' href=\'http://sni-ads-creativeservices.s3.amazonaws.com/cs-core/templates/mobilePD/mobilePDCSS.css\' />)</script>';
                $creative->creative = $FNMobileOverride;
                $creative->url = "http://www.foodnetwork.com/";
            }
        }*/


        //$data['sites'] = Site::all()->lists('name', 'id');
        $data['sites'] = Site::orderby('name')->lists('name', 'id');
        //$data['adtypes'] = AdType::all()->lists('name', 'id');
        $data['adtypes'] = AdType::orderby('name')->lists('name', 'id');
        // Get adtags setup for this site.
        $data['adtags'] = AdTag::where('site_id', $project->site_id)->lists('name', 'id');

        $data['project'] = $project;
        return View::make('frontend.projects.edit', $data);
    }

    /**
        filter projects by multiple options
    */
    public function filter(){

        //$data = Input::all();
        parse_str($_SERVER['QUERY_STRING']);

        if ($site > 0){
            $filterSite = Site::find($site);
            $filterSite = array($filterSite->id);
        }
        if ($adType > 0){
            $filterAdType = AdType::find($adType);
            $filterAdType = array($filterAdType->id);
        }
        if ($advertiser > 0){
            $filterAdvertiser = Advertiser::find($advertiser);
            $filterAdvertiser = array($filterAdvertiser->id);
            print_r($filterAdvertiser);
        }

        //if advertiser
        if($advertiser > 0){
            //search advertiser and site
            if($site > 0){
                $data['projects'] = Project::whereHas('site', function($q) use ($filterSite){
                    $q->whereIn('id', $filterSite);
                })
                ->whereHas('advertiser', function($q) use ($filterAdvertiser){
                    $q->whereIn('id', $filterAdvertiser);
                })
                /*->whereHas('site', function($q) use ($filterSite){
                    $q->whereIn('id', $filterSite);
                })*/
                ->orderby('updated_at', 'desc')->paginate(Config::get('iax.page_size'));
            }
            //search advertiser and adType
            else if($adType > 0){

            }
        }

        //if site and adtype
        if($site > 0 && $adType > 0){
            $data['projects'] = Project::whereHas('site', function($q) use ($filterSite){
                $q->whereIn('id', $filterSite);
            })
            ->whereHas('adtype', function($q) use ($filterAdType){
                $q->whereIn('id', $filterAdType);
            })
            ->orderby('updated_at', 'desc')->paginate(Config::get('iax.page_size'));
        }
        //if site only
        else if($site > 0 && $adType == 0){
            $data['projects'] = Project::whereHas('site', function($q) use ($filterSite){
                $q->whereIn('id', $filterSite);
            })
            ->orderby('updated_at', 'desc')->paginate(Config::get('iax.page_size'));
        }
        //if adtype only
        else if($site == 0 && $adType > 0){
            $data['projects'] = Project::whereHas('adtype', function($q) use ($filterAdType){
                $q->whereIn('id', $filterAdType);
            })
            ->orderby('updated_at', 'desc')->paginate(Config::get('iax.page_size'));
        }

        return View::make('frontend.projects.filter')->with($data);
        //return $data['projects'];
        //return Response::json($data);
        //return Response::json(array('status' => 'success', 'message' => 'Filter sucessful'));
    }



}
