<?php

use Iax\Repositories\ProjectRepositoryInterface;

class adTypesController extends BaseController {

    /**
     * @var \Iax\Repositories\ProjectRepositoryInterface
     */
    protected $project;

    public function __construct(ProjectRepositoryInterface $project)
    {
        parent::__construct();

        //Check CSRF token on POST
        $this->beforeFilter('csrf', array('on' => 'post'));

        // Inject instanec of ProjectRepositoryInterface
        $this->project = $project;
	}
	
	public function showType($adType){		
		$adType = AdType::find($adType);
		
		$data = [];
		$data['projects'] = $this->project->getByAdType( $adType->name );
		$data['adtype'] = $adType->name;
	   
		return View::make('frontend.adTypes.index', $data);
    }

    
}
