<?php
/*
| ---------------------------------------------------------------------------
| LoginController.php
| ---------------------------------------------------------------------------
| Author: Andy Hutchins <ahutchins@scrippsnetworks.com>
| ---------------------------------------------------------------------------
|
| All login functionality is handled in this controller. This allows us the put the auth filters
| in thier basecontroller used by all the controllers requiring the user being logged in. This include, log in,
| log out, forgot password, reset password. Anything the user would not be logged in for related to logging in.
|
*/

class LoginController extends GuestBaseController {

	public function __construct()
	{
		parent::__construct();
	}

	/**
	 * Show the login page.
	 *
	 * @return Response
	 */
	public function get_login()
	{
	    if(! Sentry::check() ){
	        // Get the login page.
	        return View::make('frontend.user.login');
	    } else {
	        // Already logged in.
	        return Redirect::to('/');
	    }
	}

	/**
	 * Attempts to login user.
	 * @return Response
	 */
	public function post_login()
	{
	    $input = array(
	        'username' => Input::get('username'),
	        'password' => Input::get('password'),
	        );

	    $rules = array(
	        'username' => 'required',
	        'password' => 'required'
	        );

	    // Create validator object.
	    $v = Validator::make($input, $rules);

	    if( $v->fails() )
	    {
	        // Form validation failed
	        return Redirect::to('login')->withErrors($v)->withInput();
	    }
	    else
	    {
	        try
	        {
	        	// Sentry Authenticatation
	        	if( Input::get('remember') ){
	        		// Authenticate and remember
	        		$authUser = Sentry::authenticateAndRemember($input, false);
	        	} else {
	        		// Just authenticate
	        		$authUser = Sentry::authenticate($input, false);
	        	}
	            // Login was succesful.
	            //return Redirect::to('/');
				return Redirect::intended('/');
	        }
	        catch (Cartalyst\Sentry\Users\LoginRequiredException $e)
	        {
	            Session::flash('flash-error', 'Invalid username or password.');
	            return Redirect::to('login')->withErrors($v)->withInput();
	        }
	        catch (Cartalyst\Sentry\Users\PasswordRequiredException $e)
	        {
	            Session::flash('flash-error', 'Invalid username or password.');
	            return Redirect::to('login')->withErrors($v)->withInput();
	        }
	        catch (Cartalyst\Sentry\Users\WrongPasswordException $e)
	        {
	            Session::flash('flash-error', 'Invalid username or password.');
	            return Redirect::to('login')->withErrors($v)->withInput();
	        }
	        catch (Cartalyst\Sentry\Users\UserNotFoundException $e)
	        {
	            Session::flash('flash-error', 'Invalid username or password.');
	            return Redirect::to('login')->withErrors($v)->withInput();
	        }
	        catch (Cartalyst\Sentry\Users\UserNotActivatedException $e)
	        {
	            Session::flash('flash-error', 'Invalid username or password.');
	            return Redirect::to('login')->withErrors($v)->withInput();
	        }
	        catch (Cartalyst\Sentry\Throttling\UserSuspendedException $e)
	        {
	            Session::flash('flash-error', 'This account has been suspended.');
	            return Redirect::to('login')->withErrors($v)->withInput();
	        }
	        catch (Cartalyst\Sentry\Throttling\UserBannedException $e)
	        {
	            Session::flash('flash-error', 'This account has been banned.');
	            return Redirect::to('login')->withErrors($v)->withInput();
	        }
	    }
	}

	/**
	 * Logout the user.
	 * @return Response
	 */
	public function post_logout()
	{
	    Sentry::logout();
	    Session::flash('flash-info', 'You are now logged out.');
	    return Redirect::to('/login');
	}


	/**
	 * Show the reset password form.
	 * @return Response
	 */
	public function get_reset()
	{
		if( Sentry::check() ){
			return Redirect::to('/');
		}

	    // Show reset form
	    return Response::view('frontend.user.reset');
	}

	/**
	 * Process password reset form post submission.
	 * @return Response
	 */
	public function post_reset()
	{
		if( Sentry::check() ){
			return Redirect::to('/');
		}

	    $input = array(
	        'username' => Input::get('username')
	        );

	    $rules = array(
	        'username' => 'required'
	        );

	    $v = Validator::make($input, $rules);

	    if( $v->fails() )
	    {
	        // Form validation failed
	        return Redirect::to('reset')->withErrors($v)->withInput();
	    }
	    else
	    {
	        // Validation Passed. Proceed with sending reset email
	        try
	        {
	            // Find the user using the user email address
	            $user = Sentry::getUserProvider()->findByLogin($input['username']);
	            $resetCode = $user->getResetPasswordCode();

	            $data['reset_code'] = $resetCode;
	            $data['user_id'] = $user->id;
	            $data['user_fullname'] = $user->first_name . ' ' . $user->last_name;
	            $data['user_email'] = $user->email;

	            // Attempt sending reset email
	            try {
	                Mail::send('email.reset', $data, function($message) use ($data)
	                {
	                    $message->to($data['user_email'], $data['user_fullname'])->subject('IAX Password Reset');
	                });
	            } catch (Swift_TransportException $e) {
	                Log::error($e->getMessage());
	            }
	        }
	        catch (Cartalyst\Sentry\Users\UserNotFoundException $e)
	        {
	            // Log failed attempt.
	            Log::warning('Reset password attempt with unfound user', array(
	                'Message' => $e->getMessage(),
	                'IP' => ( !empty($_SERVER['REMOTE_ADDR']) ) ? $_SERVER['REMOTE_ADDR'] : 'N/A', // IP if available
	                'Forward IP' => ( !empty($_SERVER['HTTP_X_FORWARDED_FOR']) ) ? $_SERVER['HTTP_X_FORWARDED_FOR'] : 'N/A' // Forwarded IP if available
                ));
	        }

	        Session::flash('flash-info', 'Password reset instructions have been sent to the account <strong>' . $input['username'] . '</strong>.');
	        return Redirect::to('/login');
	    }
	}

	/**
	 * Process the link clicked in the reset password email
	 * @return Response
	 */
	public function get_newpassword($reset_code)
	{
		if( Sentry::check() ){
			// Return to user
			return Redirect::to('/');
		}

		// Test that provided $reset_code exists
		if( User::where('reset_password_code', '=', $reset_code)->exists() ){
			    $data['reset_code'] = $reset_code;

			    // Show new password form
			    return View::make('frontend.user.newpassword')->with($data);
		} else {
			Session::flash('flash-error', "The reset password url was invalid!");
			return View::make('frontend.user.login');
		}
	}

	/**
	 * Process change password post request from password reset.
	 * @return Response
	 */
	public function post_newpassword()
	{
	    $input = array(
	        'reset_code' => Input::get('reset_code'),
	        'password' => Input::get('password'),
	        'password_confirmation' => Input::get('password_confirmation')
	    );

	    $rules = array(
	        'reset_code' => 'required|alpha_num|size:42',
	        'password' => 'required|min:4|confirmed',
	        'password_confirmation' => 'required'
	        );

	    $v = Validator::make($input, $rules);

	    if( $v->fails() )
	    {
	        return Redirect::to('doreset')->withErrors($v)->withInput()->except('password');
	    }
	    else
	    {
	        try {
	            if( $user = Sentry::getUserProvider()->findByResetPasswordCode($input['reset_code']) ){
	                if( $user->checkResetPasswordCode($input['reset_code']) ){
	                    if( $user->attemptResetPassword($input['reset_code'], $input['password']) ){
	                        // New password success
	                        Session::flash('flash-success', "Your password has been changed successfully!");
	                        return Redirect::to('login');
	                    } else {
	                        Log::error('Password reset failed.');
	                    }
	                } else {
	                    Log::error('Reset code was not valid.');
	                }
	            } else {
	                Log::error('Failed to find user with reset code.');
	            }
	        } catch (Exception $e) {
	            Log::notice($e);
                Session::flash('flash-error', "An error occured.");
                return Redirect::to('login');
	        }
	    }
	}
}
