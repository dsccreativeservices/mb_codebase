<?php

class GuestBaseController extends Controller {

    protected $isClient;
    protected $isAdmin;

    /**
     * Instantiate a new BaseController
     */
    public function __construct()
    {
        $this->isAdmin = FALSE;

        //Check CSRF token on POST
        $this->beforeFilter('csrf', array('on' => 'post'));

        if (Request::is('client*') )
        {
            $this->isClient = TRUE;
        } else {
            $this->isClient = FALSE;
        }

        // Share user across views.
        View::share('user', Sentry::getUser());
        View::share('isClient', $this->isClient);
        View::share('isAdmin', $this->isAdmin);
    }


    /**
     * Setup the layout used by the controller.
     *
     * @return void
     */
    protected function setupLayout()
    {
        if ( ! is_null($this->layout))
        {
            $this->layout = View::make($this->layout);
        }
    }

}
