<?php

class CampaignController extends BaseController {

    protected $user;

    public function __construct()
    {
        parent::__construct();

        $this->user = Sentry::getUser();

        //Check CSRF token on POST
        $this->beforeFilter('csrf', array('on' => 'post'));
    }

    /**
     * Show new campagin form. If ids for dropdown options passed, set those to selected.
     * @param  integer $advertiser_id Optional advertiser id
     * @param  integer $campaign_id   Optional campaign id.
     * @return Reponse
     */
    public function get_new($advertiser_id = 0)
    {
        $data['advertiser_selected'] = $advertiser_id;

        $data['advertisers'] = Advertiser::getDropdown();

        return View::make('frontend.campgaigns.new')->with($data);
    }


    /**
     * Get details by id
     * @return Response
     */
    public function getDetail($campaign_id)
    {
        $data['campaign'] = Campaign::find($campaign_id);

        // Load project only user actions if any.
        $data['campaign']->load(array('projects.useractions' => function($q)
        {
            $q->where('item_type', '=', 'project');
        }));

        // Get related campaign data
        $data['campaign']->load('advertiser', 'projects.creatives.preview');

        $data['projects'] = Project::where('campaign_id', '=', $campaign_id)->orderby('updated_at', 'desc')->get();

        $data['previews'] =  Preview::where('campaign_id', '=', $campaign_id)->get();//->first()->orderby('updated_at', 'desc');


        return View::make('frontend.campaigns.detail')->with($data);
    }

    /**
     * Generate Multi-project preview
     * @return Response
     */
    public function multiPreview($preview_id){
        $data['preview'] = $preview = Preview::where('preview_id', '=', $preview_id)->first();
        $campaign = Campaign::find($preview->campaign_id);

        //get the items array
        $items = unserialize($preview->creative_items);
        //loop through array find projects and creative
        //array is (adtag, project ID, adtag, project id) etc
        for ($i = 0; $i < count($items); ++$i) {
            $item = 'item'.$i;
            $$item = $items[$i];
        }
        //get adtags and creatives
        /*$data['adtag1'] = $adtag1 = AdTag::find($item0);
        $data['adtag2'] = $adtag2 = AdTag::find($item2);
        $data['creative1'] = $creative1 = Creative::where('project_id', '=', $item1)->first();
        $data['creative2'] = $creative2 = Creative::where('project_id', '=', $item3)->first();*/

        //find projects, then get adtags and creatives
        $project1 = Project::find($item0);
        $project2 = Project::find($item1);
        $data['adtag1'] = $adtag1 = AdTag::find($project1->adtag_id);
        $data['adtag2'] = $adtag2 = AdTag::find($project2->adtag_id);
        $data['creative1'] = $creative1 = $project1->creatives->first();
        $data['creative2'] = $creative2 = $project2->creatives->first();

        //get preview page

        $html = new Htmldom($preview->scrape_url);
        $data['preview_source'] = $html;

        //set base tag
        $url = parse_url($preview->scrape_url);
        $baseTag = '<base href="' . $url['scheme'] . '://' . $url['host'] . '/" target="_blank" />';
		$data['preview_source'] = preg_replace('/<\/title>/', '</title>' .  $baseTag, $data['preview_source']);
        //$data['preview_url'] = $preview->scrape_url;

        //change page title
        $title = 'Interactive Ad Experience | Preview';
        $data['preview_source'] = preg_replace('/<title>(.*)<\/title>/', '<title>' .  $title . '</title>', $data['preview_source']);

        //attempt to remove DFP script tag
        $data['preview_source'] = str_ireplace("http://code.adsales.snidigital.com/branches/prod/home/sni-ads.min.js", "", $data['preview_source']); //home
        $data['preview_source'] = str_ireplace("https://code.adsales.snidigital.com/lib/1/0/sni-ads.min.js", "", $data['preview_source']); //food network
        $data['preview_source'] = str_ireplace("http://code.adsales.snidigital.com/lib/1/0/sni-ads.min.js", "", $data['preview_source']); //food.com (not https)

        //write iframes
        // Build path: <uploaddir>/advertiserId/CampaignId/ProjectId/iframeSrc
        $destinationPath = Config::get('iax.upload.upload_dir') . DIRECTORY_SEPARATOR;
        $destinationPath .= $campaign->advertiser->id . DIRECTORY_SEPARATOR;
        $destinationPath .= $campaign->id . DIRECTORY_SEPARATOR;

        function writeIframes($adtag, $creative, $campaign, $destinationPath) {
            $sHTML = "<html><head><style>body{margin:0; padding:0}</style></head><body>";
            $sHTML .= $creative->creative;
            $sHTML .= "</body></html>";

            $filename = preg_replace('/\s+/', '', $adtag->name) . ".html";

            $filePath = $destinationPath;
            $filePath .= $filename;

            //check if folder structure exists, if not make it
            if (!file_exists($destinationPath)) {
                mkdir($destinationPath, 0777, true);
            }

            $uploaded = file_put_contents ( $filePath , $sHTML );
        }
        writeIframes($adtag1, $creative1, $campaign, $destinationPath);
        writeIframes($adtag2, $creative2, $campaign, $destinationPath);

        //URLS
        $data['creative1_url'] = $destinationPath.$adtag1->name.".html";
        $data['creative2_url'] = $destinationPath.$adtag2->name.".html";
        $data['scrape_url'] = $preview->scrape_url;

        return View::make('previews.multiProject')->with($data);
    }

}
