<?php

use Iax\Repositories\ProjectRepositoryInterface;

class FavoriteController extends BaseController {

    protected $user;
    protected $project;

    public function __construct(ProjectRepositoryInterface $project)
    {
        parent::__construct();

        $this->user = Sentry::getUser();

        //Check CSRF token on POST
        $this->beforeFilter('csrf', array('on' => 'post'));

        // Inject instanec of ProjectRepositoryInterface
        $this->project = $project;
    }

    public function index(){
        // Get all projects that have a favorites record for the current user
        /*$data['projects'] = Project::whereHas('favorites', function($q){
            $q->where('user_id', '=', $this->user->id);
        })->get();

        return View::make('frontend.user.favorites', $data);
        */

        //get all projects marked as favorites
        $data['projects'] = Project::whereHas('favorites', function($q){
            $q->where('user_id', '>', '0');
        })->orderby('updated_at', 'desc')->paginate(Config::get('iax.page_size'));

        return View::make('frontend.projects.favorites', $data);
    }


    public function remove(){
        $project_id = Input::get('project_id');
        $mode = Input::get('mode');

        if( $mode === 'remove_single' ){
            // Remove favorite for single project
            try {
                //$f = Favorite::whereRaw('user_id = ' . $this->user->id . ' AND project_id = ' . $project_id)->first();
                $f = Favorite::where('project_id', '=', $project_id)->first();
                $f->delete();
                Session::flash('flash-success', 'Project with ID of ' . $project_id . ' removed from favorites.');
            } catch (Exception $e) {
                Session::flash('flash-error', 'Error while removing project with ID of ' . $project_id . '.');
            }
        } else {
            // Remove all favorites
            $f = Favorite::where('user_id', '=', $this->user->id)->delete();
            Session::flash('flash-success', 'All favorites removed.');
        }

        return Redirect::back();
    }

}
