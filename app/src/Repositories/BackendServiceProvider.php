<?php namespace Iax\Repositories;

use Illuminate\Support\ServiceProvider;

class BackendServiceProvider extends ServiceProvider {
	public function register(){
		// Bind backend repositories
        $this->app->bind('Iax\Repositories\ProjectRepositoryInterface', 'Iax\Repositories\DbProjectRepository');
		$this->app->bind('Iax\Repositories\TagRepositoryInterface', 'Iax\Repositories\DbTagRepository');
		$this->app->bind('Iax\Repositories\AdvertiserRepositoryInterface', 'Iax\Repositories\DbAdvertiserRepository');
		$this->app->bind('Iax\Repositories\UserRepositoryInterface', 'Iax\Repositories\DbUserRepository');
        $this->app->bind('Iax\Repositories\ImageRepositoryInterface', 'Iax\Repositories\DbImageRepository');
		$this->app->bind('Iax\Repositories\UserActionRepositoryInterface', 'Iax\Repositories\DbUserActionRepository');
	}

}