<?php namespace Iax\Repositories;

use Advertiser;
use DB;

class DbAdvertiserRepository implements AdvertiserRepositoryInterface {

	public function getAll(){
		$ret = Advertiser::orderBy('name', 'asc')->get();
		return $ret;
	}

	public function getAllWithAlpha(){
		$ret = $this->getAll();
		$ret->load('campaigns');

		// Create alphabet breaks
		$ret = \Iax\Helpers::createAlphaArray($ret);

		return $ret;
	}

	public function detailsById($adv_id){
		return Advertiser::with('campaigns')->where('id', $adv_id)->first();
	}

}