<?php namespace Iax\Repositories;

interface UserActionRepositoryInterface {

    public function recent($limit);

}
