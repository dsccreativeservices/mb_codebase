<?php namespace Iax\Repositories;

interface ImageRepositoryInterface {

    public function add($project, $imageFile);

    public function getAll($project_id);

    public function getSingle($image_id);

    public function update($image_id, $images);

    public function updatePositions($image_name, $images);

    public function remove($image_id);

}
