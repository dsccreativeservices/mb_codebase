<?php namespace Iax\Repositories;

interface AdvertiserRepositoryInterface {

	public function getAll();

	public function getAllWithAlpha();

	public function detailsById($adv_id);

}