<?php namespace Iax\Repositories;

use Project, DB, Creative, Preview, Hash, Tag, Config;

/**
* Project repository
* @implements DbRepositoryInterface
* @namespace Iax\Repositories
*/
class DbProjectRepository implements ProjectRepositoryInterface
{

	/**
	 * Get all projects including related info.
	 * @param  String $sort   "ASC" or "DESC" by modified date. Default: "DESC"
	 * @return Object         Elqoquent
	 */
	public function getRecent($sort = "DESC"){
			return Project::with('campaign.advertiser', 'tags')->orderBy($sort)->get();
	}

	/**
	 * Get all project associated with a given array of tags.
	 * @param  Array $tag Array of tags to match on.
	 * @return Object          Eloquent collection
	 */
	public function getByTags($tags)
	{
		// Convert to array if single tag passed.
		if( is_string($tags) ){
			$tags = array($tags);
		}
		// Find all projects that use the passed in tag.
		$ret = Project::whereHas('tags', function($q) use ($tags){
			$q->whereIn('name', $tags);
		})
		->paginate(Config::get('iax.page_size'));

		return $ret;
	}

	/**
	 * Get all projects including related info with paging.
	 * @param  String $sort   "ASC" or "DESC" by modified date. Default: "DESC"
	 * @param  Int	  $offset Starting index
	 * @param  Int	  $limit  Amount per page
	 * @return Object         Elqoquent
	 */
	public function getRecentPaged($offset, $limit, $sort = "DESC"){
		return Project::with('campaign.advertiser')->offset($offset)->limit($limit)->orderby($sort)->get();
	}

	public function getPaginatedProjects(){
		return Project::with('campaign.advertiser')->orderby('updated_at', 'desc')->paginate(Config::get('iax.page_size'));
	}

	/**
	 * Return recent project for a given user id.
	 * @param  Integer $userId User id
	 * @return Object         Eloquent object containing all recent projects for user.
	 */
	public function getRecentByUser($userId){
		return Project::with('campaign.advertiser', 'site', 'adtype', 'creatives.preview')->where('created_by', '=', $userId)->orderby('updated_at', 'desc')->paginate(Config::get('iax.page_size'));
	}

	/**
	 * Get project data by project id
	 * @param  Integer $pid Project id
	 * @return Object
	 */
	public function getByProjectId($pid){
		$ret = Project::find($pid);

		if( $ret ){
			$ret->load('campaign.advertiser', 'site', 'adtype', 'tags', 'images', 'creatives.preview');
		} else {
			$ret = FALSE;
		}
		return $ret;
	}

	/**
	 * Generate a preview for project using the first creative. Additional
	 * creatives will be handled within a future Preview repository.
	 * @param  Integer $pid Project Id
	 * @return String      The url for the new preview.
	 */
	public function generateCreativePreview($cid){
		$creative = Creative::find($cid);
		$creative->load('project');

		// Create new preview
		if( Preview::where('creative_id', '=', $cid)->exists() ){
			// Update existing preview
			$preview = Preview::where('creative_id', '=', $cid)->first();
		} else {
			$preview = new Preview();
			// Create hash for new preview only.
			$preview->preview_id = hash('md5', $cid . microtime(true));
		}

		// Use creative url if set. Otherwise use project url.
		$preview->scrape_url = ($creative->url) ? $creative->url : $creative->project->url;
		$preview->creative_id = $creative->id;
		$preview->site_id = $creative->project->site_id;

		$preview->save();

		// Array of Preview URLs.  Source url has no IAX UI.
		$url['preview'] = '/preview/view/' . $preview->preview_id;
		$url['source'] = '/preview/iframe/' . $preview->preview_id;

		return $url;
	}

	public function getCompletedProjects(){
		return Project::with('campaign.advertiser', 'site', 'adtype', 'creatives.preview')->where('status', '=', 'complete')->orderby('updated_at', 'desc')->paginate(Config::get('iax.page_size'));
	}
}