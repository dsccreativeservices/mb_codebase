<?php namespace Iax\Repositories;

interface UserRepositoryInterface {

	/**
	 * Return an array of current logged in user's permissions
	 * @return Array Permissions
	 */
	public function getPermissions();

	public function getAll();

	/**
	 * Check if current user has the provided permision
	 * @param  String  $permission Permission to test. (eg: "create_project")
	 * @return boolean
	 */
	public function hasAccess($permission);

}