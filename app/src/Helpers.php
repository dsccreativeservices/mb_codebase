<?php namespace Iax;

/**
 * A collection of general functions used throughout the app
 */
abstract class Helpers {

    /**
     * Takes a data collection and return a modified collection with breaks for letter sorting.
     * @param Object $data - Collection to modify.
     * @return Array of containing each letter that contains an array of Advertiser objects.
     */
    public static function createAlphaArray($data)
    {
        $ret = array();
        $alpha = str_split('ABCDEFGHIJKLMNOPQRSTUVWXYZ'); // create array of letters.
        for ($i=0; $i < count($alpha); $i++) {
            $ret[$alpha[$i]] = array(); // create empty array for current letter
            foreach ($data as $value) {
                // Loop over data set and push to letter array if first letter matched.
                if( $alpha[$i] === strtoupper(substr($value->name, 0, 1)) ){
                    array_push($ret[$alpha[$i]], $value);
                }
            }
        }
        return $ret;
    }

    /**
     * Generate mark up for checkbox and sets checked status appropriately.
     * @param  String $name    Name attribute to use in HTML
     * @param  Bool $checked   Whether the checkbox should be checked
     * @return String          The HTML input element.
     */
    public static function convertToCheckbox($name, $checked)
    {
        if( $checked ){
            $html = '<input type="checkbox" id="' . str_replace(".", "_", $name) . '" name="' . $name . '" checked="checked" value="1">';
        } else {
            $html = '<input type="checkbox" id="' . str_replace(".", "_", $name) . '" name="' . $name . '" value="0">';
            $html .= '<input type="hidden" id="' . str_replace(".", "_", $name) . '" name="' . $name . '" value="0">';
        }
        return $html;
    }

    /**
     * Generates multi column list using an list of items.
     * @param  Array    $data
     * @param  Int      $columns   How many columns to create
     * @param  String   $link      Create a link using this and the item id. Like: campaigns/ID
     * @return String              HTML for list
     */
    public static function createMultiColumnList($data, $columns, $link)
    {
        $count = count($data);


        if( $count > 0){
            $html = '<div class="iax-multi-column clearfix">';
            $cArray = ( $count > 1 ) ? array_chunk($data, max($count / $columns, 1)) : array($data);

            // Loop over column chunks
            for ($i=0; $i < count($cArray); $i++) {
                $html .= '<ul class="list-col">';
                //loop over column item
                for ($z=0; $z < count($cArray[$i]); $z++) {
                    $html .= '<li><a href="/' . $link . '/' . $cArray[$i][$z]['id'] . '">' . $cArray[$i][$z]['name'] . '</a></li>';
                }
                $html .= '</ul>';
            }
            $html .= '</div>';

            return $html;
        } else {
            return "No items.";
        }
    }

    /**
     * Print out formatted object/array or string and die
     * @param  Mixed $input Item to print
     */
    public static function debug($input)
    {
        if( is_object($input) || is_array($input) ){
            echo "<pre>";
            print_r($input);
            echo "</pre>";
            die();
        } else {
            echo $input;
            die();
        }
    }


}