<?php

class AdTypesTableSeeder extends Seeder {

    public function run()
    {
        // Uncomment the below to wipe the table clean before populating
        DB::table('adtypes')->truncate();

        // Food Network Ads
        AdType::create(array(
            'site' => 1,
            'name' => 'Leaderboard',
            'tag_id' => 1
            ));

        AdType::create(array(
            'site' => 1,
            'name' => 'Pushdown',
            'tag_id' => 2
            ));

        AdType::create(array(
            'site' => 1,
            'name' => 'Brandscape',
            'tag_id' => 2
            ));

        AdType::create(array(
            'site' => 1,
            'name' => '300x250',
            'tag_id' => 4
            ));

        AdType::create(array(
            'site' => 1,
            'name' => 'RSI Module',
            'tag_id' => 5
            ));

        AdType::create(array(
            'site' => 1,
            'name' => '300x150',
            'tag_id' => 6
            ));

    }

}