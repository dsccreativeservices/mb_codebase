<?php

class SentryUserGroupSeeder extends Seeder {

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users_groups')->truncate();

        // Get users
        $admin = Sentry::getUserProvider()->findByLogin('ahutchins');
        $designer = Sentry::getUserProvider()->findByLogin('designer');
        $adops = Sentry::getUserProvider()->findByLogin('adops');

        // Get groups
        $adminsGroup = Sentry::getGroupProvider()->findByName('Admins');
        $designersGroup = Sentry::getGroupProvider()->findByName('Designers');
        $operationsGroup = Sentry::getGroupProvider()->findByName('Operations');

        // Assign the groups to the users
        $admin->addGroup($adminsGroup);
        $designer->addGroup($designersGroup);
        $adops->addGroup($operationsGroup);
    }

}