<?php

class AdTagsTableSeeder extends Seeder {

    public function run()
    {
        // Uncomment the below to wipe the table clean before populating
        DB::table('adtags')->truncate();

        // Food Network
        AdTag::create(array(
            'site_id' => 1,
            'name' => 'Leaderboard',
            'tag_selector' => '#leaderboard',
            'regex' => '',
        ));

        AdTag::create(array(
            'site_id' => 1,
            'name' => 'Pushdown',
            'tag_selector' => '#pushdown_adtag>.iax_inner',
            'regex' => '',
        ));
        AdTag::create(array(
            'site_id' => 1,
            'name' => 'Bigbox 300x250',
            'tag_selector' => '#bigbox',
            'regex' => '',
        ));
        AdTag::create(array(
            'site_id' => 1,
            'name' => '300x150',
            'tag_selector' => '',
            'regex' => 'BigboxAd300x150(.)',
        ));
        AdTag::create(array(
            'site_id' => 1,
            'name' => 'Superstitial 1',
            'tag_selector' => '',
            'regex' => 'Superstitial(1);',
        ));
        AdTag::create(array(
            'site_id' => 1,
            'name' => 'Superstitial 2',
            'tag_selector' => '',
            'regex' => 'Superstitial(2);',
        ));

        // HGTV

        // DIY Network

        // Food.com

        // Travel Channel

        // HGTV Remodels

        // HGTV Gardens

        // Cooking Channel

        // Frontdoor

        // GAC

        // Rachael Ray

    }

}