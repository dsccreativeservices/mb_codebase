<?php

class TagsTableSeeder extends Seeder {

    public function run()
    {
        // Uncomment the below to wipe the table clean before populating
        // DB::table('tags')->truncate();

        // Just some general tags to get started
        Tag::create(array('name' => 'HTML 5'));
        Tag::create(array('name' => 'Non-Flash'));
        Tag::create(array('name' => 'Poll'));
        Tag::create(array('name' => 'Pushdown'));
        Tag::create(array('name' => 'Showcase'));
        Tag::create(array('name' => 'Flite'));
        Tag::create(array('name' => 'Holidays'));
        Tag::create(array('name' => 'Mobile'));
    }

}