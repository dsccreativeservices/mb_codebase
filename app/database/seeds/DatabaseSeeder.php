<?php

class DatabaseSeeder extends Seeder {

	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	public function run()
	{
		Eloquent::unguard();

		// Sentry seeds. Uncomment to ressed user data
		$this->call('SentryGroupSeeder');
		$this->call('SentryUserSeeder');
		$this->call('SentryUserGroupSeeder');
		$this->command->info('Sentry package seeded!');

		$this->call('PermissionsTableSeeder');
		$this->command->info('User permissions table seeded!');

		// Tags seeds.
		$this->call('TagsTableSeeder');
		$this->command->info('Tags table seeded!');

		// Project seeds.
		$this->call('AdvertisersTableSeeder');
		$this->command->info('Advertisers table seeded!');
		$this->call('CampaignsTableSeeder');
		$this->command->info('Campaigns table seeded!');
		$this->call('ProjectsTableSeeder');
		$this->command->info('Projects table seeded!');
		$this->call('CreativesTableSeeder');
		$this->command->info('Creatives table seeded!');

		// Sites and Ad Tags
		$this->call('SitesTableSeeder');
		$this->command->info('Sites table seeded!');
		$this->call('AdTagsTableSeeder');
		$this->command->info('AdTags table seeded!');
		$this->call('AdTypesTableSeeder');
		$this->command->info('AdTypes table seeded!');
	}

}