<?php

class SitesTableSeeder extends Seeder {

    public function run()
    {
        // Uncomment the below to wipe the table clean before populating
        DB::table('sites')->truncate();

        Site::create(array(
            'name' => 'Food Network',
            'code' => 'FN',
            'host' => 'www.foodnetwork.com',
        ));
        Site::create(array(
            'name' => 'HGTV',
            'code' => 'HGTV',
            'host' => 'www.hgtv.com',
        ));
        Site::create(array(
            'name' => 'DIY Network',
            'code' => 'DIY',
            'host' => 'www.diynetwork.com',
        ));
        Site::create(array(
            'name' => 'Food.com',
            'code' => 'FCOM',
            'host' => 'www.food.com',
        ));
    }

}