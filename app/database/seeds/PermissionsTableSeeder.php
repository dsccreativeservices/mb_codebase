<?php

class PermissionsTableSeeder extends Seeder {

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('permissions')->truncate();

        Permission::create(array('name' => "superuser"));

        Permission::create(array('name' => "user.create"));
        Permission::create(array('name' => "user.delete"));
        Permission::create(array('name' => "user.view"));
        Permission::create(array('name' => "user.update"));
        Permission::create(array('name' => "advertiser.create"));
        Permission::create(array('name' => "advertiser.delete"));
        Permission::create(array('name' => "advertiser.view"));
        Permission::create(array('name' => "advertiser.update"));
        Permission::create(array('name' => "campaign.create"));
        Permission::create(array('name' => "campaign.delete"));
        Permission::create(array('name' => "campaign.view"));
        Permission::create(array('name' => "campaign.update"));
        Permission::create(array('name' => "project.create"));
        Permission::create(array('name' => "project.delete"));
        Permission::create(array('name' => "project.view"));
        Permission::create(array('name' => "project.update"));
        Permission::create(array('name' => "creative.create"));
        Permission::create(array('name' => "creative.delete"));
        Permission::create(array('name' => "creative.view"));
        Permission::create(array('name' => "creative.update"));
    }

}