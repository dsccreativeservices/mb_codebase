<?php

class AdvertisersTableSeeder extends Seeder {

    public function run()
    {
    	// Uncomment the below to wipe the table clean before populating
    	DB::table('advertisers')->truncate();

        // Advertiser::create(array(
        //     'name' => 'System Testing',
        //     'created_by' => 1
        // ));
    }
}