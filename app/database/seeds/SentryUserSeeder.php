<?php

class SentryUserSeeder extends Seeder {

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->truncate();

        Sentry::getUserProvider()->create(array(
            'email'    => 'ahutchins@scrippsnetworks.com',
            'username' => 'ahutchins',
            'password' => 'pass',
            'activated' => 1,
            'first_name' => 'Andy',
            'last_name' => 'Hutchins',
        ));

        Sentry::getUserProvider()->create(array(
            'email'    => 'test_user_designer@iax.com',
            'username' => 'designer',
            'password' => 'pass',
            'activated' => 1,
            'first_name' => 'Test',
            'last_name' => 'User',
        ));

        Sentry::getUserProvider()->create(array(
            'email'    => 'test_user_adops@iax.com',
            'username' => 'adops',
            'password' => 'pass',
            'activated' => 1,
            'first_name' => 'Test',
            'last_name' => 'User',
        ));

    }

}