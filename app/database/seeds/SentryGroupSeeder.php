<?php

class SentryGroupSeeder extends Seeder {

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('groups')->truncate();

        Sentry::getGroupProvider()->create(array(
            'name'        => 'Admins',
            'permissions' => array(
                "superuser" => 1,
                "user.create" => 1,
                "user.delete" => 1,
                "user.view" => 1,
                "user.update" => 1,
                "advertiser.create" => 1,
                "advertiser.delete" => 1,
                "advertiser.view" => 1,
                "advertiser.update" => 1,
                "campaign.create" => 1,
                "campaign.delete" => 1,
                "campaign.view" => 1,
                "campaign.update" => 1,
                "project.create" => 1,
                "project.delete" => 1,
                "project.view" => 1,
                "project.update" => 1,
                "creative.create" => 1,
                "creative.delete" => 1,
                "creative.view" => 1,
                "creative.update" => 1)
            )
        );

        Sentry::getGroupProvider()->create(array(
            'name'        => 'Operations',
            'permissions' => array()
            )
        );

        Sentry::getGroupProvider()->create(array(
            'name'        => 'Designers',
            'permissions' => array()
            )
        );

        Sentry::getGroupProvider()->create(array(
            'name'        => 'Advertisers',
            'permissions' => array()
            )
        );
    }

}