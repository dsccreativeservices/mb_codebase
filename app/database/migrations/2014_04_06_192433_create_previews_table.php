<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreatePreviewsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('previews', function(Blueprint $table) {
			$table->increments('id');
			$table->integer('site_id');
			$table->integer('creative_id');
			$table->string('scrape_url', 255);
			$table->string('preview_id', 255);
			$table->string('creative_items', 255);
			$table->timestamps();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('previews');
	}

}
