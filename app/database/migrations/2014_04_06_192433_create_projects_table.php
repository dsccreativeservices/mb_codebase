<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateProjectsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('projects', function(Blueprint $table) {
			$table->increments('id');
			$table->integer('campaign_id');
			$table->string('name', 255);
			$table->string('desc', 255);
			$table->string('url', 255);
			$table->integer('adtype_id');
			$table->integer('site_id');
			$table->string('status', 255);
			$table->integer('created_by');
			$table->integer('custom_adtag_id')->nullable();
			$table->softDeletes();
			$table->timestamps();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('projects');
	}

}
