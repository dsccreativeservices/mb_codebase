<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateAdtagsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('adtags', function(Blueprint $table) {
			$table->increments('id');
			$table->string('name', 255);
			$table->string('tag_selector', 255)->nullable();
			$table->string('regex', 255)->nullable();
			$table->integer('site_id');
			$table->timestamps();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('adtags');
	}

}
