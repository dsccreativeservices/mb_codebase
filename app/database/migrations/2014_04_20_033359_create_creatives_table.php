<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateCreativesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('creatives', function(Blueprint $table) {
			$table->increments('id');
			$table->integer('project_id');
			$table->string('name', 255);
			$table->text('creative');
			$table->string('url', 255);
			$table->integer('created_by');
			$table->boolean('locked');
			$table->datetime('deleted_at')->nullable();
			$table->timestamps();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('creatives');
	}

}
