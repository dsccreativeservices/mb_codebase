<?php namespace Iax\Repositories;

interface PermissionRepositoryInterface {

    public function getGroupPermissions($group_id);

    public function hasAccess($user_id, $permission);

    public function setPermission($user_id, $permission);

    public function removePermission($user_id, $permission);

}
