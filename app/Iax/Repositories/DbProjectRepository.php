<?php namespace Iax\Repositories;

use Project, DB, Creative, Preview, Hash, Tag, Config;

/**
* Project repository
* @implements DbRepositoryInterface
* @namespace Iax\Repositories
*/
class DbProjectRepository implements ProjectRepositoryInterface
{

	/**
	 * Get all projects including related info.
	 * @param  String $sort   "ASC" or "DESC" by modified date. Default: "DESC"
	 * @return Object         Elqoquent
	 */
	public function getRecent($sort = "DESC"){
			return Project::with('campaign.advertiser', 'tags')->orderBy($sort)->get();
	}

	/**
	 * Get all project associated with a given array of tags.
	 * @param  Array $tag Array of tags to match on.
	 * @return Object          Eloquent collection
	 */
	public function getByTags($tags)
	{
		// Convert to array if single tag passed.
		if( is_string($tags) ){
			$tags = array($tags);
		}
		// Find all projects that use the passed in tag.
		$ret = Project::whereHas('tags', function($q) use ($tags){
			$q->whereIn('name', $tags);
		})
		->paginate(Config::get('iax.page_size'));

		return $ret;
	}

	/**
	 * Get all project associated with a given array of sites. - Mike Added
	 * @param  Array $site Array of tags to match on.
	 * @return Object          Eloquent collection
	 */
	public function getBySites($site)
	{
		// Convert to array if single tag passed.
		if( is_string($site) ){
			$site = array($site);
		}
		// Find all projects that use the passed in site.
		$ret = Project::whereHas('site', function($q) use ($site){
			$q->whereIn('name', $site);
		})
		->orderby('updated_at', 'desc')->paginate(Config::get('iax.page_size'));

		return $ret;
	}

	/**
	 * Get all project associated with a given array of ad types. - Mike Added
	 * @param  Array $adType Array of tags to match on.
	 * @return Object          Eloquent collection
	 */
	public function getByAdType($adType)
	{
		// Convert to array if single tag passed.
		if( is_string($adType) ){
			$adType = array($adType);
		}
		// Find all projects that use the passed in tag.
		$ret = Project::whereHas('adtype', function($q) use ($adType){
			$q->whereIn('name', $adType);
		})
		->orderby('updated_at', 'desc')->paginate(Config::get('iax.page_size'));

		return $ret;
	}

	/**
	 * Get all projects including related info with paging.
	 * @param  String $sort   "ASC" or "DESC" by modified date. Default: "DESC"
	 * @param  Int	  $offset Starting index
	 * @param  Int	  $limit  Amount per page
	 * @return Object         Elqoquent
	 */
	public function getRecentPaged($offset, $limit, $sort = "DESC"){
		return Project::with('campaign.advertiser')->offset($offset)->limit($limit)->orderby($sort)->get();
	}


    /**
     * Get all projects with pagination.
     */
    public function getPaginatedProjects(){
		return Project::with('campaign.advertiser', 'adtype')->orderby('updated_at', 'desc')->paginate(Config::get('iax.page_size'));
	}

	/**
	 * Return recent project for a given user id.
	 * @param  Integer $userId User id
	 * @return Object         Eloquent object containing all recent projects for user.
	 */
	public function getRecentByUser($userId){
		return Project::with('campaign.advertiser', 'site', 'adtype', 'creatives.preview')->where('created_by', '=', $userId)->orderby('updated_at', 'desc')->paginate(Config::get('iax.page_size'));
	}

	/**
	 * Get project data by project id
	 * @param  Integer $pid Project id
	 * @return Object
	 */
	public function getByProjectId($pid){
		$ret = Project::find($pid);

		if( $ret ){
			$ret->load('campaign.advertiser', 'site', 'adtype', 'tags', 'images', 'creatives.preview');
		} else {
			$ret = FALSE;
		}
		return $ret;
	}

	/**
	 * Generate a preview for project using the first creative. Additional
	 * creatives will be handled within a future Preview repository.
	 * @param  Integer $cid Creative Id
	 * @return String      The url for the new preview.
	 */
	public function generateCreativePreview($cid){
		$creative = Creative::find($cid);
		$creative->load('project');

		// Create new preview
		if( Preview::where('creative_id', '=', $cid)->exists() ){
			// Update existing preview
			$preview = Preview::where('creative_id', '=', $cid)->first();
		} else {
			$preview = new Preview();
			// Create hash for new preview only.
			$preview->preview_id = hash('md5', $cid . microtime(true));
		}

		// Use creative url if set. Otherwise use project url.
		$preview->scrape_url = ($creative->url) ? $creative->url : $creative->project->url;
		$preview->creative_id = $creative->id;
		$preview->site_id = $creative->project->site_id;

		$preview->save();

		// Array of Preview URLs.  Source url has no IAX UI.
		$url['preview'] = '/preview/iframe/' . $preview->preview_id;
		$url['source'] = '/preview/iframe/' . $preview->preview_id;

		return $url;
	}

	public function getCompletedProjects(){
		return Project::with('campaign.advertiser', 'site', 'adtype', 'creatives.preview')->where('status', '=', 'complete')->orderby('updated_at', 'desc')->paginate(Config::get('iax.page_size'));
	}


	/**
	 * find and replace any S3 URLs with CloudFront URL
	 * update click macro by adding it to the var
	 * @return Object
	 */
	public function modifyCreativeForTraffic($creative){

		$ret = $creative;

		//update S3 URLS to CloudFront
		$S3URLs = ["s3.amazonaws.com/sni-ads-creativeservices", "sni-ads-creativeservices.s3.amazonaws.com"]; //array of S3 URL formats
		$CFURL = "creative.snidigital.com";
		$ret = str_replace($S3URLs, $CFURL, $ret);
		//$ret = str_replace("http://", "https://", $ret); //one day we'll force everything to be secure, for now not sure if all 3rd party tracking and plugins support it yet

		//update click macro value
		//multuple versions depending on quotes and spacing
		$clickReplace = [
			"clickMacro = ''",
			"clickMacro=''",
			"clickMacro =''",
			"clickMacro= ''",
			'clickMacro = ""',
			'clickMacro=""',
			'clickMacro =""',
			'clickMacro= ""'
		];
		$clickMacro = "%%CLICK_URL_UNESC%%";
		$ret = str_replace($clickReplace, "clickMacro = '$clickMacro'", $ret);

		//find anchor tags and append click macro
		$ret = preg_replace('/<a(.*)href="([^"]*)"(.*)>/',"<a$1href='$clickMacro$2'$3>", $ret);

		return $ret;
	}


}
