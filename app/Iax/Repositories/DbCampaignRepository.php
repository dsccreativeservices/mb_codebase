<?php namespace Iax\Repositories;

//use Config, File, Image, DB, ImageInt;
use Campaign, DB, Preview;

class DbCampaignRepository implements CampaignRepositoryInterface{

    public function multiPreview($campaign, $previewData){
        // Create file name
        /*$filename = $file->getClientOriginalName();
        $ext = $file->getClientOriginalExtension();
        $filenameParts = explode('.', $filename);
        $filename = $filenameParts[0] . '_' . round(microtime(true));

        // Get new position based on existing images
        $highestPosImage = DB::table('images')->where('project_id', '=', $project->id)->orderBy('pos', 'DESC')->first();
        if( $highestPosImage ){
            $newPos = $highestPosImage->pos + 1;
        } else {
            $newPos = 0; // First image
        }

        $size = $file->getSize();

        // Slug the filename
        $imageName = strtolower(str_replace(' ', '_', $filename)) . ".$ext";
        $imageNameThumb = strtolower(str_replace(' ', '_', $filename)) . "_thumb.$ext";

        // Build path: <uploaddir>/advertiserId/CampaignId/ProjectId/ImageName
        $destinationPath = Config::get('iax.upload.upload_dir') . DIRECTORY_SEPARATOR;
        $destinationPath .= $project->campaign->advertiser->id . DIRECTORY_SEPARATOR;
        $destinationPath .= $project->campaign->id . DIRECTORY_SEPARATOR;
        $destinationPath .= $project->id . DIRECTORY_SEPARATOR;

        try {
            // Save original, unresized image
            $uploaded = $file->move(public_path($destinationPath), $imageName);

            if( $uploaded ){
                // Image files saved. Now create DB record
                $image = new Image();
                $image->project_id = $project->id;
                //$image->title = $imageName; removed by mike so it doesn't add file name by default
                $image->description = $project->name;
                $image->path = $destinationPath . $imageName;
                $image->thumbPath = $destinationPath . $imageNameThumb;
                $image->pos = $newPos;
                $image->size = ($size > 0) ? $size : 0;
                $image->save();

                // Generate thumbnail version cropped at 120
                ImageInt::make($image->path)
                    ->grab(120)
                    ->save( public_path($destinationPath) . $imageNameThumb);


                return $image;
            } else {
                return array(
                    'status' => false,
                    'error' => "Upload Failed: An error occured while uploading."
                );
            }
        } catch (Exception $e) {
            return array(
                'status' => false,
                'error' => "Upload Failed: An error occured while uploading."
            );
        }*/
        return Response::json(array('status' => 'success', 'message' => 'Multi-Preview Created.'));
    }
}


