<?php namespace Iax\Repositories;

use UserAction;

class DbUserActionRepository implements UserActionRepositoryInterface{

    public function recent($limit = 10)
    {
        return UserAction::with('user')->orderBy('id', 'desc')->limit($limit)->get();
    }

}