<?php namespace Iax\Repositories;

interface CampaignRepositoryInterface {

    public function multiPreview($campaign, $previewData);

}
