<?php namespace Iax\Repositories;

use Project, DB, Tag;

/**
* Tag repository
* @implements TagRepositoryInterface
* @namespace Iax\Repositories
*/
class DbTagRepository implements TagRepositoryInterface
{
    public function getAllTags(){
        return Tag::all();
    }

    public function getTagsByProject($project_id){
        return Project::find($project_id)->tags;
    }

    public function getProjectsByTag($tag){
        $projects = Tag::where('name', '=', $tag)->get();
        $projects = $projects->load('campaign.advertiser');

        return $projects;
    }

}