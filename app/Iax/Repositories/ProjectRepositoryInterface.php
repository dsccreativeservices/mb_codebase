<?php namespace Iax\Repositories;

interface ProjectRepositoryInterface {
    public function getRecent($sort);

    public function getPaginatedProjects();

    public function getByTags($tags);
	
	public function getBySites($site); //mike added
	
	public function getByAdType($adType); //mike added

	public function getRecentPaged($offset, $limit, $sort);

	public function getRecentByUser($userId);

	public function getByProjectId($pid);

    public function generateCreativePreview($cid);

    public function getCompletedProjects();
}