<?php namespace Iax\Repositories;

class DbPermissionRepository implements PermissionRepositoryInterface{

    public function getGroupPermissions($group_id){
        $group = \Sentry::findGroupById($group_id);
        return $group->getPermissions();
    }

    public function hasAccess($user, $permission){
        return  $user->hasAccess($permission);
    }

    public function setPermission($user, $permission){
        // TODO
        return TRUE;
    }

    public function removePermission($user, $permission){
        // TODO
        return TRUE;
    }

}


