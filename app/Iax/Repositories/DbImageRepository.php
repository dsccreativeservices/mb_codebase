<?php namespace Iax\Repositories;

use Config, File, Image, DB, ImageInt;

class DbImageRepository implements ImageRepositoryInterface{

    public function getAll($project_id){
        $images = Image::where('project_id', '=', $project_id)->orderBy('pos', 'ASC')->get();
        return $images;
    }

    /**
     * Retrive a single image instance.
     * @param  Integer $image_id
     * @return Mixed   Return an Image object or status array on failure
     */
    public function getSingle($image_id){
        $image = Image::find($image_id);

        if( is_null($image) ){
            return array(
                'status' => false,
                'error' => 'Image does not exist.'
            );
        }
        return $image;
    }

    public function add($project, $file){
        // Create file name
        $filename = $file->getClientOriginalName();
        $ext = $file->getClientOriginalExtension();
        $filenameParts = explode('.', $filename);
        $filename = $filenameParts[0] . '_' . round(microtime(true));

        // Get new position based on existing images
        $highestPosImage = DB::table('images')->where('project_id', '=', $project->id)->orderBy('pos', 'DESC')->first();
        if( $highestPosImage ){
            $newPos = $highestPosImage->pos + 1;
        } else {
            $newPos = 0; // First image
        }

        $size = $file->getSize();

        // Slug the filename
        $imageName = strtolower(str_replace(' ', '_', $filename)) . ".$ext";
        $imageNameThumb = strtolower(str_replace(' ', '_', $filename)) . "_thumb.$ext";

        // Build path: <uploaddir>/advertiserId/CampaignId/ProjectId/ImageName
        $destinationPath = Config::get('iax.upload.upload_dir') . DIRECTORY_SEPARATOR;
        $destinationPath .= $project->campaign->advertiser->id . DIRECTORY_SEPARATOR;
        $destinationPath .= $project->campaign->id . DIRECTORY_SEPARATOR;
        $destinationPath .= $project->id . DIRECTORY_SEPARATOR;

        try {
            // Save original, unresized image
            $uploaded = $file->move(public_path($destinationPath), $imageName);

            if( $uploaded ){
                // Image files saved. Now create DB record
                $image = new Image();
                $image->project_id = $project->id;
                //$image->title = $imageName; removed by mike so it doesn't add file name by default
                $image->description = $project->name;
                $image->path = $destinationPath . $imageName;
                $image->thumbPath = $destinationPath . $imageNameThumb;
                $image->pos = $newPos;
                $image->size = ($size > 0) ? $size : 0;
                $image->save();

                // Generate thumbnail version cropped at 120
                ImageInt::make($image->path)
                    ->grab(120)
                    ->save( public_path($destinationPath) . $imageNameThumb);


                return $image;
            } else {
                return array(
                    'status' => false,
                    'error' => "Upload Failed: An error occured while uploading."
                );
            }
        } catch (Exception $e) {
            return array(
                'status' => false,
                'error' => "Upload Failed: An error occured while uploading."
            );
        }
    }

    public function update($image_id, $images){
		//mike added - doesn't work
		$image = Image::find($image_id);
		
		if( is_null($image) ){
            return array(
                'status' => false,
                'error' => 'No Image: Image file does not exist.'
            );
        } else {
            if( File::isFile($image->path) ){
				
				
				
                // update image file
                try {
                    File::update($image->path);
                } catch (Exception $e) {
                    return array(
                        'status' => false,
                        'msg' => 'Error updating image file.'
                    );
                }

                return array(
                    'status' => true,
                    'msg' => 'Image was successfully Updated.'
                );
            } else {
                return array(
                    'status' => false,
                    'msg' => 'Update Failed: An error occured while updating image.'
                );
            }
        }
    }

    public function updatePositions($image_id, $image_index){
        $image = Image::find($image_id);
        if( is_null($image) ){
            return array(
                'status' => false,
                'error' => 'Image does not exist.'
            );
        } else {
            try {
                $image->pos = $image_index;
                $image->save();
                return TRUE;
            } catch (Exception $e) {
                return FALSE;
            }
        }
    }

    /**
     * Remove an image from system by $image_id. Will remove file and database record.
     * @param  Integer $image_id ID of image to remove.
     * @return Array   Result array with status and any other info that is important.
     */
    public function remove($image_id){
        $image = Image::find($image_id);

        if( is_null($image) ){
            return array(
                'status' => false,
                'error' => 'No Image: Image file does not exist.'
            );
        } else {
            if( File::isFile($image->path) ){

                // Delete image file
                try {
                    File::delete($image->path);
                } catch (Exception $e) {
                    return array(
                        'status' => false,
                        'msg' => 'Error deleting image file.'
                    );
                }

                // Delete image DB record
                try {
                    $image->delete();
                } catch (Exception $e) {
                    return array(
                        'status' => false,
                        'msg' => 'Error deleting image record.'
                    );
                }

                return array(
                    'status' => true,
                    'msg' => 'Image was successfully deleted.'
                );
            } else {
                return array(
                    'status' => false,
                    'msg' => 'Delete Failed: An error occured while deleting image.'
                );
            }
        }
    }
}


