<?php namespace Iax\Repositories;

use Sentry;
use User;

class DbUserRepository implements UserRepositoryInterface {

	public function getPermissions(){
		$current_user = Sentry::getUser();
		return $current_user->getPermissions();
	}

	public function getAll(){
		return Sentry::findAllUsers();
	}

	/**
	 * Check is user has provided permissions.
	 * @param  String  $permission Permission to test. (eg: "create_project")
	 * @return boolean
	 */
	public function hasAccess($permission){
		return Sentry::hasAccess($permission);
	}
}

