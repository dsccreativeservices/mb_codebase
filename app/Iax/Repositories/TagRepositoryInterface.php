<?php namespace Iax\Repositories;

interface TagRepositoryInterface {

    public function getAllTags();

    public function getTagsByProject($project_id);

    public function getProjectsByTag($tag_id);

}