<?php
    namespace Iax\nativeModules;

    class templates {

        /*
            get the HTML of the module starter templates
            requires adType and site
            returns HTML template
        */
        public function getTmpl($adtypeName, $site){

            $COREsites = ["hgtv", "diy", "gac", "fn", "cctv", "trvl"];
            $directory = "http://sni-ads-creativeservices.s3.amazonaws.com/cs-lib/moduleBuilder/templates";

            //if CORE supported site
            if( in_array($site, $COREsites) ){

                switch($adtypeName){
                    case "Native - 1 Across":
                        $adType = "1across";
                        $fileName = "1across-starter.html";
                        break;
                    case "Native - 2 Across":
                        $adType = "2across";
                        $fileName = "2across-starter.html";
                        break;
                    case "Native - 3 Across":
                        $adType = "3across";
                        $fileName = "3across-starter.html";
                        break;
                    case "RSI Module":
                        $adType = "RSI";
                        $fileName = "FN/coreRSI.html";
                        break;
                    case "Native Ingredient":
                        $adType = "ingredient";
                        $fileName = "nativeIngredient.html";
                        break;
                }

                //template URL
                $url = $directory . "/" . $adType . "/" . $fileName;
            }
            //non CORE supported site
            else{

                switch($adtypeName){
                    case "Native - 1 Across":
                        $adType = "1across";
                        $fileName = "1across-starter.html";
                        break;
                    case "Native - 2 Across":
                        $adType = "2across";
                        $fileName = "2across-starter.html";
                        break;
                    case "Native - 3 Across":
                        $adType = "3across";
                        $fileName = "3across-starter.html";
                        break;
                    case "RSI Module":
                        $adType = "RSI";
                        $fileName = "rsi-starter.html";
                        break;
                    case "Native Ingredient":
                        $adType = "ingredient";
                        $fileName = "nativeIngredient.html";
                        break;
                }

                //template URL
                $url = $directory . "/non-core" . "/" . $site . "/" . $adType . "/" . $fileName;
            }



            //check if template exists
            function get_http_response_code($url) {
                 $headers = get_headers($url);
                 return substr($headers[0], 9, 3);
            }
            if(get_http_response_code($url) != "200"){
               $template = "Template does not exist for this site and native type yet" . $url; //no template exists so make blank
            }else{
               $template = file_get_contents($url);
            }

            //return HTML of template
            return $template;
        }


        /*
            setup a default preview URL
            requires site
            returns URL
        */
        public function getPreviewURL($site, $adtype){

            switch($site){
                case "fn":
                    if($adtype == "RSI Module"){
                        $url = "http://www.foodnetwork.com/search/chicken-";
                    }else{
                        $url = "http://www.foodnetwork.com/recipes/chicken-burgers-recipe.html";
                    }
                    break;
                case "hgtv":
                    $url = "http://www.hgtv.com/design/packages/high-low";
                    break;
                case "diy":
                    $url = "http://www.diynetwork.com/how-to/packages/halloween-howl-to";
                    break;
                case "trvl":
                    $url = "http://www.travelchannel.com/destinations/us/fl";
                    break;
                case "hgrm":
                    $url = "http://www.hgtv.com/remodel/outdoors/dreamy-pool-design-ideas-pictures";
                    break;
                case "hgar":
                    $url = "http://www.hgtvgardens.com/photos/hillside-garden";
                    break;
                case "fcom":
                    if($adtype == "RSI Module"){
                        $url = "http://www.food.com/search/burger";
                    }else{
                        $url = "http://www.food.com/recipe/crock-pot-whole-chicken-33671";
                    }
                    break;
                case "gac":
                    $url = "http://www.greatamericancountry.com/shows/junk-gypsies/trash-to-treasure-projects-from-the-junk-gypsies-pictures";
                    break;
                case "cctv":
                    $url = "http://www.cookingchanneltv.com/recipes/breakfast-brunch-recipes-ideas.html";
                    break;
            }

            return $url;
        }
    }

?>
