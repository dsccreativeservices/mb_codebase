<?php

/*
|--------------------------------------------------------------------------
| Application & Route Filters
|--------------------------------------------------------------------------
|
| Below you will find the "before" and "after" events for the application
| which may be used to do any work before or after a request into your
| application. Here you may also register your custom route filters.
|
*/

App::before(function($request)
{
	//
});


App::after(function($request, $response)
{
	//
});

/*
|--------------------------------------------------------------------------
| Authentication Filters
|--------------------------------------------------------------------------
|
| The following filters are used to verify that the user of the current
| session is logged into this application. The "basic" filter easily
| integrates HTTP Basic authentication for quick, simple checking.
|
*/

Route::filter('auth', function()
{
    if (!Sentry::check())
    {
        if( !Request::is('preview*') ){
            // if not logged in and not on a preview page, redirect to login
            //Session::put('redirect', URL::full());
            //return Redirect::to('/login');
             return Redirect::guest('/login');
        }

        Session::flash('error', 'You must be logged in to view that area.');
        // return Redirect::to('/login');
    }
});

Route::filter('api_auth', function()
{
    if (!Sentry::check())
    {
        $response = array();
        $response['status'] = 'fail';
        $response['code'] = 'LOGGED_OUT';
        $response['msg'] = 'User is not logged in.';

        return Response::json($response);
    }
});


Route::filter('admin_auth', function()
{
    if( !Sentry::check() )
    {
        Session::flash('error', "You must be logged in to access this page.");

        if( !Request::is('preview/*') ){
            // if not logged in and not on a preview page, redirect to login
            return Redirect::to('/login');
        }
    }

    $adminGroup = Sentry::findGroupByName('Admins');

    if (!Sentry::getUser()->inGroup($adminGroup))
    {
        return Redirect::to('/')->with("flash-error", "You do not have the required access level to view this page.");
    }
});


/*
|--------------------------------------------------------------------------
| AdOps Filter
|--------------------------------------------------------------------------
|
| This filter is used for the adops areas. Right now, we are still working
| out what/if any type of authentication the will use. RIght now, it will
| be just for creative delivery.
|
*/

Route::filter('adops', function()
{
	// Fill in if needed
});

/*
|--------------------------------------------------------------------------
| CSRF Protection Filter
|--------------------------------------------------------------------------
|
| The CSRF filter is responsible for protecting your application against
| cross-site request forgery attacks. If this special token in a user
| session does not match the one given in this request, we'll bail.
|
*/

Route::filter('csrf', function()
{
	if (Session::token() != Input::get('_token'))
	{
		throw new Illuminate\Session\TokenMismatchException;
	}
});




Route::filter('external', function()
{
    $credentials = Input::get('credentials');
    //first check password
    if (password_verify('password', $credentials['password'])) {
        Sentry::authenticate(array(
            'username'    => $credentials['username'],
            'password' => 'password',
        ));
    }else{
        return 'Password is wrong!';
    }

});
