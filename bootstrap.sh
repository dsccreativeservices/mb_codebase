#!/usr/bin/env bash

echo "Updating packages. This may take a while."
apt-get update

echo "Installing additonal packages. This will also take a while."
apt-get install -y apache2
apt-get install -y php5
apt-get install -y php5-cli
apt-get install -y php5-mcrypt
apt-get install -y php5-gd
apt-get install -y php5-apc
apt-get install -y beanstalkd
apt-get install -y git
apt-get install -y sqlite
apt-get install -y php5-sqlite
apt-get install -y php5-mysql
apt-get install -y curl
apt-get install -y php5-curl

sudo debconf-set-selections <<< 'mysql-server-<version> mysql-server/root_password password root'
sudo debconf-set-selections <<< 'mysql-server-<version> mysql-server/root_password_again password root'
sudo apt-get -y install mysql-server


# Setup hosts file
VHOST=$(cat <<EOF
    <VirtualHost *:80>
            ServerAdmin webmaster@localhost

            DocumentRoot /var/www/public
            <Directory />
                    Options FollowSymLinks
                    AllowOverride All
            </Directory>
            <Directory /var/www/public/>
                    Options Indexes FollowSymLinks MultiViews
                    AllowOverride All
                    Order allow,deny
                    allow from all
            </Directory>
            DirectoryIndex index.php
            ScriptAlias /cgi-bin/ /usr/lib/cgi-bin/
            <Directory "/usr/lib/cgi-bin">
                    AllowOverride None
                    Options +ExecCGI -MultiViews +SymLinksIfOwnerMatch
                    Order allow,deny
                    Allow from all
            </Directory>
    </VirtualHost>
EOF
)
echo "${VHOST}" > /etc/apache2/sites-enabled/000-default

# Enable mod_rewrite
sudo a2enmod rewrite
sudo service apache2 restart

#######################
# IAX Laravel Startup #
#######################

# Change to web root. This is shared with host.
cd /vagrant

# Create database
mysql -u root -p"root" -e "create database iax;"

# Install composer
curl -sS https://getcomposer.org/installer | php

# Install required packages. May take a few minutes.
php composer.phar install

# Install migration tables
php artisan migrate:install

# Run Sentry package migrations
php artisan migrate --package=cartalyst/sentry

# Run IAX migrations
php artisan migrate

# Seed the database with testing data (See: app/database/seeds/)
php artisan db:seed

# All done!
echo "Interactive Ad Experience should be accessible at http://localhost:8888/ now."