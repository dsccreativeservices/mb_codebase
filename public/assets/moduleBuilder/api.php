<?php

header( "Access-Control-Allow-Origin: *" );
//header("Access-Control-Allow-Methods: GET, POST, OPTIONS");

if ( !class_exists( 'S3' ) ) require_once('inc/S3.php');

$accessKey = 'AKIAI7JM6B6QT6APURQQ';
$secretKey = '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';

/* AWS access info */
if ( !defined( 'awsAccessKey' ) ) define( 'awsAccessKey', $accessKey );
if ( !defined( 'awsSecretKey' ) ) define( 'awsSecretKey', $secretKey );

if ( !extension_loaded( 'curl' ) && !@dl( PHP_SHLIB_SUFFIX == 'so' ? 'curl.so' : 'php_curl.dll' ) ) {
	exit( "\nERROR: CURL extension not loaded\n\n" );
}




/* instantiate the class */
$s3 = new S3( awsAccessKey, awsSecretKey );



/* get posted vars */
$method			= isset( $_REQUEST[ 'method' ] ) ? $_REQUEST[ 'method' ] : '';

$advertiser		= isset( $_REQUEST[ 'advertiser' ] ) ? $_REQUEST[ 'advertiser' ] : '';
$site			= isset( $_REQUEST[ 'site' ] ) ? $_REQUEST[ 'site' ] : '';
$type			= isset( $_REQUEST[ 'type' ] ) ? $_REQUEST[ 'type' ] : '';

$filename		= isset( $_REQUEST[ 'filename' ] ) ? $_REQUEST[ 'filename' ] : '';
$backup			= isset( $_REQUEST[ 'backup' ] ) ? $_REQUEST[ 'backup' ] : '';
$data			= isset( $_REQUEST[ 'data' ] ) ? $_REQUEST[ 'data' ] : '';


$year = date( 'Y' );

/* setup paths */
$path = "$advertiser/$year/native/$type/$site/";
$localFolder = dirname(__FILE__) . "/clients/$path";
$remoteFolder = "$path";


$localFile = $localFolder . $filename;
$localImageFile = $localFolder . "img/" . $filename;
$backupFile = $localFolder . 'backups/' . $backup;


switch ( $method ) {

	/*
		Local file upload for drag and drop on the client side
		Moves tmp from [tmp]location to $localFolder
	*/
	case 'upload_file' : {
		//$ret = "error: Image was not uploaded.";
		if ( strtolower( $_SERVER[ 'REQUEST_METHOD' ] ) == 'post' && !empty( $_FILES ) ) {

			//if error fail
			if ( $_FILES[ 'file' ][ 'error' ] ) {
				die( "Upload failed with error code " . $_FILES[ 'file' ][ 'error' ] );
			}

			//create directory on scrippsonline
			$imgFolder	= $localFolder . 'img/';

			if ( !file_exists( $imgFolder ) ) {
				mkdir( $imgFolder, 0777, true );
			}

			$filename = $_FILES[ 'file' ][ 'name' ];
			$tmpFile = $_FILES[ 'file' ][ 'tmp_name' ];
			$imageType = $_FILES['file']['type'];

			$remoteFile	= $remoteFolder . 'img/' . $filename;

			if ( !empty( $tmpFile ) && is_uploaded_file( $tmpFile ) ) {
				//upload to scrippsonline as a check, not needed since users are already authenticated
				if ( ! move_uploaded_file( $tmpFile, $localImageFile ) ) {
					die( "Failed moving the uploaded file" );
				}

				if ( $s3->putObjectFile( $localImageFile, "sni-ads-creativeservices", $remoteFile, S3::ACL_PUBLIC_READ ) ) {
					$ret = $remoteFile;
				} else {
					$ret = "error: Image upload failed.";
				}
			}
		}
		echo $ret;
		break;
	}

	case 'seeIt' : {
		$ret = "false";
		if ( $s3->putObjectFile( dirname(__FILE__) . "/see-it.js", "sni-ads-creativeservices", "cs-lib/four-sticks.js", S3::ACL_PUBLIC_READ ) ) {
			$ret = "true";
		}
		return $ret;
	}

	case 'save' : {

		$ret = 'false';

		if ( !file_exists( $localFolder ) ) {
			mkdir( $localFolder, 0777, true );
		}

		/*
			local file will be
			clients/[advertiser]/native/[type]/[site]/[filename].js
		*/
		$fh = fopen( $localFile, 'w+' ) or die( 'error' );
		//$stringData = rawurldecode( $data );
		$stringData = stripslashes( $data );
		fwrite( $fh, $stringData );
		fclose( $fh );

		echo $stringData . ' - ' . $data;

		/*
			backup copy will be
			clients/[advertiser]/native/[type]/[site]/backups/[filename] - [timestamp].js
		*/
		if ( !file_exists( $localFolder . 'backups/' ) ) {
			mkdir( $localFolder . 'backups/', 0777, true );
		}
		$fh = fopen( $backupFile, 'w+' ) or die( 'error' );
		$stringData = rawurldecode( $data );
		fwrite( $fh, $stringData );
		fclose( $fh );

		$files = glob( "$localFolder*" );

		$remoteFile = $remoteFolder . $filename;

		if ( $s3->putObjectFile( $localFile, "sni-ads-creativeservices", $remoteFile, S3::ACL_PUBLIC_READ ) ) {
			$ret = "true";
		}

		echo $ret;
		break;
	}

	case 'get_file_size' : {
		$img = $_REQUEST[ 'img' ];
		$headers  = get_headers($img, 1);
		$filesize = $headers['Content-Length'];
		echo $filesize;
	}
}

?>
