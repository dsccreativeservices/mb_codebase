/**
 *
 * @Name: Interactive Ad Experience Config
 * @package: IAX
 * @Version: 0.1
 * @Modifed: 08/19/2013
 * @Author: Andy Hutchins <ahutchins@scrippsnetworks.com>
 *
 **/

if( typeof(IAX) === "undefined" ){IAX = {};}

IAX.Config = {
    debug: true, //enable console methods and other debugging code.
    apiEndpoint: '/api/v1',
    fadeDuration: 150, // Fade duration in ms.
    preview_base_url: "http://iax.scrippsonline.com/preview/view/"
};
