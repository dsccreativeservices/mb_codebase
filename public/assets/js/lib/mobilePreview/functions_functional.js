// JavaScript Document

$(window).load(function(){
	
	var footer = $(".footer .footerInner"),
		footerTop = 0, 
		footerVisible = 0,
		footerHidden = 50,
		header = $(".header"),
		slider = $(".pageContent"),
		lastScrollTop = 0,
		st = 0,
		initialScroll = true,
		totalScroll = 0


    function sliderScroll(whichSlider){
        if(whichSlider.scrollTop() > 40){
            console.log("slider scrolled past 40");	
            st = whichSlider.scrollTop();
            if (st > lastScrollTop){
                console.log("scrolling down");
                if(initialScroll){
                        var scrollDist = st - lastScrollTop - 40;
                    }else{
                        var scrollDist = st - lastScrollTop;
                    }
                //shrink footer
                footerTop = parseInt(footer.css("top"));
                if(footerTop < footerHidden){
                    //console.log("scroll distance is " + scrollDist);
                    footer.css("top",footerTop + scrollDist);
                }
                totalScroll = st;
                //console.log("total scroll is " + totalScroll);

                if(totalScroll > 40){
                    header.addClass("minimize");
                    $(".pageContent").addClass("scroll");
                }

                initialScroll = false;
            } else {
                console.log("scrolling up");
                //expanded footer
                footerTop = parseInt(footer.css("top"));
                if(footerTop >= 0){
                    var scrollDist = st - lastScrollTop;
                    if(footerTop + scrollDist >= 0){
                        footer.css("top",footerTop + scrollDist);
                    }
                }

                totalScroll = st;

                header.removeClass("minimize");
                $(".pageContent").removeClass("scroll");
            }

            lastScrollTop = st;	
        }
    }
    sliderScroll(slider); //check slider position initially
    slider.scroll(function(){
        sliderScroll($(this))
    });	

	
});