// JavaScript Document

$(window).load(function(){
	$(".alertBox button.close").on("click",function(){
		$(".alertBox").hide();
	});
	$(".alertBox button.closeSetCookie").on("click",function(){
		$(".alertBox").hide();
		console.log("set cookie");
		$.cookie('alwaysHideAlert', "alertHidden", { expires: 365, path: '/' });
	});
	
	var cookieValue;
	var screenWidth = $(window).width();
	cookieValue = $.cookie("alwaysHideAlert");
	console.log("cookie for alert value is " + cookieValue);
	if(cookieValue === "alertHidden"){
		//don't show alert because user has already seen it
		$(".alertBox").hide();
	}else if(screenWidth <= 600){
		$(".alertBox").show();
	}
	
	//populate slider	
	
	$('.flexslider').flexslider({
		animation: "slide",
		animationLoop: false,
		slideshow: false,
		itemWidth: 320,
		itemMargin: 5,
		pausePlay: false,
		start: function(slider){
			$('body').removeClass('loading');
		},
		before: function(){
			header.removeClass("minimize");
			footer.css("top",0);	
		}
	});
	var footer = $(".footer .footerInner"),
		footerTop = 0, 
		footerVisible = 0,
		footerHidden = 50,
		header = $(".header"),
		slider = $(".carousel .slides"),
		lastScrollTop = 0,
		st = 0,
		initialScroll = true,
		totalScroll = 0
	
	if(screenWidth <= 600){
		var bullets = $(".flex-control-paging");
		var active = bullets.find("li a.flex-active").parent().index(bullets);
		console.log(active);	
	}
		
	/*slider.find("li").eq(0).scroll(function(){
		sliderScroll($(this))
	});
	slider.find("li").eq(1).scroll(function(){
		sliderScroll($(this))
	});*/
	
	if(screenWidth > 600){
		var numSlides = slider.find("li").length
		console.log("number of slides is " + numSlides);
		for( var i = 0; i < numSlides; i++){
			slider.find("li").eq(i).scroll(function(){
				sliderScroll($(this))
			});	
		}
	}
	
    if(screenWidth > 600){
        function sliderScroll(whichSlider){
            if(whichSlider.scrollTop() > 40){
                console.log("slider scrolled past 40");	
                st = whichSlider.scrollTop();
                if (st > lastScrollTop){
                    console.log("scrolling down");
                    if(initialScroll){
                            var scrollDist = st - lastScrollTop - 40;
                        }else{
                            var scrollDist = st - lastScrollTop;
                        }
                    //shrink footer
                    footerTop = parseInt(footer.css("top"));
                    if(footerTop < footerHidden){
                        //console.log("scroll distance is " + scrollDist);
                        footer.css("top",footerTop + scrollDist);
                    }
                    totalScroll = st;
                    //console.log("total scroll is " + totalScroll);

                    if(totalScroll > 40){
                        header.addClass("minimize");
                        $(".carousel").addClass("scroll");
                    }

                    initialScroll = false;
                } else {
                    console.log("scrolling up");
                    //expanded footer
                    footerTop = parseInt(footer.css("top"));
                    if(footerTop >= 0){
                        var scrollDist = st - lastScrollTop;
                        if(footerTop + scrollDist >= 0){
                            footer.css("top",footerTop + scrollDist);
                        }
                    }

                    totalScroll = st;

                    header.removeClass("minimize");
                    $(".carousel").removeClass("scroll");
                }

                lastScrollTop = st;	
            }
        }
    }
	
});