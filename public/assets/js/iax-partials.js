/**
 *
 * @Name:     Interactive Ad Experience Partials
 * @description: Any template elements used in the UI.
 * @Version:  0.1
 * @Modifed:  09/05/2013 11:39:02
 * @Author:   Andy Hutchins <ahutchins@scrippsnetworks.com>
 *
 **/

if( typeof(IAX) !== 'object' ){throw new Error('IaxCoreMissing');}

IAX.Partials = {
  imageItem: [
      '<div class="iax-image-item well well-small image-{{image_id}}" id="dragArea" data-id="{{image_id}}">',
          '<div class="row">',
          '<div class="span2">',
          '<a data-lightbox="image-comps" title="{{url}}" data-title="{{url}}" href="/{{path}}" target="_blank"><img class="img-polaroid" src="/{{thumbPath}}" width="120" height="120" /></a><br>',
          '</div>',
          '<div class="span8">',
          '<p><input class="iax-title" placeholder="Title [optional]" value="{{name}}"><br>',
          '<input class="iax-description" placeholder="Description [optional]" value="{{description}}"><br>',
          '<a class="btn btn-mini btn-default iax-edit-image" data-imageid="{{image_id}}"><i class="fa fa-save"></i> Save</a>&nbsp;',
          '<a class="btn btn-mini btn-danger iax-delete-image" data-delete="Image delete." data-imageid="{{image_id}}"><i class="fa fa-trash-o"></i> Delete</a><br>',
          '<small>Updated: {{updated_at}}</small>',
          '</p>',
      '</div></div></div>'
      ].join("\n"),


    creativeItem: [
        '<div class="iax-creative-item">',
            '<p class="iax-title">{{name}}<br>',
            '<small class="iax-created">Created: {{created_at}}</small></p>',
        '</div>'
        ].join("\n"),

    creativeForm: [
        '<div class="iax-creative-form">',
            '<form><p class="iax-title">{{name}}<br>',
            '<small class="iax-created">Created: {{created_at}}</small></p>',
        '</div>'
        ].join("\n"),
    
     stackItem: [
        '<div class="iax-stack-item editable" style="left:-150%" rel="{{project_id}}" data-site="{{site}}">',
            '<h5>{{name}}</h5>',
            '<div class="timestamp">',
                '<p class="createModInfo">',
                    '<small>Created: {{updated_at}}</small>',
                    '<small>Modified: {{updated_at}}</small>',
                '</p>',
            '</div>',
            '<div class="status">',
                '<p class="statusText">Empty</p>',
                '<span></span>',
                '<span></span>',
                '<span></span>', 
            '</div>',
            '<div class="controls">',
                '<a href="/project/{{project_id}}/edit" class="iax-edit" rel="{{project_id}}" title="Edit">',
                    '<svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" version="1.1" id="Layer_1" x="0px" y="0px" viewBox="0 0 22 22" enable-background="new 0 0 22 22" xml:space="preserve"><path id="gray" fill="#58595B" d="M19.1 2.8c-0.7-0.7-1.6-1.2-2.4-1.2c-0.4 0-0.8 0.1-1.1 0.4l-1.3 1.3L3.8 13.8c-0.1 0.1-0.2 0.2-0.2 0.3 l-1.3 4.5c-0.1 0.3 0 0.5 0.2 0.7c0.1 0.1 0.3 0.2 0.5 0.2c0.1 0 0.1 0 0.2 0l4.5-1.3c0.1 0 0.2-0.1 0.3-0.2L18.6 7.5c0 0 0 0 0 0 l1.3-1.3C20.6 5.5 20.3 4 19.1 2.8z M16.6 3.1C16.6 3.1 16.7 3.1 16.6 3.1c0.3 0 0.9 0.2 1.4 0.7c0.6 0.6 0.8 1.2 0.7 1.4L18.1 6 l-2.1-2.1L16.6 3.1z M4.7 15.8l1.4 1.4l-2 0.6L4.7 15.8z M7.5 16.5l-2.1-2.1l9.5-9.5L17 7L7.5 16.5z"/></svg>',
                '</a>',
                '<a href="#" class="iax-delete" rel="{{project_id}}" title="Delete">',
                    '<svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" version="1.1" id="Layer_1" x="0px" y="0px" viewBox="0 0 22 22" enable-background="new 0 0 22 22" xml:space="preserve"><path id="gray" fill="#58595B" d="M18.9 5.2h-3.4V4.3c0-1.4-1.1-2.6-2.6-2.6H9.5C8.1 1.8 7 2.9 7 4.3v0.9H3.5C3.1 5.2 2.7 5.5 2.7 6 c0 0.5 0.4 0.9 0.9 0.9h0.9v10.2c0 1.4 1.1 2.6 2.6 2.6h8.5c1.4 0 2.6-1.1 2.6-2.6V6.9h0.9c0.5 0 0.9-0.4 0.9-0.9 C19.8 5.5 19.4 5.2 18.9 5.2z M8.7 4.3c0-0.5 0.4-0.9 0.9-0.9h3.4c0.5 0 0.9 0.4 0.9 0.9v0.9H8.7V4.3z M16.3 17.1 c0 0.5-0.4 0.9-0.9 0.9H7c-0.5 0-0.9-0.4-0.9-0.9V6.9h10.2V17.1z"/></svg>',
                '</a>',
                '<a href="#" class="iax-copy" rel="{{project_id}}" title="Duplicate this project">',
                    '<svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" version="1.1" id="Layer_1" x="0px" y="0px" viewBox="0 0 22 22" enable-background="new 0 0 22 22" xml:space="preserve"><path id="gray" fill="#58595B" d="M14.9 1.6h-1.2H8.6c-1 0-1.8 0.8-1.8 1.8v1.2H5.3c-1 0-1.8 0.8-1.8 1.8v11.3c0 1 0.8 1.8 1.8 1.8h8.8 c1 0 1.8-0.8 1.8-1.8v-1.2h1.5c1 0 1.8-0.8 1.8-1.8V5.9C19.3 5.9 14.9 1.6 14.9 1.6z M14.7 3.7l1.3 1.3l1.2 1.2h-2.4V3.7z M14.3 17.7c0 0.1-0.1 0.2-0.2 0.2H5.3c-0.1 0-0.2-0.1-0.2-0.2V6.4c0-0.1 0.1-0.2 0.2-0.2h1.5v8.5c0 1 0.8 1.8 1.8 1.8h5.6V17.7z M17.6 14.7c0 0.1-0.1 0.2-0.2 0.2H8.6c-0.1 0-0.2-0.1-0.2-0.2V3.4c0-0.1 0.1-0.2 0.2-0.2H13v4.5h4.6V14.7z"/></svg>',
                '</a>',       
            '</div>',
        '</div>'
        ].join("\n"),


    /*==================================================================================================================
    =            Form Partials - The template renderer will replace token with the hidden token on the page            =
    ==================================================================================================================*/
    newAdvertiser: [
        '<form class="form-inline iax-inline-form" id="iax-new-advertiser">',
            '<input name="_token" type="hidden" value="{{ token }}">',
            '<div class="span4">',
               '<input type="text" name="advertiser_name" placeholder="New Advertiser" class="advertiser_name">   ',
               '<button type="submit" class="btn btn-mini btn-success"><i class="fa fa-check"></i></button> ',
               '<button class="iax-cancel btn btn-mini btn-"><i class="fa fa-times"></i></button> ',
            '</div>',
        '</form>'
        ].join("\n"),

    newCampaign: [
    '<form class="form-inline iax-inline-form" id="iax-new-campaign">',
       '<input name="_token" type="hidden" value="{{ token }}">',
       '<div class="span4">',
           '<input type="text" name="campaign_name" placeholder="New Campaign" class="campaign_name">',
           '<input type="hidden" name="advertiser_id" class="advertiser_id">',
           '<button type="submit" class="btn btn-mini btn-success"><i class="fa fa-check"></i></button>',
           '<button class="iax-cancel btn btn-mini btn-"><i class="fa fa-times"></i></button>',
       '</div>',
    '</form>'
    ].join("\n")
};