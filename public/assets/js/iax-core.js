/**
*
* IaxCore.js - General javascript that is relied on by all pages. Any general boottrapping code goes here.
* The global IAX object is initialized here.
*
**/

// Avoid `console` errors in browsers that lack a console.
(function() {
    var method;
    var noop = function noop() {};
    var methods = [
        'assert', 'clear', 'count', 'debug', 'dir', 'dirxml', 'error',
        'exception', 'group', 'groupCollapsed', 'groupEnd', 'info', 'log',
        'markTimeline', 'profile', 'profileEnd', 'table', 'time', 'timeEnd',
        'timeStamp', 'trace', 'warn'
    ];
    var length = methods.length;
    var console = (window.console = window.console || {});

    while (length--) {
        method = methods[length];

        // Only stub undefined methods.
        if (!console[method]) {
            console[method] = noop;
        }
    }
}());


/**
* Initialize Bootstrap plugins.
**/
$('.iax-env-msg a[data-toggle=popover]').popover({
    placement: 'bottom'
});
$('[data-toggle=tooltip]').tooltip({
    placement: 'top'
});

// Setup AJAX loading indicators
$(document).bind("ajaxSend", function(){
    var opts = {
      lines: 15, // The number of lines to draw
      length: 20, // The length of each line
      width: 5, // The line thickness
      radius: 35, // The radius of the inner circle
      corners: 0.5, // Corner roundness (0..1)
      rotate: 80, // The rotation offset
      direction: 1, // 1: clockwise, -1: counterclockwise
      color: '#000', // #rgb or #rrggbb or array of colors
      speed: 0.9, // Rounds per second
      trail: 41, // Afterglow percentage
      shadow: false, // Whether to render a shadow
      hwaccel: false, // Whether to use hardware acceleration
      className: 'spinner', // The CSS class to assign to the spinner
      zIndex: 2e9, // The z-index (defaults to 2000000000)
      top: 'auto', // Top position relative to parent in px
      left: 'auto' // Left position relative to parent in px
    };
    var target = document.getElementById('iax-spinner');
    var spinner = new Spinner(opts).spin(target);
    $(".iax-loading").height($('html').height()).fadeIn(150);
}).bind("ajaxComplete", function(){
    // Force a 1sec delay so loading is not so jarring.
    _.delay(function(){
        $(".iax-loading").fadeOut(150);
    }, 250);
});

if( typeof($.fn.editableform) === "function" ){
  $.fn.editableform.buttons = [
  '<button class="btn btn-small btn-success" type="submit" class="editable-submit"><i class="fa fa-check"></i></button>'
  ].join('\n');
}

// Declare global IAX object
if( typeof(IAX) === "undefined" ){IAX = {}}