/**
 *
 * @Name:     Interactive Ad Experience Main
 * @Version:  0.1
 * @Modifed:  02/20/2014
 * @Author:   Andy Hutchins <ahutchins@scrippsnetworks.com>
 *
 **/

var IAX = IAX || {};

if (typeof(IAX.Main) !== 'object') {
    IAX.Main = {};
}

IAX.Main = {
    /**
    * The following provide an easy reference to meta values provided by the server.
    * They can be accessed anywhere within the application via IAX.Main.meta.<VALUE>
    **/
    meta: {
        csrfToken: $('meta[name=csrf_token]').attr('content') || "",
        userId: $('meta[name=user_id]').attr('content') || "",
        projectId: $('meta[name=project_id]').attr('content') || "",
        creativeId: $('meta[name=creative_id]').attr('content') || "",
        creativeLocked: $('meta[name=creative_locked]').attr('content') || ""
    },

    vars: {
        clickMacro: '<script> var clickMacro = ""; </script>'
    },

    favesInit: false,

    /*===========================================
    =            Internal Properties            =
    ===========================================*/
    // Loaded flag to prevent reinit of some plugins
    codeMirrorFlag: false,
    imagesLoaded: false,

    // Code Mirror Editor Properties
    codeMirror: {},
    creativeDirty: false, // Creative has been changed

    // Flag for whether new or edit project state
    newProjectMode: false,

    /*========================================
    =            Internal Methods            =
    ========================================*/


    setImageHolderSortable : function() {
        //mike version with rounds
        $( '.iax-image-holder li .roundItems' ).sortable({
            axis        : 'y',
            sort : function( e, ui ) {
                $( '.ui-state-highlight', this ).css( {
                    'line-height'   : $( ui.item ).outerHeight( true ) + 'px',
                    'height'        : $( ui.item ).outerHeight( true ) + 'px'
                });
            },
            stop : function( e, ui ) {
                var list = [],
                    round = $(this); //process only round that was changed.

                round.children("div").each(function(){
                    var el = $(this),
                        imageId = el.data("id"),
                        order = el.index();
                    if(order < 0){
                        order = 0;
                    }
                    list.push({
                        'index'     : order,
                        'id'        : imageId
                    });
                });

                console.log( list );

                $.ajax({
                    url: IAX.Config.apiEndpoint + '/projects/image/update_sort',
                    type: 'post',
                    global: false,
                    data: {
                        'list'      : list,
                        '_token'    : IAX.Main.meta.csrfToken
                    },
                    dataType: 'json',
                    success: function( data ) {
                        if ( data ) {
                            console.log( data );
                        }

                        if( data.status ){
                            IAX.Utils.notify(data.message, 'success', round);
                            //round.prepend("<span>Image order updated successfully</span>");
                        } else {
                            IAX.Utils.notify(data.message, 'error');
                        }

                    },
                    error: function(e) {

                    }
                });
            }
        });//.disableSelection();
    },

    /**
     * Intialize image upload tab
     */
    initImageUploadTab: function(){
        var _self = IAX.Main;

        if( !_self.imagesLoaded ){
            var opts = {
              lines: 11, // The number of lines to draw
              length: 7, // The length of each line
              width: 2, // The line thickness
              radius: 4, // The radius of the inner circle
              corners: 1, // Corner roundness (0..1)
              rotate: 0, // The rotation offset
              direction: 1, // 1: clockwise, -1: counterclockwise
              color: '#000', // #rgb or #rrggbb or array of colors
              speed: 1, // Rounds per second
              trail: 60, // Afterglow percentage
              shadow: false, // Whether to render a shadow
              hwaccel: false, // Whether to use hardware acceleration
              className: 'spinner', // The CSS class to assign to the spinner
              zIndex: 2e9, // The z-index (defaults to 2000000000)
              top: 'auto', // Top position relative to parent in px
              left: 'auto' // Left position relative to parent in px
            };
            var target = document.getElementById('iax-images-loading'),
                spinner = new Spinner(opts).spin(target),
                imageWrap = $('.iax-image-holder'),
                roundList,
                roundNum = 0,
                currRound = 0;

            $.ajax({
                url: IAX.Config.apiEndpoint + '/projects/image/all',
                type: 'POST',
                dataType: 'json',
                data: {
                    project_id: IAX.Main.meta.projectId,
                    _token: IAX.Main.meta.csrfToken
                },
                global: false, // Prevent loading indicator from showing
                success: function(data, textStatus, xhr) {
                    _self.imagesLoaded = true; // set flag so we dont reload on tab change
                    if (data.length > 0) {
                        //process images by round, sort into buckets for each round
                        for (var i = 0; i <= data.length - 1; i++) {
                            var newImagePartial = IAX.Utils.renderTemplate('imageItem', {
                                name: data[i].title,
                                description: data[i].description,
                                path: data[i].path,
                                url: 'http://' + window.location.host + '/' + data[i].path,
                                thumbPath: data[i].thumbPath,
                                image_id: data[i].id,
                                updated_at: data[i].updated_at
                            });

                            //get round - 0 based - append structure
                            roundNum = parseInt(data[i].round);
                            //console.log(roundNum);
                            if(imageWrap.find("ul").length === 0){
                                imageWrap.append("<ul></ul>");
                                roundList = imageWrap.find("ul");
                            }
                            if(roundList.find(".round" + roundNum).length === 0){
                                roundList.append("<li class='round" + roundNum + "'><h5>Round " + (roundNum + 1) + "</h5><span class='arrow'></span><a class='viewRound btn btn-mini' href='/client/project/"+ IAX.Main.meta.projectId + "/images?round=" + (roundNum + 1) + "' target='_blank'>View Images</a><div class='roundItems'></div></li>");
                            }

                            //set current round to highest number
                            if(roundNum > currRound){
                                currRound = roundNum;
                                console.log("highest round is", currRound + 1);
                            }

                            $('#iax-images-loading').hide();
                            // Add new image to DOM
                            roundList.find("li.round" + roundNum).find(".roundItems").append(newImagePartial);

                        }

                        //sort rounds in case images came out of order
                        roundList.find("li").each(function(){
                            var el = $(this),
                                num = parseInt(el.attr("class").replace("round",""));
                            if(num !== el.index()){
                                console.log("move round");
                                if(num === 0){
                                    roundList.prepend(el);
                                }else{
                                    el.insertAfter(roundList.find("li").eq(num - 1));
                                }
                            }
                        });

                        //set input field to current round
                        $(".roundNum #round").val((currRound + 1));

                        //expand active round
                        roundList.find("li").eq(currRound).addClass("active");

                        //setup expand/collapse buttons
                        roundList.find("li .arrow").on("click",function(){
                            roundList.find("li.active").removeClass("active");
                            $(this).parent().toggleClass("active");
                        });

                        _self.bindImageEditButtons();
                        _self.setImageHolderSortable();
                    } else {
                        $('.iax-image-holder').append('<div class="iax-no-images alert alert-info">This project has no images.</span>');
                        $("#iax-images-loading").fadeOut(150);
                    }
                },
                error: function(xhr, textStatus, errorThrown) {
                    // alert('Unable to contact IAX API.');
                    return false;
                }
            });
        }


        // Init uploader - no longer in use because it doesn't support rounds
        $('#file_upload').uploadify({
            'swf'      : '/assets/uploadify/uploadify.swf',
            'uploader' : IAX.Config.apiEndpoint + '/projects/image/upload',
            'debug': true,
            'preventCaching': false,
            'multi': false,
            'buttonText': "Upload Image",
            'fileSizeLimit': '5000kb', // max 5mb
            'fileTypeExts' : '*.gif; *.jpg; *.jpeg; *.png',
            'formData': {
                '_token': IAX.Main.meta.csrfToken,
                'project_id': IAX.Main.meta.projectId
            },
            'onUploadSuccess': function(file, data, response){
                $('.iax-no-images').remove();
                data = JSON.parse(data);
                var newImagePartial = IAX.Utils.renderTemplate('imageItem', {
                    name: data.title,
                    description: data.description,
                    path: data.path,
                    url: 'http://' + window.location.host + '/' + ( data.length && typeof data[ 0 ].path != 'undefined' ? data[ 0 ].path : '' ),
                    thumbPath: data.thumbPath,
                    image_id: data.id,
                    updated_at: data.updated_at
                });

                $('.iax-image-holder').append( newImagePartial );
                $(".compsPlaceholder").css("display","block");
                $(".emptyMessage").css("display","none");
                _self.bindImageEditButtons();
            }
        });

        //upload images via drag and drop - mike added
        setTimeout(function(){
            var dropzone = $(".dropArea");
            var el = dropzone;
            el.on("dragover",function(e){
                if(!el.hasClass("hover")){
                    el.addClass("hover");
                    console.log("hover");
                }
                e.preventDefault();
            });
            el.on("dragleave",function(e){
                el.removeClass("hover");
                e.preventDefault();
            });
            el.on("dragend",function(e){
                el.removeClass("hover");
                e.preventDefault();
            });
            el.on("drop",function(e){
                el.removeClass("hover");
                e.preventDefault();

                var files = e.originalEvent.dataTransfer.files,
                    imageNameArray = [],
                    imageDataArray = [],
                    projectId = $("meta[name=project_id]").attr("content"),
                    addToRound = $(".roundNum #round").val() - 1;
                console.log("add to round", addToRound + 1);

                for (var i = 0, f; f = files[i]; i++) {
                    var reader = new FileReader();
                    var image;
                    var name = f.name;
                    imageNameArray.push(name);
                    reader.onload = function (theFile) {
                        image = theFile.target.result;
                        imageDataArray.push(image);
                    };
                    // Read in the image file as a data URL.
                    reader.readAsDataURL(f);
                }

                setTimeout(function(){
                    console.log(imageNameArray, imageDataArray);
                    createImages(imageNameArray, imageDataArray, projectId);
                },500);

                function createImages(names, data, id){
                    var imageData = {
                        project: id,
                        round: addToRound,
                        names: names,
                        data: data,
                        _token: IAX.Main.meta.csrfToken
                    };

                    var request = $.ajax({
                        url: IAX.Config.apiEndpoint + "/projects/image/multiImages",
                        type: "POST",
                        data: imageData
                    });

                    //if successful, return message
                    request.done(function(data) {
                        if( data.status ){
                           if(data.status === false){
                                IAX.Utils.notify(data.message, 'error');
                           }else{
                               IAX.Utils.notify(data.message, 'success');
                           }

                            console.log("number of items uploaded", data.paths.length);
                            for (i = 0; i < data.paths.length; i++) {
                                //extrapolate thumb path
                                var ext = data.paths[i].split('.').pop();
                                var thumbPath = data.paths[i].replace('.' + ext,'_thumb.' + ext);
                                console.log("path is", thumbPath);
                                var newImagePartial = IAX.Utils.renderTemplate('imageItem', {
                                    //name: data.items[i].title,
                                    //description: data.items[i].description,
                                    name: '',
                                    description: '',
                                    path: data.paths[i],
                                    url: 'http://' + window.location.host + '/' + ( data.paths.length && typeof data.paths[i] != 'undefined' ? data.paths[i].path : '' ),

                                    thumbPath: thumbPath,
                                    image_id: data.id[i],
                                    updated_at: "Just now"
                                });

                                //if no rounds exist
                                if($(".iax-image-holder").find("ul").length < 1){
                                    //no rounds currently available, so create ul
                                    $('.iax-image-holder').html("").append("<ul></ul>");
                                }

                                //append to correct round if exists, else make new
                                if($('.iax-image-holder').find("li").eq(addToRound).length > 0){
                                    $('.iax-image-holder').find("li").eq(addToRound).find(".roundItems").append( newImagePartial );
                                }else{
                                    $('.iax-image-holder').find("ul").append("<li class='round" + addToRound + " active'><h5>Round " + (addToRound + 1) + "</h5><div class='roundItems'>" + newImagePartial + "</div></li>");
                                }

                                //$('.iax-image-holder').append( newImagePartial );
                            }

                            //add to sortable list
                            _self.setImageHolderSortable();

                            //reinitialize buttons
                            _self.bindImageEditButtons();
                            $(".compsPlaceholder").css("display","block");
                            $(".emptyMessage").css("display","none");
                        }else{
                           IAX.Utils.notify(data.message, 'error');
                        }

                    });
                    //if fail, return error
                    request.fail(function(jqXHR, textStatus) {
                        IAX.Utils.notify(data.message, 'error');
                        //error message
                    });
                }
                return false;
            });
        },1000);

        //update images
        setTimeout(function(){
            var holder = $(".iax-image-holder").find(".iax-image-item");

            holder.each(function( index ) {
                var el = $(this);
                //console.log(el);
                el.on("dragover",function(e){
                    if(!el.hasClass("hover")){
                        el.addClass("hover");
                        console.log("hover");
                    }
                    e.preventDefault();
                });
                el.on("dragleave",function(e){
                    el.removeClass("hover");
                    e.preventDefault();
                });
                el.on("dragend",function(e){
                    el.removeClass("hover");
                    e.preventDefault();
                });
                el.on("drop",function(e){
                    el.removeClass("hover");
                    e.preventDefault();
                    var image = "";
                    var name = "";
                    var imageID = el.data("id");

                    //var file = e.dataTransfer.files[0],
                    var file = e.originalEvent.dataTransfer.files[0];
                    name = file.name;
                    console.log(name);
                    reader = new FileReader();

                    reader.onload = function (event) {
                        console.log(event.target);
                        image = event.target.result;
                        sendUpdate(image, name);
                    };
                    console.log(file);
                    reader.readAsDataURL(file);

                    function sendUpdate(newImage, name){
                        var updateData = {
                            id: imageID,
                            newImg: newImage,
                            name: name,
                            _token: IAX.Main.meta.csrfToken
                        };

                        var request = $.ajax({
                            url: IAX.Config.apiEndpoint + "/projects/image/updateImg",
                            type: "POST",
                            data: updateData
                        });

                        //if successful, return message
                        request.done(function(data) {
                            if( data.status ){
                                //IAX.Utils.notify('Image updated successfully!', 'success', el);
                                IAX.Utils.smallNote('Image updated successfully!', 'success', el, 'center right', 3000);
                                console.log("success, set image", data.thumb);
                                var newSrc = "/" + data.thumb;
                                el.find(".img-polaroid").attr("src", newSrc);
                            }
                        });
                        //if fail, return error
                        request.fail(function(jqXHR, textStatus) {
                            //error message
                        });
                    }
                    return false;
                });
            });
        },1000);


    },

    bindImageEditButtons: function(){
		// _ image button
        $('.iax-image-holder a.iax-edit-image').on('click', function(e){
            if( e.target.tagName.toUpperCase() === 'A' ){

                var el = $(this);
                var updateData = {
                    id: el.data('imageid'),
                    title: el.closest('.iax-image-item').find('.iax-title').val(),
                    desc: el.closest('.iax-image-item').find('.iax-description').val(),
                    _token: IAX.Main.meta.csrfToken
                };

                var request = $.ajax({
                    url: IAX.Config.apiEndpoint + "/projects/image/update",
                    type: "POST",
                    data: updateData
                });

                //if successful, return message
                request.done(function(data) {
                    if( data.status ){
                        //IAX.Utils.notify('Image info updated successfully!', 'success');
                        IAX.Utils.smallNote(data.message, data.status, el.parents(".iax-image-item"), 'center right', 3000);
                    }
                });
                //if fail, return error
                request.fail(function(jqXHR, textStatus) {
                    //error message
                });
            }
        });



		//update image button - mike added
		$('.iax-image-holder a.iax-update-image').one('click', function(e){
            var el = $(this);
            var image = el.data('imageid');
            console.log("image id is", image);

			var imgInput = $('#imgUpdateInput');
			imgInput.trigger('click');
			imgInput.change(function(){
				/*var newImgSrc = imgInput.val();
				console.log(newImgSrc);
                var name = newImgSrc.replace("C:\\fakepath\\","");
                console.log("name is", name);
				sendUpdate(newImgSrc, name);*/
                var file = document.getElementById('imgUpdateInput').files[0];
                if (file) {
                    // create reader
                    var reader = new FileReader();
                    reader.readAsText(file);
                    reader.onload = function(e) {
                        // browser completed reading file - display it
                        console.log(e.target.result);
                    };
                }
			});



			//send new image to database to update
			function sendUpdate(updateSrc, name){
				var updateData = {
					id: image,
					newImg: updateSrc,
                    name: name,
					_token: IAX.Main.meta.csrfToken
				};

				var request = $.ajax({
					url: IAX.Config.apiEndpoint + "/projects/image/updateImg",
					type: "POST",
					data: updateData
				});

				//if successful, return message
				request.done(function(data) {
					if( data.status ){
						IAX.Utils.notify('Image updated successfully!', 'success');
					}
				});
				//if fail, return error
				request.fail(function(jqXHR, textStatus) {
					//error message
				});
			}

            /*if( e.target.tagName.toUpperCase() === 'A' ){

				//not ready yet

                var updateData = {
                    id: $(this).data('imageid'),
                    title: $(this).closest('.iax-image-item').find('.iax-title').val(),
                    desc: $(this).closest('.iax-image-item').find('.iax-description').val(),
                    _token: IAX.Main.meta.csrfToken
                };

                var request = $.ajax({
                    url: IAX.Config.apiEndpoint + "/projects/image/update",
                    type: "POST",
                    data: updateData
                });

                //if successful, return message
                request.done(function(data) {
                    if( data.status ){
                        IAX.Utils.notify('Image updated successfully!', 'success');
                    }
                });
                //if fail, return error
                request.fail(function(jqXHR, textStatus) {
                    //error message
                });
            }*/
        });

        $('.iax-image-holder a.iax-delete-image').on('click', function(e){

            if( e.target.tagName.toUpperCase() === 'A' ){
                var imageItemWrap = $(this).closest('.iax-image-item');

                var updateData = {
                    image_id: $(this).data('imageid'),
                    _token: IAX.Main.meta.csrfToken
                };

                var request = $.ajax({
                    url: IAX.Config.apiEndpoint + "/projects/image/remove",
                    type: "POST",
                    data: updateData
                });

                //if successful, return message
                request.done(function(data) {
                    if( data.status ){
                        imageItemWrap.fadeOut().remove();
                        IAX.Utils.notify('Image was deleted successfully!', 'success');
                    }
                });
                //if fail, return error
                request.fail(function(jqXHR, textStatus) {
                    //error message
                });
            }
        });
    },

    /**
     * Intialize Code Mirror on creative textfield.
     */
    initCodeMirror: function() {
        var _self = this;
        var $btnLock = $('.iax-editor-lock');

        // Prevent code mirror from loading multiple times
        if (_self.codeMirrorFlag) return;

        // Toggle locked state
        var toggleCreativeLocked = function() {
            var _self = IAX.Main;
            $.ajax({
                url: IAX.Config.apiEndpoint + '/creative/' + _self.meta.creativeId + '/toggle_lock',
                type: 'GET',
                dataType: 'json',
                global: false,
                success: function(data, textStatus, xhr) {
                    if (data.status === 'ok') {
                        if (data.creative) {
                            // Locked
                            $btnLock.removeClass('btn-default').addClass('btn-danger');
                            $btnLock.find('i').removeClass('fa-unlock').addClass('fa-lock');
                            $('.CodeMirror-wrap').addClass('locked');

                            // Disable editing
                            iax_code.setOption('readOnly', 'nocursor');
                        } else {
                            // Un-Locked
                            $btnLock.removeClass('btn-danger').addClass('btn-default');
                            $btnLock.find('i').removeClass('fa-lock').addClass('fa-unlock');
                            $('.CodeMirror-wrap').removeClass('locked');

                            // Enable editing
                            _self.codeMirror.setOption('readOnly', false);
                        }
                    } else {
                        // Redirect to login page.
                        window.location = '/login';
                        return false;
                    }
                },
                error: function(xhr, textStatus, errorThrown) {
                    alert('Unable to contact IAX API.');
                    return false;
                }

            });
        };

        // Create code mirror
        var cm = document.getElementById('iax-code-editor');
        if (cm) {
            var iax_code = _self.codeMirror = CodeMirror.fromTextArea(cm, {
                // theme : 'monokai',
                tabSize: 2,
                lineWrapping: true,
                tabindex: 2,
                lineNumbers: true,
                mode: 'xml'
            });

            // If locked on initial load
            if (_self.creativeLocked === 'true') {
                $btnLock.removeClass('btn-default').addClass('btn-danger');
                $btnLock.find('i').removeClass('fa-unlock').addClass('fa-lock');
                $('.CodeMirror-wrap').addClass('locked');

                // Disable editing
                iax_code.setOption('readOnly', 'nocursor');
            }


            // Get code mirror doc.
            _self.doc = iax_code.getDoc();
            _self.hist = _self.doc.getHistory();

            iax_code.on('change', function(e) {
                _self.hist = _self.doc.getHistory();
                if (_self.hist.done.length > 0) {
                    $('label[for=creative] i').show();
                    $('.iax-editor-undo').removeClass('disabled');
                } else {
                    $('label[for=creative] i').hide();
                    $('.iax-editor-undo').addClass('disabled');
                }

                $('.iax-editor-undo').click(function(e) {
                    e.preventDefault();
                    _self.doc.undo();
                });

                _self.creativeDirty = true;
            });

            $(".btn.cachebuster").on("click",function(){
                iax_code.replaceSelection("%%CACHEBUSTER%%");
            });

            $(".btn.clickMacro").on("click",function(){
                console.log("insert click macro variable");
                iax_code.replaceSelection(_self.vars.clickMacro);
            });

            // Setup creative copy
            $('.iax-editor-copy').click(function(e) {
                e.preventDefault();
                // TODO Copy to clipboard via flash

                displayMessage('Copied to clipboard.', 'info');
                return false;
            });

            // Setup editor lock
            $('.iax-editor-lock').click(function(e) {
                e.preventDefault();
                toggleCreativeLocked();
                return false;
            });

            // Setup tootips
            $('a[data-toggle=tooltip]').tooltip();
        }
        _self.codeMirrorFlag = true;
    },

    /**
     * Save new project
     */
    saveNewProject: function(_t) {
        // Remove any error classes.
        $('.control-group').removeClass('error');

        var isValid = true;

        // Serialize form data.
        var $form = $('#iax-project-form');
        var data = $form.serialize();

        // Get status.
        var status = $('.iax-status-bar button.active').attr('data') || 'internal'; // default to staging

        if (status !== 'undefined') {
            data += '&status=' + status;
        }


        // Basic dropdown validation. Check that item is selected.
        $('.error').removeClass('error');

        if( $('#advertiser_id').val() === "0" ){
            $('#advertiser_id').closest('.control-group').addClass('error');
            isValid = false;
        }

        // If new advertiser field is not submitted
        if( $('#advertiser_id').css('display') === 'none' ){
            $('#advertiser_id').closest('.control-group').addClass('error');
            isValid = false;
        }

        // If new campaign field is not submitted
        if( $('#campaign_id').css('display') === 'none' ){
            $('#campaign_id').closest('.control-group').addClass('error');
            isValid = false;
        }

        if( $('#campaign_id').val() === "0" ){
            $('#campaign_id').closest('.control-group').addClass('error');
            isValid = false;
        }

        if( $('#project_name').val().length <= 1  ){
            $('#project_name').closest('.control-group').addClass('error');
            isValid = false;
        }
        if( $('#adtype_id').val() === "0" ){
            $('#adtype_id').closest('.control-group').addClass('error');
            isValid = false;
        }

        if( $('#adtag_id').val() == "0" ){
            $('#adtag_id').closest('.control-group').addClass('error');
            isValid = false;
        }

        // If any erros tripped, cancel
        if( !isValid ){
            // Remove error class if no errors found
            return false;
        }

        // Submit the new project.
        $.ajax({
            url: IAX.Config.apiEndpoint + '/project/new',
            type: 'POST',
            dataType: 'json',
            data: data,
            success: function(data, textStatus, xhr) {
                // Redirect to edit after creating new project
                window.location = '/project/' + data.id + '/edit';
            },
            error: function(xhr, textStatus, errorThrown) {
                //called when there is an error
                return false;
            }
        });
    },

    /**
     * Update project creative
     */
    updateProjectCreative: function(_t) {
        var _self = this;
        // Save CM content into textarea
        IAX.Main.codeMirror.save();

        var $form = $('#iax-project-form-creative');
        //console.log(data);

        //validate URL
        function validateURL(url){
            var regex = /^(https?|ftp):\/\/(((([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(%[\da-f]{2})|[!\$&'\(\)\*\+,;=]|:)*@)?(((\d|[1-9]\d|1\d\d|2[0-4]\d|25[0-5])\.(\d|[1-9]\d|1\d\d|2[0-4]\d|25[0-5])\.(\d|[1-9]\d|1\d\d|2[0-4]\d|25[0-5])\.(\d|[1-9]\d|1\d\d|2[0-4]\d|25[0-5]))|((([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])*([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])))\.)+(([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])*([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])))\.?)(:\d*)?)(\/((([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(%[\da-f]{2})|[!\$&'\(\)\*\+,;=]|:|@)+(\/(([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(%[\da-f]{2})|[!\$&'\(\)\*\+,;=]|:|@)*)*)?)?(\?((([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(%[\da-f]{2})|[!\$&'\(\)\*\+,;=]|:|@)|[\uE000-\uF8FF]|\/|\?)*)?(\#((([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(%[\da-f]{2})|[!\$&'\(\)\*\+,;=]|:|@)|\/|\?)*)?$/i;
            return regex.test(url);
        }
        var urlInput = $form.find("input#project_url");

        function validateProjectURL(){
            if(validateURL(urlInput.val()) === false){
                //if missing http://
                if(urlInput.val().indexOf("http") === -1){
                    console.log("missing http");
                    urlInput.val("http://" + urlInput.val());
                    validateProjectURL();
                }else{
                    IAX.Utils.smallNote('Invalid URL', 'error', urlInput.parent(), 'float nextToInput', 3000);
                    console.log("invalid url");
                    return false;
                }
            }
            else{
                updateCreative();
            }
        }
        validateProjectURL();

        function updateCreative(){
            var data = $form.serialize();
            //console.log(data);
            $.ajax({
                url: IAX.Config.apiEndpoint + '/creative/update',
                type: 'POST',
                dataType: 'json',
                data: data,
                success: function(data, textStatus, xhr) {
                    // The data var contains our new creative object.
                    // Update our cached id since they were not available on page load
                    _self.meta.creativeId = data.id;
                    _self.creativeLocked = data.locked;

                    if(data.status === 'fail'){
                       IAX.Utils.notify(data.message, 'error');
                    }else{
                       IAX.Utils.notify('Creative changes saved successfully!', 'success');
                    }

                    if(data.previewUrl){
                        data.previewUrl = data.previewUrl.replace("preview/view","preview/iframe");
                        var previewURL = window.location.protocol + "//" + window.location.host + data.previewUrl;
                        $("#preview-url").attr("href", previewURL);
                        $(".previewPlaceholder").css("display","block");

                        //if module and builder button is not visible then add it once preview URL is provided
                        if($('#adtype_id option:selected').text() === "Module - 3 Across"){
                            if(!$.trim( $('.launchContainer').html()).length){
                                console.log("add button");
                                var launchHTML = '<a href="' + previewURL + '?builder=true" class="btn btn-primary" id="iax-project-launchBuilder" target="_blank">Launch Module Builder</a>';
                                $(".launchContainer").html(launchHTML);
                            }
                        }
                        $(".emptyMessage").css("display","none");
                    }
                },
                error: function(xhr, textStatus, errorThrown) {
                    //called when there is an error
                    return false;
                }
            });
        }
    },

    /**
     * Update project info (First tab)
     */
    updateProject: function() {
        var _self = this;

        var isValid = true;

        var $form = $('#iax-project-form');
        var data = $form.serialize();

        // Get status
        var status = $('.iax-status-bar button.active').attr('data') || 'internal';
        if (status !== 'undefined') {
            data += '&status=' + status;
        }
        //when selecting traffic status check if click macro exists
        if(status === "complete"){
            var adtypeId = parseInt( $("#adtype_id option:selected").val() );
            //5 RSI
            //35 sweeps
            //36 hub
            //40 newsletter
            //41 other
            //42 billboard
            //49 3 across
            //51 1 across
            //52 snapchat
            //53 ingredient
            //54 2 across
            //55 infographic
            var ignore = [5,35,36,40,41,42,49,51,52,53,54,55];

            //check if adtype is in ignore array or not, if not check for DFP click macro
            if( ignore.indexOf(adtypeId) === -1 ){
                console.log("check for click macro");
                if($("#iax-code-editor").val().indexOf(_self.vars.clickMacro) === -1){
                    var check = confirm("Creative does not include the DFP Click Macro. Click OK to proceed without a Click Macro.");
                    if (check === false) {
                        isValid = false;
                    }
                }
            }

        }

        // Basic dropdown validation. Check that item is selected.
        $('.error').removeClass('error');

        if( $('#advertiser_id').val() === "0" ){
            $('#advertiser_id').closest('.control-group').addClass('error');
            isValid = false;
        }
        if( $('#campaign_id').val() === "0" ){
            $('#campaign_id').closest('.control-group').addClass('error');
            isValid = false;
        }

        if( $('#project_name').val().length <= 1  ){
            $('#project_name').closest('.control-group').addClass('error');
            isValid = false;
        }
        if( $('#adtype_id').val() === "0" ){
            $('#adtype_id').closest('.control-group').addClass('error');
            isValid = false;
        }

        if( $('#adtag_id').val() === "0" ){
            $('#adtag_id').closest('.control-group').addClass('error');
            isValid = false;
        }

        // If any erros tripped, cancel
        if( !isValid ){
            // Remove error class if no errors found
            return false;
        }

        $.ajax({
            url: IAX.Config.apiEndpoint + '/project/update',
            type: 'POST',
            dataType: 'json',
            data: data,
            success: function(data, textStatus, xhr) {
                IAX.Utils.notify('Project changes saved successfully!', 'success');

                // Lock creative if needed
                if( data.status === 'complete' ){
                    $.ajax({
                        url: IAX.Config.apiEndpoint + '/creative/' + _self.meta.creativeId + '/lock',
                        type: 'GET',
                        dataType: 'json',
                        global: false,
                        success: function(data, textStatus, xhr) {
                            var $btnLock = $('.iax-editor-lock');
                            if (data.status === 'ok') {
                                if (data.creative ) {
                                    // Locked
                                    $btnLock.removeClass('btn-default').addClass('btn-danger');
                                    $btnLock.find('i').removeClass('fa-unlock').addClass('fa-lock');
                                    $('.CodeMirror-wrap').addClass('locked');

                                    // Disable editing
                                    IAX.Main.codeMirror.setOption('readOnly', 'nocursor');
                                } else {
                                    // Un-Locked
                                    $btnLock.removeClass('btn-danger').addClass('btn-default');
                                    $btnLock.find('i').removeClass('fa-lock').addClass('fa-unlock');
                                    $('.CodeMirror-wrap').removeClass('locked');

                                    // Enable editing
                                    IAX.Main.codeMirror.setOption('readOnly', false);
                                }
                            } else {
                                // Redirect to login page.
                                window.location = '/login';
                                return false;
                            }
                        },
                        error: function(xhr, textStatus, errorThrown) {
                            alert('Unable to contact IAX API.');
                            return false;
                        }

                    });
                    $(".trafficPlaceholder").css("display","block");
                }else{
                    $(".trafficPlaceholder").css("display","none");
                }

				//force page reload
				/*setTimeout(function(){
					location.reload();
				},1000);*/

                // Project updated
                return true;

            },
            error: function(xhr, textStatus, errorThrown) {
                //called when there is an error
                return false;
            }
        });
    },

    /*=========================================
  =            Page Init Methods            =
  =========================================*/
    /**
     * Boot up IAX. Called on every page.
     */
    boot: function(page) {
        var _self = this;
        // _self.bindClicks();

        // Init favorites
        _self.initFavorites();

        // If not in debug mode, disable any console method calls
        if (!IAX.Config.debug) {
            IAX.Utils.disableConsole();
        }

        // If a page is provided, call its init method
        if (typeof(page) !== 'undefined') {
            _self['init' + page]();
        }
    },

    /**
     * Initialise the preoject new page.
     */
    initNewProjectPage: function() {
        var _self = this;

        _self.newProjectMode = true;

        _self.initProjectForm();
    },

    /**
     * Init the project in update mode.
     * @return {[type]} [description]
     */
    initEditProjectPage: function(){
        var _self = this;

        _self.initProjectForm();
    },

    /**
     * Init the advertiser page.
     */
    initAdvertiserPage: function() {
        var _self = this,
            $w = $(window),
            $toTopBtn = $('.iax-to-top'),
            btnHeight = $toTopBtn.outerHeight(),
            toTopOn = false,
            scrollTrigger = $('.iax-advertisers').offset().top - $('.navbar-inner').height() - 10,
            throttle = 200, // Limit scrollUpdate calls to once every this ms. Higer value reduce load, but reduce UI responsives.
            lastCalled = IAX.Utils.getTimeStamp(); //Last time scroll update was called.

        // On scroll callback
        var scrollUpdate = function(e) {
            // Throttle scroll event to every 200ms
            var now = IAX.Utils.getTimeStamp();
            if (now - lastCalled < throttle) {
                return;
            } // too soon

            lastCalled = IAX.Utils.getTimeStamp(); // Update timestamp
            if ($w.scrollTop() > scrollTrigger) {
                if (!toTopOn) {
                    toTopOn = true;
                    $('.iax-to-top').css({
                        right: (document.width > 1170) ? ((document.width - 1170) / 2) + 'px' : 20 + 'px'
                    });
                    $toTopBtn.clearQueue().animate({
                        bottom: 0
                    });
                }
            } else {
                toTopOn = false;
                // Rebind listener
                $w.on('scroll', scrollUpdate);
                $toTopBtn.animate({
                    bottom: -btnHeight
                });
            }
        };
        // Bind scroll event
        $w.on('scroll', scrollUpdate);

        $('.iax-to-top').on('click', function(e) {
            // unbind scroll event
            $w.unbind('scroll', scrollUpdate);
            $toTopBtn.animate({
                bottom: -btnHeight
            });
            $('html, body').animate({
                scrollTop: 0
            }, 300, function() {
                // Rebind after animation done.
                $w.on('scroll', scrollUpdate);
            });
            return e.preventDefault();
        });

        // Bind letter nav events
        $('.iax-letter-nav').find('a').on('click', function(e) {
            e.preventDefault();
            $w.unbind('scroll', scrollUpdate);
            // Determine new scroll position with offset for top navbar
            var newPos = $($(this).attr('href')).offset().top - 40;
            $('html, body').animate({
                scrollTop: newPos
            }, 300, function() {
                scrollUpdate();
                $w.on('scroll', scrollUpdate);
            });
            return false;
        });
    },

	/* init advertiser specific page */
	initAdvertiserDetailPage: function(){
		//delete functionality Mike added
		$(".iax-stack-item .iax-delete").on("click", function(e){
			e.preventDefault();
			var btnClicked = $(this);
			var campaignID = btnClicked.prop("rel");
			console.log("campaign is", campaignID);

			var numProjects = parseInt(btnClicked.parent().siblings(".count").text().replace("Projects",""));
			console.log("number of project in campaign is", numProjects);
			if(numProjects === 0){
				var check = confirm("Are you sure you want to delete this campaign?");
				if (check === true) {
					var updateData = {
						campaign_id: campaignID,
						_token: IAX.Main.meta.csrfToken
					};

					var request = $.ajax({
						url: IAX.Config.apiEndpoint + "/campaign/delete",
						type: "POST",
						data: updateData
					});

					//if successful, return message
					request.done(function(data) {
						if( data.status ){
							IAX.Utils.notify('Campaign successfully deleted!', 'success');
							btnClicked.parents(".iax-stack-item").hide();
						}
					});
					//if fail, return error
					request.fail(function(jqXHR, textStatus) {
						//error message
						IAX.Utils.notify('Something went wrong!', 'error');
					});
				}
			}else{
				IAX.Utils.notify('This Campaign contains projects. Cannot delete!', 'error');
			}

		});
	},

    /**
     * Init the home page
     */
    initHomePage: function() {
        var _self = this;

        // Any home page actions
    },

    /**
     * Init the group permissions page.
     */
    initGroupPermissionsPage: function() {
        // initialize the switch controls in the permissions page.
        $('.iax-switch').on('switch-change', function(e, data) {
            $el = $(data.el);
            if (data.value) {
                // Update checkbox element value and remove hidden field if present
                $el.attr("value", "1");
                $('#' + $el.attr("id") + "[type=hidden]").remove();
            } else {
                // Set checkbox value
                $el.attr("value", "0");
                // Insert hidden text field for false values so they will be sent to the server.
                $("#" + $el.attr("id")).after('<input type="hidden" name="' + $el.attr("name") + '" id="' + $el.attr("id") + '" value="0">');
            }
        });
    },

    initCampaignDetail: function() {
        var _self = this;
        _self.initStackViews();
        _self.bindClicks();
    },

    loadAdvertiserDropdown: function(id) {
        var _self = this;

        // If id is passed, that will be selected.
        id = id || 0;
        // var jqxhr = $.getJSON(IAX.Config.apiEndpoint + '/advertisers', function(data) {
        var jqxhr = $.ajax({
            url: IAX.Config.apiEndpoint + '/advertisers',
            type: "GET",
            dataType: "json",
            global: false,
            success: function(data) {
                var options = '';

                // Create a string of options for each advertiser. THe API returns a numeric index object of objects
                // to maintain sort order in Chrome. Chrome does not maintain order on objects.
                // See: https://code.google.com/p/chromium/issues/detail?id=37404
                $.each(data, function(k, value){
                    options += '<option value="' + value.key + '">' + value.value + '</option>';
                });

                // Clear dropdown and append new options.
                $('#advertiser_id').empty().append(options);

                // Set to id if given
                if (id) {
                    $('#advertiser_id').val(id);
                    _self.loadCampaignDropdown(id);
                } else {
                    _self.loadCampaignDropdown($('#advertiser_id').find('option:first').val());
                }
            }
        })
            .fail(function(e) {
                if (IAX.Config.debug)
                    console.error("loadAdvertiserDropdown failed.", e);
            });
    },

    loadCampaignDropdown: function(adv_id, camp_id) {
        var _self = this;

        // If id is passed, that will be selected.
        adv_id = parseInt(adv_id, 10) || 0;
        camp_id = parseInt(camp_id, 10) || $('#campaign_id').attr('rel');

        if( adv_id === 0 ){
            $('#campaign_id').empty().append('<option>(Select or create advertiser first)</option>').attr('disabled');

            return false;
        }

        var jqxhr = $.ajax({
            url: IAX.Config.apiEndpoint + '/campaigns/' + adv_id,
            type: "get",
            dataType: "json",
            global: false,
            success: function(data){
                var options = '';
                if (IAX.Utils.getObjectSize(data) > 1 )  {
                    // Create a string of options for each advertiser. THe API returns a numeric index object of objects
                    // to maintain sort order in Chrome. Chrome does not maintain order on objects.
                    // See: https://code.google.com/p/chromium/issues/detail?id=37404
                    $.each(data, function(k, value){
                        options += '<option value="' + value.key + '">' + value.value + '</option>';
                    });

                    // Clear dropdown and append new options.
                    $('#campaign_id').empty().append(options);

                    //If camp_id provided, set to current selection
                    if (camp_id ) {
                        $('#campaign_id').val(camp_id);
                    }

                    // Enable dropdown
                    $('#campaign_id').removeAttr('disabled');
                } else {
                    _self.showNewCampaignField();
                }
            },
            failure: function(e){
                if (IAX.Config.debug){
                    console.error("loadCampaignDropdown failed.", e);
                }
            }
        });
    },

    loadAdTagDropdown: function(site_id, adtype_id) {
        var _self = this;

        // If id is passed, that will be selected.
        site_id = parseInt(site_id, 10) || 0;
        adtype_id = parseInt(adtype_id);

        if( site_id === 0 ){
            $('#adtag_id').empty().append('<option>(Must select a site first)</option>').attr('disabled');
            return false;
        }

        // Check for adv_id attribute and call load method
        adtag_id = $('#adtag_id').attr('rel');

        var jqxhr = $.ajax({
            url: IAX.Config.apiEndpoint + '/adtags/' + site_id,
            type: "get",
            dataType: "json",
            global: false,
            success: function(data){
                console.log(data);
                var options = '<option value="0">Select Ad Tag...';
                // Create a string of options for each advertiser. THe API returns a numeric index object of objects
                // to maintain sort order in Chrome. Chrome does not maintain order on objects.
                // See: https://code.google.com/p/chromium/issues/detail?id=37404
                /*$.each(data, function(k, value){
                    options += '<option value="' + value.key + '">' + value.value + '</option>';
                });*/

                for (var i = 0; i < data.length; i++) {
                    options += '<option value="' + data[i].id + '">' + data[i].name + '</option>';
                }

                // Clear dropdown and append new options.
                $('#adtag_id').empty().append(options);

                //If adtag_id available, set to current selection
                if (adtag_id) {
                    $('#adtag_id').val(adtag_id);
                }else{
                   //////////////////// automatic adtag selection  ////////////////////
                   console.log("no adtag set, matching against current adtype", $("#adtype_id").val());

                   var adtypeText = $("#adtype_id").find(":selected").text();
                   _self.findAdtagByAdtype(adtypeText, data);


                   //////////////////// END automatic adtag selection  ////////////////////
                }

                // Enable if site has been selected
                $('#adtag_id').removeAttr('disabled');

            },
            failure: function(e){
            }
        });
    },

    findAdtagByAdtype: function(adtypeText, data){
        $('#adtag_id').removeClass("auto");

        //start with adtags for given site
        for (var j = 0; j < data.length; j++) {
            var adtype = data[j].adtype;
            console.log(adtype);
            if( typeof adtype !== "undefined" && adtype !== ""){
                var adtypeOptions = JSON.parse(adtype);

                for (var k = 0; k < adtypeOptions.length; k++) {
                    console.log($("#adtype_id").val(), adtypeOptions[k]);
                    if(adtypeOptions[k] === $("#adtype_id").val()){
                        //console.log("matches");
                        //if adtype matches selected adtype, return adtag
                        $('#adtag_id').val(data[j].id);
                        $('#adtag_id').addClass("auto");
                        break;
                    }
                }
            }
        }
    },

    showNewCampaignField: function(advertiser_id) {
        var _self = this;
        var $t = $('#campaign_id'); //grab ref to dropdown
        var $newForm;

        // Render new campaign form once.
        if ($('#iax-new-campaign').length < 1) {
            var newForm = IAX.Utils.renderTemplate('newCampaign', {
                'token': $('#iax-csrf-token').find('input').val()
            });
            $t.after(newForm);
        }

        $t.hide(); // append new form and hide dropdown

        $newForm = $('#iax-new-campaign');
        $newForm.show();
        $newForm.find('input[type=text]').val('').focus();
        $newForm.find('input.advertiser_id').val(advertiser_id);

        // Bind fom event.
        $newForm.find('.iax-cancel').on('click', function() {
            _self.hideNewCampaignField();
        });
        $newForm.bind('submit', function(e) {
            e.preventDefault();
            _self.saveNewCampaign();
            return false;
        });
    },

    hideNewCampaignField: function() {
        var _self = this;
        var $t = $('#campaign_id');
        $('#campaign_id').removeAttr('disabled');
        var $newForm = $('#iax-new-campaign');
        if ($newForm.is(':visible')) {
            $newForm.hide();
            if ($t.val() === 'new') {
                $t.val($t.find("option:first").val());
            }
            $t.show();
            // Clear error state
            $('#campaign_id').closest('.control-group').removeClass('error');
        }
    },

    saveNewCampaign: function() {
        var _self = this,
            is_valid = false,
            is_not_similar = false,
            $form = $('#iax-new-campaign'),
            campaign = $form.find('.campaign_name').val();
        $form.find('.advertiser_id').val($('#advertiser_id').val());

        // TODO: Validate
        if (campaign.length > 2) {
            is_valid = true;
        }

        if (is_valid) {
            // Save new advertiesr.
            var data = $form.serialize();

            // Submit the new goodies.
            $.ajax({
                url: IAX.Config.apiEndpoint + '/campaign/new',
                type: 'GET',
                dataType: 'json',
                data: data,
                success: function(data, textStatus, xhr) {
                    // Hide new campaign field.
                    $form.hide();
                    $('#campaign_id').show();
                    // Reload dropdown with new option selected
                    _self.loadCampaignDropdown($('#advertiser_id').val(), data.id);

                    // Clear error state
                    $('#campaign_id').closest('.control-group').removeClass('error');

                    // Focus Project Name fields
                    $('#project_name').focus();
                },
                error: function(xhr, textStatus, errorThrown) {
                    return false;
                }
            });
        } else {
            //invalid input
            $form.find('.advertiser_name').addClass("error").prop("placeholder", "Invalid Entry");
            return false;
        }
    },

    // Advertiser Dropdown
    showNewAdvertiserField: function() {
        var _self = this;
        var $t = $('#advertiser_id'); //grab ref to dropdown
        var $newForm;

        // Render new advertiser form once.
        if ($('#iax-new-advertiser').length < 1) {
            newForm = IAX.Utils.renderTemplate('newAdvertiser', {
                'token': $('#iax-csrf-token').find('input').val()
            });
            $t.after(newForm);
        }
        $newForm = $('#iax-new-advertiser');
        $newForm.show();
        $t.hide(); // append new form and hide dropdown
        $newForm.find('input[type=text]').val('').focus();

        // Bind fom event.
        $newForm.find('.iax-cancel').on('click', function() {
            _self.hideNewAdvertiserField();
        });
        $newForm.bind('submit', function(e) {
            e.preventDefault();
            _self.saveNewAdvertiser();
            return false;
        });
    },

    hideNewAdvertiserField: function() {
        var $newForm = $('#iax-new-advertiser');
        if ($newForm.is(':visible')) {
            $newForm.hide();
            $('#advertiser_id').show();
            // Clear error state
            $('#advertiser_id').closest('.control-group').removeClass('error');
        }
    },

    initDropdowns: function() {
        var _self = this;

        // Setup dropdown bindings for advertisers.
        $('#advertiser_id').on('change', function() {
            // hide any forms that were open
            _self.hideNewCampaignField();
            _self.hideNewAdvertiserField();

            var $t = $(this);
            var cur_val = $t.find(":selected").val();

            if (cur_val === 'new') {
                _self.showNewAdvertiserField();
            } else {
                _self.loadCampaignDropdown(cur_val);
            }
        });

        // Setup dropdown action bindings
        $('#campaign_id').bind('change', function() {
            var $t = $(this);
            var cur_val = $t.find(":selected").val();
            if (cur_val === 'new') {
                _self.showNewCampaignField();
            }
        });

        $('#site_id').bind('change', function() {
            var $t = $(this);
            var siteID = $t.find(":selected").val();
            var adtypeID = $("#adtype_id").find(":selected").val();
            _self.loadAdTagDropdown(siteID, adtypeID);
        });

        // Setup dropdown action bindings
        $('#adtype_id').bind('change', function() {
            $("#adtag_id").attr("rel","");
        });

    },

    saveNewAdvertiser: function() {
        _self = this;
        var $form = $('#iax-new-advertiser');
        var advertiser_name = $form.find('.advertiser_name').val();

        // TODO: Validate
        var is_valid = false;
        if (advertiser_name.length > 1) {
            is_valid = true;
        }

        if (is_valid) {
            // Save new advertiesr.
            var data = $form.serialize();

            // Submit the new goodies.
            $.ajax({
                url: IAX.Config.apiEndpoint + '/advertiser/new',
                type: 'GET',
                dataType: 'json',
                data: data,
                success: function(data, textStatus, xhr) {
                    // New item added successfully.
                    $form.hide();
                    // Reload dropdown and select new item.
                    _self.loadAdvertiserDropdown(data.id);
                    $('#advertiser_id').show();

                    // Clear error state
                    $('#advertiser_id').closest('.control-group').removeClass('error');

                    // Put campaign into new mode since no campaigns exist for this new advertiser yet.
                    _self.showNewCampaignField(data.id);
                },
                error: function(xhr, textStatus, errorThrown) {
                    return false;
                }
            });
        } else {
            //invalid advertiser name input
            $form.find('.advertiser_name').addClass("error").prop("placeholder", "Invalid Entry");
            return false;
        }
    },

    refreshProjectOverview: function(){
        var _self = this;
        $.ajax({
            url: IAX.Config.apiEndpoint + '/project/' + _self.meta.projectId,
            type: 'POST',
            data: {_token:_self.meta.csrfToken},
            dataType: 'json',
            success: function(data, textStatus, xhr) {

                // Update the overview values
                $("#advertiser").text(data.campaign.advertiser.name);
                $("#campaign").text(data.campaign.name);
                $("#project-name").text(data.name);
                if( data.creatives[0].preview ) {
                    if( typeof(data.creatives[0].preview.scrape_url) !== 'undefined' ){
                        $("#scrape-url").prop('href', data.creatives[0].preview.scrape_url).text(data.creatives[0].preview.scrape_url);
                    }
                    if( typeof(data.creatives[0].preview.preview_id) !== 'undefined' ){
                        $("#preview-url").prop('href', window.location.origin + '/preview/view/' + data.creatives[0].preview.preview_id).text(window.location.origin + '/preview/view/' + data.creatives[0].preview.preview_id);
                    }
                }
                if( data.status === 'complete' ){
                    var trafficUrl = document.location.origin + '/adops/traffic/project/' + data.id;
                    $('#iax-traffic-link').attr('href', trafficUrl).text(trafficUrl);
                }
            },
            error: function(xhr, textStatus, errorThrown) {
                IAX.Utils.notify('An error occured.', 'error');
                return false;
            }
        });
    },

    /**
     * Initialize project form
     */
    initProjectForm: function() {
        var _self = this;

        // Check for adv_id attribute and call load method
        var adv_id = $('#advertiser_id').attr('rel');
        if (typeof(adv_id) !== 'undefined' && adv_id) {
            _self.loadAdvertiserDropdown(adv_id);
        } else {
            _self.loadAdvertiserDropdown();
        }

        var site_id = $('#site_id').attr('rel');
        if (typeof(site_id) !== 'undefined' && site_id) {
            _self.loadAdTagDropdown(site_id);
        } else {
            _self.loadAdTagDropdown();
        }

        _self.initDropdowns();

        // set project status
        var status = $('#status').attr('rel');
        if (typeof(status) !== 'undefined' && status) {
            $('.iax-status-bar button').attr('class', 'btn btn-mini'); // clear classes
            $('.iax-status-bar button[data=' + status + ']').addClass('btn-primary');
        }

        // Change Project Status
        $('.iax-status-bar button').on('click', function(e) {
            $('.iax-status-bar button').attr('class', 'btn btn-mini'); // clear classes
            $(this).addClass($(this).attr('rel').toLowerCase());
        });

        // Project Form Tabs
        $('.nav-tabs a').click(function(e) {
            e.preventDefault();
            // Disable
            if ($(this).parent('li').hasClass('disabled')) return false;
            $(this).tab('show');
            var hash = $(this).attr("href").replace("#iax-","").replace("-tab","");
            console.log(hash);
            switch (hash) {
                case "client-info":
                    window.location.hash = "";
                    break;
                case "comp-upload":
                    window.location.hash = "#flatComp";
                    break;
                case "creative":
                    window.location.hash = "#functional";
                    break;
                default:

            }
        });

        // Save New Project
        $('#iax-project-tab1-save').click(function(e) {
            var _t = this;
            e.preventDefault();
            var data = _self.saveNewProject(_t);
            return false;
        });

        // Update Existing Project
        $('#iax-project-tab1-update').click(function(e) {
            var _t = this;
            e.preventDefault();
            var data = _self.updateProject(_t);
            return false;
        });

        // Save Project Creative
        $('#iax-project-tab3-save').click(function(e) {
            var _t = this;
            e.preventDefault();
            var data = _self.updateProjectCreative(_t);
            return false;
        });

        // Update preview URL button for modules
         $('#iax-project-tab3-updateURL').click(function(e) {
            var _t = this;
            e.preventDefault();
            var data = _self.updateProjectCreative(_t);
            return false;
        });

        // Handle tab change event
        $('a[data-toggle="tab"]').on('shown', function(e) {
            if (e.target.href.indexOf('iax-creative-tab') > -1) {
                //if content module init module builder
                _self.initCodeMirror();
            } else if (e.target.href.indexOf('iax-comp-upload-tab') > -1) {
                _self.initImageUploadTab();
                //_self.setImageHolderSortable(); removed from here, added to function where images are looped into rounds

            } else if (e.target.href.indexOf('iax-comp-overview-tab') > -1) {
                // Reload overview so any changes since last page load are shown
                _self.refreshProjectOverview();
            }
        });

        //if URL has hash go to that tab - mike added
        if(window.location.hash) {
            var hash = window.location.hash;
            console.log(hash);
            hash = hash.replace("#","");
            $(".tab-pane").removeClass("active");
            $(".nav-tabs li").removeClass("active");
            if (hash === "functional") {
                console.log("go to functional tab");
                $("#iax-creative-tab").addClass("active");
                $(".nav-tabs li").eq(2).addClass("active");
                _self.initCodeMirror();
            } else if (hash === "flatComp") {
                console.log("go to flat comp tab");
                $("#iax-comp-upload-tab").addClass("active");
                $(".nav-tabs li").eq(1).addClass("active");
                _self.initImageUploadTab();
                _self.setImageHolderSortable();
            }
        }

    },

    /*==================================
    =            List views            =
    ==================================*/
    initStackViews: function() {
        var _self = this;

        $('.accordion-toggle').on('click', function(e) {
            // Collapse expansions
            $('.iax-acc-box.in').collapse('hide');
            var $t = $(this);

            // Get project id
            var pid = $t.attr('rel');

            if (!$t.hasClass('collapsed')) {
                // Expand
            } else {
                // load creative when expanded.
                var $tbl = $('#project-' + pid);
                if ($tbl.find('tr').length < 1) {
                    // Load creatives
                    _self.loadCreativesByProject(pid, $t);
                }
            }
        });
    },

    loadCreativesByProject: function($id, $t) {
        // Get creatives.
        var projectId = $id;

        // Get creatives
        $.ajax({
            url: IAX.Config.apiEndpoint + '/projects/' + projectId + '/creatives',
            type: 'GET',
            dataType: 'json',
            success: function(d) {
                // Select element to append items to.
                var $hook = $('#project-' + projectId).find('.iax-box-inner .iax-items');
                $hook.empty(); // remove existing items.
                var tmpl = ''; // will hold html to append
                for (var i = 0; i < d.length; i++) {
                    tmpl += IAX.Utils.renderTemplate('creativeItem', d[i]);
                }

                $hook.append(tmpl);
            },
            error: function(e) {
                // AJAX failed
            }
        });
    },

    /**
     * Bind clicks on project page.
     */
    bindClicks: function() {
        var _self = this;

        // Setup delete event
       /* $('body').bind('click', function(e) {
            var el = e.target.className; // get clicked class
            switch (el) {
                case 'iax-delete':
                    e.preventDefault();
                    alert("Deleting projects is unsupported in BETA version.");
                    return false;
                    // case 'another action':

            }
        });*/

        //select boxes - Mike added
        //if 2 or more are selected show multi options box
        var selectedCampaigns = [],
            campaignOptions = $(".campaignOptions");
        $(".iax-stack-item > input[type='checkbox']").on("click",function(){
            var projectId = $(this).attr("rel");
            if(this.checked){
                selectedCampaigns.push(projectId);
            }else{
                var index = selectedCampaigns.indexOf(projectId);
                if (index > -1) {
                    selectedCampaigns.splice(index, 1);
                }
            }
            if(selectedCampaigns.length >= 2){
                campaignOptions.addClass("show");
                campaignOptions.find("li span").each(function(){
                    $(this).text("(" + selectedCampaigns.length + ")");
                });
            }else{
                campaignOptions.removeClass("show");
            }
        });

        campaignOptions.find("li").on("click",function(){
            var method = $(this).attr("class");
            switch (method) {
                case "editProjects":
                    openProjects();
                    break;
                case "flatComps":
                    console.log("get flat comp links");
                    flatComps();
                    break;
                case "previewProjects":
                    console.log("get preview links");
                    previewProjects();
                    break;
                case "trafficProjects":
                    console.log("set projects to traffic and generate links");
                    trafficProjects();
                    break;
                default:
            }
        });
        function openProjects(){
            $(".iax-stack-item > input[type='checkbox']:checked").each(function(){
                var item = $(this),
                    projectId = item.attr("rel");
                window.open("/project/" + projectId + "/edit", "_blank");
            });
        }
        function flatComps(){
            var compLinks = "";
            $(".iax-stack-item > input[type='checkbox']:checked").each(function(){
                var item = $(this),
                    projectName = item.siblings("h5").text(),
                    compURL = item.siblings(".status").find("span").eq(0)[0].getAttribute('onclick');
                if(typeof compURL !== "undefined" && compURL !== null){
                    compURL = compURL.match(/'([^']+)'/)[1];
                    compURL = document.location.origin + compURL;
                }else{
                    compURL = "No Flat Comps exist for this project";
                }
                //console.log(compURL);
                compLinks += "<br/>" + projectName + ": " + compURL;
            });
            $("body").append("<div class='module flatComps'><span class='close'></span>" + compLinks + "</div>");
            $(".module .close").on("click",function(){
                $(".module").remove();
            });
        }
        function previewProjects(){
            var previewLinks = "";
            $(".iax-stack-item > input[type='checkbox']:checked").each(function(){
                var item = $(this),
                    projectName = item.siblings("h5").text(),
                    previewURL = item.siblings("h5").find("a").attr("href");
                //window.open(previewURL, "_blank");
                previewLinks += "<br/>" + projectName + ": " + previewURL;
            });
            $("body").append("<div class='module previews'><span class='close'></span>" + previewLinks + "</div>");
            $(".module .close").on("click",function(){
                $(".module").remove();
            });
        }
        function trafficProjects(){
            //ask about click macro first
            var check = confirm("Have you added the DFP Click Macro to each project? Click OK to proceed.");
            if (check === false) {
                return false;
            }
            var trafficLinks = "";
            $(".iax-stack-item > input[type='checkbox']:checked").each(function(){
                var item = $(this),
                    projectId = item.attr("rel"),
                    projectName = item.siblings("h5").text(),
                    data = {
                        project_id: projectId,
                        _token: IAX.Main.meta.csrfToken
                    };
                $.ajax({
                    url: IAX.Config.apiEndpoint + '/project/setTraffic',
                    type: 'POST',
                    dataType: 'json',
                    data: data,
                    success: function(data, textStatus, xhr) {
                        console.log("project set to traffic");
                        return true;
                    },
                    error: function(xhr, textStatus, errorThrown) {
                        //called when there is an error
                        return false;
                    }
                });
                trafficLinks += "<br/>" + projectName + ": " + document.location.origin + "/adops/traffic/project/" + projectId;
            });
            $("body").append("<div class='module trafficCode'><span class='close'></span>" + trafficLinks + "</div>");
            $(".module .close").on("click",function(){
                $(".module").remove();
            });
        }



		//delete functionality Mike added
		$(".iax-stack-item .iax-delete").on("click", function(e){
            var btn = $(this);
            deleteItem(e, btn);
        });
        function deleteItem(e, btn){
            e.preventDefault();
			var btnClicked = btn;

			var check = confirm("Are you sure you want to delete this project?");
			if (check === true) {
				console.log("yes, delete");
				//e.preventDefault();

				var projectID = btnClicked.prop("rel");
				console.log("project is", projectID);

				var updateData = {
					project_id: projectID,
					_token: IAX.Main.meta.csrfToken
				};

				var request = $.ajax({
					url: IAX.Config.apiEndpoint + "/project/delete",
					type: "POST",
					data: updateData
				});

				//if successful, return message
				request.done(function(data) {
					if( data.status ){
						IAX.Utils.notify('Project successfully deleted!', 'success');
						btnClicked.parents(".iax-stack-item").css("left","-150%");
                        setTimeout(function(){
                            btnClicked.parents(".iax-stack-item").remove();
                        },600);
					}
				});
				//if fail, return error
				request.fail(function(jqXHR, textStatus) {
					//error message
					IAX.Utils.notify('Something went wrong!', 'error');
				});
			} else {
				console.log("no, don't delete");
			}
		}

        //set to traffic, Mike added
        $(".iax-stack-item .iax-traffic").on("click", function(e){
            e.preventDefault();
            var btn = $(this);
            //update project
            var projectID = btn.prop("rel");
            console.log(projectID);
            var data = {
                project_id: projectID,
                _token: IAX.Main.meta.csrfToken
            };
            $.ajax({
                url: IAX.Config.apiEndpoint + '/project/setTraffic',
                type: 'POST',
                dataType: 'json',
                data: data,
                success: function(data, textStatus, xhr) {
                    IAX.Utils.notify('Project successfully set to traffic!', 'success');
                    //set status text to traffic
                    btn.parent().siblings(".status").find(".statusText").text("Traffic");
                    //set status icons to traffic
                    btn.parent().siblings(".status").find("span").eq(2).css({"background":"#007fbf", "border":"0", "cursor":"pointer"});
                    btn.parent().siblings(".status").find("span").eq(2).on("click",function(){
                        window.open('http://iax-staging.scrippsonline.com/adops/traffic/project/' + projectID,'_blank');
                    });
                    return true;
                },
                error: function(xhr, textStatus, errorThrown) {
                    //called when there is an error
                    return false;
                }
            });
            return false;
        });

        //copy functionality Mike added
        $(".iax-stack-item .iax-copy").on("click", function(e){
            var btn = $(this);
            copy(e, btn);
        });
        function copy(e, btn){
			e.preventDefault();
            var btnClicked = btn;
			var check = confirm("Are you sure you want to copy this project?");
            if (check === true) {
                var projectID = btnClicked.prop("rel"),
                    userID = $("meta[name='user_id']").attr("content");
                console.log(userID);
                var copyProject = {
					project_id: projectID,
                    user_id: userID,
                    status: 'internal',
					_token: IAX.Main.meta.csrfToken
				};

				var request = $.ajax({
					url: IAX.Config.apiEndpoint + "/project/copy",
					type: "POST",
					data: copyProject
				});

				//if successful, return message
				request.done(function(data) {
                    if( data.status ){
						IAX.Utils.notify('Project successfully copied!', 'success');
                        switch(data.site) {
                            case 1:
                                data.site = "Food Network";
                                break;
                            case 2:
                                data.site = "HGTV";
                                break;
                            case 3:
                                data.site = "DIY Network";
                                break;
                            case 4:
                                //none for some reason?
                                break;
                            case 5:
                                data.site = "uLive.com";
                                break;
                            case 6:
                                data.site = "TravelChannel.com";
                                break;
                            case 7:
                                data.site = "HGTV Remodels";
                                break;
                            case 8:
                                data.site = "HGTV Gardens";
                                break;
                            case 9:
                                data.site = "Food.com";
                                break;
                            case 10:
                                data.site = "Frontdoor";
                                break;
                            case 11:
                                data.site = "GAC";
                                break;
                            case 12:
                                data.site = "Cooking Channel";
                                break;
                        }
                        var newRowPartial = IAX.Utils.renderTemplate('stackItem', {
                            name: data.name,
                            site: data.site,
                            project_id: data.id,
                            updated_at: "Just now"
                        });
                        // Add new image to DOM
                        $(newRowPartial).insertBefore($(".iax-stack-item").eq(0));
                        setTimeout(function(){
                            $(".iax-stack-item").eq(0).css("left","0px");
                        },500);
                        //reinitialize new buttons
                        $(".iax-stack-item").last().find(".iax-copy").on("click", function(e){
                            var btn = $(this);
                            copy(e, btn);
                        });
                        $(".iax-stack-item").last().find(".iax-delete").on("click", function(e){
                            var btn = $(this);
                            deleteItem(e, btn);
                        });
					}
				});
				//if fail, return error
				request.fail(function(jqXHR, textStatus) {
					//error message
					IAX.Utils.notify('Something went wrong!', 'error');
				});
            }
        }

        //hold down on edit to reveal direct links to flat comps and functional
        var mousedownTimer,
            isMouseDown = false,
            lastItem;
        $('.iax-edit').on('mousedown',function(e) {
            e.preventDefault();
            var el = $(this);
            if(!el.siblings(".editExpand").hasClass("show")){
                mousedownTimer = setTimeout(function() {
                    console.log("timer done");
                    isMouseDown = true;
                    $(".editExpand").removeClass("show");
                    el.parent().find(".editExpand").addClass("show");
                    lastItem = el;
                }, 1000);
            }else{
                isMouseDown = false;
            }
        });
        $('.iax-edit').on('mouseup',function() {
            if (mousedownTimer) {
                console.log("clear timer");
                clearTimeout(mousedownTimer);
                setTimeout(function(){
                    isMouseDown = false;
                },300);
            }
        });

        $('.iax-edit').on('click', function(e) {
            if(isMouseDown === true){
                console.log("mousedown active so prevent click");
                e.preventDefault();
            }else{
                clearTimeout(mousedownTimer);
            }
        });
    },

    /**
     * Setup project favorite support.
     */
    initFavorites: function() {
        var _self = this;
        if( _self.favesInit ){
            return false;
        }
        _self.favesInit = true;
        $('.iax-favorite').on("click", function() {
            _self.toggleFavorite($(this).prop('rel'), this);
        });
    },

    /**
     * Toggle current project to favorites
     */
    /**
     * Toggle current project to favorites
     * @param  {integer} id Current project id
     * @param  {object} el The clicked object (this). Allows this method to be used on several pages.
     */
    toggleFavorite: function(id, el) {
        // Toggle creative API request
        $.ajax({
            url: IAX.Config.apiEndpoint + '/favorite/toggle',
            type: 'GET',
            global: false, // prevents global loading indicator
            data: {
                "project_id": id
            },
            dataType: 'json',
            success: function(d) {
                if (d.is_favorite) {
                    $(el).find('i').removeClass('fa-star-o').addClass('fa-star').prop('title', 'Un-Favorite');
                    IAX.Utils.notify('Added to favorites!', 'success');
                } else {
                    $(el).find('i').removeClass('fa-star').addClass('fa-star-o').prop('title', 'Favorite');
                    IAX.Utils.notify('Removed from favorites!', 'success');
                }
            },
            error: function(e) {

            }
        });
    }
}; // End IAX.MAIN
