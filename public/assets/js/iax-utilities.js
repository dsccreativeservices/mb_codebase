/**
 *
 * @Name:     Interactive Ad Experience Utilities
 * @Version:  0.1
 * @Modifed:  08/29/2013 10:42:48
 * @Author:   Andy Hutchins <ahutchins@scrippsnetworks.com>
 *
 **/

if( typeof(IAX) !== 'object' ){throw new Error('IaxCoreMissing')};

IAX.Utils = {
    /**
     * Get object size excluding properties inherited through the prototype
     * @param  {object} obj
     * @return {int}
     */
    getObjectSize: function(obj){
        var size = 0, key;
        for (key in obj) {
            if (obj.hasOwnProperty(key)) size++;
        }
        return size;
    },

    /**
     * Returns current timestamp using Date.now() with fallback to getTime().
     * @return {string} Timestamp in milliseconds
     */
    getTimeStamp: function(){
        if( typeof(Date.now) !== 'function' ){
            return (new Date()).getTime();
        }
        return Date.now();
    },

    /**
     * Disable all console methods by setting to empty function
     */
    disableConsole: function(){
        console.log('%c *************************************************************** ', 'background:red; color: white;');
        console.log('%c ** Console methods disabled. Set IAX.debug = true to enable. ** ', 'background:red; color: white;font-weight:bold;');
        console.log('%c *************************************************************** ', 'background:red; color: white;');

        var methods = [
            'assert', 'clear', 'count', 'debug', 'dir', 'dirxml', 'error',
            'exception', 'group', 'groupCollapsed', 'groupEnd', 'info', 'log',
            'markTimeline', 'profile', 'profileEnd', 'table', 'time', 'timeEnd',
            'timeStamp', 'trace', 'warn'
        ];
        var method;
        var length = methods.length;
        while( length-- ){
            method = methods[length];
            // Overrided method with empty function
            window.console[method] = function(){};
        }
    },

    // Notify.js wrapper
    notify: function(msg, type, el){
        type = type || 'success'; //default to success

        var opts = {
            elementPosition: 'top center',
            globalPosition: 'top center',
            // Hide delay
            autoHideDelay: 4000,
            // show animation
            showAnimation: 'slideDown',
            // show animation duration
            showDuration: 200,
            // hide animation
            hideAnimation: 'slideUp',
            // hide animation duration
            hideDuration: 200,
            className: type
        };

        if(el){
            $.notify(el, msg, opts);
        } else {
            $.notify(msg, opts);
        }
    },

    /* mike custom notificiation */
    smallNote: function(msg, type, el, pos, delay){
        var content ="<p class='note " + pos + " " + type + "'>" + msg + "</p>";

        el.append(content);
        if(delay){
            setTimeout(function(){
                el.find("p.note").fadeOut(500);
            },delay);
        }
    },

    /**
     * Render a partial template using _.template.
     * @param  {string} template Name of partial. Tempaltes are set in assets/js/iax-partials.js
     * @param  {[type]} data     Object containing template data. Should map to placeholders in template.
     * @return {[type]}          [description]
     */
    renderTemplate: function(template, data){
        _.templateSettings = {
          interpolate : /\{\{(.+?)\}\}/g
        };

        data = data || {};

        var tmpl = _.template(IAX.Partials[template]);

        return tmpl(data);
    }
};
